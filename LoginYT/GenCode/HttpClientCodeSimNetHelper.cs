﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GenCode
{
	public class FbVerifySMSData
	{
		public string number { get; set; }
		public string sender { get; set; }
		public int id { get; set; }
	}

	public class CodeSimData
	{
		public int id_giaodich { get; set; }
		public int giaodich_id { get; set; }
		public string phoneNumber { get; set; }
		public List<FbVerifySMSData> listSms { get; set; }
	}

	public class CodeSimApiResBody
	{
		public CodeSimData data { get; set; }
		public string msg { get; set; }
		public int stt { get; set; }
	}

	// chothuesimcode
	public class ChoThueSimCodeData
	{
		public int Id { get; set; }
		public string Number { get; set; }
		public string SMS { get; set; }
		public string Code { get; set; }
	}

	public class ChoThueSimCodeApiResBody
	{
		public ChoThueSimCodeData Result { get; set; }
		public string Msg { get; set; }
		public int ResponseCode { get; set; }
	}

	internal class HttpClientCodeSimNetHelper
	{

		private static HttpClientCodeSimNetHelper _instance;

		private HttpClient client;
		private string apiHost = "";
		public string apikey = "";
		private int giaodich_id = 0;
		public string phoneNum = "";
		public string service = "";
		public static HttpClientCodeSimNetHelper Instance
		{
			get
			{
				HttpClientCodeSimNetHelper._instance = (HttpClientCodeSimNetHelper._instance ?? new HttpClientCodeSimNetHelper());
				return HttpClientCodeSimNetHelper._instance;
			}
		}

		public HttpClientCodeSimNetHelper(string apiKey = "", string service = "codesim.net")
		{
            if (!string.IsNullOrEmpty(apiKey))
            {
				this.apikey = apiKey;
			}

			this.service = service;
			switch (this.service)
			{
				case "codesim.net":
					this.apiHost = "http://api.codesim.net/api/CodeSim/";
					break;
				case "chothuesimcode.com":
					this.apiHost = "https://chothuesimcode.com/api";
					break;
			}

			this.initHttpClient();
		}

		private void initHttpClient()
		{
			this.client = new HttpClient();
			this.client.DefaultRequestHeaders.Remove("User-Agent");
			this.client.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36");
		}

		public async Task<string> getFromStream(Stream response)
		{
			string text = await Task.Run<string>(delegate()
			{
				StreamReader reader = new StreamReader(response);
				string result = "";
				while (!reader.EndOfStream)
				{
					result += reader.ReadLine();
				}
				return result;
			});
			string x = text;
			text = null;
			return x;
		}

		public async Task<string> getUrl(string url)
		{
			this.initHttpClient();
			Stream stream = await this.client.GetStreamAsync(url);
			Stream st = stream;
			stream = null;
			string text = await this.getFromStream(st);
			string rs = text;
			text = null;
			return rs;
		}

		public async Task<bool> dangKyGiaoDich()
		{
			try
			{
				switch (this.service)
				{
					case "codesim.net":
						await codesimNetDangKyGiaoDich();
						break;
					case "chothuesimcode.com":
						await choThueSimCodeComDangKyGiaoDich();
						break;
				}
				return true;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
				return false;
			}
		}

		public async Task<bool> codesimNetDangKyGiaoDich()
		{
			try
			{
				string url = apiHost + "DangKy_GiaoDich?apikey=" + this.apikey + "&dichvu_id=1&so_sms_nhan=1";
				this.initHttpClient();
				HttpResponseMessage httpResponseMessage = await this.client.GetAsync(url);
				HttpResponseMessage wcfResponse = httpResponseMessage;
				httpResponseMessage = null;
				string text = await wcfResponse.Content.ReadAsStringAsync();
				string rs = text;
				text = null;
				CodeSimApiResBody _rsApi = JsonConvert.DeserializeObject<CodeSimApiResBody>(rs);
				if ((_rsApi != null) && (_rsApi.stt == 1) && (_rsApi.data != null) && (_rsApi.data.id_giaodich > 0))
				{
					giaodich_id = _rsApi.data.id_giaodich;
					phoneNum = _rsApi.data.phoneNumber;
				}
				return true;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
				return false;
			}
		}

		public async Task<bool> choThueSimCodeComDangKyGiaoDich()
		{
			try
			{
				string url = apiHost + "?act=number&apik=" + this.apikey + "&appId=1001";
				this.initHttpClient();
				HttpResponseMessage httpResponseMessage = await this.client.GetAsync(url);
				HttpResponseMessage wcfResponse = httpResponseMessage;
				httpResponseMessage = null;
				string text = await wcfResponse.Content.ReadAsStringAsync();
				string rs = text;
				text = null;
				ChoThueSimCodeApiResBody _rsApi = JsonConvert.DeserializeObject<ChoThueSimCodeApiResBody>(rs);
				if ((_rsApi != null) && (_rsApi.ResponseCode == 0) && (_rsApi.Result != null) && (_rsApi.Result.Id > 0))
				{
					giaodich_id = _rsApi.Result.Id;
					phoneNum = "0" + _rsApi.Result.Number;
				}
				return true;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
				return false;
			}
		}

		public async Task<bool> huyGiaoDich()
		{
			try
			{
                if (giaodich_id == 0)
                {
					return false;
				}

				bool res = false;
				switch (this.service)
				{
					case "codesim.net":
						res = await codesimNetHuyGiaoDich();
						break;
					case "chothuesimcode.com":
						res = await choThueSimCodeComHuyGiaoDich();
						break;
				}

				return res;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
				return false;
			}
		}

		public async Task<bool> codesimNetHuyGiaoDich()
		{
			try
			{
				if (giaodich_id == 0)
				{
					return false;
				}

				string url = apiHost + "HuyGiaoDich?apikey=" + this.apikey + "&giaodich_id=" + giaodich_id.ToString();
				this.initHttpClient();
				HttpResponseMessage httpResponseMessage = await this.client.GetAsync(url);
				HttpResponseMessage wcfResponse = httpResponseMessage;
				httpResponseMessage = null;
				string text = await wcfResponse.Content.ReadAsStringAsync();
				string rs = text;
				text = null;
				CodeSimApiResBody _rsApi = JsonConvert.DeserializeObject<CodeSimApiResBody>(rs);
				if ((_rsApi != null) && (_rsApi.stt == 1))
				{
					giaodich_id = 0;
					phoneNum = "";
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
				return false;
			}
		}

		public async Task<bool> choThueSimCodeComHuyGiaoDich()
		{
			try
			{
				if (giaodich_id == 0)
				{
					return false;
				}

				string url = apiHost + "?act=expired&apik=" + this.apikey + "&id=" + giaodich_id.ToString();
				this.initHttpClient();
				HttpResponseMessage httpResponseMessage = await this.client.GetAsync(url);
				HttpResponseMessage wcfResponse = httpResponseMessage;
				httpResponseMessage = null;
				string text = await wcfResponse.Content.ReadAsStringAsync();
				string rs = text;
				text = null;
				ChoThueSimCodeApiResBody _rsApi = JsonConvert.DeserializeObject<ChoThueSimCodeApiResBody>(rs);
				if ((_rsApi != null) && (_rsApi.ResponseCode == 0))
				{
					giaodich_id = 0;
					phoneNum = "";
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
				return false;
			}
		}

		public async Task<string> layMaFacebook()
		{
			try
			{
				if (giaodich_id == 0)
				{
					return null;
				}

				string res = "";
				switch (this.service)
				{
					case "codesim.net":
						res = await codesimNetLayMaFacebook();
						break;
					case "chothuesimcode.com":
						res = await choThueSimCodeComLayMaFacebook();
						break;
				}

				return res;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
				return null;
			}
		}

		public async Task<string> codesimNetLayMaFacebook()
		{
			string code = null;
			try
			{
				string url = apiHost + "KiemTraGiaoDich?apikey=" + this.apikey + "&giaodich_id=" + giaodich_id.ToString();
				this.initHttpClient();
				HttpResponseMessage httpResponseMessage = await this.client.GetAsync(url);
				HttpResponseMessage wcfResponse = httpResponseMessage;
				httpResponseMessage = null;
				string text = await wcfResponse.Content.ReadAsStringAsync();
				string rs = text;
				text = null;
				CodeSimApiResBody _rsApi = JsonConvert.DeserializeObject<CodeSimApiResBody>(rs);
				if ((_rsApi != null) && (_rsApi.stt == 1) && (_rsApi.data != null) && (_rsApi.data.listSms.Count > 0))
				{
					code = _rsApi.data.listSms.First().number;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
			}

			return code;
		}

		public async Task<string> choThueSimCodeComLayMaFacebook()
		{
			string code = null;
			try
			{
				string url = apiHost + "?act=code&apik=" + this.apikey + "&id=" + giaodich_id.ToString();
				this.initHttpClient();
				HttpResponseMessage httpResponseMessage = await this.client.GetAsync(url);
				HttpResponseMessage wcfResponse = httpResponseMessage;
				httpResponseMessage = null;
				string text = await wcfResponse.Content.ReadAsStringAsync();
				string rs = text;
				text = null;
				ChoThueSimCodeApiResBody _rsApi = JsonConvert.DeserializeObject<ChoThueSimCodeApiResBody>(rs);
				if ((_rsApi != null) && (_rsApi.ResponseCode == 0) && (_rsApi.Result != null))
				{
					code = _rsApi.Result.Code;
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine("Attempted divide by zero. {0}", ex.Message);
			}

			return code;
		}
	}
}
