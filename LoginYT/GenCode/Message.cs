﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenCode
{
    public class FireBaseMessage
    {
        public string Id { get; set; }
        public int Type { get; set; }
        public string BmId { get; set; }
        public string Data { get; set; }

        public FireBaseMessage()
        {
            Id = "";
            Type = 0;
            BmId = "";
            Data = "";
        }

        public FireBaseMessage(string id, int type, string bmid = null, string data = null)
        {
            Id = id;
            Type = type;
            BmId = bmid;
            Data = data;
        }
    }
}
