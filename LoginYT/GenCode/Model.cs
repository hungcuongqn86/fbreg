﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoginYT.GenCode
{
    public class AdAccountsResBody
    {
        public List<AdAccounts> data { get; set; }
        public Paging paging { get; set; }
    }

    public class AdAccounts
    {
        public int stt { get; set; }
        public string id { get; set; }
        public string name { get; set; }
        public string account_id { get; set; }
        public int account_status { get; set; }
        public float adtrust_dsl { get; set; }
        public string currency { get; set; }
        public string business_country_code { get; set; }
        public bool can_pay_now { get; set; }
        public string hasCard { get; set; }
        public Adspaymentcycle adspaymentcycle { get; set; }
        public BilledSpend current_unbilled_spend { get; set; }
        public string amount_spent { get; set; }
        public Ads ads { get; set; }
        public Adsets adsets { get; set; }
        public Campaigns campaigns { get; set; }
        public Addrafts addrafts { get; set; }
        public Adspixels adspixels { get; set; }
        public string pixel_id { get; set; }
        public paymentmethods all_payment_methods { get; set; }
        public default_values default_values { get; set; }
    }

    public class default_values
    {
        public ad_group ad_group { get; set; }
    }

    public class ad_group
    {
        public string related_page_id { get; set; }
    }

    public class credential
    {
        public string credential_id { get; set; }
        public string display_string { get; set; }
        public string last_four_digits { get; set; }
        public string card_association_name { get; set; }
    }

    public class creditcard
    {
        public List<credential> data { get; set; }
    }

    public class paymentmethods
    {
        public creditcard pm_credit_card { get; set; }
        public creditcard payment_method_direct_debits { get; set; }
    }
    public class Adspixels
    {
        public List<pixel> data { get; set; }
        public Paging paging { get; set; }
    }

    public class pixel
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Campaigns
    {
        public List<CampaignsItem> data { get; set; }
        public Paging paging { get; set; }
    }

    public class Adsets
    {
        public List<AdsetsItem> data { get; set; }
        public Paging paging { get; set; }
    }

    public class Ads
    {
        public List<AdsItem> data { get; set; }
        public Paging paging { get; set; }
    }

    public class CampaignsItem
    {
        public string id { get; set; }
        public string status { get; set; }
        public float lifetime_budget { get; set; }
        public DeliveryStatus delivery_status { get; set; }
    }

    public class AdsetsItem
    {
        public string id { get; set; }
        public string status { get; set; }
        public DeliveryStatus delivery_status { get; set; }
    }

    public class AdsItem
    {
        public string id { get; set; }
        public string status { get; set; }
        public DeliveryStatus delivery_status { get; set; }
    }

    public class Addrafts
    {
        public List<DraftItem> data { get; set; }
        public Paging paging { get; set; }
    }

    public class DraftItem
    {
        public string id { get; set; }
    }

    public class DeliveryStatus
    {
        public string status { get; set; }
    }

    public class Adspaymentcycle
    {
        public List<Threshold> data { get; set; }
        public Paging paging { get; set; }
    }
    public class Threshold
    {
        public float threshold_amount { get; set; }
    }

    public class BilledSpend
    {
        public float amount { get; set; }
        public float amount_in_hundredths { get; set; }
        public string currency { get; set; }
        public float offsetted_amount { get; set; }
    }

    public class TfaSecResBody
    {
        public string token { get; set; }
    }

    public class NLPageResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public PageResBody data { get; set; }
    }

    public class PageResBody
    {
        public List<page> data { get; set; }
        public Paging paging { get; set; }
    }

    public class page
    {
        public string id { get; set; }
        public string name { get; set; }
    }
    public class NLAdsResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public AdsResBody data { get; set; }
    }

    public class AdsResBody
    {
        public List<ads> data { get; set; }
        public Paging paging { get; set; }
    }

    public class ads
    {
        public string id { get; set; }
    }

    public class Paging
    {
        public Cursors cursors { get; set; }
    }

    public class Cursors
    {
        public string before { get; set; }
        public string after { get; set; }
    }

    public class FbRegParam
    {
        public int time_delay { get; set; }
        public string verifyData { get; set; }
        public string userAgent { get; set; }
    }

    public class UserResBody
    {
        public List<user> data { get; set; }
        public Paging paging { get; set; }
    }
    public class user
    {
        public string id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
    }

    public class PeCampParam
    {
        public string page_id { get; set; }
        public string video_id { get; set; }
        public string video_s_url { get; set; }
        public string thumb_url { get; set; }
        public string link { get; set; }
        public string title { get; set; }
        public string message { get; set; }
        public string link_description { get; set; }
        public string genders { get; set; }
        public string age_max { get; set; }
        public string age_min { get; set; }
        public string interests { get; set; }
        public string countries { get; set; }
        public string currency { get; set; }
        public string daily_budget { get; set; }
        public string budget_remaining { get; set; }
        public int default_selected_budget { get; set; }
        public int lifetime_budget { get; set; }
        public int campaign_length { get; set; }
        public int estimates_lower { get; set; }
        public int estimates_upper { get; set; }
        public string pixel_id { get; set; }
        public string conversion_domain { get; set; }
    }

    public class BillingPaymentMethod
    {
        public credential credential { get; set; }
        public bool is_primary { get; set; }
        public string usability { get; set; }
    }
    public class BillingPaymentAccount
    {
        public List<BillingPaymentMethod> billing_payment_methods { get; set; }
    }

    public class BillableAccountByPaymentAccount
    {
        public BillingPaymentAccount billing_payment_account { get; set; }
        public bool is_reauth_restricted { get; set; }
    }
    public class BillingAMNexusRootQueryData
    {
        public BillableAccountByPaymentAccount billable_account_by_payment_account { get; set; }
    }

    public class BillingAMNexusRootQuery
    {
        public BillingAMNexusRootQueryData data { get; set; }
    }

    // User nguyenlieu
    public class NlUser
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string flow1 { get; set; }
        public string flow2 { get; set; }
        public string flow3 { get; set; }
        public string flow4 { get; set; }
        public string flow5 { get; set; }
        public string flow6 { get; set; }
        public string flow7 { get; set; }
        public string flow8 { get; set; }
        public string flow9 { get; set; }
    }

    public class NlUserResBody
    {
        public NlUser success { get; set; }
    }

    // Clone nguyenlieu
    public class Clone
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string uid { get; set; }
        public string pass { get; set; }
        public string email { get; set; }
        public string e_pass { get; set; }
        public string s2fa { get; set; }
        public string verify_link { get; set; }
        public string cookie { get; set; }
        public string ip { get; set; }
        public string country { get; set; }
        public string ads_id { get; set; }
        public string page_id { get; set; }
        public int status { get; set; }
    }

    public class CloneResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public Clone data { get; set; }
    }

    // BmLink nguyenlieu
    public class BmLink
    {
        public int id { get; set; }
        public int user_id { get; set; }
        public string link { get; set; }
        public string via_id { get; set; }
        public string bm_id { get; set; }
        public string page_id { get; set; }
    }

    public class BmLinkResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public BmLink data { get; set; }
    }

    // Int
    public class Int
    {
        public string id { get; set; }
    }

    public class InstagramAccounts
    {
        public List<Int> data { get; set; }
    }

    public class IntData
    {
        public InstagramAccounts instagram_accounts { get; set; }
        public InstagramAccounts page_backed_instagram_accounts { get; set; }
    }

    // BM nguyenlieu
    public class NLBmDetailResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public NLBm data { get; set; }
    }

    public class NLBm
    {
        public int id { get; set; }
        public string bm_id { get; set; }
        public string token { get; set; }
    }

    // Pro Page nguyenlieu
    public class NLProPageDetailResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public NLProPage data { get; set; }
    }

    public class NLProPage
    {
        public int id { get; set; }
        public string uid { get; set; }
    }

    // Resource nguyenlieu
    public class NLResourceResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public NLResource data { get; set; }
    }

    public class NLResource
    {
        public int id { get; set; }
        public string data { get; set; }
    }

    // Param nguyenlieu
    public class NLParamResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public NLParam data { get; set; }
    }

    public class NLParamsResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public List<NLParam> data { get; set; }
    }

    public class NLParam
    {
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string page_name { get; set; }
        public string bm_page { get; set; }
        public string user_page { get; set; }
        public string bm_ads { get; set; }
        public string tkcn_qg_code { get; set; }
        public string tkcn_card_qg_code { get; set; }
        public string tkcn_tt { get; set; }
        public string tkcn_tz { get; set; }
        public string tkcn_the { get; set; }
        public string tkbm_qg_code { get; set; }
        public string tkbm_card_qg_code { get; set; }
        public string tkbm_tt { get; set; }
        public string tkbm_tz { get; set; }
        public string tkbm_the { get; set; }
        public string bm_qg_code { get; set; }
        public string bm_qg_name { get; set; }
        public string bm_the { get; set; }
        public string bmlink_via_id { get; set; }
        public string status { get; set; }
    }

    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }

    // Camp nguyenlieu
    public class NLCampsResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public List<NLCamp> data { get; set; }
    }

    public class NLCampResBody
    {
        public bool status { get; set; }
        public int code { get; set; }
        public NLCamp data { get; set; }
    }

    public class NLCamp
    {
        public string id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string version { get; set; }
        public NLMedia temp_file_data { get; set; }
        public NLMedia mockup_data { get; set; }
    }

    public class NLMedia
    {
        public string id { get; set; }
        public string url { get; set; }
        public string dir { get; set; }
        public string name { get; set; }
    }

    public class Card
    {
        public string code { get; set; }
        public string bm_id { get; set; }
        public string credential_id { get; set; }
    }

    public class ShareLog
    {
        public string bm_id { get; set; }
        public string via_id { get; set; }
    }
}
