﻿using GenCode;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoginYT.GenCode
{
    public partial class Flow : Form
    {
        public Flow()
        {
            InitializeComponent();
        }

        private void Flow_Load(object sender, EventArgs e)
        {
            // dataGridView1
            DataGridViewTextBoxColumn dgvcStepName = new DataGridViewTextBoxColumn();
            dgvcStepName.HeaderText = "Bước thực hiện";
            dgvcStepName.ReadOnly = true;
            dgvcStepName.AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
            dgvcStepName.Width = 235;
            dgvcStepName.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            DataGridViewCheckBoxColumn dgvcfend = new DataGridViewCheckBoxColumn();
            dgvcfend.HeaderText = "False_end";
            dgvcfend.AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
            dgvcfend.Width = 65;
            dgvcfend.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewCheckBoxColumn dgvcfstop = new DataGridViewCheckBoxColumn();
            dgvcfstop.HeaderText = "False_stop";
            dgvcfstop.AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
            dgvcfstop.Width = 65;
            dgvcfstop.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewCheckBoxColumn dgvcfpause = new DataGridViewCheckBoxColumn();
            dgvcfpause.HeaderText = "False_pause";
            dgvcfpause.AutoSizeMode = DataGridViewAutoSizeColumnMode.NotSet;
            dgvcfpause.Width = 65;
            dgvcfpause.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            DataGridViewTextBoxColumn dgvcStepCode = new DataGridViewTextBoxColumn();
            dgvcStepCode.HeaderText = "Code";
            dgvcStepCode.Visible = false;

            dataGridView1.Columns.Add(dgvcStepName);
            dataGridView1.Columns.Add(dgvcfend);
            dataGridView1.Columns.Add(dgvcfstop);
            dataGridView1.Columns.Add(dgvcfpause);
            dataGridView1.Columns.Add(dgvcStepCode);

            dataGridView1.AllowUserToAddRows = false;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            // Load data
            string m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string cardfile = m_exePath + "\\GenCode\\steps.txt";
            System.IO.StreamReader file = new System.IO.StreamReader(cardfile);

            string newline;
            while ((newline = file.ReadLine()) != null)
            {
                string[] stepData = newline.Split(new char[] { '|' });
                dataGridView1.Rows.Add(stepData);
            }
            file.Close();

            dataGridView1.ClearSelection();

            ManageChannel formMain = (ManageChannel)Application.OpenForms["ManageChannel"];
            this.richTextBox1.Text = formMain.FlowValue;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow r in dataGridView1.SelectedRows)
            {
                if (!string.IsNullOrEmpty(r.Cells[4].Value.ToString()))
                {
                    string cmd = r.Cells[4].Value.ToString();
                    if (r.Cells[1].Value != null && (Convert.ToBoolean(r.Cells[1].Value) == true))
                    {
                        cmd = cmd + ":false_end";
                    }

                    if (r.Cells[2].Value != null && (Convert.ToBoolean(r.Cells[2].Value) == true))
                    {
                        cmd = cmd + ":false_stop";
                    }

                    if (r.Cells[3].Value != null && (Convert.ToBoolean(r.Cells[3].Value) == true))
                    {
                        cmd = cmd + ":false_pause";
                    }

                    addCmd(cmd);
                }
            }
        }

        private void addCmd(string s)
        {
            TextBoxUtils.Instance.AppendRichTextLn(this, this.richTextBox1, s);
        }

        private void cancelBtn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void okbtn_Click(object sender, EventArgs e)
        {
            ManageChannel formMain = (ManageChannel)Application.OpenForms["ManageChannel"];
            formMain.FlowValue = this.richTextBox1.Text;
            this.Close();
        }
    }
}
