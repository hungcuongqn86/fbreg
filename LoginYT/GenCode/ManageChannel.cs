﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Gma.System.MouseKeyHook;
using Newtonsoft.Json;
using OpenQA.Selenium;
using System.IO.Compression;
using System.Configuration;
using System.Linq;
using System.Net.Sockets;
using LoginYT.GenCode;
using Firebase.Database;
using Firebase.Database.Query;
using AutoUpdaterDotNET;
using System.Management;
using xNet;
using Newtonsoft.Json.Linq;

namespace GenCode
{
	public partial class ManageChannel : Form
	{
		[DllImport("user32.dll")]
		[return: MarshalAs(UnmanagedType.Bool)]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
		private Thread _myThread;
		private EventWaitHandle signal;
		private bool signalValue = false;
		private bool _active = false;

		private string _crFlow = "";
		private string root_email = "";

		private bool is_stop;
		public string mode = "Chrome-Private";
		public string paramid = "0";
		public string campid = "0";
		public string _dataDir = "tut25flowdata";
		public string _driverDir = "FlowDriver";

		public bool freeProxy = false;
		public bool use911 = false;
		public bool useHma = false;
		public bool curuse911 = false;
		public bool auto911 = false;
		public bool autoHma = false;
		public bool changeProxy = false;
		public string _u_agent = "0";
		public string _store_token = "";
		public string _location = "";
		private List<string> _listKeyword;
		private List<string> _listFakePhone;
		private string _hIp = "";

		private int turnIndex = 0;
		private string _pre_clone_id = "";
		private string _clone_id = "";
		private int _clone_rep = 0;
		private int _clone_max_rep = 3;

		private string bmAdsLink = "";
		private string bmPageLink = "";

		public AutoRequest autoRequest;

		public RegClone _reg = null;
		private FirebaseClient firebase;
		public static List<Task> TaskList = new List<Task>();
		public NLParam paramData = null;

		public ManageChannel()
		{
			this.InitializeComponent();
			string _json = File.ReadAllText("GenCode/keyword.json");
			this._listKeyword = JsonConvert.DeserializeObject<List<string>>(_json);
			this.SubscribeGlobal();
			base.FormClosing += new FormClosingEventHandler(this.Main_Closing);
		}

		public async Task getIP()
		{
			try
			{
				HttpClient _client = new HttpClient();
				string text = await _client.GetStringAsync("http://edns.ip-api.com/json");
				string _rs = text;
				text = null;
				this.IP = Regex.Match(_rs, "\"ip\": \"(.*?)\"").Groups[1].Value;
			}
			catch (Exception ex)
			{
				this.log(ex);
			}

			this.log("Your IP: " + this.IP);
			string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			_dataPath = desktopPath + "\\" + _dataDir + "\\";
			this._folderPath = _dataPath + Path.Combine("DATA", "Dropbox", "change", this.IP + "_" + DateTime.Now.ToString("yyyy-dd-MM HH-mm-ss"));
			if (!Directory.Exists(this._folderPath))
			{
				Directory.CreateDirectory(this._folderPath);
			}
		}

		private void Main_Closing(object sender, CancelEventArgs e)
		{
			this.Unsubscribe();
		}

		private void SubscribeGlobal()
		{
			this.Unsubscribe();
			this.Subscribe(Hook.GlobalEvents());
		}

		private void Subscribe(IKeyboardMouseEvents events)
		{
			this.m_Events = events;
			this.m_Events.KeyUp += this.OnKeyUp;
		}

		private void Unsubscribe()
		{
			bool flag = this.m_Events == null;
			if (!flag)
			{
				this.m_Events.KeyUp -= this.OnKeyUp;
				this.m_Events.Dispose();
				this.m_Events = null;
			}
		}

		private void OnKeyUp(object sender, KeyEventArgs e)
		{
		}

		private void setShopify(string val)
		{
			TextBoxUtils.Instance.SetInputText(this, this.textBox4, val);
		}

		private string[] getFlow()
		{
            if (string.IsNullOrEmpty(_crFlow))
            {
				return TextBoxUtils.Instance.RichTextReadAll(this, this.richTextBox2);
			}

			// Get from nguyenlieu
			return autoRequest.getFlow(_crFlow);
		}

		private string getBmLink()
		{
			return TextBoxUtils.Instance.RichTextReadAndRemoveLastLine(this, this.richTextBox6, true);
		}

		private string getClone(bool rm)
		{
			return TextBoxUtils.Instance.RichTextReadAndRemoveLastLine(this, this.richTextBox1, rm);
		}

		private string getBmLinkInput(bool rm)
		{
			return TextBoxUtils.Instance.RichTextReadAndRemoveLastLine(this, this.richTextBox8, rm);
		}

		private string getCloneMoi(int index)
		{
			return TextBoxUtils.Instance.RichTextReadLineByIndex(this, this.richTextBox7, index);
		}

		private string getCard(string cardCode)
		{
			if (string.IsNullOrEmpty(cardCode))
			{
				return "";
			}

			NLResource card = this.autoRequest.getResource(cardCode);
			if (card == null)
			{
				return "";
			}

			return card.data;
		}

		private void logAds(string s)
		{
			string datapath = _dataPath + "\\DATA";
			bool flag = Directory.Exists(datapath);
			if (flag)
			{
				for (; ; )
				{
					try
					{
						File.AppendAllText(Path.Combine(datapath, "ads.txt"), s + "\n");
						break;
					}
					catch
					{
					}
					Thread.Sleep(500);
				}
			}
		}

		private void log(string s)
		{
			TextBoxUtils.Instance.AppendRichTextLn(this, this.tbInfo, s);
			bool flag = Directory.Exists(this._folderPath);
			if (flag)
			{
				for (;;)
				{
					try
					{
						File.AppendAllText(Path.Combine(this._folderPath, "log.txt"), s + "\n");
						break;
					}
					catch
					{
					}
					Thread.Sleep(500);
				}
			}
		}

		private void logWithThread(string threadName, string s)
		{
			TextBoxUtils.Instance.AppendRichTextLn(this, this.tbInfo, "Thread " + threadName + "->" + s);
			bool flag = Directory.Exists(this._folderPath);
			if (flag)
			{
				for (; ; )
				{
					try
					{
						File.AppendAllText(Path.Combine(this._folderPath, "log.txt"), "Thread " + threadName + "->" + s + "\n");
						break;
					}
					catch
					{
					}
					Thread.Sleep(500);
				}
			}
		}

		private void log(Exception ex)
		{
			TextBoxUtils.Instance.AppendRichTextLn(this, this.tbInfo, ex.Message + "\n" + ex.StackTrace);
			bool flag = Directory.Exists(this._folderPath);
			if (flag)
			{
				File.AppendAllText(Path.Combine(this._folderPath, "log.txt"), ex.Message + "\n" + ex.StackTrace + "\n");
			}
		}

		private void AutoUpdaterOnCheckForUpdateEvent(UpdateInfoEventArgs args)
		{
			if (args.IsUpdateAvailable)
			{
				DialogResult dialogResult;
				dialogResult =
						MessageBox.Show(
							$@"Tool đã có phiên bản mới {args.CurrentVersion}. Bạn có muốn cập nhật phần mềm không?", @"Cập nhật phần mềm",
							MessageBoxButtons.YesNo,
							MessageBoxIcon.Information);

				if (dialogResult.Equals(DialogResult.Yes) || dialogResult.Equals(DialogResult.OK))
				{
					try
					{
						if (AutoUpdater.DownloadUpdate(args))
						{
							Application.Exit();
						}
					}
					catch (Exception exception)
					{
						MessageBox.Show(exception.Message, exception.GetType().ToString(), MessageBoxButtons.OK,
							MessageBoxIcon.Error);
					}
				}
			}
			else
			{
				MessageBox.Show(@"Phiên bản Bạn đang sử dụng đã được cập nhật mới nhất.", @"Cập nhật phần mềm",
					MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
		}

		private void StoreCamp(string campLink)
		{
			bool storeCamp = autoRequest.storeCamp(campLink);
			if (!storeCamp)
			{
				this.log("NINI BUG CAMP!");
			}
		}

		private void SetTurnStatus()
		{
			try
			{
				Color cl = Color.Green;
                if (_clone_rep == (_clone_max_rep - 1))
                {
					cl = Color.Orange;
				}
				if (_clone_rep >= _clone_max_rep)
				{
					cl = Color.Red;
				}
				string sStatus = "T" + turnIndex.ToString() + ": " + _clone_id + "...(" + _clone_rep + ")";
				TextBoxUtils.Instance.SetLabelText(this, this.label14, sStatus, cl);
			}
			catch (Exception error)
			{
				this.log(error);
				return;
			}
		}

		private async void ManageChannel_Load(object sender, EventArgs e)
		{
			try
			{
				await getIP();
				this.StartPosition = FormStartPosition.CenterScreen;
				signal = new EventWaitHandle(false, EventResetMode.AutoReset);
				signalValue = false;

				// Check Update
				System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
				System.Diagnostics.FileVersionInfo fvi = System.Diagnostics.FileVersionInfo.GetVersionInfo(assembly.Location);
				AutoUpdater.CheckForUpdateEvent += AutoUpdaterOnCheckForUpdateEvent;

				string version = fvi.FileVersion;
				this.Text = this.Text + version;

				// Check token
				_hIp = this.getCPUId() + this.getUUID();
				this.autoRequest = new AutoRequest(_hIp);
				AutoRequest autoRq = autoRequest;
				autoRq._logDelegate = (AutoRequest.LogDelegate)Delegate.Combine(autoRq._logDelegate, new AutoRequest.LogDelegate(this.LogDelegate));

				root_email = this.autoRequest.checkKey();
				if (!string.IsNullOrEmpty(root_email))
				{
					_active = true;
				}

				if (!_active)
				{
					// Not key
					this.log("WARNING! KHÔNG ACTIVE ĐƯỢC TOOL, MỘT SỐ CHỨC NĂNG CỦA TOOL SẼ KHÔNG HOẠT ĐỘNG ĐƯỢC, BẠN THỬ ACTIVE LẠI HOẶC LIÊN HỆ ADMIN ĐỂ LẤY KEY KHÁC!");
					this.log("=================================");
					this.log("***********I Love You!***********");
					this.log("=================================");
				}

				if (_active)
				{
					this.Text = this.Text + " Active - " + root_email;
					this.button10.Visible = false;
					this.button10.Enabled = false;

					this.textBox18.Visible = false;
					this.textBox18.Enabled = false;
				}
				else
				{
					this.button10.Visible = true;
					this.button10.Enabled = true;

					this.textBox18.Visible = true;
					this.textBox18.Enabled = true;
				}
				AutoUpdater.DownloadPath = "update";
				BasicAuthentication basicAuthentication = new BasicAuthentication("nini", "1234567");
				AutoUpdater.BasicAuthXML = AutoUpdater.BasicAuthDownload = AutoUpdater.BasicAuthChangeLog = basicAuthentication;

				SetTurnStatus();

				// get init data
				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir;
				string m_Path = dataPath + "\\init\\init.txt";
				if (File.Exists(m_Path))
				{
					string[] lines = File.ReadLines(m_Path).ToArray();
					if (lines.Length > 7)
					{
						string[] line1 = lines[0].Split('|');
						if (line1.Length > 0 && !string.IsNullOrEmpty(line1[0]))
						{
							this.checkBox6.Checked = bool.Parse(line1[0]);
						}
						if (line1.Length > 1 && !string.IsNullOrEmpty(line1[1]))
						{
							this.checkBox1.Checked = bool.Parse(line1[1]);
						}
						if (line1.Length > 2 && line1[2] != null)
						{
							this.numericUpDown2.Text = line1[2];
						}
						if (line1.Length > 3 && line1[2] != null)
						{
							this.textBox2.Text = line1[3];
						}

						if (lines.Length > 1 && lines[1] != null)
						{
							this.textBox4.Text = lines[1];
						}

						if (lines.Length > 4 && lines[4] != null)
						{
							this.textBox9.Text = lines[4];
						}

						if (lines.Length > 6 && lines[6] != null)
						{
							this.textBox10.Text = lines[6];
						}

						if (lines.Length > 9 && lines[9] != null)
						{
							this.numericUpDown1.Text = lines[9];
						}

						if (lines.Length > 11 && lines[11] != null)
						{
							string[] line14 = lines[11].Split('|');
							this.textBox14.Text = line14[0];
							if (line14.Length > 1)
							{
								this.textBox1.Text = line14[1];
							}
							if (line14.Length > 2)
							{
								this.numericUpDown7.Text = line14[2];
							}
						}

						if (lines.Length > 13 && lines[13] != null)
						{
							this.textBox15.Text = lines[13];
						}

						if (lines.Length > 14 && lines[14] != null)
						{
							this.textBox17.Text = lines[14];
						}

						if (lines.Length > 16 && lines[16] != null)
						{
							string[] line16 = lines[16].Split('|');
							if (line16.Length == 2)
							{
								this.checkBox5.Checked = bool.Parse(line16[0]);
								this.numericUpDown6.Text = line16[1];
							}
						}

						if (lines.Length > 18 && lines[18] != null)
						{
							this.textBox25.Text = lines[18];
						}

						if (lines.Length > 19 && lines[19] != null)
						{
							this.textBox26.Text = lines[19];
						}

						if (lines.Length > 27 && lines[27] != null)
						{
							this.checkBox7.Checked = bool.Parse(lines[27]);
						}

						if (lines.Length > 28 && lines[28] != null)
						{
							paramid = lines[28];
						}

						if (lines.Length > 29 && lines[29] != null)
						{
							campid = lines[29];
						}

						if (lines.Length > 30 && lines[30] != null)
						{
							this.textBox3.Text = lines[30];
						}
					}
				}

				// comboBox1
				comboBox1.Items.Add("Chrome-Private");
				comboBox1.Items.Add("Chrome-Portable");
				comboBox1.Items.Add("Chrome-Profile");
				comboBox1.SelectedIndex = 1;

				// Param
				List<NLParam> paramsData = this.autoRequest.getParams();
				if ((paramsData == null) || (paramsData.Count == 0))
				{
					this.log("Không có cấu hình tham số!");
                }
                else
                {
					foreach (NLParam pram in paramsData)
					{
						ComboboxItem item = new ComboboxItem();
						item.Text = pram.title;
						item.Value = pram.id;
						comboBox2.Items.Add(item);
					}
					SelectItemByValue(comboBox2, paramid);
				}

				loadCamps();

				// get clone
				string clonefile = dataPath + "\\DATA\\clone.txt";
				System.IO.StreamReader sr = new System.IO.StreamReader(clonefile);
				richTextBox1.Text = sr.ReadToEnd();
				sr.Close();

				// get bmlink
				string bmlinkfile = dataPath + "\\DATA\\bmlink.txt";
				if (File.Exists(bmlinkfile))
				{
					sr = new System.IO.StreamReader(bmlinkfile);
					richTextBox6.Text = sr.ReadToEnd();
					sr.Close();
				}

				// get flow
				string flowfile = dataPath + "\\DATA\\flow.txt";
				if (File.Exists(flowfile))
				{
					sr = new System.IO.StreamReader(flowfile);
					richTextBox2.Text = sr.ReadToEnd();
					sr.Close();
				}

				// get clone moi
				string clonemoifile = dataPath + "\\DATA\\clone_moi.txt";
				if (File.Exists(clonemoifile))
				{
					sr = new System.IO.StreamReader(clonemoifile);
					richTextBox7.Text = sr.ReadToEnd();
					sr.Close();
				}

				// get bm link
				string bmlinkInputfile = dataPath + "\\DATA\\bmlink_input.txt";
				if (File.Exists(bmlinkInputfile))
				{
					sr = new System.IO.StreamReader(bmlinkInputfile);
					richTextBox8.Text = sr.ReadToEnd();
					sr.Close();
				}

				setBtnFlowAction();

				if (!StartFirebase())
				{
					this.log("Không khởi tạo được dịch vụ của Firebase!");
					this.log("Ae cần chạy lại tool!");
				}

				/*FireBaseMessage message = new FireBaseMessage();
				int Timestamp = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
				message.Id = Timestamp.ToString();
				message.Data = "";
				await firebase.Child("make_primary/" + RemoveSpecialChars(root_email) + "nini1").Child(message.Id).PutAsync(message);*/
			}
			catch (Exception error)
			{
				this.log(error);
				return;
			}
		}

		private bool StartFirebase()
		{
			bool result;
			try
			{
				string FirebaseUrl = ConfigurationManager.AppSettings["FirebaseUrl"];
				string FirebaseSecretKey = ConfigurationManager.AppSettings["FirebaseSecretKey"];

				firebase = new FirebaseClient(FirebaseUrl, new FirebaseOptions
				{
					AuthTokenAsyncFactory = () => Task.FromResult(FirebaseSecretKey)
				});
				result = true;
			}
			catch
			{
				result = false;
			}
			return result;
		}

		public string FlowValue
		{
			get { return richTextBox2.Text; }
			set { richTextBox2.Text = value; }
		}

		private void LogDelegate(string _s)
		{
			this.log(_s);
		}

		private void saveInitParam()
		{
			try
			{
				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir;
				string logPath = dataPath + "\\init";
				bool exists = System.IO.Directory.Exists(logPath);
				if (!exists)
					System.IO.Directory.CreateDirectory(logPath);

				string proxyDfData = this.checkBox6.Checked + "|" + this.checkBox1.Checked + "|" + this.numericUpDown2.Value + "|" + this.textBox2.Text.Trim();
				string shopify = this.textBox4.Text.Trim();
				string bmPage = "";
				string bmAd = "";
				string viaAd = this.textBox9.Text.Trim();
				string coutry = "";
				string zipcode = this.textBox10.Text.Trim();
				string pxData = "#";
				string card2 = "";
				int numberRep = (int)this.numericUpDown1.Value;
				string pagenamep = "";
				string regParam = this.textBox14.Text.Trim() + "|" + this.textBox1.Text.Trim() + "|" + this.numericUpDown7.Value;

				string bmCountryInput = "";
				string bmZipcode = textBox15.Text.Trim();
				string whatsapp = textBox17.Text.Trim();
				string bmlinkVia = "";
				string proxyHmaData = this.checkBox5.Checked + "|" + this.numericUpDown6.Value;
				string card1 = "";
				string timezone = "";
				string bmCardCode = "";

				string tkbmCountry = "";
				string tkbmTimezone = "";
				string tkbmcardcode = "";

				string bmLimitAd = this.textBox25.Text.Trim();
				string viaLimitAd = this.textBox26.Text.Trim();

				string bmlinkpagevia = "";
				string bmlinkadsvia = "";

				string useHma = this.checkBox7.Checked + "";
				paramid = TextBoxUtils.Instance.getComboBoxSelect(this, comboBox2);
				campid = TextBoxUtils.Instance.getComboBoxSelect(this, comboBox3);

				string taskcode = this.textBox3.Text.Trim();

				string[] initdata = { proxyDfData, shopify, bmPage, bmAd, viaAd, coutry, zipcode, pxData, card2, numberRep.ToString(), pagenamep, 
					regParam, bmCountryInput, bmZipcode, whatsapp, bmlinkVia, proxyHmaData, card1,
					bmLimitAd, viaLimitAd, timezone, bmCardCode, tkbmCountry, tkbmTimezone, tkbmcardcode, bmlinkpagevia, bmlinkadsvia, useHma, paramid, campid, taskcode};
				File.WriteAllLines(logPath + "\\init.txt", initdata);
			}
			catch
			{
				this.log("Save Init Param --- False!");
			}
		}

		private static void Delay(int Time_delay)
		{
			int i = 0;
			System.Timers.Timer _delayTimer = new System.Timers.Timer();
			_delayTimer.Interval = Time_delay;
			_delayTimer.AutoReset = false; //so that it only calls the method once
			_delayTimer.Elapsed += (s, args) => i = 1;
			_delayTimer.Start();
			while (i == 0) { };
		}

		private string getIp()
		{
			try
			{
				string source = "http://ip-api.com/json/|query";
				string[] sourceData = source.Split(new char[] { '|' });
				string getdata = GetData(sourceData[0]);
				return Regex.Match(getdata, "\"" + sourceData[1] + "\":\"(.*?)\"").Groups[1].Value;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return "";
			}
		}

		private string GetData(string url)
		{
			string result;
			try
			{
				xNet.HttpRequest http = new xNet.HttpRequest
				{
					Cookies = new CookieDictionary(false)
				};

				http.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
				string _proxy_str = _proxy_host + ":" + _proxy_port;
				http.Proxy = Socks5ProxyClient.Parse(_proxy_str);
				http.ConnectTimeout = 120000;
				http.EnableEncodingContent = true;

				http.AddHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
				http.AddHeader("accept-language", "en-US,en;q=0.9");
				http.AddHeader("sec-ch-ua", "\"Chromium\";v=\"94\", \"Google Chrome\";v=\"94\", \";Not A Brand\";v=\"99\"");
				http.AddHeader("sec-ch-ua-mobile", "?0");
				http.AddHeader("sec-ch-ua-platform", "\"Windows\"");
				http.AddHeader("sec-fetch-dest", "document");
				http.AddHeader("sec-fetch-mode", "navigate");
				http.AddHeader("sec-fetch-site", "none");
				http.AddHeader("sec-fetch-user", "?1");
				http.AddHeader("upgrade-insecure-requests", "1");

				result = http.Get(url, null).ToString();
			}
			catch (Exception ex2)
			{
				result = ex2.Message;
			}
			return result;
		}

		private bool change911Proxy(string coutr_code = "us")
		{
			try
			{
				string oldIp = getIp();
				this.log("IP cũ: " + oldIp);

				string proxyUrl = "http://localhost:9049/v1/ips?num=1&country=" + coutr_code + "&port=" + _proxy_port;
				HttpClient _client = new HttpClient();
				string _rs = _client.GetStringAsync(proxyUrl).Result;
				if (string.IsNullOrEmpty(_rs))
				{
					this.log("Không phản hồi!");
					this.log("Không đổi được IP!");
					return false;
				}

				this._proxy_host = Regex.Match(_rs, "(.*?):" + _proxy_port).Groups[1].Value;
				if (string.IsNullOrEmpty(this._proxy_host))
				{
					this.log("Not host!");
					this.log(_rs);
					this.log("Không đổi được IP!");
					return false;
				}

				string newIp = getIp();

				int ChkPageIndex = 1;
				while ((string.IsNullOrEmpty(newIp) || (newIp == oldIp)) && (ChkPageIndex <= 8))
				{
					Delay(5000);
					newIp = getIp();
					this.log("check....." + newIp);
					ChkPageIndex++;
				}

				if(newIp == oldIp)
                {
					this.log("Ip không thay đổi!!");
					return false;
				}

				this.log("New IP: " + newIp);
				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool changeHmaProxy()
		{
			try
			{
				Process process = new Process
				{
					StartInfo = new ProcessStartInfo
					{
						CreateNoWindow = true,
						UseShellExecute = true,
						FileName = "cmd.exe",
						WindowStyle = ProcessWindowStyle.Hidden,
						Arguments = "/C netsh interface set interface  \"HMA! Pro VPN\" disable",
						Verb = "runas"
					}
				};
				process.Start();
				process.WaitForExit();
				Delay(3000);
				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private string GetLocalIPAddress()
		{
			var host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (var ip in host.AddressList)
			{
				if (ip.AddressFamily == AddressFamily.InterNetwork)
				{
					return ip.ToString();
				}
			}
			return "";
		}

		private string[] GetPixelConfig()
		{
			string pixelDatastr = autoRequest.getSetting("pixel");
			return pixelDatastr.Split(new char[] { '|' });
		}

		private bool validate()
		{
			try
			{
				/*if (this.textBox3.Text.Trim().Length == 0)
				{
					MessageBox.Show("911 - Phải nhập user 911!", "Thông báo!", MessageBoxButtons.OK);
					return false;
				}*/

				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool loginStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				_reg.switchRegTabHandle();
				string type = "none";
				if (stepData.Length > 1)
				{
					type = stepData[1];
				}

				bool login = false;
				if (type == "none")
				{
					this.logWithThread(_threadName, "Login with username and pass...");
					login = _reg.LoginUP(_reg._clone_data);
				}

				if (type == "verify_link")
				{
					this.logWithThread(_threadName, "Login with verify_link...");
					login = _reg.LoginVerifyLink(_reg._clone_data);
                    if (!login)
                    {
						if (!_reg._check_point)
						{
							autoRequest.resetClone(_reg._clone_uid);
						}
					}
				}

				if (type == "cookie")
				{
					this.logWithThread(_threadName, "Login cookie...");
					login = _reg.LoginCookie(_reg._clone_data);
				}

				if (type == "2fa")
				{
					this.logWithThread(_threadName, "Login 2fa...");
					login = _reg.Login2Fa(_reg._clone_data);
				}

				if (type == "m2fa")
				{
					this.logWithThread(_threadName, "Login 2fa mobile...");
					login = _reg.LoginM2Fa(_reg._clone_data);
				}

				if (!login)
				{
                    if (_reg._check_point)
                    {
						this.removeClone(_threadName);
						autoRequest.removeClone(_reg._clone_uid);
					}
                }
                else
                {
					autoRequest.removeClone(_reg._clone_uid);
				}

				return login;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool regStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				FbRegParam regParam = new FbRegParam();
				regParam.verifyData = this.textBox14.Text.Trim();
				regParam.userAgent = this.textBox1.Text.Trim();
				bool changeUA = false;
                if (!string.IsNullOrEmpty(regParam.userAgent))
                {
					// Set userAgent
					this.logWithThread(_threadName, "Set User Agent!");
					if (this.mode == "Chrome-Portable")
					{
						string chrome = "portable";
						initBrowse(chrome);
						_reg.initChromePortable(chrome, regParam.userAgent, true);
					}
					Delay(5000);
					changeUA = true;
				}
				regParam.time_delay = (int)this.numericUpDown7.Value;
				bool reg = _reg.fbRegMb(regParam);

				if (changeUA)
				{
					// Set userAgent
					this.logWithThread(_threadName, "Rollback!");
					if (this.mode == "Chrome-Portable")
					{
						string chrome = "portable";
						initBrowse(chrome);
						_reg.initChromePortable(chrome);
					}
					Delay(5000);
				}

				return reg;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool createBmStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				// Create BM
				_reg._create_bm = false;

				string type = "shopify";
				if (stepData.Length > 1)
				{
					type = stepData[1];
				}

				bool createBm = false;
				if (type == "shopify")
				{
					this.logWithThread(_threadName, "Create BM With Shopify!");
					string shopifyDataStr = this.textBox4.Text.Trim();
					if (string.IsNullOrEmpty(shopifyDataStr))
					{
						this.logWithThread(_threadName, "Not Shopify!");
						this.is_stop = true;
						return false;
					}

					string[] stepcode = this.autoRequest.getStepCodes("CreateShopifyBm");
					if (stepcode.Count() == 0)
					{
						Delay(1000);
						stepcode = this.autoRequest.getStepCodes("CreateShopifyBm");
					}

					if (stepcode.Count() == 0)
					{
						Delay(1000);
						stepcode = this.autoRequest.getStepCodes("CreateShopifyBm");
					}

					if (stepcode.Count() == 0)
                    {
						this.logWithThread(_threadName, "Kún ơi! Active tool để có thể thực hiện được step này!");
						createBm = false;
					}
                    else
                    {
						createBm = _reg.CreateShopifyBm(shopifyDataStr, stepcode);
					}
				}

				_reg.switchBusinessTabHandle();

				if (type == "whatsapp")
				{
					this.logWithThread(_threadName, "Create BM With Whatsapp Verify!");
					string whatsapp = this.textBox17.Text.Trim();

					string m_exePath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
					string messagePath = m_exePath + "\\whatsapp\\message.json";
					if (File.Exists(messagePath))
					{
						File.Delete(messagePath);
					}
					createBm = _reg.CreateWhatsAppBm(whatsapp, messagePath);
				}

				if (type == "script")
				{
					this.logWithThread(_threadName, "Create BM With Script!");
					string[] emailStepCode = this.autoRequest.getStepCodes("CreateEmailBm");
					if (emailStepCode.Count() == 0)
					{
						Delay(1000);
						emailStepCode = this.autoRequest.getStepCodes("CreateEmailBm");
					}
					if (emailStepCode.Count() == 0)
					{
						Delay(1000);
						emailStepCode = this.autoRequest.getStepCodes("CreateEmailBm");
					}

					_reg.emailBmConfirm = emailStepCode[0];
					_reg.emailPassBmConfirm = emailStepCode[1];

					bool verify = true;
					if (stepData.Contains("noverify"))
					{
						verify = false;
					}
					createBm = _reg.CreateScriptBm(emailStepCode, verify);
				}

				if (_reg._create_bm)
				{
					_reg.getBmInfo();

					if (stepData.Contains("RemoveClone"))
					{
						string crClone1 = getClone(false);
						if (crClone1 == _reg._clone_data)
						{
							this.logWithThread(_threadName, "Remove clone ...");
							this.getClone(true);
							this.logWithThread(_threadName, "Remove clone: OK");
						}
					}
				}

				return createBm;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool createChienbnBmStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				// Create BM
				_reg.switchBusinessTabHandle();
				_reg._create_bm = false;
				bool createBm = false;
				this.logWithThread(_threadName, "Create BM With Script!");
				string[] emailStepCode = this.autoRequest.getStepCodes("CreateEmailBm");
				if (emailStepCode.Count() == 0)
				{
					Delay(1000);
					emailStepCode = this.autoRequest.getStepCodes("CreateEmailBm");
				}
				if (emailStepCode.Count() == 0)
				{
					Delay(1000);
					emailStepCode = this.autoRequest.getStepCodes("CreateEmailBm");
				}

				_reg.emailBmConfirm = emailStepCode[0];
				_reg.emailPassBmConfirm = emailStepCode[1];
				createBm = _reg.CreateScriptBm(emailStepCode, false);
                if (!createBm)
                {
                    if (_reg.createBmErrCode == "Restricted")
                    {
						string crClonecbn = getClone(false);
						if (crClonecbn == _reg._clone_data)
						{
							logWithThread(_threadName, "Xóa clone ...");
							getClone(true);
						}
                    }
                    else
                    {
						Change911ProxyWrap();
						this.logWithThread(_threadName, "15 giây trôi qua ...");
						Delay(15000);
					}
                }
                else
                {
					_reg.getBmInfo();
				}

				return createBm;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool reciveShareBmPageStep(RegClone _reg, string _threadName, string[] stepData, NLParam paramData)
		{
			try
			{
				bool received = _reg.receiveBm(bmPageLink);
				if (received)
				{
					// update to nguyenlieu.site
					NLBm bm = this.autoRequest.postBmUpdate(paramData.bm_page, _reg._bm_id, _reg._accessToken, _reg._clone_uid, _reg._cookie);
					if (bm == null)
					{
						logWithThread(_threadName, "Không lưu được BM...");
					}

					bmPageLink = "";
					string crClone1 = getClone(false);
					if (crClone1 == _reg._clone_data)
					{
						logWithThread(_threadName, "Xóa clone...");
						getClone(true);
					}
					saveInitParam();
				}

				return received;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool reciveShareBmAdsStep(RegClone _reg, string _threadName, string[] stepData, NLParam paramData)
		{
			try
			{
				bool received = _reg.receiveBm(bmAdsLink);
				if (received)
				{
					// update to nguyenlieu.site
					NLBm bm = this.autoRequest.postBmUpdate(paramData.bm_ads, _reg._bm_id, _reg._accessToken, _reg._clone_uid, _reg._cookie);
					if (bm == null)
					{
						logWithThread(_threadName, "Không lưu được BM...");
					}

					bmAdsLink = "";
					string crClone1 = getClone(false);
					if (crClone1 == _reg._clone_data)
					{
						logWithThread(_threadName, "Xóa clone...");
						getClone(true);
					}
					saveInitParam();
				}

				return received;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool reciveBmStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				this.logWithThread(_threadName, "Get link ...");
				string bmLink = this.getBmLinkInput(false);
				this.logWithThread(_threadName, "Link: " + bmLink);
				bool received = _reg.receiveBm(bmLink);
				if (received)
				{
					string crBmLink = getBmLinkInput(false);
					if (bmLink == crBmLink)
					{
						this.logWithThread(_threadName, "Remove link ...");
						this.getBmLinkInput(true);
						this.logWithThread(_threadName, "Remove link: OK");
					}
				}
				return received;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool reciveProfilePageStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				// Get Page Code
				// Todo: cau hinh len site
				// string pagecode = this.textBox5.Text.Trim();
				string pagecode = "";
				if (string.IsNullOrEmpty(pagecode))
				{
					this.logWithThread(_threadName, "Không có Page code!");
					return false;
				}

				// Lay Page
				NLProPage proPage = this.autoRequest.getProPageDetail(pagecode);
				if ((proPage == null) || (string.IsNullOrEmpty(proPage.uid)))
				{
					this.log("ProPage data id empty!");
					return false;
				}

				// Invite
				this.log("Invite ...");
				string data = this.autoRequest.inviteAdminPage(pagecode, _reg._clone_uid);
				string profile_admin_invite_id = Regex.Match(data, "\"profile_admin_invite_id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(profile_admin_invite_id))
				{
					this.log("Invite: FALSE!");
				}

				// Accept
				this.log("Accept ...");
				return _reg.agreeAdminProPage(proPage.uid, profile_admin_invite_id);
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool reciveNguyenLieuBmStepAsync(RegClone _reg, string _threadName, string[] stepData, NLParam paramData)
		{
			try
			{
				this.logWithThread(_threadName, "Get link ...");
				string viaid = paramData.bmlink_via_id;
                if (string.IsNullOrEmpty(viaid))
                {
					this.logWithThread(_threadName, "Not VIA ID!");
					return false;
				}

				BmLinkResBody bmLink = this.autoRequest.getBmLink(viaid);
                if (!bmLink.status)
                {
					this.logWithThread(_threadName, "Không lấy được BM link từ nguyenlieu.site!");
					return false;
				}

				this.logWithThread(_threadName, "Link: " + bmLink.data.link);
				this.logWithThread(_threadName, "via: " + bmLink.data.via_id);
				this.logWithThread(_threadName, "bm: " + bmLink.data.bm_id);
				this.logWithThread(_threadName, "Page: " + bmLink.data.page_id);
				bool received = _reg.receiveBmAPI(bmLink.data);
				return received;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool editClonePageStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				return _reg.apiEditClonePage();
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool editBmPageStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				string type = "api";
				if (stepData.Length > 1)
				{
					type = stepData[1];
				}

				bool editPage = false;
				if (type == "api")
				{
					editPage = _reg.apiEditBmPage();
				}

				return editPage;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool getCloneStep(string _threadName, string[] stepData)
		{
			try
			{
				if (stepData.Contains("nguyenlieu.site"))
				{
					this.logWithThread(_threadName, "Lấy clone từ nguyenlieu.site!");
					_reg._clone_data = autoRequest.setClone(stepData.Contains("checkip"));
                    if (string.IsNullOrEmpty(_reg._clone_data) && stepData.Contains("wait"))
                    {
						this.logWithThread(_threadName, "Chưa có clone! không sao, có thể ngồi đây đợi 1h nữa !!");
						int ChkLoadingIndex = 1;
						while (string.IsNullOrEmpty(_reg._clone_data) && (ChkLoadingIndex <= 180))
						{
							Delay(20000);
							this.logWithThread(_threadName, "20 giây trôi qua ...");
							_reg._clone_data = autoRequest.setClone(stepData.Contains("checkip"));
							ChkLoadingIndex++;
						}
					}
				}
                else
                {
					this.logWithThread(_threadName, "Lấy clone từ form input!");
					_reg._clone_data = getClone(false);
				}
				
				if (string.IsNullOrEmpty(_reg._clone_data))
				{
					return false;
				}
				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool createClonePageStepAsync(RegClone _reg, string _threadName, string[] stepData, NLParam paramData)
		{
			try
			{
				// Create page
				string pageNamePr = paramData.page_name;

				string type = "pro5";
				if (stepData.Contains("page"))
				{
					type = "page";
				}

				bool createPage = false;
				_reg.switchRegTabHandle();

				if (type == "page")
				{
					createPage = _reg.createClonePage(pageNamePr);
                }
                else
                {
					createPage = _reg.createProfile(pageNamePr);
				}

				return createPage;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool cloneSharePageStepAsync(RegClone _reg, string _threadName, string[] stepData, NLParam paramData)
		{
			try
			{
				// Get BM
				string bmcode = paramData.bm_page;
                if (string.IsNullOrEmpty(bmcode))
                {
					this.logWithThread(_threadName, "Không có BM code!");
					return false;
				}

				bool share = _reg.sharePageToBm(bmcode);

				if (!share)
				{
					if (_reg.shareToBmErrCode == "change_bm_link")
					{
						// Set bmlink
						logWithThread(_threadName, "Lấy link BM...");
						if (!string.IsNullOrEmpty(bmcode))
						{
							BmLinkResBody bmLink = this.autoRequest.getBmLink(bmcode);
							if (bmLink.status)
							{
								if ((bmLink.data != null) && string.IsNullOrEmpty(bmPageLink))
								{
									this.logWithThread(_threadName, "Link: " + bmLink.data.link);
									bmPageLink = bmLink.data.link;
								}
							}
							else
							{
								this.logWithThread(_threadName, "Không lấy được BM link từ nguyenlieu.site!");
							}
						}
						else
						{
							this.logWithThread(_threadName, "KHông cấu hình BM Code!");
						}
					}
				}

				return share;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool recievedCurrentBMStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				// Set bmlink
				if (!string.IsNullOrEmpty(_reg._bm_id))
				{
					logWithThread(_threadName, "Lấy link BM...");
					logWithThread(_threadName, "BM: " + _reg._bm_id);
					BmLinkResBody bmLink = this.autoRequest.getBmLinkAndRemove(_reg._bm_id);
					if (bmLink.status)
					{
						if (bmLink.data != null)
						{
							return _reg.receiveBmAPI(bmLink.data);
                        }
                        else
                        {
							this.logWithThread(_threadName, "Không lấy được BM link từ nguyenlieu.site 2!");
							return false;
						}
					}
					else
					{
						this.logWithThread(_threadName, "Không lấy được BM link từ nguyenlieu.site 1!");
						return false;
					}
				}
				else
				{
					this.logWithThread(_threadName, "Không có BM!");
					return false;
				}
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool setRoleAdStepAsync(RegClone _reg, string _threadName, string[] stepData, string ojbtype)
		{
			try
			{
				return _reg.SetRoleAd(ojbtype);
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool genBmReceiveLinkStepAsync(RegClone _reg, string _threadName, string[] stepData, string stepStr)
		{
			try
			{
				_reg.switchBusinessTabHandle();

				string email = "hihi@ctc.com";
				string pass = "123456";

				string camp_id = "";
				if (stepData.Contains("camp"))
				{
					camp_id = TextBoxUtils.Instance.getComboBoxSelect(this, comboBox3);
					if (string.IsNullOrEmpty(camp_id))
					{
						log("CHƯA CHỌN CAMP!");
						return false;
					}
				}

				string[] emailStepCode = this.autoRequest.getStepCodes("genBmReceiveLink");
				if (emailStepCode.Count() == 0)
				{
					emailStepCode = this.autoRequest.getStepCodes("genBmReceiveLink");
				}

				if (emailStepCode.Count() > 1)
				{
					email = emailStepCode[0];
					pass = emailStepCode[1];
				}

				bool setPageRole = false;
				if (stepData.Contains("page_role"))
                {
					setPageRole = true;
				}

				bool setAdsRole = false;
				if (stepData.Contains("ads_role"))
				{
					setAdsRole = true;
				}

				int emailTime = 6;
				string waitEmailTimeStr = Regex.Match(stepStr, "wait_email_time_(.*?)#").Groups[1].Value;
				if (!string.IsNullOrEmpty(waitEmailTimeStr))
				{
					Int32.TryParse(waitEmailTimeStr, out emailTime);
				}

				bool longlink = false;
				if (stepData.Contains("long_link"))
				{
					longlink = true;
				}

				string viaInputStr = this.textBox9.Text.Trim();
				string[] viaInputData = viaInputStr.Split(new char[] { '|' });
				string viaid = viaInputData[0];

				// Check has share
				bool has_share = false;
                if (_reg._listShareBmToVIA != null)
                {
					foreach (ShareLog logItem in _reg._listShareBmToVIA)
					{
						if ((logItem.via_id == viaid) && (logItem.bm_id == _reg._bm_id))
						{
							has_share = true;
							break;
						}
					}
				}

				string genLink = "http://nini2bi.com";
                if (!has_share)
                {
					genLink = _reg.genBmReceiveLink("mail.tm", email, pass, setPageRole, setAdsRole, emailTime, longlink);
					if (string.IsNullOrEmpty(genLink))
					{
						genLink = _reg.genBmReceiveLink("mail.gw", email, pass, setPageRole, setAdsRole, emailTime, longlink);
					}

					if (string.IsNullOrEmpty(genLink))
					{
						genLink = _reg.genBmReceiveLink("10minutemail.net", email, pass, setPageRole, setAdsRole, emailTime, longlink);
					}

					if (string.IsNullOrEmpty(genLink))
					{
						genLink = _reg.genBmReceiveLink("gmail.com", email, pass, setPageRole, setAdsRole, emailTime, longlink);
					}

					if (string.IsNullOrEmpty(genLink))
					{
						return false;
					}
				}

				if (!string.IsNullOrEmpty(genLink) && stepData.Contains("autoReceive"))
				{
					int waitTime = 8;
					string waitTimeStr = Regex.Match(stepStr, "wait_time_(.*?)#").Groups[1].Value;
					if (!string.IsNullOrEmpty(waitTimeStr))
					{
						Int32.TryParse(waitTimeStr, out waitTime);
					}

					string first_name = "Nini";
					string last_name = new Random().Next(100000, 999999).ToString();

					string bm_credential_id = "";
					if (stepData.Contains("make_primary"))
                    {
						foreach (Card billPm in _reg._bm_cards)
						{
							if (stepData.Contains(billPm.code) && (billPm.bm_id == _reg._bm_id))
							{
								bm_credential_id = billPm.credential_id;
								break;
							}
						}
					}

					string genLinkOpt = "";
					if (stepData.Contains("genlink"))
                    {
						genLinkOpt = "GENLINK";
					}

					string outBmOpt = "";
					if (stepData.Contains("outbm"))
					{
						outBmOpt = "OUTBM";
					}

					string notwaitOpt = "";
					if (stepData.Contains("notwait"))
					{
						notwaitOpt = "NOTWAIT";
					}

					bool autoReceive = Task.Run(async () => await putFirebaseAsync(viaid, 1, genLink + "|" + first_name + "|" + last_name + "|" + _reg._bm_ads_id + "|" + bm_credential_id + "|" + _reg._bm_id + "|" + camp_id + "|" + _reg.pageID + "|" + genLinkOpt + "|" + outBmOpt)).GetAwaiter().GetResult();
					if (autoReceive)
					{
						this.logWithThread(_threadName, "Đã chuyển yêu cầu VIA nhận BM!");
                        // check bm
                        if (!has_share && string.IsNullOrEmpty(notwaitOpt))
                        {
							this.logWithThread(_threadName, "Hãy đợi VIA nhận BM...");
							bool check = _reg.checkBmUser(first_name, last_name, waitTime);
							if (check)
							{
								ShareLog logItem = new ShareLog()
								{
									bm_id = _reg._bm_id,
									via_id = viaid
								};

                                if (string.IsNullOrEmpty(outBmOpt))
                                {
									_reg._listShareBmToVIA.Add(logItem);
								}
								
								this.logWithThread(_threadName, "VIA đã nhận BM!");
							}
							else
							{
								this.logWithThread(_threadName, "Không đợi được rồi, hãy kiểm tra VIA!");
								return false;
							}
						}

						// check card
						if (stepData.Contains("make_primary"))
                        {
							if (checkCardExist(_reg._bm_ads_id))
							{
								this.logWithThread(_threadName, "Đã có thẻ!");
							}
							else
							{
								this.logWithThread(_threadName, "Không thấy thẻ!!");
								return false;
							}
						}

						// check camp
						if (stepData.Contains("camp"))
						{
							if (checkCampExist(_reg._bm_ads_id, waitTime))
							{
								this.logWithThread(_threadName, "Đã có camp!");
							}
							else
							{
								this.logWithThread(_threadName, "Không thấy camp!!");
								return false;
							}
						}
					}
					else
					{
						this.logWithThread(_threadName, "Không chuyển được yêu cầu VIA nhận BM!");
						return false;
					}
				}

				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private string RemoveSpecialChars(string input)
		{
			return Regex.Replace(input, @"[^0-9a-zA-Z]", string.Empty);
		}

		private async Task<bool> putMakeCardFirebaseAsync(string code, string data)
		{
			try
			{
				FireBaseMessage message = new FireBaseMessage();
				int Timestamp = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
				message.Id = Timestamp.ToString();
				message.Data = data;

				await firebase.Child("make_primary/" + RemoveSpecialChars(root_email) + code).Child(message.Id).PutAsync(message);
				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool delayStepAsync(string _threadName, string stepStr)
		{
			try
			{
				int waitTime = 15;
				string waitTimeStr = Regex.Match(stepStr, "wait_time_(.*?)#").Groups[1].Value;
				if (!string.IsNullOrEmpty(waitTimeStr))
				{
					Int32.TryParse(waitTimeStr, out waitTime);
				}
				this.logWithThread(_threadName, "Tool tạm dừng " + waitTime.ToString() + " giây...");

				Delay(waitTime * 1000);
				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool toMakePrimaryCenterStepAsync(RegClone _reg, string _threadName, string[] stepData, string stepStr)
		{
			try
			{
				_reg.switchBusinessTabHandle();

				string codeStr = this.textBox3.Text.Trim();
				if (string.IsNullOrEmpty(codeStr))
				{
					this.logWithThread(_threadName, "Chưa nhập mã make!");
					return false;
				}

				string bm_credential_id = "";
				foreach (Card billPm in _reg._bm_cards)
				{
					if (stepData.Contains(billPm.code) && (billPm.bm_id == _reg._bm_id))
					{
						bm_credential_id = billPm.credential_id;
						break;
					}
				}

                if (string.IsNullOrEmpty(bm_credential_id))
                {
					this.logWithThread(_threadName, "Không thấy thẻ cần make!");
					return false;
				}
				string data = _reg._cookie + "|" + _reg._bm_id + "|" + _reg._bm_ads_id + "|" + bm_credential_id;
				bool autoReceive = Task.Run(async () => await putMakeCardFirebaseAsync(codeStr, data)).GetAwaiter().GetResult();
				if (autoReceive)
				{
					this.logWithThread(_threadName, "Đã chuyển yêu cầu make!");
					// check card
					int waitTime = 15;
					string waitTimeStr = Regex.Match(stepStr, "wait_time_(.*?)#").Groups[1].Value;
					if (!string.IsNullOrEmpty(waitTimeStr))
					{
						Int32.TryParse(waitTimeStr, out waitTime);
					}

					if (checkCardExist(_reg._bm_ads_id, waitTime))
					{
						this.logWithThread(_threadName, "Đã có thẻ!");
					}
					else
					{
						this.logWithThread(_threadName, "Không thấy thẻ!!");
						return false;
					}
				}
				else
				{
					this.logWithThread(_threadName, "Không chuyển được yêu cầu!");
					return false;
				}

				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		public bool checkCardExist(string adId, int waitTime = 8)
		{
			try
			{
				bool check = false;
				BillingAMNexusRootQuery pmData = _reg.getAdPaymentInfo(adId);
				if ((pmData != null) && (pmData.data != null) && (pmData.data.billable_account_by_payment_account != null))
				{
					if ((pmData.data.billable_account_by_payment_account.billing_payment_account != null)
						&& (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods != null)
						&& (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods.Count > 0))
					{
						foreach (BillingPaymentMethod bpmItem in pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods)
						{
							if (bpmItem.credential != null)
							{
								this.log("Thẻ: " + bpmItem.credential.card_association_name + "-" + bpmItem.credential.last_four_digits);
								check = true;
							}
						}
					}
				}

				int ChkIndex = 1;
				while (!check && (ChkIndex <= waitTime))
				{
					Delay(5000);
					this.log("Check thẻ Lần: " + ChkIndex.ToString());
					pmData = _reg.getAdPaymentInfo(adId);
					if ((pmData != null) && (pmData.data != null) && (pmData.data.billable_account_by_payment_account != null))
					{
						if ((pmData.data.billable_account_by_payment_account.billing_payment_account != null)
							&& (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods != null)
							&& (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods.Count > 0))
						{
							foreach (BillingPaymentMethod bpmItem in pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods)
							{
								if (bpmItem.credential != null)
								{
									this.log("Thẻ: " + bpmItem.credential.card_association_name + "-" + bpmItem.credential.last_four_digits);
									check = true;
								}
							}
						}
					}
					ChkIndex++;
				}

				return check;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool checkCardNotExist(string adId, int waitTime = 8)
		{
			try
			{
				bool check = false;
				BillingAMNexusRootQuery pmData = _reg.getAdPaymentInfo(adId);
				if ((pmData == null) || (pmData.data == null) || (pmData.data.billable_account_by_payment_account == null)
					|| (pmData.data.billable_account_by_payment_account.billing_payment_account == null)
					|| (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods == null)
					|| (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods.Count == 0))
				{
					check = true;
				}

				int ChkIndex = 1;
				while (!check && (ChkIndex <= waitTime))
				{
					Delay(5000);
					this.log("Check thẻ Lần: " + ChkIndex.ToString());
					pmData = _reg.getAdPaymentInfo(adId);
					if ((pmData == null) || (pmData.data == null) || (pmData.data.billable_account_by_payment_account == null)
										|| (pmData.data.billable_account_by_payment_account.billing_payment_account == null)
										|| (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods == null)
										|| (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods.Count == 0))
					{
						check = true;
					}
					ChkIndex++;
				}

				return check;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool checkCampErrorOnSite(string adId, int waitTime = 18)
		{
			try
			{
				bool check = this.autoRequest.checkDataCheckExist(adId);
				int ChkIndex = 1;
				while (!check && (ChkIndex <= waitTime))
				{
					Delay(6000);
					this.log("Check Error Lần: " + ChkIndex.ToString());
					check = this.autoRequest.checkDataCheckExist(adId);
					ChkIndex++;
				}

				return check;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool checkCampExist(string adId, int waitTime = 8)
		{
			try
			{
				bool check = _reg.CheckHasCamp(adId);
				int ChkIndex = 1;
				while (!check && (ChkIndex <= waitTime))
				{
					Delay(5000);
					this.log("Check thẻ Lần: " + ChkIndex.ToString());
					check = _reg.CheckHasCamp(adId);
					ChkIndex++;
				}

				return check;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		private bool createPageStepAsync(RegClone _reg, string _threadName, string[] stepData, NLParam paramData)
		{
			try
			{
				bool createPage = _reg.CreateBmPage(paramData.page_name, stepData.Contains("pro5"));
				return createPage;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool sharePageStepAsync(RegClone _reg, string _threadName, string[] stepData, NLParam paramData)
		{
			try
			{
				// Get BM
				string bmcode = paramData.bm_page;
				if (string.IsNullOrEmpty(bmcode))
				{
					this.logWithThread(_threadName, "Không có BM code!");
					return false;
				}

				// Lay Bm
				NLBm bm = this.autoRequest.getBmDetail(bmcode);
				if ((bm == null) || (string.IsNullOrEmpty(bm.bm_id)))
				{
					this.log("BM data id empty!");
					return false;
				}

				bool share = _reg.ShareBmPageToOtherBm(bm.bm_id);
                if (share)
                {
					if (stepData.Contains("set_role"))
					{
						this.log("Set Quyền Page: ...");
                        if (!string.IsNullOrEmpty(paramData.user_page))
                        {
							string type = "page";
                            if (_reg.CheckPro5Page(_reg.pageID))
                            {
								type = "pro5";
							}
							string data = this.autoRequest.setPageRole(bm.id.ToString(), _reg.pageID, type, paramData.user_page);
							string successes = Regex.Match(data, "\\\\\"successes\\\\\":{(.*?)}").Groups[1].Value;
							if (string.IsNullOrEmpty(successes))
							{
								this.log(data);
								this.log("Set Quyền Page: FALSE!");
								share = false;
								logWithThread(_threadName, "Lấy link BM...");
								BmLinkResBody bmLink = this.autoRequest.getBmLink(bmcode);
								if (bmLink.status)
								{
									if ((bmLink.data != null) && string.IsNullOrEmpty(bmPageLink))
									{
										this.logWithThread(_threadName, "Link: " + bmLink.data.link);
										bmPageLink = bmLink.data.link;
									}
								}
								else
								{
									this.logWithThread(_threadName, "Không lấy được BM link từ nguyenlieu.site!");
								}
							}
                            else
                            {
								this.log("Set Quyền Page: TRUE!");
							}
                        }
                        else
                        {
							this.log("Không có tham số user_page!");
							this.log("Set Quyền Page: FALSE!");
						}
					}
				}

				return share;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool RecBmPageFromBmStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				string page_id = this.textBox22.Text.Trim();
                if (string.IsNullOrEmpty(page_id))
                {
					this.logWithThread(_threadName, "Không có Page ID!");
				}
				string bmrec_id = this.textBox21.Text.Trim();
				if (string.IsNullOrEmpty(bmrec_id))
				{
					this.logWithThread(_threadName, "Không có BM ID!");
				}
				string bmrec_tk = this.textBox20.Text.Trim();
				if (string.IsNullOrEmpty(bmrec_tk))
				{
					this.logWithThread(_threadName, "Không có BM Token!");
				}

				return _reg.RecBmPageFromBm(page_id, bmrec_id, bmrec_tk);
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool waitBillUpStepAsync(RegClone _reg, string _threadName, string stepStr, string[] stepData, string type)
		{
			try
			{
				int waitTime = 10;
				string waitTimeStr = Regex.Match(stepStr, "wait_time_(.*?)#").Groups[1].Value;
                if (!string.IsNullOrEmpty(waitTimeStr))
                {
					Int32.TryParse(waitTimeStr, out waitTime);
				}

				string minStr = Regex.Match(stepStr, "min_(.*?)#").Groups[1].Value;
				if (string.IsNullOrEmpty(minStr))
				{
					minStr = "2";
				}

				return _reg.waitBillUp(waitTime, minStr, type);
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool shareNolimitAdIntoOtherBmStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
                if (_reg.adAccount == null)
                {
					this.logWithThread(_threadName, "Không có TK!");
					return false;
				}

				if (_reg.adAccount.adtrust_dsl > 0)
				{
					this.logWithThread(_threadName, "TK Limit: " + _reg.adAccount.adtrust_dsl.ToString());
					return false;
				}

				this.logWithThread(_threadName, "TK NOLIMIT!");
				this.logWithThread(_threadName, "Share...");

				// Get BM
				string bmcode = this.textBox25.Text.Trim();
				if (string.IsNullOrEmpty(bmcode))
				{
					this.logWithThread(_threadName, "Không có BM code!");
					return false;
				}

				// Lay Bm
				NLBm bm = this.autoRequest.getBmDetail(bmcode);
				if ((bm == null) || (string.IsNullOrEmpty(bm.bm_id)))
				{
					this.log("BM data id empty!");
					return false;
				}

				return _reg.shareAdsToBmByAPI(bmcode, bm);
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool shareAdIntoOtherBmStepAsync(RegClone _reg, string _threadName, string[] stepData, NLParam paramData)
		{
			try
			{
				// Get BM
				string bmcode = paramData.bm_ads;

				if (string.IsNullOrEmpty(bmcode))
				{
					this.logWithThread(_threadName, "Không có BM code!");
					return false;
				}

				// Lay Bm
				NLBm bm = this.autoRequest.getBmDetail(bmcode);
				if ((bm == null) || (string.IsNullOrEmpty(bm.bm_id)))
				{
					this.log("BM data id empty!");
					return false;
				}

				bool share = _reg.shareAdsToBmByAPI(bmcode, bm);
				if (share && stepData.Contains("via_set_role"))
				{
					string viaInputStr = this.textBox9.Text.Trim();
					string[] viaInputData = viaInputStr.Split(new char[] { '|' });
					string viaid = viaInputData[0];
					if (!string.IsNullOrEmpty(viaid))
					{
						Task.Run(async () => await putFirebaseAsync(viaid, 6, _reg._ads_id, bm.bm_id)).GetAwaiter().GetResult();
					}
				}

				if (!share)
				{
					if (_reg.shareToBmErrCode == "change_bm_link")
					{
						// Set bmlink
						logWithThread(_threadName, "Lấy link BM...");
						if (!string.IsNullOrEmpty(bmcode))
						{
							BmLinkResBody bmLink = this.autoRequest.getBmLink(bmcode);
							if (bmLink.status)
							{
								if ((bmLink.data!= null) && string.IsNullOrEmpty(bmAdsLink))
								{
									this.logWithThread(_threadName, "Link: " + bmLink.data.link);
									bmAdsLink = bmLink.data.link;
								}
							}
                            else
                            {
								this.logWithThread(_threadName, "Không lấy được BM link từ nguyenlieu.site!");
							}
                        }
                        else
                        {
							this.logWithThread(_threadName, "KHông cấu hình BM code!");
						}
					}
				}

				return share;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool checkCardStepAsync(RegClone _reg, string adId, string _threadName, string[] stepData)
		{
			try
			{
				BillingAMNexusRootQuery pmData = _reg.getAdPaymentInfo(adId);
                if ((pmData == null) || (pmData.data == null) || (pmData.data.billable_account_by_payment_account == null))
                {
					this.logWithThread(_threadName, "Không thấy thẻ!");
					return false;
				}

				bool verified = false;
				if ((pmData.data.billable_account_by_payment_account.billing_payment_account != null) 
					&& (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods != null)
					&& (pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods.Count > 0))
				{
					foreach (BillingPaymentMethod bpmItem in pmData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods)
					{
                        if (bpmItem.is_primary)
                        {
                            if (bpmItem.credential != null)
                            {
								this.logWithThread(_threadName, bpmItem.credential.card_association_name + "-" + bpmItem.credential.last_four_digits);
							}

                            if (bpmItem.usability == "UNVERIFIABLE")
                            {
								verified = true;
								break;
							}
                        }
					}
				}

                if (verified)
                {
					this.logWithThread(_threadName, "Cannot be verified!");
					return false;
				}

				if (pmData.data.billable_account_by_payment_account.is_reauth_restricted)
                {
					this.logWithThread(_threadName, "Insufficient funds!");
					return false;
				}

				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool addFriendStepAsync(RegClone _reg, string viaInputStr, string _threadName, string[] stepData)
		{
			try
			{
				bool addfriend = false;
				string[] viaInputData = viaInputStr.Split(new char[] { '|' });
				string viaid = viaInputData[0];

				this.logWithThread(_threadName, "Check Friend!");
				bool checkFriend = _reg.CheckFiend(viaid);
                if (checkFriend)
                {
					this.logWithThread(_threadName, "Đã là bạn rồi!");
					return true;
				}

				if (!string.IsNullOrEmpty(viaid))
				{
					addfriend = _reg.addFriend(viaid);
					if (addfriend)
					{
						addfriend = Task.Run(async () => await putFirebaseAsync(viaid, 4, _reg._clone_uid)).GetAwaiter().GetResult();
					}
				}

                if (addfriend)
                {
					bool checkf = false;
					int ChkfIndex = 1;
					while (!checkf && (ChkfIndex <= 5))
					{
						checkf = _reg.CheckFiend(viaid);
						if (!checkf)
						{
							this.logWithThread(_threadName, "Chưa là bạn, đợi chút!");
							Delay(6000);
							ChkfIndex++;
						}
					}

					addfriend = checkf;
				}

				return addfriend;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool agreeFriendStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				string viaInputStr = this.textBox9.Text.Trim();
				if (string.IsNullOrEmpty(viaInputStr))
				{
					this.logWithThread(_threadName, "Not VIA input!");
					return false;
				}
				string[] viaInputData = viaInputStr.Split(new char[] { '|' });
				string viaid = viaInputData[0];
				return _reg.agreeFriends(viaid);
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool viaAddFriendStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				string viaInputStr = this.textBox9.Text.Trim();
				if (string.IsNullOrEmpty(viaInputStr))
				{
					this.logWithThread(_threadName, "Not VIA input!");
					return false;
				}
				string[] viaInputData = viaInputStr.Split(new char[] { '|' });
				string viaid = viaInputData[0];
				return Task.Run(async () => await putFirebaseAsync(viaid, 3, _reg._clone_uid)).GetAwaiter().GetResult();
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool viaRemoveCardsStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				string viaInputStr = this.textBox9.Text.Trim();
				if (string.IsNullOrEmpty(viaInputStr))
				{
					this.logWithThread(_threadName, "Not VIA input!");
					return false;
				}
				string[] viaInputData = viaInputStr.Split(new char[] { '|' });
				string viaid = viaInputData[0];
				bool ss = Task.Run(async () => await putFirebaseAsync(viaid, 2, _reg._bm_ads_id)).GetAwaiter().GetResult();

                // Check error
                if (ss)
                {
                    if (checkCampErrorOnSite(_reg._bm_ads_id))
                    {
                        this.logWithThread(_threadName, "Camp Đã Error!");
                    }
                    else
                    {
                        this.logWithThread(_threadName, "Camp Không Error!!");
                        return false;
                    }
                }

                return ss;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool shareNolimitAdIntoVIAStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				if (_reg.adAccount == null)
				{
					this.logWithThread(_threadName, "Không có TK!");
					return false;
				}

				if (_reg.adAccount.adtrust_dsl > 0)
				{
					this.logWithThread(_threadName, "TK Limit: " + _reg.adAccount.adtrust_dsl.ToString());
					return false;
				}

				this.logWithThread(_threadName, "TK NOLIMIT!");
				this.logWithThread(_threadName, "Share...");

				bool share = false;
				string viaInputStr = this.textBox26.Text.Trim();
				if (this.addFriendStepAsync(_reg, viaInputStr, _threadName, stepData))
				{
					string[] viaInputData = viaInputStr.Split(new char[] { '|' });
					string viaid = viaInputData[0];
					share = _reg.shareAdsToVia(viaid, false);
				}

				return share;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool shareAdIntoVIAStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				bool share = false;
				string viaInputStr = this.textBox9.Text.Trim();
				if (this.addFriendStepAsync(_reg, viaInputStr, _threadName, stepData))
                {
					string[] viaInputData = viaInputStr.Split(new char[] { '|' });
					string viaid = viaInputData[0];

					bool wait = false;
					if (stepData.Contains("wait"))
					{
						wait = true;
					}

					share = _reg.shareAdsToVia(viaid, wait);
				}

				return share;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool shareBmAdShareOtherBmStepAsync(RegClone _reg, string _threadName, string[] stepData, NLParam paramData)
		{
			try
			{
				// Get BM
				string bmcode = paramData.bm_ads;
				if (string.IsNullOrEmpty(bmcode))
				{
					this.logWithThread(_threadName, "Không có BM code!");
					return false;
				}

				// Lay Bm
				NLBm bm = this.autoRequest.getBmDetail(bmcode);
				if ((bm == null) || (string.IsNullOrEmpty(bm.bm_id)))
				{
					this.log("BM data id empty!");
					return false;
				}

				return _reg.ShareBmAd(bm.bm_id);
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool bmRemoveCardStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				if (stepData.Contains("all"))
				{
					return _reg.bmRemoveAllCardAsync();
                }
                else
                {
					return _reg.bmRemoveCardAsync();
				}
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool updateCamp(NLCamp campData)
		{
			try
			{
				string campFolderPath = _dataPath + "\\camp2.0";
				string campItemFolderPath = campFolderPath + "\\" + campData.id;
				string info_file = campItemFolderPath + "\\info.txt";

				string json = JsonConvert.SerializeObject(campData);
				File.WriteAllText(info_file, json);

				// Dowload file
				if ((campData.temp_file_data != null) && (!string.IsNullOrEmpty(campData.temp_file_data.name)))
				{
					string[] filenameSplit = campData.temp_file_data.name.Split('.');
					string tempfilename = filenameSplit[1] + "." + filenameSplit[2];
					string tempfile = campItemFolderPath + "\\" + tempfilename;
					using (var client = new WebClient())
					{
						client.DownloadFile(campData.temp_file_data.url, tempfile);
						if (File.Exists(tempfile))
						{
							string _json = File.ReadAllText(tempfile);
							PeCampParam bpCampParam = JsonConvert.DeserializeObject<PeCampParam>(_json);
                            if (!string.IsNullOrEmpty(bpCampParam.thumb_url))
                            {
								tempfile = campItemFolderPath + "\\thumb.jpg";
								client.DownloadFile(bpCampParam.thumb_url, tempfile);
							}
						}
					}
				}

				if ((campData.mockup_data != null) && (!string.IsNullOrEmpty(campData.mockup_data.name)))
				{
					string[] filenameSplit = campData.mockup_data.name.Split('.');
					string tempfilename = filenameSplit[1] + "." + filenameSplit[2];
					string mockupfile = campItemFolderPath + "\\" + tempfilename;
					using (var client = new WebClient())
					{
						client.DownloadFile(campData.mockup_data.url, mockupfile);
					}
				}
				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool checkCamp(string campId)
		{
			try
			{
				NLCamp campData = this.autoRequest.getCamp(campId);
				if (campData == null)
				{
					log("Không lấy được camp " + campId);
					return false;
				}

				string campFolderPath = _dataPath + "\\camp2.0";
				string campItemFolderPath = campFolderPath + "\\" + campData.id;
				string info_file = campItemFolderPath + "\\info.txt";
				if (!File.Exists(info_file))
				{
					log("Không thấy camp trên hệ thống, hãy load lại camp!");
					return false;
				}

				string text = File.ReadAllText(info_file);
				if (string.IsNullOrEmpty(text))
				{
					log("Không thấy thông tin Camp, hãy load lại camp!");
					return false;
				}
				JObject jobject = JObject.Parse(text);
				string oldVerSion = "0";
				if (jobject.ContainsKey("version"))
				{
					oldVerSion = jobject["version"].ToString();
				}

                if (oldVerSion != campData.version)
                {
					log("Update phiên bản Camp mới...");
                    if (updateCamp(campData))
                    {
						log("Hoàn thành Update Camp!");
                    }
                    else
                    {
						log("Không update đc Camp, hãy load lại camp!");
						return false;
					}
				}

				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool createPostStepAsync(RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				string campId = TextBoxUtils.Instance.getComboBoxSelect(this, comboBox3);
				if (string.IsNullOrEmpty(campId))
				{
					log("CHƯA CHỌN CAMP!");
					return false;
				}

				if (!checkCamp(campId))
				{
					return false;
				}

				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir + "\\camp2.0\\" + campId + "\\";

				string jsonfile = dataPath + @"pe_camp_temp.json";
				if (!File.Exists(jsonfile))
				{
					jsonfile = dataPath + @"bp_camp_temp.json";
				}

				if (!File.Exists(jsonfile))
				{
					this.log("Not camp temp file!");
					return false;
				}
				string _json = File.ReadAllText(jsonfile);
				PeCampParam bpCampParam = JsonConvert.DeserializeObject<PeCampParam>(_json);
				string postMessage = bpCampParam.message;

				string imgPath = dataPath + @"img_temp.jpg";
				if (!File.Exists(imgPath))
				{
					this.log("Không thấy file img_temp.jpg!");
					return false;
				}

                if (stepData.Contains("video"))
                {
					string file_url = bpCampParam.video_s_url;
					string title = bpCampParam.title;
					string message = postMessage;
					if (string.IsNullOrEmpty(file_url))
					{
						this.log("Not video file url!");
						return false;
					}

					string thumbPath = dataPath + @"thumb.jpg";
					if (!File.Exists(thumbPath))
					{
						this.log("Không thấy file thumb!");
						return false;
					}
					return _reg.TKBMVideoPost(file_url, title, message, thumbPath);
				}
                else
                {
					return _reg.CreatePostAPI(imgPath, postMessage);
				}
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool peCampStepAsync(string type, RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				// Check camp
				string campId = TextBoxUtils.Instance.getComboBoxSelect(this, comboBox3);
				if (string.IsNullOrEmpty(campId))
				{
					log("CHƯA CHỌN CAMP!");
					return false;
				}

				if (!checkCamp(campId))
                {
					return false;
				}

				// Check pixel
				string[] pxInputData2 = this.GetPixelConfig();
				if (pxInputData2.Length < 3)
				{
					logWithThread(_threadName, "Không lấy đc pixel, hãy thực hiện lại!...");
					return false;
				}

				bool offAds = false;
				if (stepData.Contains("off_ads"))
                {
					offAds = true;
				}

				if (type == "TKCN")
                {
					return _reg.TKCNPeLaunchingCamp(campId, pxInputData2[0], offAds);
                }
                else
                {
					return _reg.TKBMPeLaunchingCamp(campId, pxInputData2[0], offAds);
				}
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool bpCampStepAsync(string type, RegClone _reg, string _threadName, string[] stepData)
		{
			try
			{
				string campId = TextBoxUtils.Instance.getComboBoxSelect(this, comboBox3);
				if (string.IsNullOrEmpty(campId))
				{
					log("CHƯA CHỌN CAMP!");
					return false;
				}

				if (!checkCamp(campId))
				{
					return false;
				}

				// Check pixel
				string[] pxInputData2 = this.GetPixelConfig();
				if (pxInputData2.Length < 3)
				{
					logWithThread(_threadName, "Không lấy đc pixel, hãy thực hiện lại!...");
					return false;
				}

				return _reg.BoostPostlaunchingCamp(type, campId, pxInputData2[0], stepData.Contains("create_post"), stepData.Contains("video"));
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private bool setParamsStepAsync(string paramId, string _threadName)
		{
			try
			{
				paramData = this.autoRequest.getParam(paramId);
				if (paramData == null)
				{
					logWithThread(_threadName, "Không lấy được tham số " + paramId);
					return false;
				}

				if (!string.IsNullOrEmpty(paramData.tkcn_tz))
				{
					paramData.tkcn_tz = "%22" + Uri.EscapeDataString(paramData.tkcn_tz) + "%22";
				}

				if (!string.IsNullOrEmpty(paramData.tkbm_tz))
				{
					paramData.tkbm_tz = "%22" + Uri.EscapeDataString(paramData.tkbm_tz) + "%22";
				}

                if (string.IsNullOrEmpty(paramData.tkcn_card_qg_code) && !string.IsNullOrEmpty(paramData.tkcn_qg_code))
                {
					paramData.tkcn_card_qg_code = paramData.tkcn_qg_code;
				}

				if (string.IsNullOrEmpty(paramData.tkbm_card_qg_code) && !string.IsNullOrEmpty(paramData.tkbm_qg_code))
				{
					paramData.tkbm_card_qg_code = paramData.tkbm_qg_code;
				}
				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private void removeClone(string _threadName)
		{
			try
			{
				this.logWithThread(_threadName, "Remove clone ...");
				this.getClone(true);
				this.logWithThread(_threadName, "Remove clone: OK");
			}
			catch (Exception error)
			{
				this.log(error.Message);
				this.logWithThread(_threadName, "Remove clone: NOT OK");
			}
		}

		private Task RegProcess(int num)
        {
			try
			{
				string _threadName = num.ToString().Clone().ToString();
                if (_clone_rep > _clone_max_rep)
                {
					logWithThread(_threadName, "Clone " + _clone_id + "Đã chạy " + _clone_max_rep + " lần!");
					string crClonerc = getClone(false);
					string[] cloneData = crClonerc.Split(new char[] { '|' });
					if (cloneData[0] == _clone_id)
					{
						logWithThread(_threadName, "Xóa clone ...");
						getClone(true);
					}
					_clone_id = "";
					_pre_clone_id = "";
					return Task.CompletedTask;
				}

				if (_reg == null)
				{
					return Task.CompletedTask;
				}
				
                // Flow
                if (!string.IsNullOrEmpty(_crFlow))
                {
					logWithThread(_threadName,"FLOW: " + _crFlow);
				}

				string[] flowData = getFlow();
				string nextStep = "";
				_reg._listShareBmToVIA = new List<ShareLog>() { };

				if (flowData != null && flowData.Length > 0)
				{
					// Lay tham so
					string paramId = TextBoxUtils.Instance.getComboBoxSelect(this, comboBox2);
					if (string.IsNullOrEmpty(paramId))
					{
						log("KHÔNG CÓ THAM SỐ CẤU HÌNH!");
						return Task.CompletedTask;
					}

					setParamsStepAsync(paramId, _threadName);

					int stepIndex = 1;
					bool first = true;
					foreach (string stepStr in flowData)
					{
                        if (!string.IsNullOrEmpty(_reg._clone_uid))
                        {
                            if (first)
                            {
								first = false;
								_clone_id = _reg._clone_uid;
								if (!string.IsNullOrEmpty(_clone_id))
								{
									if (_clone_id == _pre_clone_id)
									{
										_clone_rep++;
									}
									else
									{
										_pre_clone_id = _clone_id;
										_clone_rep = 1;
									}
								}
								else
								{
									_clone_rep = 1;
								}
							}
						}
						SetTurnStatus();

						if (!string.IsNullOrEmpty(stepStr))
						{
							string[] stepData = stepStr.Split(new char[] { ':' });
							if (stepData.Length > 0)
							{
								string stepCode = stepData[0];
								bool run = true;
                                if (!string.IsNullOrEmpty(nextStep))
                                {
                                    if (stepCode == nextStep)
                                    {
										run = true;
										nextStep = "";
                                    }
                                    else
                                    {
										run = false;
									}
                                }

                                if (run)
                                {
									int runIndex = 1;
									int runMaxIndex = 2;
									bool showIndex = false;
									if (Regex.Matches(stepStr, "false_repeat").Count > 0)
									{
										runMaxIndex = runIndex + Regex.Matches(stepStr, "false_repeat").Count;
										showIndex = true;
									}

									logWithThread(_threadName, "Step: " + stepIndex.ToString() + " - " + stepCode);

									chay_lai_tu_day:
									if (showIndex)
									{
										logWithThread(_threadName, "LẦN: " + runIndex);
									}
									bool resProcess = false;

									string cnpjcode = "";
									if (stepData.Contains("cnpjcode"))
									{
										string cnpjCodeFile = "GenCode\\cnpjcode.txt";
										if (File.Exists(cnpjCodeFile))
										{
											string[] cnpjdata = File.ReadAllLines(cnpjCodeFile);
											List<string> listCnpjCode = new List<string>(cnpjdata);
											if (listCnpjCode.Count > 0)
											{
												cnpjcode = listCnpjCode[new Random(Guid.NewGuid().GetHashCode()).Next(0, listCnpjCode.Count)];
											}
										}
										logWithThread(_threadName, "cnpjcode: " + cnpjcode);
										cnpjcode = Regex.Replace(cnpjcode, @"[^0-9a-zA-Z]+", "");
									}

									switch (stepCode)
									{
										case "hma_change":
											resProcess = changeHmaProxy();
											break;
										case "set_param":
											paramId = Regex.Match(stepStr, "param_(.*?)#").Groups[1].Value;
											resProcess = setParamsStepAsync(paramId, _threadName);
											break;
										case "reset_chrome":
											resProcess = _reg.reset_chrome();
											break;
										case "gotoUrl":
											string url = "";
											if (stepData.Length > 1)
											{
												foreach (Match item in Regex.Matches(stepStr, @"(http|ftp|https):\/\/([\w\-_]+(?:(?:\.[\w\-_]+)+))([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?"))
												{
													url = item.Value;
												}
											}
											resProcess = _reg.gotoUrl(url);
											break;
										case "login_clone_moi":
											_reg._clone_data = getCloneMoi(num);
											resProcess = _reg.Login2Fa(_reg._clone_data);
											break;
										case "fb_reg":
											resProcess = regStepAsync(_reg, _threadName, stepData);
											break;
										case "get_clone":
											resProcess = getCloneStep(_threadName, stepData);
											break;
										case "rm_clone":
											getClone(true);
											resProcess = true;
											break;
										case "login":
											resProcess = loginStepAsync(_reg, _threadName, stepData);
											break;
										case "change_language":
											resProcess = _reg.changeLang();
											break;
										case "get_birthday":
											string birthday = _reg.getBirthDay();
											resProcess = true;
											break;
										case "logout_all_sessions":
											resProcess = _reg.logoutAllSessions();
											break;
										case "allow_all_cookie":
											resProcess = _reg.apiAllowAllCookie();
											break;
										case "clone_email_verify":
											string source = "mail.tm";
											if (stepData.Contains("mail.gw"))
											{
												source = "mail.gw";
											}
											if (stepData.Contains("10minutemail.net"))
											{
												source = "10minutemail.net";
											}
											resProcess = _reg.apiCloneEmailVerify(source);
											break;
										case "set_security":
											resProcess = _reg.SetSecurity();
											break;
										case "clone_page_create":
											resProcess = createClonePageStepAsync(_reg, _threadName, stepData, paramData);
											break;
										case "clone_page_edit_info":
											resProcess = editClonePageStepAsync(_reg, _threadName, stepData);
											break;
										case "clone_page_change_avata":
											resProcess = _reg.changePageAvata();
											break;
										case "clone_page_share_with_other_BM":
											resProcess = cloneSharePageStepAsync(_reg, _threadName, stepData, paramData);
											if (!resProcess)
											{
												if (!string.IsNullOrEmpty(bmPageLink))
												{
													string crClone2 = getClone(false);
													if (crClone2 == _reg._clone_data)
													{
														logWithThread(_threadName, "Xóa clone ...");
														getClone(true);
													}
													return Task.CompletedTask;
												}
											}
											break;
										case "Clone_ad_share_with_other_BM":
											resProcess = shareAdIntoOtherBmStepAsync(_reg, _threadName, stepData, paramData);
											if (!resProcess)
											{
												if (!string.IsNullOrEmpty(bmAdsLink))
												{
													string crClone2 = getClone(false);
													if (crClone2 == _reg._clone_data)
													{
														logWithThread(_threadName, "Xóa clone ...");
														getClone(true);
													}
													return Task.CompletedTask;
												}
											}
											break;
										case "clone_ad_deactive":
											resProcess = _reg.cloneAdDeactive();
											break;
										case "Shopify_reg":
											resProcess = _reg.regShopify();
											if (resProcess)
											{
												setShopify(_reg.shopifyRegData);
											}
											break;
										case "Clone_comet_profile_plus":
											resProcess = _reg.apiCometProfilePlus();
											break;
										case "BM_create":
											resProcess = createBmStepAsync(_reg, _threadName, stepData);
											break;
										case "BM_create_chienbn":
											resProcess = createChienbnBmStepAsync(_reg, _threadName, stepData);
                                            if (!resProcess)
                                            {
												return Task.CompletedTask;
											}
											break;
										case "BM_email_verify":
											resProcess = _reg.apiBMEmailVerify();
											break;
										case "Whatsapp_page_disconect":
											resProcess = _reg.WhatsAppPageDisconect();
											break;
										case "Whatsapp_Bm_disconect":
											string whatsapp = this.textBox17.Text.Trim();
											resProcess = _reg.WhatsAppBmDisconect(whatsapp);
											break;
										case "BM_gen_receive_link":
											resProcess = genBmReceiveLinkStepAsync(_reg, _threadName, stepData, stepStr);
											break;
										case "BM_remove_set":
											resProcess = _reg.setRemoveBm();
											break;
										case "BM_remove":
											resProcess = _reg.removeBm();
											break;
										case "BM1_set":
											resProcess = _reg.setBm1Var();
											break;
										case "BM_switch_BM1":
											resProcess = _reg.switchBm1();
											break;
										case "BM2_set":
											resProcess = _reg.setBm2Var();
											break;
										case "BM_switch_BM2":
											resProcess = _reg.switchBm2();
											break;
										case "BM_create_page":
											resProcess = createPageStepAsync(_reg, _threadName, stepData, paramData);
											break;

										case "BM_add_owned_page":
											resProcess = _reg.BmAddOwnedPage();
											break;
										case "BM_page_delete_user":
											resProcess = _reg.BmPageDeleteUser();
											break;
										case "Page_set_country":
											resProcess = _reg.PageSetCountry();
											break;
										case "BM_rec_Bm_page":
											resProcess = RecBmPageFromBmStepAsync(_reg, _threadName, stepData);
											break;
										case "receive_profile_page":
											resProcess = reciveProfilePageStepAsync(_reg, _threadName, stepData);
											break;
										case "Clone_receive_bm_page":
											resProcess = reciveShareBmPageStep(_reg, _threadName, stepData, paramData);
											if (resProcess)
											{
												logWithThread(_threadName, stepCode + ": OK");
												logWithThread(_threadName, "Kết thúc lượt chạy!");
												return Task.CompletedTask;
											}
											break;
										case "Clone_receive_bm_ads":
											resProcess = reciveShareBmAdsStep(_reg, _threadName, stepData, paramData);
											if (resProcess)
											{
												logWithThread(_threadName, stepCode + ": OK");
												logWithThread(_threadName, "Kết thúc lượt chạy!");
												return Task.CompletedTask;
											}
											break;
										case "receive_bm":
											resProcess = reciveBmStepAsync(_reg, _threadName, stepData);
											break;
										case "receive_current_bm":
											resProcess = recievedCurrentBMStepAsync(_reg, _threadName, stepData);
											break;
										case "receive_nguyen_lieu_bm":
											resProcess = reciveNguyenLieuBmStepAsync(_reg, _threadName, stepData, paramData);
											break;
										case "BM_page_edit_info":
											resProcess = editBmPageStepAsync(_reg, _threadName, stepData);
											break;
										case "BM_share_page":
											resProcess = sharePageStepAsync(_reg, _threadName, stepData, paramData);
											if (!resProcess)
											{
												if (stepData.Contains("set_role"))
                                                {
													if (!string.IsNullOrEmpty(bmPageLink))
													{
														string crClone5 = getClone(false);
														if (crClone5 == _reg._clone_data)
														{
															logWithThread(_threadName, "Xóa clone ...");
															getClone(true);
														}
														return Task.CompletedTask;
													}
												}
											}
											break;

										case "BM_page_remove":
											resProcess = _reg.BmPageRemove();
											break;

										case "TKCN_share_with_BM":
											resProcess = _reg.ShareAdToSelf();
											break;
										case "BM_set_ad_role":
											resProcess = setRoleAdStepAsync(_reg, _threadName, stepData, "ad");
											break;
										case "BM_ad_create":
											resProcess = _reg.CreateBmAd();
											break;

										case "BM_set_BMad_role":
											resProcess = setRoleAdStepAsync(_reg, _threadName, stepData, "BMad");
											break;

										case "BM_set_BMad_role_other":
											resProcess = _reg.SetRoleAdOther("BMad");
											break;

										case "TKCN_nolimit_share_BM":
											resProcess = shareNolimitAdIntoOtherBmStepAsync(_reg, _threadName, stepData);
											break;

										case "Clone_ad_share_with_VIA":
											resProcess = shareAdIntoVIAStepAsync(_reg, _threadName, stepData);
											break;
										case "TKCN_nolimit_share_VIA":
											resProcess = shareNolimitAdIntoVIAStepAsync(_reg, _threadName, stepData);
											break;

										case "Clone_add_friend":
											string viaInputStr12 = this.textBox9.Text.Trim();
											resProcess = addFriendStepAsync(_reg, viaInputStr12, _threadName, stepData);
											break;
										case "Clone_agree_friends":
											resProcess = agreeFriendStepAsync(_reg, _threadName, stepData);

											break;
										case "Via_add_friend":
											resProcess = viaAddFriendStepAsync(_reg, _threadName, stepData);
											break;

										case "Via_remove_cards":
											resProcess = viaRemoveCardsStepAsync(_reg, _threadName, stepData);
											break;

										case "Via_ad_share_with_clone":
											string viaInputStr1 = this.textBox9.Text.Trim();
											string[] viaInputData1 = viaInputStr1.Split(new char[] { '|' });
											if (viaInputData1.Length < 3)
											{
												logWithThread(_threadName, "Via Input không đúng định dạng!");
												resProcess = false;
											}
											else
											{
												resProcess = _reg.ViaShareAdToClone(viaInputData1[1], viaInputData1[2]);
											}
											break;

										case "Clone_create_new_ad":
											string viaInputStr2 = this.textBox9.Text.Trim();
											string[] viaInputData2 = viaInputStr2.Split(new char[] { '|' });
											if (viaInputData2.Length < 2)
											{
												logWithThread(_threadName, "Via Input không đúng định dạng!");
												resProcess = false;
											}
											else
											{
												resProcess = _reg.cloneCreateNewAd(paramData.tkcn_qg_code, paramData.tkcn_tt, viaInputData2[1]);
											}
											break;

										case "BM_ad_share_with_other_BM":
											resProcess = shareBmAdShareOtherBmStepAsync(_reg, _threadName, stepData, paramData);
											break;
										case "BM_add_card":
                                            if (_reg._bm_cards == null)
                                            {
												_reg._bm_cards = new List<Card>() { };
											}

											if (stepData.Contains("auto"))
											{
												string bmCountryNameInput = paramData.bm_qg_name;
												if (string.IsNullOrEmpty(bmCountryNameInput))
												{
													bmCountryNameInput = "United States of America";
												}
												resProcess = _reg.addCardToBm(bmCountryNameInput, paramData.bm_the);
                                            }
                                            else
                                            {
												resProcess = _reg.apiAddCardToBm(paramData.bm_qg_code, paramData.bm_the);
											}
											break;
										case "BM_remove_card":
											resProcess = bmRemoveCardStepAsync(_reg, _threadName, stepData);
											break;

										case "TKCN_update_info":
											resProcess = _reg.TKCN_UpdateInfo(cnpjcode);
											break;
										case "TKCN_add_card":
											resProcess = _reg.TKCN_AddCard(paramData.tkcn_qg_code, paramData.tkcn_card_qg_code, paramData.tkcn_tt, paramData.tkcn_tz, paramData.tkcn_the, cnpjcode);
											break;
										case "TKCN_set_tax":
											resProcess = _reg.setTax(_reg._ads_id, "US", "CA", "San Francisco", "3252 Boring Lane", "3252 Boring Lane", "10000");
											break;
										case "TKCN_add_card_page_ads":
											resProcess = _reg.addCardPageAds(paramData.tkcn_qg_code, paramData.tkcn_card_qg_code, paramData.tkcn_tt, paramData.tkcn_tz, paramData.tkcn_the, "Ad", cnpjcode, this.autoRequest.getStepCodes("AddCardBp"));
											break;
										case "TKCN_add_card_page_ads_suite_test":
											resProcess = _reg.addCardPageAdsSuiteTest(paramData.tkcn_qg_code, paramData.tkcn_card_qg_code, paramData.tkcn_tt, paramData.tkcn_tz, paramData.tkcn_the, "Ad", cnpjcode, this.autoRequest.getStepCodes("AddCardBp"));
											break;

										case "TKCN_set_primary_card":
											resProcess = _reg._makeChinhCard(_reg._ads_id);
											break;
										case "TKBM_set_primary_card":
											resProcess = _reg._makeChinhCard(_reg._bm_ads_id);
											break;

										case "TKCNSBM_add_card":
											resProcess = _reg.TKCNSBM_AddCard(paramData.tkcn_qg_code, paramData.tkcn_card_qg_code, paramData.tkcn_tt, paramData.tkcn_tz, paramData.tkcn_the);
											break;

										case "ad_add_banking":
											resProcess = _reg.apiAddBanking(paramData.tkcn_qg_code, paramData.tkcn_tt);
											break;
										case "TKCN_add_card_bp":
											resProcess = _reg.adAddCardBp(paramData.tkcn_qg_code, paramData.tkcn_card_qg_code, paramData.tkcn_tt, paramData.tkcn_tz, paramData.tkcn_the, this.autoRequest.getStepCodes("AddCardBp"));
											break;

										case "ad_add_card_permit":
											resProcess = _reg.adPermitCard();
											break;
										case "BM_ad_add_card_permit":
											resProcess = _reg.BmAdPermitCard();
											break;
										case "ad_update_spend":
											// Todo: set cau hinh tren site nguyenlieu
											// resProcess = _reg.adUpdateSpend(tkcn_countryInputStr);
											break;
										case "BM_ad_update_spend":
											// Todo: set cau hinh tren site nguyenlieu
											// resProcess = _reg.BMAdUpdateSpend(tkbm_countryInputStr);
											break;
										case "Clone_ad_prepaid_balance":
											resProcess = _reg.setPrepaidBalance();
											break;

										case "BM_ad_prepaid_balance":
											resProcess = _reg.setBmAdPrepaidBalance();
											break;
										case "BM_ad_make_primary_card_send_req":
											resProcess = toMakePrimaryCenterStepAsync(_reg, _threadName, stepData, stepStr);
											break;

										case "app_delay":
											resProcess = delayStepAsync(_threadName, stepStr);
											break;

										case "BM_ad_billing_make_primary_state":
											bool without911 = false;
											if (stepData.Contains("without911"))
											{
												without911 = true;
											}
											string screen = "";
											if (stepData.Contains("pe_screen"))
											{
												screen = "pe";
											}
											resProcess = _reg.BMadBillingMakePrimaryState(stepData, without911, screen);
											break;
										case "TKBM_update_info":
											resProcess = _reg.TKBM_UpdateInfo(cnpjcode);
											break;

										case "TKBM_update_country_info":
											resProcess = _reg.TKBM_UpdateCountryInfo(paramData.tkbm_qg_code, paramData.tkbm_tt, paramData.tkbm_tz);
											break;
										case "TKBM_add_card":
											string screen1 = "";
											if (stepData.Contains("pe_screen"))
											{
												screen1 = "pe";
											}
											resProcess = _reg.TKBM_AddCard(paramData.tkbm_qg_code, paramData.tkbm_card_qg_code, paramData.tkbm_tt, paramData.tkbm_tz, paramData.tkbm_the, screen1, cnpjcode);
											break;
										case "TKBM_set_tax":
											resProcess = _reg.setTax(_reg._bm_ads_id, "US", "CA", "San Francisco", "3252 Boring Lane", "3252 Boring Lane", "10000");
											break;
										case "BM_ad_check_card":
											resProcess = checkCardStepAsync(_reg, _reg._bm_ads_id, _threadName, stepData);
											break;
										case "TKBM_check_card":
											resProcess = checkCardStepAsync(_reg, _reg._bm_ads_id, _threadName, stepData);
											break;
										case "TKBM_check_hold":
											resProcess = _reg.checkAdPaymentHold(_reg._bm_ads_id);
											break;
										case "TKCN_check_hold":
											resProcess = _reg.checkAdPaymentHold(_reg._ads_id);
											break;
										case "TKCN_check_card":
											resProcess = checkCardStepAsync(_reg, _reg._ads_id, _threadName, stepData);
											break;
										case "TKBM_add_card_bp":
											resProcess = _reg.BMadAddCardBP(paramData.tkbm_qg_code, paramData.tkbm_card_qg_code, paramData.tkbm_tt, paramData.tkbm_tz, paramData.tkbm_the, this.autoRequest.getStepCodes("AddCardBp"));
											break;
										case "TKBM_add_card_page_ads":
											resProcess = _reg.addCardPageAds(paramData.tkbm_qg_code, paramData.tkbm_card_qg_code, paramData.tkbm_tt, paramData.tkbm_tz, paramData.tkbm_the, "BMad", cnpjcode, this.autoRequest.getStepCodes("AddCardBp"));
											break;

										case "TKBM_add_card_page_ads_suite_test":
											resProcess = _reg.addCardPageAdsSuiteTest(paramData.tkbm_qg_code, paramData.tkbm_card_qg_code, paramData.tkbm_tt, paramData.tkbm_tz, paramData.tkbm_the, "BMad", cnpjcode, this.autoRequest.getStepCodes("AddCardBp"));
											break;

										case "TKCN_remove_card":
											resProcess = _reg.removeCardAsync(_reg._ads_id, stepData);
											break;
										case "TKBM_remove_card":
											resProcess = _reg.removeCardAsync(_reg._bm_ads_id, stepData);
											break;
										case "ad_pixel_share":
											resProcess = _reg.adPixelShare();
											break;
										case "BM_ad_pixel_share":
											resProcess = _reg.BmAdPixelShare();
											break;
										case "create_post":
											resProcess = createPostStepAsync(_reg, _threadName, stepData);
											break;

										case "Clone_Self_cert":
											resProcess = _reg.selfCert();
											break;

										case "TKCN_pe_import_camp":
											resProcess = _reg.peLaunchingCamp();
											break;

										case "TKCN_pe_camp":
											resProcess = peCampStepAsync("TKCN", _reg, _threadName, stepData);
											break;
										case "TKCN_summary_camp":
											string[] pxInputData3 = this.GetPixelConfig();
											if (pxInputData3.Length < 3)
											{
												logWithThread(_threadName, "Không lấy đc pixel, hãy thực hiện lại!...");
												resProcess = false;
											}
											else
											{
												resProcess = _reg.TKCNSummaryLaunchingCamp(pxInputData3[0]);
											}
											break;

										case "BM_ad_pe_camp":
											resProcess = peCampStepAsync("TKBM", _reg, _threadName, stepData);
											break;
										case "BM_ad_pe_video_camp":
											string[] pxInputData4 = this.GetPixelConfig();
											if (pxInputData4.Length < 3)
											{
												logWithThread(_threadName, "Không lấy đc pixel, hãy thực hiện lại!...");
												resProcess = false;
											}
											else
											{
												resProcess = _reg.TKBMPeVideoLaunchingCamp(pxInputData4[0]);
											}
											break;
										case "TKCN_camp_boost_post":
											resProcess = bpCampStepAsync("TKCN", _reg, _threadName, stepData);
											break;
										case "TKBM_camp_boost_post":
											resProcess = bpCampStepAsync("TKBM", _reg, _threadName, stepData);
											break;

										case "TKCN_camp_off":
											resProcess = _reg.TKCNCampOnOff("ACTIVE", "PAUSED");
											break;
										case "TKCN_camp_on":
											resProcess = _reg.TKCNCampOnOff("PAUSED", "ACTIVE");
											break;

										case "TKBM_camp_off":
											resProcess = _reg.TKBMCampOnOff("ACTIVE", "PAUSED");
											break;
										case "TKBM_camp_on":
											resProcess = _reg.TKBMCampOnOff("PAUSED", "ACTIVE");
											break;

										case "TKCN_wait_camp_error":
											int waitTimeCn = 10;
											string waitTimeCnStr = Regex.Match(stepStr, "wait_time_(.*?)#").Groups[1].Value;
											if (!string.IsNullOrEmpty(waitTimeCnStr))
											{
												Int32.TryParse(waitTimeCnStr, out waitTimeCn);
											}
											resProcess = _reg.waitAdsCampError(waitTimeCn);
											break;
										case "TKBM_wait_camp_error":
											int waitTime = 10;
											string waitTimeStr = Regex.Match(stepStr, "wait_time_(.*?)#").Groups[1].Value;
											if (!string.IsNullOrEmpty(waitTimeStr))
											{
												Int32.TryParse(waitTimeStr, out waitTime);
											}
											resProcess = _reg.waitBMAdsCampError(waitTime);
											break;

										case "set_change_budget_schedule":
											string viaInputStr = textBox9.Text.Trim();
											if (!string.IsNullOrEmpty(viaInputStr))
											{
												string[] viaInputData = viaInputStr.Split(new char[] { '|' });
												string viaid = viaInputData[0];
												int setupBudget = (int)numericUpDown3.Value;
												resProcess = _reg.SetChangeBudgetSchedule(viaid, setupBudget);
											}
											break;
										case "logout":
											resProcess = _reg.logout();
											break;

										case "BM_ad_wait_bill_up":
											resProcess = this.waitBillUpStepAsync(_reg, _threadName, stepStr, stepData, "BMad");
											break;
										case "Clone_ad_wait_bill_up":
											resProcess = this.waitBillUpStepAsync(_reg, _threadName, stepStr, stepData, "ad");
											break;
										case "BM_ad_change_budget":
											string valuestr = "150";
											if (stepData.Length > 1)
											{
												valuestr = stepData[1];
											}
											resProcess = _reg.BMAdCampChangeBudget(valuestr);
											break;

										case "check_ad":
											resProcess = _reg.CheckAdAllInOneAsync("ad");
											break;
										case "BM_ad_check":
											resProcess = _reg.CheckAdAllInOneAsync("BMad");
											break;
										case "check_live":
											resProcess = _reg.checkLiveAsync().Result;
											break;
										case "save_ad":
											string ads = _reg._clone_uid + "|" + _reg._ads_id;
											this.logAds(ads);
											resProcess = true;
											break;
										default:
											logWithThread(_threadName, "Không có Step này!");
											break;
									}

									if (resProcess)
									{
										logWithThread(_threadName, stepCode + ": OK");
										if (stepData.Contains("true_end"))
										{
											string crClone = getClone(false);
											if (crClone == _reg._clone_data)
											{
												logWithThread(_threadName, "Xóa clone...");
												getClone(true);
											}
											return Task.CompletedTask;
										}
									}
									else
									{
										logWithThread(_threadName, stepCode + ": NOT OK");
										if (stepData.Contains("false_checkcheckpoint"))
										{
											logWithThread(_threadName, "Check checkpoint...");
											if (_reg.checkCheckPoint())
											{
												string crClone = getClone(false);
												if (crClone == _reg._clone_data)
												{
													logWithThread(_threadName, "Xóa clone...");
													getClone(true);
												}
												return Task.CompletedTask;
											}
										}

										if (stepData.Contains("false_repeat"))
										{
											if (runIndex <= runMaxIndex)
											{
												logWithThread(_threadName, "Tool TỰ ĐỘNG thực hiện lại step...!");
												runIndex++;
												goto chay_lai_tu_day;
											}
										}

										if (stepData.Contains("false_pause"))
										{
											logWithThread(_threadName, "Tool đang tạm dừng đấy Kún!");
											logWithThread(_threadName, "Thực hiện lại step bị lỗi và nhấn Yes để tiếp tục!");
											// this.label13.Text = "Pause..., nhấn Yes để tiếp tục!";
											this.button7.BackColor = Color.MediumSlateBlue;
											this.button7.ForeColor = Color.Azure;
											signal.Reset();
											signal.WaitOne();
											if (!signalValue)
											{
												// Goto
												if (stepData.Contains("false_goto"))
												{
													nextStep = stepData.Last();
													logWithThread(_threadName, "Tool sẽ chạy tiếp từ step: " + nextStep);
                                                }
                                                else
                                                {
													// Stop
													if (stepData.Contains("false_stop"))
													{
														is_stop = true;
														logWithThread(_threadName, "Tool sẽ dừng sau lượt này!");
													}

													// End
													if (stepData.Contains("false_end"))
													{
														return Task.CompletedTask;
													}
												}
											}
											else
											{
												if (this.checkBox3.Checked)
												{
													logWithThread(_threadName, "Tool thực hiện lại step...!");
													goto chay_lai_tu_day;
												}
											}
										}
										else
										{
											// Goto
											if (stepData.Contains("false_goto"))
											{
												nextStep = stepData.Last();
												logWithThread(_threadName, "Tool sẽ chạy tiếp từ step: " + nextStep);
                                            }
                                            else
                                            {
												// Stop
												if (stepData.Contains("false_stop"))
												{
													is_stop = true;
													logWithThread(_threadName, "Tool sẽ dừng sau lượt này!");
												}

												// End
												if (stepData.Contains("false_end"))
												{
													return Task.CompletedTask;
												}
											}
										}
									}
								}
							}
						}

						SetTurnStatus();
						stepIndex++;
					}
                }
                else
                {
					logWithThread(_threadName, "FLOW CHƯA CÓ DỮ LIỆU!");
				}

				return Task.CompletedTask;
			}
			catch (Exception error)
			{
				this.log(error.Message);
				return Task.CompletedTask;
			}
        }

        private async Task<bool> putFirebaseAsync(string viaid, int type, string data, string bmid = null)
		{
			try
			{
				FireBaseMessage message = new FireBaseMessage();
				int Timestamp = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
				message.Id = Timestamp.ToString();
				message.Type = type;
				message.Data = data;
				message.BmId = bmid;
				await firebase.Child("share/" + viaid).Child(message.Id).PutAsync(message);
				return true;
			}
			catch (Exception error)
			{
				this.log(error);
				return false;
			}
		}

		private void Change911ProxyWrap()
		{
			use911 = this.checkBox6.Checked;
			useHma = this.checkBox7.Checked;
            if (useHma)
            {
				this.log("CHANGE HMA...");
				changeHmaProxy();
			}
			if (use911)
			{
				this.log("CHANGE 911...");
				string qg = this.textBox2.Text.Trim();
				this.log("QG: " + qg);
				if (change911Proxy(qg))
				{
					this.log("CHANGE PROXY OK!");
					this.log("PROXY:" + _proxy_host + ":" + _proxy_port);
				}
				else
				{
					this.log("CHANGE PROXY: FALSE!");
				}
			}
		}

		private void ChangeHMAProxyWrap()
		{
			MessageBox.Show(new Form { TopMost = true }, "Bật HMA và nhấn OK để tiếp tục!");
		}

		private void runFlow(string fl = "")
        {
			try
			{
				_crFlow = fl;

				if (_myThread != null)
				{
					this.log("Hủy process!");
					_myThread.Abort();
				}

				this.label13.Text = "...";
				// this.button7.UseVisualStyleBackColor = true;
				this.button7.BackColor = default(Color);
				this.button7.ForeColor = default(Color);

				if (!this.validate())
				{
					return;
				}
				this.mode = this.comboBox1.SelectedItem.ToString();

				this.saveData();
				int _numberRep = (int)this.numericUpDown1.Value;
				saveInitParam();

				use911 = this.checkBox6.Checked;
				auto911 = this.checkBox1.Checked;
				int auto911oo = (int)this.numericUpDown2.Value;
				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string a911Path = desktopPath + "\\911";

				autoHma = this.checkBox5.Checked;
				int autoHmaoo = (int)this.numericUpDown6.Value;

				_clone_max_rep = (int)this.numericUpDown4.Value;

				turnIndex = 1;
				int indexChangeProxy = 1;
				this.is_stop = false;

				_myThread = new Thread(async delegate ()
				{
					if ((_reg is null) || _reg.isBrowserClosed() || (_reg._mode != this.mode) ||(curuse911 != use911))
					{
						bool init = await openDfChrome();
						if (!init)
						{
							this.log("END!");
							return;
						}
					}

					_reg._listKeyword = this._listKeyword;
					_reg._listFakePhone = this._listFakePhone;

					while ((!this.is_stop) && (turnIndex <= _numberRep))
					{
						this.log("TURN: " + turnIndex.ToString() + "...");
						SetTurnStatus();

						int checkChangeProxy = 1;
						if (auto911oo > 0)
						{
							checkChangeProxy = indexChangeProxy % auto911oo;
						}
						if ((auto911 && (checkChangeProxy == 0)) || (auto911 && changeProxy))
						{
							this.log("CHANGE 911 :D PROXY!");
							this.change911Proxy("us");
							indexChangeProxy = 0;
						}

						if (autoHmaoo > 0)
						{
							checkChangeProxy = indexChangeProxy % autoHmaoo;
						}
						if ((autoHma && (checkChangeProxy == 0)) || (autoHma && changeProxy))
						{
							this.log("CHANGE HMA PROXY!");
							this.changeHmaProxy();
							indexChangeProxy = 0;
						}

						changeProxy = false;
						_reg.ResetDriver();
						bool init = false;
						if (this.mode == "Chrome-Private")
						{
							init = _reg.initChromePrivate();
						}
						if (this.mode == "Chrome-Portable")
						{
							string chrome = "portable";
							initBrowse(chrome);
							init = _reg.initChromePortable(chrome);
						}
						if (this.mode == "Chrome-Profile")
						{
							string chrome = "profile";
							initBrowse(chrome);
							init = _reg.initChromeProfile(chrome);
						}

						if (init)
						{
							await RegProcess(0);
						}
						else
						{
							this.log("INIT FALSE!");
						}

						this.log("END TURN!");
						turnIndex++;
						indexChangeProxy++;
					}
				});

				_myThread.Start();
			}
			catch (Exception error)
			{
				this.log(error);
				return;
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{
			runFlow();
		}

		private void initBrowse(string viaName)
		{
			try
			{
				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir;

				string folderPath = dataPath + "\\via";
				bool exists = System.IO.Directory.Exists(folderPath);
				if (!exists)
					System.IO.Directory.CreateDirectory(folderPath);

				string viaPath = folderPath + "\\" + viaName;

				bool viaExists = System.IO.Directory.Exists(viaPath);
				if (!viaExists)
				{
					string portablePath = dataPath + "\\portable\\GoogleChromePortable.zip";
					bool portableExists = File.Exists(portablePath);
					if (!portableExists)
					{
						this.log("Chưa có bộ cài portable, liên hệ cuongnh!");
						return;
					}
					// Create via portable
					System.IO.Directory.CreateDirectory(viaPath);
					ZipFile.ExtractToDirectory(portablePath, viaPath);
				}
			}
			catch (Exception error)
			{
				this.log(error.Message);
				return;
			}
		}

		private IKeyboardMouseEvents m_Events;

		private string IP = "127.0.0.1";

		private string _proxy_host = "127.0.0.1";
		private string _proxy_port = "38689";

		private string _folderPath = "";
		private string _dataPath = "";

        private void ManageChannel_FormClosing(object sender, FormClosingEventArgs e)
        {
			this.saveInitParam();
			this.saveData();
			if (_myThread != null)
			{
				this.log("Hủy process!");
				this.log("Hãy đợi Tool tự tắt ...");
				_myThread.Abort();
			}

			if (_reg != null)
			{
				_reg.Dispose();
				_reg = null;
			}
		}

		private void saveData()
        {
			try
			{
				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir + "\\DATA";

				bool exists = System.IO.Directory.Exists(dataPath);
				if (!exists)
					System.IO.Directory.CreateDirectory(dataPath);

				// Bm link
				byte[] buffer = Encoding.ASCII.GetBytes(richTextBox6.Text);
				MemoryStream ms = new MemoryStream(buffer);
				FileStream file = new FileStream(dataPath + "\\bmlink.txt", FileMode.Create, FileAccess.Write);
				ms.WriteTo(file);
				file.Close();
				ms.Close();

				// Clone file
				buffer = Encoding.ASCII.GetBytes(richTextBox1.Text);
				ms = new MemoryStream(buffer);
				file = new FileStream(dataPath + "\\clone.txt", FileMode.Create, FileAccess.Write);
				ms.WriteTo(file);
				file.Close();
				ms.Close();

				// Clone moi
				buffer = Encoding.ASCII.GetBytes(richTextBox7.Text);
				ms = new MemoryStream(buffer);
				file = new FileStream(dataPath + "\\clone_moi.txt", FileMode.Create, FileAccess.Write);
				ms.WriteTo(file);
				file.Close();
				ms.Close();

				// BM link
				buffer = Encoding.ASCII.GetBytes(richTextBox8.Text);
				ms = new MemoryStream(buffer);
				file = new FileStream(dataPath + "\\bmlink_input.txt", FileMode.Create, FileAccess.Write);
				ms.WriteTo(file);
				file.Close();
				ms.Close();

				// Flow file
				buffer = Encoding.ASCII.GetBytes(richTextBox2.Text);
				ms = new MemoryStream(buffer);
				file = new FileStream(dataPath + "\\flow.txt", FileMode.Create, FileAccess.Write);
				ms.WriteTo(file);
				file.Close();
				ms.Close();
			}
			catch
			{
				MessageBox.Show("Không lưu được data!", "Thông báo!", MessageBoxButtons.OK);
			}
		}

        private void button8_Click(object sender, EventArgs e)
        {
			this.is_stop = true;
		}

        private void button1_Click(object sender, EventArgs e)
        {
			Flow flowForm = new Flow();
			flowForm.ShowDialog();
		}

        private void button2_Click(object sender, EventArgs e)
        {
            if (_reg != null)
            {
				_reg.gotoUrl("https://m.facebook.com/business_payments/");
			}
		}

		private string GetMainModuleFilepath(int processId)
		{
			string wmiQueryString = "SELECT ProcessId, ExecutablePath FROM Win32_Process WHERE ProcessId = " + processId;
			using (var searcher = new ManagementObjectSearcher(wmiQueryString))
			{
				using (var results = searcher.Get())
				{
					ManagementObject mo = results.Cast<ManagementObject>().FirstOrDefault();
					if (mo != null)
					{
						return (string)mo["ExecutablePath"];
					}
				}
			}
			return null;
		}

		private Task<bool> openDfChrome()
		{
			try
			{
				string _threadName = "S_";
				if (_reg != null)
				{
					_reg.killChrome();
					_reg.Dispose();
					_reg = null;
				}
				_reg = new RegClone();
				_reg._mode = this.mode;
				_reg._ThreadName = (_threadName ?? "");

				_reg.autoRequest = new AutoRequest(_hIp);
				AutoRequest autoRq = _reg.autoRequest;
				autoRq._logDelegate = (AutoRequest.LogDelegate)Delegate.Combine(autoRq._logDelegate, new AutoRequest.LogDelegate(this.LogDelegate));

                if (use911)
                {
					curuse911 = true;
					_reg._use_proxy = true;
					_reg._proxy_host = _proxy_host;
					_reg._proxy_port = _proxy_port;
					_reg._proxy_type = "socks5";
				}

				RegClone regClone = _reg;
				regClone._logDelegate = (RegClone.LogDelegate)Delegate.Combine(regClone._logDelegate, new RegClone.LogDelegate(this.LogDelegate));
				regClone._campDelegate = (RegClone.CampDelegate)Delegate.Combine(regClone._campDelegate, new RegClone.CampDelegate(this.StoreCamp));
				regClone._getCardDelegate = (RegClone.GetCardDelegate)Delegate.Combine(regClone._getCardDelegate, new RegClone.GetCardDelegate(this.getCard));
				regClone._changeProxyDelegate = (RegClone.ChangeProxyDelegate)Delegate.Combine(regClone._changeProxyDelegate, new RegClone.ChangeProxyDelegate(this.Change911ProxyWrap));
				regClone._changeHMAProxyDelegate = (RegClone.ChangeHMAProxyDelegate)Delegate.Combine(regClone._changeHMAProxyDelegate, new RegClone.ChangeHMAProxyDelegate(this.ChangeHMAProxyWrap));

				this.logWithThread(_threadName, "Check proxy...");
				if (!_reg.checkProxy())
				{
					changeProxy = true;
					return Task.FromResult(false);
				}

				this._location = _reg._location.ToString();
				string areacodeFile = "GenCode\\areacode_" + this._location + ".json";
				bool fileExists = File.Exists(areacodeFile);
				if (!fileExists)
				{
					areacodeFile = "GenCode\\areacode_US.json";
				}

				string _json = File.ReadAllText(areacodeFile);
				this._listFakePhone = JsonConvert.DeserializeObject<List<string>>(_json);
				logWithThread(_threadName, "COUNTRY: " + _reg._location);
				logWithThread(_threadName, "IP: " + _reg._ip);

				return Task.FromResult(true);
			}
			catch (Exception error)
			{
				this.log(error.Message);
				return Task.FromResult(false);
			}
		}

        private void button5_Click(object sender, EventArgs e)
        {
			int min = 406;
			if (this.Width == min)
            {
				this.Width = 716;
				this.groupBox10.Left = 318;
				this.groupBox4.Left = 318;
				this.groupBox1.Left = 318;
				this.groupBox8.Left = 11;
				this.groupBox6.Left = 11;
				this.button5.Text = "<";
			}
            else
            {
				this.Width = min;
				this.groupBox1.Left = 11;
				this.groupBox8.Left = 500;
				this.groupBox6.Left = 500;
				this.groupBox10.Left = 11;
				this.groupBox4.Left = 11;
				this.button5.Text = ">";
			}
		}

        private void button6_Click(object sender, EventArgs e)
        {
			AutoUpdater.Start("http://nguyenlieu.site/toolreg/update.xml");
		}

        private void button7_Click(object sender, EventArgs e)
        {
			this.label13.Text = "...";
			// this.button7.UseVisualStyleBackColor = true;
			this.button7.BackColor = default(Color);
			this.button7.ForeColor = default(Color);

			signalValue = true;
			signal.Set();
		}

        private void button9_Click(object sender, EventArgs e)
        {
			this.label13.Text = "...";
			// this.button7.UseVisualStyleBackColor = true;
			this.button7.BackColor = default(Color);
			this.button7.ForeColor = default(Color);

			signalValue = false;
			signal.Set();
		}

		private string getBios()
        {
			string key = "Win32_BIOS";
			ManagementObjectSearcher selectvalue = new ManagementObjectSearcher("select * from " + key);
			string serialId = "";
			foreach (ManagementObject getserial in selectvalue.Get())
			{
				serialId += getserial["SerialNumber"].ToString();
			}
			return serialId;
		}

		public string getUUID()
		{
			try
			{
				Process process = new Process();
				ProcessStartInfo startInfo = new ProcessStartInfo();
				startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
				startInfo.FileName = "CMD.exe";
				startInfo.Arguments = "/C wmic csproduct get UUID";
				process.StartInfo = startInfo;
				process.StartInfo.UseShellExecute = false;
				process.StartInfo.RedirectStandardOutput = true;
				process.Start();
				process.WaitForExit();
				string output = process.StandardOutput.ReadToEnd();
				output = Regex.Replace(output, @"\r\n?|\n", "");
				output = Regex.Replace(output, @"\s+", "");
				return output;
			}
			catch
			{
				return "";
			}
		}

		public string getCPUId()
		{
			try
			{
				string cpuInfo = string.Empty;
				ManagementClass mc = new ManagementClass("win32_processor");
				ManagementObjectCollection moc = mc.GetInstances();

				foreach (ManagementObject mo in moc)
				{
					if (cpuInfo == "")
					{
						//Get only the first CPU's ID
						cpuInfo = mo.Properties["processorID"].Value.ToString();
						break;
					}
				}
				return cpuInfo;
			}
			catch
			{
				return "";
			}
		}

		private void button10_Click(object sender, EventArgs e)
        {
			if (this.textBox18.Text.Trim().Length == 0)
			{
				MessageBox.Show("Phải nhập Key!", "Thông báo!", MessageBoxButtons.OK);
				return;
			}

			string key = this.textBox18.Text.Trim();
			int checkActive = this.autoRequest.activeKey(key);
			if (checkActive == 0)
			{
				_active = true;
				root_email = this.autoRequest.checkKey();
			}

			if (_active)
			{
				this.log("Active! ... Success!");
				this.Text = this.Text + " Active - " + root_email;
				this.button10.Visible = false;
				this.button10.Enabled = false;

				this.textBox18.Visible = false;
				this.textBox18.Enabled = false;
			}
			else
			{
				this.log("Active! ...");
				this.log("WARNING! KHÔNG ACTIVE ĐƯỢC TOOL, MỘT SỐ CHỨC NĂNG CỦA TOOL SẼ KHÔNG HOẠT ĐỘNG ĐƯỢC, KÚN THỬ ACTIVE LẠI HOẶC LIÊN HỆ ADMIN ĐỂ LẤY KEY KHÁC!");
				this.log("=================================");
				this.button10.Visible = true;
				this.button10.Enabled = true;

				this.textBox18.Visible = true;
				this.textBox18.Enabled = true;
			}
		}

        private void button11_Click(object sender, EventArgs e)
        {
			this.log("Xóa clone! ...");
			getClone(true);
		}

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
	
		}

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
	
		}

        private void button2_Click_1(object sender, EventArgs e)
        {
			this.log("Xóa BM link! ...");
			getBmLinkInput(true);
		}

		private void setBtnFlowAction(string fl = "")
		{
			Color backColor = Color.DarkGreen;
			Color textColor = Color.White;
			this.button12.BackColor = default(Color);
			this.button12.ForeColor = default(Color);

			this.button13.BackColor = default(Color);
			this.button13.ForeColor = default(Color);

			this.button14.BackColor = default(Color);
			this.button14.ForeColor = default(Color);

			this.button15.BackColor = default(Color);
			this.button15.ForeColor = default(Color);

			this.button16.BackColor = default(Color);
			this.button16.ForeColor = default(Color);

			if (fl == "flow1")
			{
				this.button16.BackColor = backColor;
				this.button16.ForeColor = textColor;
			}

			if (fl == "flow2")
			{
				this.button15.BackColor = backColor;
				this.button15.ForeColor = textColor;
			}

			if (fl == "flow3")
			{
				this.button14.BackColor = backColor;
				this.button14.ForeColor = textColor;
			}

			if (fl == "flow4")
			{
				this.button13.BackColor = backColor;
				this.button13.ForeColor = textColor;
			}

			if (fl == "flow5")
			{
				this.button12.BackColor = backColor;
				this.button12.ForeColor = textColor;
			}
		}

		private void setFlowAction(string fl = "")
        {
			setBtnFlowAction(fl);
			runFlow(fl);
		}

		private void button16_Click(object sender, EventArgs e)
		{
			setFlowAction("flow1");
		}
		private void button15_Click(object sender, EventArgs e)
        {
			setFlowAction("flow2");
		}

        private void button14_Click(object sender, EventArgs e)
        {
			setFlowAction("flow3");
		}

        private void button13_Click(object sender, EventArgs e)
        {
			setFlowAction("flow4");
		}

        private void button12_Click(object sender, EventArgs e)
        {
			setFlowAction("flow5");
		}

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void button17_Click(object sender, EventArgs e)
        {
			Change911ProxyWrap();
		}

        private void button18_Click(object sender, EventArgs e)
        {
			string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
			string filePath = desktopPath + "\\" + _dataDir + "\\DATA\\sys\\nini.txt";
			if (File.Exists(filePath))
            {
				File.Delete(filePath);
				Application.Restart();
			}
		}

        private void button19_Click(object sender, EventArgs e)
        {
			changeHmaProxy();
		}

        private void richTextBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
			setFlowAction("flow6");
		}

        private void button20_Click(object sender, EventArgs e)
        {
			setFlowAction("flow7");
		}

        private void button22_Click(object sender, EventArgs e)
        {
			setFlowAction("flow8");
		}

        private void button21_Click(object sender, EventArgs e)
        {
			setFlowAction("flow9");
		}

		private void SelectItemByValue(ComboBox cbo, string value)
		{
			for (int i = 0; i < cbo.Items.Count; i++)
			{
				string val = (cbo.Items[i] as ComboboxItem).Value.ToString();
				if (val == value)
				{
					TextBoxUtils.Instance.SetComboBoxSelect(this, cbo, i);
					break;
				}
			}
		}

		private void button23_Click(object sender, EventArgs e)
        {
			List<NLParam> paramsData = this.autoRequest.getParams();
			if ((paramsData == null) || (paramsData.Count == 0))
			{
				this.log("Không có cấu hình tham số!");
			}
			else
			{
				comboBox2.Items.Clear();
				foreach (NLParam pram in paramsData)
				{
					ComboboxItem item = new ComboboxItem();
					item.Text = pram.title;
					item.Value = pram.id;
					comboBox2.Items.Add(item);
				}
				SelectItemByValue(comboBox2, paramid);
				this.log("Load cấu hình tham số: DONE!");
			}
		}

		private void loadCamps()
        {
			try
			{
				Thread newThread = new Thread(async delegate ()
				{
					List<NLCamp> campsData = this.autoRequest.getCamps();
					if ((campsData == null) || (campsData.Count == 0))
					{
						this.log("Không có cấu hình Camps!");
					}
					else
					{
						this.log("Load Camp...");
						string campFolderPath = _dataPath + "\\camp2.0";
						if (!Directory.Exists(campFolderPath))
						{
							Directory.CreateDirectory(campFolderPath);
						}

						TextBoxUtils.Instance.AddComboBoxItem(this, this.comboBox3, null);
						foreach (NLCamp camp in campsData)
						{
							ComboboxItem item = new ComboboxItem();
							item.Text = camp.title;
							item.Value = camp.id;
							TextBoxUtils.Instance.AddComboBoxItem(this, this.comboBox3, item);

							string campItemFolderPath = campFolderPath + "\\" + camp.id;
							if (!Directory.Exists(campItemFolderPath))
							{
								Directory.CreateDirectory(campItemFolderPath);
							}

							updateCamp(camp);
						}
						SelectItemByValue(comboBox3, campid);
						this.log("Load Camp: DONE!");
					}
				});
				newThread.Start();
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				this.log("Load Camp: FALSE!");
			}
		}

        private void button24_Click(object sender, EventArgs e)
        {
			loadCamps();
		}

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
			string campId = TextBoxUtils.Instance.getComboBoxSelect(this, comboBox3);
			checkCamp(campId);
		}
    }
}
