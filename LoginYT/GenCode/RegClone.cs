﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using LoginYT;
using LoginYT.GenCode;
using LoginYT.Types;
using MailKit;
using MailKit.Net.Imap;
using MailKit.Search;
using MimeKit;
using Newtonsoft.Json;
using OpenPop.Pop3;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using SmorcIRL.TempMail;
using SmorcIRL.TempMail.Models;
using xNet;

namespace GenCode
{
	public class RegClone : IDisposable
	{
		public AutoRequest autoRequest;

		private IWebDriver _driver = null;
		private string regTabHandle = null;
		private string businessTabHandle = null;
		private string shopifyHandle = null;
		private string otherTabHandle = null;

		public string shopifyRegData = null;
		public bool rePostCreate = false;

		public bool reBPost = false;

		public delegate string GetCardDelegate(string cardCode);
		public RegClone.GetCardDelegate _getCardDelegate { get; set; }

		public delegate void LogDelegate(string s);
		public RegClone.LogDelegate _logDelegate { get; set; }

		public delegate void CampDelegate(string s);
		public RegClone.CampDelegate _campDelegate { get; set; }

		public delegate void ChangeProxyDelegate();
		public RegClone.ChangeProxyDelegate _changeProxyDelegate { get; set; }

		public delegate void ChangeHMAProxyDelegate();
		public RegClone.ChangeHMAProxyDelegate _changeHMAProxyDelegate { get; set; }

		public string _mode { get; set; }
		public string _ThreadName { get; set; }
		public string _clone_data { get; set; }
		public string _clone_uid { get; set; }
		public string _bm_id { get; set; }
		private string _sel_bm_id { get; set; }
		private string _bm1_id { get; set; }
		private string _bm2_id { get; set; }
		private string _bm1_page_id { get; set; }
		private string _bm2_page_id { get; set; }
		private string _bm1_ads_id { get; set; }
		private string _bm2_ads_id { get; set; }
		private string _bm1_credential_id { get; set; }
		private string _bm2_credential_id { get; set; }

		public string _payment_account_id { get; set; }
		public string _business_user_id { get; set; }
		public string _credential_id { get; set; }
		public List<Card> _bm_cards { get; set; }
		public string _bm_link { get; set; }

		public string _card_name = "LEE NGUYEN";
		public string _curentCard { get; set; }

		public bool _create_bm = false;
		public bool _check_point = false;
		public string _accessToken { get; set; }
		public string _sessionID { get; set; }
		public string _cloneAccessToken { get; set; }
		public string _clone_email { get; set; }

		public string _clone_pass = "ctcANKak47";
		public string _clone_email_pass { get; set; }
		public string _2fa_code { get; set; }

		public AdAccounts adAccount { get; set; }
		public string _ads_id = "";
		public string _adAccessToken { get; set; }
		public string _bm_ads_id { get; set; }

		public bool shareAds = false;
		public bool _isNavigate = true;
		public string _cookie { get; set; }

		public string _location = "";
		public string _time_zone = "TZ_UNKNOWN";
		public string _ip = "";

		public bool _use_proxy = false;
		public string _proxy_host = "";
		public string _proxy_port = "";
		public string _proxy_user = "";
		public string _proxy_pass = "";
		public string _proxy_type = "";

		public string emailBmConfirm = "";
		public string emailPassBmConfirm = "";

		private string rcBm_first_name = "";
		private string rcBm_last_name = "";

		public List<string> _listKeyword { get; set; }
		public List<string> _listFakePhone { get; set; }
		public List<ShareLog> _listShareBmToVIA { get; set; }

		private HttpClientCodeSimNetHelper codeSimHttpc { get; set; }

		private TenMinuteMail tenMinuteMail { get; set; }
		private MailClient mailClient { get; set; }
		private string mailPass = "ctctool@2022";

		public string pageID = "";
		public string pageLink = "";
		public string _rNamePage = "";
		public string _rNameAds = "";

		public string _wappPageID = "";
		public string _wappPageName = "";

		public string postID = "";

		public string shareToBmErrCode = "";
		public string createBmErrCode = "";

		private string _userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36";
		private string _contentType = "application/x-www-form-urlencoded";
		public string _dataDir = "tut25flowdata";
		public string _driverDir = "FlowDriver";

		private string randomNumber(int length)
		{
			string s = "";
			for (int i = 0; i < length; i++)
			{
				s += new Random(Guid.NewGuid().GetHashCode()).Next(0, 10).ToString();
			}
			return s;
		}

		private string randomGmail()
		{
			string _id = this._listKeyword[new Random(Guid.NewGuid().GetHashCode()).Next(0, this._listKeyword.Count)] + this._listKeyword[new Random(Guid.NewGuid().GetHashCode()).Next(0, this._listKeyword.Count)] + this.randomNumber(5);
			_id = _id.Replace(" ", "");
			return _id + "@gmail.com";
		}

		private string randomPhone()
		{
			string _phone = this._listFakePhone[new Random(Guid.NewGuid().GetHashCode()).Next(0, this._listFakePhone.Count)];
			StringBuilder encodedValue = new StringBuilder();
			foreach (char character in _phone.ToCharArray())
			{
				if (character == 'x')
				{
					encodedValue.Append(new Random(Guid.NewGuid().GetHashCode()).Next(0, 10).ToString());
				}
				else
				{
					encodedValue.Append(character);
				}
			}
			return encodedValue.ToString();
		}

		public bool isBrowserClosed()
		{
			bool isClosed = false;
			try
			{
				string tt = this._driver.Title;
			}
			catch
			{
				isClosed = true;
			}

			return isClosed;
		}

		private string fetchApi(string url, string method, string body, string friendly, string lsd, string referrer, string credentials = "include", string activeflows = "")
		{
			try
			{
				string sec = "\\\"Chromium\\\";v=\\\"94\\\", \\\"Google Chrome\\\";v=\\\"94\\\", \\\";Not A Brand\\\";v=\\\"99\\\"";
				string win = "\\\"Windows\\\"";

				string friendlyStr = "";
				if (!string.IsNullOrEmpty(friendly))
				{
					friendlyStr = "\"x-fb-friendly-name\": \"" + friendly + "\",";
				}

				string referrerStr = "";
				if (!string.IsNullOrEmpty(referrer))
				{
					referrerStr = "\"referrer\": \"" + referrer + "\",";
				}

				string activeflowsStr = "";
				if (!string.IsNullOrEmpty(activeflows))
				{
					activeflowsStr = "\"x-fb-qpl-active-flows\": \"" + activeflows + "\",";
				}

				string lsdStr = "";
				if (!string.IsNullOrEmpty(lsd))
				{
					lsdStr = "\"x-fb-lsd\": \"" + lsd + "\"";
				}

				string jsstr = "let response = await fetch(\"" + url + "\", {" +
									"\"headers\": {" +
										"\"accept\": \"*/*\"," +
										"\"accept-language\": \"en-US,en;q=0.9\"," +
										"\"content-type\": \"application/x-www-form-urlencoded\"," +
										"\"sec-ch-ua\": \"" + sec + "\"," +
										"\"sec-ch-ua-mobile\": \"?0\"," +
										"\"sec-ch-ua-platform\": \"" + win + "\"," +
										"\"sec-fetch-dest\": \"empty\"," +
										"\"sec-fetch-mode\": \"cors\"," +
										"\"sec-fetch-site\": \"same-origin\"," +
										friendlyStr + activeflowsStr + lsdStr +
									"}," + referrerStr +
									"\"referrerPolicy\": \"strict-origin-when-cross-origin\"," +
									"\"body\": \"" + body + "\"," +
									"\"method\": \"" + method + "\"," +
									"\"mode\": \"cors\"," +
									"\"credentials\": \"" + credentials + "\"" +
								"}); return await response.text();";
				// this.log("NINI BUG: " + jsstr);
				return (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private string formDataFetchApi(string url, string method, string body, string friendly, string lsd, string referrer, string credentials = "include", string activeflows = "")
		{
			try
			{
				string sec = "\\\"Chromium\\\";v=\\\"94\\\", \\\"Google Chrome\\\";v=\\\"94\\\", \\\";Not A Brand\\\";v=\\\"99\\\"";
				string win = "\\\"Windows\\\"";

				string friendlyStr = "";
				if (!string.IsNullOrEmpty(friendly))
				{
					friendlyStr = "\"x-fb-friendly-name\": \"" + friendly + "\",";
				}

				string referrerStr = "";
				if (!string.IsNullOrEmpty(referrer))
				{
					referrerStr = "\"referrer\": \"" + referrer + "\",";
				}

				string activeflowsStr = "";
				if (!string.IsNullOrEmpty(activeflows))
				{
					activeflowsStr = "\"x-fb-qpl-active-flows\": \"" + activeflows + "\",";
				}

				string lsdStr = "";
				if (!string.IsNullOrEmpty(lsd))
				{
					lsdStr = "\"x-fb-lsd\": \"" + lsd + "\"";
				}

				string jsstr = "let response = await fetch(\"" + url + "\", {" +
									"\"headers\": {" +
										"\"accept\": \"*/*\"," +
										"\"accept-language\": \"en-US,en;q=0.9\"," +
										"\"content-type\": \"multipart/form-data; boundary=----WebKitFormBoundaryabkJ1dWwF1Okeym1\"," +
										"\"sec-ch-ua\": \"" + sec + "\"," +
										"\"sec-ch-ua-mobile\": \"?0\"," +
										"\"sec-ch-ua-platform\": \"" + win + "\"," +
										"\"sec-fetch-dest\": \"empty\"," +
										"\"sec-fetch-mode\": \"cors\"," +
										"\"sec-fetch-site\": \"same-origin\"," +
										friendlyStr + activeflowsStr + lsdStr +
									"}," + referrerStr +
									"\"referrerPolicy\": \"origin-when-cross-origin\"," +
									"\"body\": \"" + body + "\"," +
									"\"method\": \"" + method + "\"," +
									"\"mode\": \"cors\"," +
									"\"credentials\": \"" + credentials + "\"" +
								"}); return await response.text();";
				return (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private string fetchGet(string url, string credentials = "include", string lsd = "", string activeflows = "")
		{
			try
			{
				string activeflowsStr = "";
				if (!string.IsNullOrEmpty(activeflows))
				{
					activeflowsStr = "\"x-fb-qpl-active-flows\": \"" + activeflows + "\",";
				}

				string lsdStr = "";
				if (!string.IsNullOrEmpty(lsd))
				{
					lsdStr = "\"x-fb-lsd\": \"" + lsd + "\"";
				}

				string sec = "\\\"Chromium\\\";v=\\\"94\\\", \\\"Google Chrome\\\";v=\\\"94\\\", \\\";Not A Brand\\\";v=\\\"99\\\"";
				string win = "\\\"Windows\\\"";

				string jsstr = "let response = await fetch(\"" + url + "\", {" +
									"\"headers\": {" +
										"\"accept\": \"*/*\"," +
										"\"accept-language\": \"en-US,en;q=0.9\"," +
										"\"content-type\": \"application/x-www-form-urlencoded\"," +
										"\"sec-ch-ua\": \"" + sec + "\"," +
										"\"sec-ch-ua-mobile\": \"?0\"," +
										"\"sec-ch-ua-platform\": \"" + win + "\"," +
										"\"sec-fetch-dest\": \"document\"," +
										"\"sec-fetch-mode\": \"navigate\"," +
										"\"sec-fetch-site\": \"none\"," +
										"\"sec-fetch-user\": \"?1\"," +
										"\"upgrade-insecure-requests\": \"1\"" +
										 activeflowsStr + lsdStr +
									"}," +
									"\"referrerPolicy\": \"strict-origin-when-cross-origin\"," +
									"\"body\": null," +
									"\"method\": \"GET\"," +
									"\"mode\": \"cors\"," +
									"\"credentials\": \"" + credentials + "\"" +
								"}); return await response.text();";

				return (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		public async Task<bool> checkLiveAsync(string cloneData = null)
		{
			try
			{
				string uid = _clone_uid;
				if (string.IsNullOrEmpty(cloneData))
                {
					cloneData = _clone_data;
                }

				if (!string.IsNullOrEmpty(cloneData))
				{
					cloneData = _clone_data;
					string[] fbData = cloneData.Split(new char[] { '|' });
					uid = fbData[0];
				}

				HttpClient _client = new HttpClient();
				string url = "https://graph.facebook.com/" + uid + "?fields=picture";
				string text = await _client.GetStringAsync(url);
				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool elmAutoFinAndClick(string elmXpathString, int waitTime = 9)
		{
			try
			{
				WaitAjaxLoading(By.XPath(elmXpathString), waitTime);
				Delay(500);
				ReadOnlyCollection<IWebElement> elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
				if (elmObjs.Count == 0)
				{
					return false;
				}

				IWebElement elmItem = elmObjs.First();
				bool check = this.isValid(elmItem);
				int ChkIndex = 1;
				while (!check && (ChkIndex <= 3))
				{
					Delay(3000);
					elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
					elmItem = elmObjs.First();
					check = this.isValid(elmItem);
					ChkIndex++;
				}

				elmObjs.First().Click();
				Delay(500);
				return true;
			}
			catch
			{
				return false;
			}
		}


		private void MouseHoverByJavaScript(IWebElement targetElement)
		{

			string javaScript = "var evObj = document.createEvent('MouseEvents');" +
								"evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);" +
								"arguments[0].dispatchEvent(evObj);";
			IJavaScriptExecutor js = _driver as IJavaScriptExecutor;
			js.ExecuteScript(javaScript, targetElement);
		}

		public void MouseHoverAndClickByJavaScript(IWebElement targetElement)
		{

			string javaScript = "var evObj = document.createEvent('MouseEvents');" +
								"evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);" +
								"arguments[0].dispatchEvent(evObj);";
			IJavaScriptExecutor js = _driver as IJavaScriptExecutor;
			js.ExecuteScript(javaScript, targetElement);
			Delay(1000);
			js.ExecuteScript("arguments[0].click();", targetElement);
			Delay(500);
		}

		private bool elmAutoFinAndJsClick(string elmXpathString, int waitTime = 9, int eIndex = 0)
		{
			try
			{
				WaitAjaxLoading(By.XPath(elmXpathString), waitTime);
				Delay(500);
				ReadOnlyCollection<IWebElement> elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
				if (elmObjs.Count == 0)
				{
					return false;
				}

				IWebElement elmItem = elmObjs[eIndex];
				bool check = this.isValid(elmItem);
				int ChkIndex = 1;
				while (!check && (ChkIndex <= 3))
				{
					Delay(3000);
					elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
					elmItem = elmObjs[eIndex];
					check = this.isValid(elmItem);
					ChkIndex++;
				}

				MouseHoverByJavaScript(elmItem);

				IJavaScriptExecutor js = (IJavaScriptExecutor)this._driver;
				js.ExecuteScript("arguments[0].click();", elmItem);
				Delay(500);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private bool elmAutoFinAndSendkey(string elmXpathString, string inputValue, int waitTime = 9)
		{
			try
			{
				WaitAjaxLoading(By.XPath(elmXpathString), waitTime);
				Delay(500);
				ReadOnlyCollection<IWebElement> elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
				if (elmObjs.Count == 0)
				{
					return false;
				}

				IWebElement elmItem = elmObjs.First();
				bool check = this.isValid(elmItem);
				int ChkIndex = 1;
				while (!check && (ChkIndex <= 3))
				{
					Delay(3000);
					elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
					elmItem = elmObjs.First();
					check = this.isValid(elmItem);
					ChkIndex++;
				}

				MouseHoverAndClickByJavaScript(elmItem);
				Delay(500);

				clearWebField(elmItem);
				elmItem.SendKeys(inputValue);

				Delay(500);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		private bool elmAutoFinAndJsSetValue(string elmXpathString, string inputValue, int waitTime = 9)
		{
			try
			{
				WaitAjaxLoading(By.XPath(elmXpathString), waitTime);
				Delay(500);
				ReadOnlyCollection<IWebElement> elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
				if (elmObjs.Count == 0)
				{
					return false;
				}

				IWebElement elmItem = elmObjs.First();
				bool check = this.isValid(elmItem);
				int ChkIndex = 1;
				while (!check && (ChkIndex <= 3))
				{
					Delay(3000);
					elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
					elmItem = elmObjs.First();
					check = this.isValid(elmItem);
					ChkIndex++;
				}

				IJavaScriptExecutor js = (IJavaScriptExecutor)this._driver;
				js.ExecuteScript("arguments[0].value='" + inputValue + "';", elmItem);
				Delay(500);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		private bool elmAutoFinAndDbClick(string elmXpathString, int waitTime = 9)
		{
			try
			{
				WaitAjaxLoading(By.XPath(elmXpathString), waitTime);
				Delay(500);
				ReadOnlyCollection<IWebElement> elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
				if (elmObjs.Count == 0)
				{
					return false;
				}

				IWebElement elmItem = elmObjs.First();
				bool check = this.isValid(elmItem);
				int ChkIndex = 1;
				while (!check && (ChkIndex <= 3))
				{
					Delay(3000);
					elmObjs = this._driver.FindElements(By.XPath(elmXpathString));
					elmItem = elmObjs.First();
					check = this.isValid(elmItem);
					ChkIndex++;
				}

				new Actions(this._driver).DoubleClick(elmObjs.First()).Perform();
				Delay(500);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public bool getLocation(string checkPort)
		{
			try
			{
				string source = "http://ip-api.com/json/?fields=countryCode|countryCode";
				string[] sourceData = source.Split(new char[] { '|' });

				string getdata = this.GetData(null, sourceData[0], null, "", checkPort);
				this._location = Regex.Match(getdata, "\"" + sourceData[1] + "\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(_location))
				{
					this._location = Regex.Match(getdata, "\"" + sourceData[1] + "\": \"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(_location))
				{
					return false;
				}

				source = "http://ip-api.com/json/|query";
				sourceData = source.Split(new char[] { '|' });
				getdata = this.GetData(null, sourceData[0], null, "", checkPort);
				this._ip = Regex.Match(getdata, "\"" + sourceData[1] + "\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(_ip))
				{
					this._ip = Regex.Match(getdata, "\"" + sourceData[1] + "\": \"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(_ip))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool checkProxy(string checkPort = null)
		{
			try
			{
				bool checkpx = this.getLocation(checkPort);
				if (!checkpx)
				{
					Delay(1000);
					this.log("Proxy false!");
					this.log("Retry ...!");
					checkpx = this.getLocation(checkPort);
				}

				if (!checkpx)
				{
					Delay(1000);
					this.log("Proxy false!");
					this.log("Retry ...!");
					checkpx = this.getLocation(checkPort);
				}

				if (!checkpx)
				{
					this.log("Proxy false!");
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private string checkXPath(string[] xpaths)
		{
			string res = "";
			foreach (string xpath in xpaths)
			{
				ReadOnlyCollection<IWebElement> elmObjs = this._driver.FindElements(By.XPath(xpath));
				if (elmObjs.Count > 0)
				{
					res = xpath;
					break;
				}
			}
			return res;
		}

		private void WaitfMultiAjaxLoading(By[] byFinters, int time = 3)
		{
			WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(time));
			Func<IWebDriver, bool> waitLoading = new Func<IWebDriver, bool>((IWebDriver Web) =>
			{
				try
				{
					bool res = false;
					foreach (By byFinter in byFinters)
					{
						ReadOnlyCollection<IWebElement> elmObjs = Web.FindElements(byFinter);
						if (elmObjs.Count > 0)
						{
							res = true;
							break;
						}
					}
					return res;
				}
				catch
				{
					return false;
				}
			});

			try
			{
				wait.Until(waitLoading);
			}
			catch { }
		}

		private void Waitf2AjaxLoading(By byFinter1, By byFinter2, int time = 3)
		{
			WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(time));
			Func<IWebDriver, bool> waitLoading = new Func<IWebDriver, bool>((IWebDriver Web) =>
			{
				try
				{
					ReadOnlyCollection<IWebElement> alertE = Web.FindElements(byFinter1);
					if (alertE.Count > 0)
					{
						return true;
					}
					else
					{
						alertE = Web.FindElements(byFinter2);
						if (alertE.Count > 0)
						{
							return true;
                        }
                        else
                        {
							return false;
						}
					}
				}
				catch
				{
					return false;
				}
			});

			try
			{
				wait.Until(waitLoading);
			}
			catch { }
		}

		private void WaitAjaxLoading(By byFinter, int time = 3, IWebDriver webdrv = null)
		{
			if (webdrv == null)
			{
				webdrv = _driver;
			}

			WebDriverWait wait = new WebDriverWait(webdrv, TimeSpan.FromSeconds(time));
			Func<IWebDriver, bool> waitLoading = new Func<IWebDriver, bool>((IWebDriver Web) =>
			{
				try
				{
					ReadOnlyCollection<IWebElement> alertE = Web.FindElements(byFinter);
					if (alertE.Count > 0)
					{
						return true;
					}
					else
					{
						return false;
					}
				}
				catch
				{
					return false;
				}
			});

			try
			{
				wait.Until(waitLoading);
			}
			catch { }
		}

		private string genXpathString(string[] tgs, string type = "text")
		{
			if (tgs.Length == 0)
			{
				return "";
			}

			List<string> arrXpath = new List<string>();
			foreach (string _tg in tgs)
			{
				if (type == "text")
				{
					string item = string.Format("contains(text(), '{0}')", _tg);
					arrXpath.Add(item);
				}

				if (type == "aria-label")
				{
					string item = string.Format("@aria-label='{0}'", _tg);
					arrXpath.Add(item);
				}
			}

			return "//*[" + String.Join(" or ", arrXpath.ToArray()) + "]";
		}

		private static void Delay(int Time_delay)
		{
			int i = 0;
			System.Timers.Timer _delayTimer = new System.Timers.Timer();
			_delayTimer.Interval = Time_delay;
			_delayTimer.AutoReset = false; //so that it only calls the method once
			_delayTimer.Elapsed += (s, args) => i = 1;
			_delayTimer.Start();
			while (i == 0) { };
		}

		private void WaitLoading(IWebDriver webdrv = null)
		{
            if (webdrv == null)
            {
				webdrv = _driver;
			}
			WebDriverWait wait = new WebDriverWait(webdrv, TimeSpan.FromSeconds(30));
			Func<IWebDriver, bool> waitLoading = new Func<IWebDriver, bool>((IWebDriver Web) =>
			{
				try
				{
					IWebElement alertE = Web.FindElement(By.Id("abccuongnh"));
					return false;
				}
				catch
				{
					return true;
				}
			});

			try
			{
				wait.Until(waitLoading);
			}
			catch { }
		}

		public void ResetDriver()
		{
			try
			{
				this._clone_data = null;
				this._clone_uid = null;
				this._bm_id = null;
				this._bm_link = null;
				this._curentCard = null;
				this._create_bm = false;
				this._check_point = false;
				this._isNavigate = false;
				this._accessToken = null;
				this._cloneAccessToken = null;
				this._clone_email = null;
				this._clone_pass = null;
				this._2fa_code = null;
				this._ads_id = null;
				this._adAccessToken = null;
				this._bm_ads_id = null;
				this.shareAds = false;
				this._cookie = null;
				this.pageID = null;
				this.postID = null;
			}
			catch (Exception ex)
			{
				this.log(ex);
			}
		}

		public void QuitDriver()
		{
			try
			{
                if (this._driver != null)
                {
					this._driver.Quit();
				}
			}
			catch (Exception ex)
			{
				this.log(ex);
			}
		}

		private string[] GenFakeInfoAsync()
		{
			try
			{
				string _rs = this.GetData(null, "https://fake-it.ws/us/");
				string _tmpName = Regex.Match(_rs, "row..Name[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
				_tmpName = _tmpName.Replace("row\">Name</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
				string[] _tmpNameArr = _tmpName.Split(new char[]
				{
								' '
				});
				string _tmpPhone = Regex.Match(_rs, "row..Phone[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
				_tmpPhone = _tmpPhone.Replace("row\">Phone</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");

				_tmpNameArr = new List<string>(_tmpNameArr) { _tmpPhone }.ToArray();
				return _tmpNameArr;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		private TfaSecResBody get2faSecurityCode(string key)
		{
			string dfurl = @"http://2fa.live/tok/{0}";
			string url = string.Format(dfurl, key);
			try
			{
				TfaSecResBody res = new TfaSecResBody();
				string html = string.Empty;
				HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
				request.AutomaticDecompression = DecompressionMethods.GZip;

				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				using (Stream stream = response.GetResponseStream())
				using (StreamReader reader = new StreamReader(stream))
				{
					html = reader.ReadToEnd();
				}

				res = JsonConvert.DeserializeObject<TfaSecResBody>(html);
				return res;
			}
			catch
			{
				TfaSecResBody res = new TfaSecResBody();
				this._driver.SwitchTo().Window(this.otherTabHandle);
				this._driver.Navigate().GoToUrl(url);
				string data = this._driver.PageSource;
				string tk = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				res = new TfaSecResBody();
				res.token = tk;
				switchRegTabHandle();
				return res;
			}
		}

		public bool checkCheckPoint()
		{
			try
			{
				this._driver.Navigate().Refresh();
				WaitLoading();
				Delay(500);
				string cppathlink = "facebook.com/checkpoint/";
				if (this._driver.Url.Contains(cppathlink))
				{
					this._check_point = true;
					this.log("Check point!");
					return true;
				}

				return false;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool reset_chrome()
		{
			try
			{
				this.portableResetSetting(this._driver);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				this.log("End!");
				return false;
			}
		}

		public bool LoginM2Fa(string fbdata)
		{
			if (string.IsNullOrEmpty(fbdata))
			{
				this.log("Clone empty!");
				return false;
			}

			try
			{
				this._isNavigate = true;
				this._driver.Manage().Cookies.DeleteAllCookies();
				IJavaScriptExecutor js = (IJavaScriptExecutor)this._driver;
				string jsCleanCache = "window.localStorage.clear(); window.sessionStorage.clear(); window.indexedDB.databases().then((r) => {for (var i = 0; i < r.length; i++) window.indexedDB.deleteDatabase(r[i].name);}).then(() => { return true; });";
				js.ExecuteScript(jsCleanCache);
				Delay(1000);

				this._driver.Navigate().GoToUrl("https://m.facebook.com/");
				WaitLoading();
				Delay(1000);
				if (this._driver.Url.Contains("business.facebook.com"))
				{
					handleUnexpectedAlert();
					Delay(1000);
					this._driver.Navigate().GoToUrl("https://www.facebook.com");
					WaitLoading();
					Delay(1000);
					if (this._driver.Url.Contains("business.facebook.com"))
					{
						this.log("Not Navigate!");
						killChrome();
						_isNavigate = false;
						return false;
					}
				}

				//btn login
				string btnContinueStr = "//button[@name='login']";
				ReadOnlyCollection<IWebElement> btnContinueObjs = this._driver.FindElements(By.XPath(btnContinueStr));
				if (btnContinueObjs.Count() == 0)
				{
					this.log("Not Login Form!");
					_isNavigate = false;
					return false;
				}

				this._driver.Manage().Cookies.DeleteAllCookies();
				js = (IJavaScriptExecutor)this._driver;
				js.ExecuteScript("window.localStorage.clear(); window.sessionStorage.clear();");
				Delay(1000);

				string[] cloneData = fbdata.Split(new char[] { '|' });
				_clone_email = cloneData[0];
				_clone_pass = cloneData[1];
				_2fa_code = cloneData[2];

				this._driver.Navigate().GoToUrl("https://m.facebook.com/");
				WaitLoading();
				Delay(1000);

				string inputAccountName = "m_login_email";
				WaitAjaxLoading(By.Id(inputAccountName));
				IWebElement input = this._driver.FindElement(By.Id(inputAccountName));
				input.Click();
				input.SendKeys(_clone_email);

				string passtag = "m_login_password";
				WaitAjaxLoading(By.Id(passtag));
				input = this._driver.FindElement(By.Id(passtag));
				input.Click();
				input.SendKeys(_clone_pass);

				//btn login
				btnContinueObjs = this._driver.FindElements(By.XPath(btnContinueStr));
				if (btnContinueObjs.Count > 0)
				{
					js.ExecuteScript("arguments[0].click();", btnContinueObjs.First());
					Delay(500);
					WaitLoading();
					Delay(1000);
				}
				else
				{
					return false;
				}

				// approvals_code
				string approvals_codetag = "approvals_code";
				WaitAjaxLoading(By.Id(approvals_codetag), 10);
				ReadOnlyCollection<IWebElement> btnApprovalsCodeObjs = this._driver.FindElements(By.Id(approvals_codetag));
				if (btnApprovalsCodeObjs.Count() > 0)
				{
					TfaSecResBody securityCode = get2faSecurityCode(_2fa_code);
                    if ((securityCode == null) || (string.IsNullOrEmpty(securityCode.token)))
                    {
						this.log("Not 2fa Security Code!");
						return false;
					}
					input = btnApprovalsCodeObjs.First();
					input.Click();
					input.SendKeys(securityCode.token);
					Delay(500);

					// btn login
					btnContinueStr = "checkpointSubmitButton-actual-button";
					WaitAjaxLoading(By.Id(btnContinueStr));
					btnContinueObjs = this._driver.FindElements(By.Id(btnContinueStr));
					if (btnContinueObjs.Count > 0)
					{
						js.ExecuteScript("arguments[0].click();", btnContinueObjs.First());
						Delay(500);
						WaitLoading();
						Delay(1000);
					}
					else
					{
						return false;
					}

					int ChkPageIndex = 1;
					bool hasNext = true;
					while (hasNext && (ChkPageIndex <= 8))
					{
						// btn next
						WaitAjaxLoading(By.Id(btnContinueStr));
						btnContinueObjs = this._driver.FindElements(By.Id(btnContinueStr));
						if (btnContinueObjs.Count > 0)
						{
							js.ExecuteScript("arguments[0].click();", btnContinueObjs.First());
							Delay(500);
							WaitLoading();
							Delay(1000);
						}

						WaitAjaxLoading(By.Id(btnContinueStr));
						btnContinueObjs = this._driver.FindElements(By.Id(btnContinueStr));
						if (btnContinueObjs.Count == 0)
						{
							hasNext = false;
						}
						ChkPageIndex++;
					}

					// a2hsbutton
					string a2hsbuttontag = "a2hsbutton";
					WaitAjaxLoading(By.Id(a2hsbuttontag));
					Delay(500);
					ReadOnlyCollection<IWebElement> a2hsbuttonObjs = this._driver.FindElements(By.Id(a2hsbuttontag));
					if (a2hsbuttonObjs.Count > 0)
					{
						js.ExecuteScript("arguments[0].click();", a2hsbuttonObjs.First());
						Delay(500);
						WaitLoading();
						Delay(1000);
					}

					if (this.checkCheckPoint())
					{
						return false;
					}

					if (this._driver.Url.Contains("facebook.com/login.php"))
					{
						this._check_point = true;
						this.log("Check point!");
						return false;
					}

					_cookie = "";
					foreach (OpenQA.Selenium.Cookie _c in this._driver.Manage().Cookies.AllCookies)
					{
						if (_c.Name == "c_user")
						{
							_clone_uid = _c.Value;
						}
						_cookie = string.Concat(new string[] { _cookie, _c.Name, "=", _c.Value, "; " });
					}
				}
				else
				{
					this.log("Not 2fa approvals_code input!");
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool LoginUP(string fbdata)
		{
			if (string.IsNullOrEmpty(fbdata))
			{
				this.log("Clone empty!");
				return false;
			}

			try
			{
				this._isNavigate = true;
				this._driver.Manage().Cookies.DeleteAllCookies();
				IJavaScriptExecutor js = (IJavaScriptExecutor)this._driver;

				string jsCleanCache = "window.localStorage.clear(); window.sessionStorage.clear(); window.indexedDB.databases().then((r) => {for (var i = 0; i < r.length; i++) window.indexedDB.deleteDatabase(r[i].name);}).then(() => { return true; });";
				js.ExecuteScript(jsCleanCache);
				Delay(1000);

				this._driver.Navigate().GoToUrl("https://www.facebook.com");
				WaitLoading();
				Delay(1000);
				if (this._driver.Url.Contains("business.facebook.com"))
				{
					handleUnexpectedAlert();
					Delay(1000);
					this._driver.Navigate().GoToUrl("https://www.facebook.com");
					WaitLoading();
					Delay(1000);
					if (this._driver.Url.Contains("business.facebook.com"))
					{
						this.log("Not Navigate!");
						killChrome();
						_isNavigate = false;
						return false;
					}
				}

				elmAutoFinAndJsClick("//button[@type='submit' and (@id='accept-cookie-banner-label' or @data-cookiebanner='accept_button')]", 5);

				//btn login
				string btnContinueStr = "//button[@name='login' and @type='submit']";
				ReadOnlyCollection<IWebElement> btnContinueObjs = this._driver.FindElements(By.XPath(btnContinueStr));
				if (btnContinueObjs.Count() == 0)
				{
					this.log("Not Login Form!");
					_isNavigate = false;
					return false;
				}

				this._driver.Manage().Cookies.DeleteAllCookies();
				js = (IJavaScriptExecutor)this._driver;
				js.ExecuteScript("window.localStorage.clear(); window.sessionStorage.clear();");
				Delay(1000);

				string[] cloneData = fbdata.Split(new char[] { '|' });
				_clone_email = cloneData[0];
				_clone_pass = cloneData[1];

				this._driver.Navigate().GoToUrl("https://www.facebook.com");
				WaitLoading();
				Delay(1000);

				string inputAccountName = "email";
				WaitAjaxLoading(By.Id(inputAccountName));
				IWebElement input = this._driver.FindElement(By.Id(inputAccountName));
				input.Click();
				input.SendKeys(_clone_email);

				string passtag = "pass";
				WaitAjaxLoading(By.Id(passtag));
				input = this._driver.FindElement(By.Id(passtag));
				input.Click();
				input.SendKeys(_clone_pass);

				//btn login
				btnContinueObjs = this._driver.FindElements(By.XPath(btnContinueStr));
				if (btnContinueObjs.Count > 0)
				{
					btnContinueObjs.First().Click();
					Delay(500);
					WaitLoading();
					Delay(1000);
				}
				else
				{
					return false;
				}

				if (this.checkCheckPoint())
				{
					return false;
				}

				if (this._driver.Url.Contains("facebook.com/login.php"))
				{
					this._check_point = true;
					this.log("Check point!");
					return false;
				}

				_cookie = "";
				foreach (OpenQA.Selenium.Cookie _c in this._driver.Manage().Cookies.AllCookies)
				{
					if (_c.Name == "c_user")
					{
						_clone_uid = _c.Value;
					}
					_cookie = string.Concat(new string[] { _cookie, _c.Name, "=", _c.Value, "; " });
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				this.log("End!");
				return false;
			}
		}

		public bool LoginVerifyLink(string fbdata)
		{
			if (string.IsNullOrEmpty(fbdata))
			{
				this.log("Clone empty!");
				return false;
			}

			try
			{
				this._isNavigate = true;
				this._driver.Manage().Cookies.DeleteAllCookies();
				IJavaScriptExecutor js = (IJavaScriptExecutor)this._driver;

				string jsCleanCache = "window.localStorage.clear(); window.sessionStorage.clear(); window.indexedDB.databases().then((r) => {for (var i = 0; i < r.length; i++) window.indexedDB.deleteDatabase(r[i].name);}).then(() => { return true; });";
				js.ExecuteScript(jsCleanCache);
				Delay(1000);

				this._driver.Navigate().GoToUrl("https://www.facebook.com");
				WaitLoading();
				Delay(1000);
				if (this._driver.Url.Contains("business.facebook.com"))
				{
					handleUnexpectedAlert();
					Delay(1000);
					this._driver.Navigate().GoToUrl("https://www.facebook.com");
					WaitLoading();
					Delay(1000);
					if (this._driver.Url.Contains("business.facebook.com"))
					{
						this.log("Not Navigate!");
						killChrome();
						_isNavigate = false;
						return false;
					}
				}

				//btn login
				string btnContinueStr = "//button[@name='login' and @type='submit']";
				ReadOnlyCollection<IWebElement> btnContinueObjs = this._driver.FindElements(By.XPath(btnContinueStr));
				if (btnContinueObjs.Count() == 0)
				{
					this.log("Not Login Form!");
					_isNavigate = false;
					return false;
				}

				this._driver.Manage().Cookies.DeleteAllCookies();
				js = (IJavaScriptExecutor)this._driver;
				js.ExecuteScript(jsCleanCache);
				Delay(1000);

				string[] cloneData = fbdata.Split(new char[] { '|' });
				_clone_uid = cloneData[0];
				this.log("UID: " + _clone_uid);
				_clone_email = cloneData[0];
				_clone_pass = cloneData[1];
				this.log("PASS: " + _clone_pass);
				string verifyLink = cloneData[3];

                if (string.IsNullOrEmpty(verifyLink))
                {
					this.log("Not verifyLink!");
					return false;
                }

				this.log("VerifyLink: " + verifyLink);

				// cookiebanner
				string cookieBanner = "button[data-cookiebanner='accept_button']";
				WaitAjaxLoading(By.CssSelector(cookieBanner));
				Delay(500);
				IWebElement _acceptBt = this.getElement(By.CssSelector(cookieBanner));
				bool flag2 = this.isValid(_acceptBt);
				if (flag2)
				{
					_acceptBt.Click();
					WaitLoading();
					Delay(1000);
				}

				this._driver.Navigate().GoToUrl(verifyLink);
				WaitLoading();
				Delay(1000);

				// cookiebanner
				WaitAjaxLoading(By.CssSelector(cookieBanner));
				Delay(500);
				_acceptBt = this.getElement(By.CssSelector(cookieBanner));
				flag2 = this.isValid(_acceptBt);
				if (flag2)
				{
					_acceptBt.Click();
					WaitLoading();
					Delay(1000);
				}

				bool login = false;
				if (this._driver.Url.Contains("facebook.com/home.php") || this._driver.Url.Contains("privacy/consent_framework/prompt") || this._driver.Url.Contains("facebook.com/gettingstarted/") || this._driver.Url.Contains("privacy/consent/user_cookie_choice") || this._driver.Url.Contains("facebook.com/?sk=welcome"))
				{
					login = true;
				}

                if (!login)
                {
					string passtag = "pass";
					WaitAjaxLoading(By.Id(passtag));
					IWebElement input = this._driver.FindElement(By.Id(passtag));
					input.Click();
					input.SendKeys(_clone_pass);

					//btn login
					btnContinueObjs = this._driver.FindElements(By.XPath(btnContinueStr));
					if (btnContinueObjs.Count > 0)
					{
						btnContinueObjs.First().Click();
						Delay(500);
						WaitLoading();
						Delay(1000);
					}
					else
					{
						return false;
					}

					if (this.checkCheckPoint())
					{
						return false;
					}

					if (this._driver.Url.Contains("facebook.com/login.php"))
					{
						this._check_point = true;
						this.log("Check point!");
						return false;
					}

					login = true;
				}

				apiWelcome();

				btnContinueStr = "//div[@role='dialog']//div[@role='button' and not(@aria-disabled='true')]";
				WaitAjaxLoading(By.XPath(btnContinueStr), 7);
				btnContinueObjs = this._driver.FindElements(By.XPath(btnContinueStr));
				if (btnContinueObjs.Count() > 0)
				{
                    if (this.isValid(btnContinueObjs.Last()))
                    {
						try
						{
							btnContinueObjs.First().Click();
							WaitLoading();
							Delay(1000);
						}
						catch
						{

						}
					}
				}

				_cookie = "";
				foreach (OpenQA.Selenium.Cookie _c in this._driver.Manage().Cookies.AllCookies)
				{
					if (_c.Name == "c_user")
					{
						_clone_uid = _c.Value;
					}
					_cookie = string.Concat(new string[] { _cookie, _c.Name, "=", _c.Value, "; " });
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				this.log("End!");
				return false;
			}
		}

		public bool Login2Fa(string fbdata)
		{
			if (string.IsNullOrEmpty(fbdata))
			{
				this.log("Clone empty!");
				return false;
			}

			try
			{
				if (!checkLiveAsync().Result)
				{
					this.log("Facebook not Live!");
					return false;
				}

				this._isNavigate = true;
				this._driver.Manage().Cookies.DeleteAllCookies();
				IJavaScriptExecutor js = (IJavaScriptExecutor)this._driver;

				string jsCleanCache = "window.localStorage.clear(); window.sessionStorage.clear(); window.indexedDB.databases().then((r) => {for (var i = 0; i < r.length; i++) window.indexedDB.deleteDatabase(r[i].name);}).then(() => { return true; });"; 
				js.ExecuteScript(jsCleanCache);
				Delay(1000);

				this._driver.Navigate().GoToUrl("https://www.facebook.com");
				WaitLoading();
				Delay(1000);
				if (this._driver.Url.Contains("business.facebook.com"))
				{
					handleUnexpectedAlert();
					Delay(1000);
					this._driver.Navigate().GoToUrl("https://www.facebook.com");
					WaitLoading();
					Delay(1000);
					if (this._driver.Url.Contains("business.facebook.com"))
					{
						this.log("Not Navigate!");
						killChrome();
						_isNavigate = false;
						return false;
					}
				}

				//btn login
				string btnContinueStr = "//button[@name='login' and @type='submit']";
				ReadOnlyCollection<IWebElement> btnContinueObjs = this._driver.FindElements(By.XPath(btnContinueStr));
				if (btnContinueObjs.Count() == 0)
                {
					this.log("Not Login Form!");
					_isNavigate = false;
					return false;
				}

				this._driver.Manage().Cookies.DeleteAllCookies();
				js = (IJavaScriptExecutor)this._driver;
				js.ExecuteScript("window.localStorage.clear(); window.sessionStorage.clear();");
				Delay(1000);

				string[] cloneData = fbdata.Split(new char[] { '|' });
				_clone_email = cloneData[0];
				_clone_pass = cloneData[1];
				_2fa_code = cloneData[2];

				this._driver.Navigate().GoToUrl("https://www.facebook.com");
				WaitLoading();
				Delay(1000);

				// cookiebanner
				string cookieBanner = "button[data-cookiebanner='accept_button']";
				WaitAjaxLoading(By.CssSelector(cookieBanner));
				Delay(500);
				IWebElement _acceptBt = this.getElement(By.CssSelector(cookieBanner));
				bool flag2 = this.isValid(_acceptBt);
				if (flag2)
				{
					_acceptBt.Click();
				}

				string inputAccountName = "email";
				WaitAjaxLoading(By.Id(inputAccountName));
				IWebElement input = this._driver.FindElement(By.Id(inputAccountName));
				input.Click();
				input.SendKeys(_clone_email);

				string passtag = "pass";
				WaitAjaxLoading(By.Id(passtag));
				input = this._driver.FindElement(By.Id(passtag));
				input.Click();
				input.SendKeys(_clone_pass);

				if (!elmAutoFinAndJsClick(btnContinueStr))
				{
					this.log("Not Submit Btn!");
					return false;
				}

				// approvals_code
				string approvals_codetag = "approvals_code";
				WaitAjaxLoading(By.Id(approvals_codetag), 10);
				ReadOnlyCollection<IWebElement> btnApprovalsCodeObjs = this._driver.FindElements(By.Id(approvals_codetag));
				if (btnApprovalsCodeObjs.Count() > 0)
				{
					TfaSecResBody securityCode = get2faSecurityCode(_2fa_code);
					if ((securityCode == null) || (string.IsNullOrEmpty(securityCode.token)))
					{
						this.log("Not 2fa Security Code!");
						return false;
					}
					input = btnApprovalsCodeObjs.First();
					input.Click();
					input.SendKeys(securityCode.token);
					Delay(500);

					//btn login
					btnContinueStr = "//*[@id='checkpointSubmitButton']";
					if (!elmAutoFinAndJsClick(btnContinueStr))
					{
						this.log("Not Submit Btn!");
						return false;
					}
					WaitLoading();
					Delay(1000);
					elmAutoFinAndJsClick(btnContinueStr);

					if (this.checkCheckPoint())
					{
                        if (elmAutoFinAndJsClick(btnContinueStr))
                        {
							WaitLoading();
							Delay(1000);
							elmAutoFinAndJsClick(btnContinueStr);

							if (this.checkCheckPoint())
							{
								return false;
							}
						}
                        else
                        {
							return false;
						}
					}

					if (this._driver.Url.Contains("facebook.com/login.php"))
					{
						this._check_point = true;
						this.log("Check point!");
						return false;
					}

					_cookie = "";
					foreach (OpenQA.Selenium.Cookie _c in this._driver.Manage().Cookies.AllCookies)
					{
						if (_c.Name == "c_user")
						{
							_clone_uid = _c.Value;
						}
						_cookie = string.Concat(new string[] { _cookie, _c.Name, "=", _c.Value, "; " });
					}

                    if (string.IsNullOrEmpty(_clone_uid))
                    {
						return false;
					}
				}
				else
				{
					this.log("Not 2fa approvals_code input!");
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				this.log("End!");
				return false;
			}
		}

		public bool LoginCookie(string fbdata)
		{
            if (string.IsNullOrEmpty(fbdata))
            {
				this.log("Clone empty!");
				return false;
			}

			try
			{
				this._isNavigate = true;
				this._driver.Manage().Cookies.DeleteAllCookies();
				IJavaScriptExecutor js = (IJavaScriptExecutor)this._driver;
				string jsCleanCache = "window.localStorage.clear(); window.sessionStorage.clear(); window.indexedDB.databases().then((r) => {for (var i = 0; i < r.length; i++) window.indexedDB.deleteDatabase(r[i].name);}).then(() => { return true; });";
				js.ExecuteScript(jsCleanCache);
				Delay(1000);

				this._driver.Navigate().GoToUrl("https://www.facebook.com");
				WaitLoading();
				Delay(1000);
				if (this._driver.Url.Contains("business.facebook.com"))
				{
					handleUnexpectedAlert();
					Delay(1000);
					this._driver.Navigate().GoToUrl("https://www.facebook.com");
					WaitLoading();
					Delay(1000);
					if (this._driver.Url.Contains("business.facebook.com"))
					{
						this.log("Not Navigate!");
						killChrome();
						_isNavigate = false;
						return false;
					}
				}

				this._driver.Manage().Cookies.DeleteAllCookies();
				js = (IJavaScriptExecutor)this._driver;
				js.ExecuteScript("window.localStorage.clear(); window.sessionStorage.clear();");
				Delay(1000);

				string[] cloneData = fbdata.Split(new char[] { '|' });
				_clone_email = cloneData[0];
				_clone_pass = cloneData[1];
				_2fa_code = cloneData[2];
				string cookie = cloneData.Last();
				string[] arrayCookie = cookie.Split(new char[] { ';' });
				for (int i = 0; i < arrayCookie.Length; i++)
				{
					string[] item = arrayCookie[i].Split(new char[] { '=' });
					if (item.Count<string>() > 1)
					{
						DateTime time = DateTime.Now.AddMinutes(50);
						OpenQA.Selenium.Cookie cookieItem = new OpenQA.Selenium.Cookie(item[0].Trim(), item[1].Trim(), ".facebook.com", "/", time);
						this._driver.Manage().Cookies.AddCookie(cookieItem);
					}
				}

				if (this.checkCheckPoint())
				{
					return false;
				}

				if (this._driver.Url.Contains("facebook.com/login.php"))
				{
					this._check_point = true;
					this.log("Check point!");
					return false;
				}

				_cookie = "";
				foreach (OpenQA.Selenium.Cookie _c in this._driver.Manage().Cookies.AllCookies)
				{
					if (_c.Name == "c_user")
					{
						_clone_uid = _c.Value;
					}
					_cookie = string.Concat(new string[] { _cookie, _c.Name, "=", _c.Value, "; " });
				}

				this._time_zone = (String)js.ExecuteScript("return Intl.DateTimeFormat().resolvedOptions().timeZone;");

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				this.log("End!");
				return false;
			}
		}

		public string getBirthDay()
		{
			try
			{
				switchRegTabHandle();

				string day = "";
				string month = "";
				string year = "";

				string url = "https://www.facebook.com/" + _clone_uid + "?sk=about_contact_and_basic_info";
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string appSectionFeedKey = Regex.Match(getdata, "\"appSectionFeedKey\":\"(.*?)\"").NextMatch().Groups[1].Value;
				if (string.IsNullOrEmpty(appSectionFeedKey))
				{
					Thread.Sleep(3000);
					appSectionFeedKey = Regex.Match(getdata, "\"appSectionFeedKey\":\"(.*?)\"").NextMatch().Groups[1].Value;
				}

				if (string.IsNullOrEmpty(appSectionFeedKey))
				{
					return null;
				}

				string collectionToken = Regex.Match(getdata, "\"collectionToken\":\"(.*?)\"").NextMatch().Groups[1].Value;
				string rawSectionToken = Regex.Match(getdata, "\"rawSectionToken\":\"(.*?)\"").NextMatch().Groups[1].Value;
				string sectionToken = Regex.Match(getdata, "\"sectionToken\":\"(.*?)\"").NextMatch().Groups[1].Value;

				url = "https://www.facebook.com/api/graphql/";
				string datarq = string.Concat(new string[]
				{
					"av=",
					_clone_uid,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHxqU5a5Q1ryUqxenFw9uu2i5U4e0ykdwSwAyUco5S3O2S7o762S1DwUx60p-0LVEtwMw65xO2O1Vwwwqo465o-cw5MKdwGxu782lwv89kbxS2218wc61axe3S1lwlE-UqwsUkxe2GewyDwkUtxGm2SUbElxm3y11xfxmu3W3y1MBx_y88E3qxWm2Sq2-azo2NwwwOg2cwMwiU8UdUcobUak1xwJwxw&__csr=ghPlHPWb998GaOlqmPilOOiOlivtuNrnAkISCy4W9jqmyntAGhtkqnFnqlmI_FejABl7UyuQ8lumiFaHAypkHBzWK9FaQX9DhajVpu9x6qEkKvhoybBUyuaDzVubzk48pwDzEGl2qypFrwBDDhFoO4bUjKi5EGm7895wCxq6oK7okwJDx29yEOhe9UC49H-2ry8hwpU9EaEb846ao4a362ecwJwd61TAwKwxxW1jxO2y9wuE4O6Evwzw8WewQwio5G5E3Tw0q0o02zlyU0zx0dq04LU20w17100wXwf25o0vtw2SOw1ke03sq05Lk0ME0DS0hW0f4w50wDw&__req=p&__hs=19314.HYP%3Acomet_pkg.2.1.0.2.1&dpr=1&__ccg=EXCELLENT&__rev=1006628650&__s=2woyyg%3A8tkyfz%3Ajqy6m5&__hsi=7167450460360415936&__comet_req=15&fb_dtsg=",
					token1,
					"&jazoest=25633&lsd=",
					token2,
					"&__spin_r=1006628650&__spin_b=trunk&__spin_t=1668802104&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=ProfileCometAboutAppSectionQuery&variables=%7B%22UFI2CommentsProvider_commentsKey%22%3A%22ProfileCometAboutAppSectionQuery%22%2C%22appSectionFeedKey%22%3A%22",
					appSectionFeedKey,
					"%22%2C%22collectionToken%22%3A%22",
					collectionToken,
					"%22%2C%22pageID%22%3A%22",
					_clone_uid,
					"%22%2C%22rawSectionToken%22%3A%22",
					rawSectionToken,
					"%22%2C%22scale%22%3A1%2C%22sectionToken%22%3A%22",
					sectionToken,
					"%22%2C%22showReactions%22%3Atrue%2C%22userID%22%3A%22",
					_clone_uid,
					"%22%2C%22__relay_internal__pv__FBReelsDisableBackgroundrelayprovider%22%3Afalse%7D&server_timestamps=true&doc_id=5671402742952533"
				});

				string postdata = this.fetchApi(url, "POST", datarq, "ProfileCometAboutAppSectionQuery", token2, this._driver.Url);
				if (string.IsNullOrEmpty(postdata))
				{
					return null;
				}

				day = Regex.Match(postdata, "\"day\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(day))
				{
					day = "00";
				}
				month = Regex.Match(postdata, "\"month\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(month))
				{
					month = "00";
				}
				year = Regex.Match(postdata, "\"year\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(year))
				{
					year = "0000";
				}
				return day + "/" + month + "/" + year;
			}
			catch (Exception ex)
			{
				return null;
			}
		}

		public bool receiveBmAPI(BmLink bmLink)
		{
			if (string.IsNullOrEmpty(bmLink.link))
			{
				this.log("BM Link empty!");
				return false;
			}

			try
			{
				switchBusinessTabHandle();

				rcBm_first_name = "Nini";
				rcBm_last_name = new Random().Next(100000, 999999).ToString();

				this._driver.Navigate().GoToUrl(bmLink.link);
				WaitLoading();
				Delay(500);

				if (!this._driver.Url.Contains("invitation/?token="))
				{
					this.log("URL IVALID!");
					return false;
				}

				Uri theUri = new Uri(this._driver.Url);
				string invitationToken = HttpUtility.ParseQueryString(theUri.Query).Get("token");
				this.log("Token: " + invitationToken);

				string getdata = this._driver.PageSource;
				string fb_dtsg = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[]
				{
					"first_name=",
					rcBm_first_name,
					"&last_name=",
					rcBm_last_name,
					"&invitation_token=",
					invitationToken,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBwjbgydwn8K2WnFwn84a2i5U4e1Fx-ewSwMxW0DUS2S4o1j8hwem0nCq1ewcG0KEswIwuo662y1nzU1vrzo5-1uwbe2l0Fwwwi85W0Mofogw9KfxW0D83mwkE5G0zE5W0HUvw6iyES0gq0Lo&__csr=&__req=8&__hs=19065.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=UNKNOWN&__rev=1005189989&__s=xdvxi4%3A8vlp4y%3A9hyee0&__hsi=7074888667330864355-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=21923&lsd=",
					lsd,
					"&__spin_r=1005189989&__spin_b=trunk&__spin_t=1647250881&__jssesw=1&ajax_password=",
					_clone_pass,
					"&confirmed=1"
				});

				string resData = this.fetchApi("https://business.facebook.com/business/invitation/login/", "POST", body, "", lsd, this._driver.Url);
				string errorSummary = Regex.Match(resData, "\"errorSummary\":\"(.*?)\"").Groups[1].Value;
				if (!string.IsNullOrEmpty(errorSummary))
				{
					this.log(resData);
					return false;
				}

				_bm_id = bmLink.bm_id;
				// pageID = bmLink.page_id;
				this.log("BM: " + _bm_id);
				this.getBmInfo();
				// this.getPageInfo();
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.ToString());
				return false;
			}
		}

		public bool receiveBm(string bmLink)
		{
			if (string.IsNullOrEmpty(bmLink))
			{
				this.log("BM Link empty!");
				return false;
			}

			try
			{
				switchBusinessTabHandle();

				this._driver.Navigate().GoToUrl(bmLink);
				WaitLoading();
				Delay(1000);

				if (!this._driver.Url.Contains("invitation/?token="))
				{
					this.log("URL IVALID!");
					return false;
				}

				Uri theUri = new Uri(this._driver.Url);
				string invitationToken = HttpUtility.ParseQueryString(theUri.Query).Get("token");

				string getdata = this._driver.PageSource;
				string fb_dtsg = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				rcBm_first_name = "Nini";
				rcBm_last_name = new Random().Next(100000, 999999).ToString();

				string body = string.Concat(new string[]
				{
					"first_name=",
					rcBm_first_name,
					"&last_name=",
					rcBm_last_name,
					"&invitation_token=",
					invitationToken,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBwjbgydwn8K2WnFwn84a2i5U4e1Fx-ewSwMxW0DUS2S4o1j8hwem0nCq1ewcG0KEswIwuo662y1nzU1vrzo5-1uwbe2l0Fwwwi85W0Mofogw9KfxW0D83mwkE5G0zE5W0HUvw6iyES0gq0Lo&__csr=&__req=8&__hs=19065.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=UNKNOWN&__rev=1005189989&__s=xdvxi4%3A8vlp4y%3A9hyee0&__hsi=7074888667330864355-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=21923&lsd=",
					lsd,
					"&__spin_r=1005189989&__spin_b=trunk&__spin_t=1647250881&__jssesw=1&ajax_password=",
					_clone_pass,
					"&confirmed=1"
				});

				string resData = this.fetchApi("https://business.facebook.com/business/invitation/login/", "POST", body, "", lsd, this._driver.Url);
				string errorSummary = Regex.Match(resData, "\"errorSummary\":\"(.*?)\"").Groups[1].Value;
				if (!string.IsNullOrEmpty(errorSummary))
				{
					this.log(resData);
					return false;
				}

				this._driver.Navigate().GoToUrl("https://business.facebook.com/settings/ad-accounts");
				WaitLoading();
				Delay(3000);

				theUri = new Uri(this._driver.Url);
				_bm_id = HttpUtility.ParseQueryString(theUri.Query).Get("business_id");
				this.log("BM: " + _bm_id);
				this.getBmInfo();
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private string getTempMailOrgEmail()
		{
			try
			{
				// SwitchTo Mail
				this._driver.SwitchTo().Window(this.shopifyHandle);
				this._driver.Navigate().GoToUrl("https://temp-mail.org/en/");
				WaitLoading();
				Delay(500);

				string email = "";
				string emailInputstr = "//input[@id='mail']";
				WaitAjaxLoading(By.XPath(emailInputstr));
				ReadOnlyCollection<IWebElement> emailInputObj = this._driver.FindElements(By.XPath(emailInputstr));
				if (emailInputObj.Count > 0)
				{
					email = emailInputObj.First().GetAttribute("value");
				}

				int ChkLoadingIndex = 1;
				while (email.Contains("Loading") && (ChkLoadingIndex <= 5))
				{
					Delay(3000);
					emailInputObj = this._driver.FindElements(By.XPath(emailInputstr));
					if (emailInputObj.Count > 0)
					{
						email = emailInputObj.First().GetAttribute("value");
					}
					ChkLoadingIndex++;
				}

				switchRegTabHandle();
				this.log("Email: " + email);
				return email;
			}
			catch (Exception ex)
			{
				this.log(ex);
				switchRegTabHandle();
				return null;
			}
		}

		private string getEmail(string source = "mail.tm")
		{
			try
			{
				this.log("Get Email!");
				this.log("Nguồn: " + source);
				if (source == "emailfake.com")
				{
					return getEmailFakeComEmail();
				}

				if ((source == "mail.tm") || (source == "mail.gw"))
				{
					return getMailTMEmailAsync(source).Result;
				}

				if (source == "temp-mail.org")
				{
					return getTempMailOrgEmail();
				}

				if (source == "10minutemail.net")
				{
					tenMinuteMail = new TenMinuteMail();
					return tenMinuteMail.GetEmailAddress().Result;
				}
				return "";
			}
			catch (Exception ex)
			{
				this.log(ex);
				switchRegTabHandle();
				return null;
			}
		}

		private async Task<string> getMailTMEmailAsync(string source = "mail.tm")
		{
			try
			{
				mailClient = new MailClient(new Uri("https://api." + source + "/"));
				string domain = await mailClient.GetFirstAvailableDomainName();
				string _id = this._listKeyword[new Random(Guid.NewGuid().GetHashCode()).Next(0, this._listKeyword.Count)] + this._listKeyword[new Random(Guid.NewGuid().GetHashCode()).Next(0, this._listKeyword.Count)] + this.randomNumber(5);
				_id = _id.Replace(" ", "");
				await mailClient.Register(_id, domain, mailPass);
				this.log("Mail: " + _id + "@" + domain);
				this.log("Pass: " + mailPass);
				return _id + "@" + domain;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private string getEmailFakeComEmail()
		{
			try
			{
				// SwitchTo Mail
				this._driver.SwitchTo().Window(this.shopifyHandle);
				this._driver.Navigate().GoToUrl("https://emailfake.com/");
				Delay(500);

				// UserName
				string userNameInputstr = "//input[@id='userName']";
				WaitAjaxLoading(By.XPath(userNameInputstr));
				ReadOnlyCollection<IWebElement> userNameInputObj = this._driver.FindElements(By.XPath(userNameInputstr));
				if (userNameInputObj.Count == 0)
				{
					this.log("Not UserName!");
					switchRegTabHandle();
				}

				string username = userNameInputObj.First().GetAttribute("value");
				bool loading = string.IsNullOrEmpty(username);
				int ChkLoadingIndex = 1;
				while (loading && (ChkLoadingIndex <= 5))
				{
					Delay(2000);
					userNameInputObj = this._driver.FindElements(By.XPath(userNameInputstr));
					if (userNameInputObj.Count > 0)
					{
						username = userNameInputObj.First().GetAttribute("value");
						loading = string.IsNullOrEmpty(username);
					}
					ChkLoadingIndex++;
				}
				Delay(1000);

				if (loading)
				{
					this.log("Not Get UserName!");
					switchRegTabHandle();
					return null;
				}

				// Domain
				string domainInputstr = "//input[@id='domainName2']";
				WaitAjaxLoading(By.XPath(domainInputstr));
				ReadOnlyCollection<IWebElement> domainInputObj = this._driver.FindElements(By.XPath(domainInputstr));
				if (domainInputObj.Count == 0)
				{
					this.log("Not Domain!");
					switchRegTabHandle();
					return null;
				}

				string domain = domainInputObj.First().GetAttribute("value");
				loading = string.IsNullOrEmpty(domain);
				ChkLoadingIndex = 1;
				while (loading && (ChkLoadingIndex <= 5))
				{
					Delay(2000);
					domainInputObj = this._driver.FindElements(By.XPath(domainInputstr));
					if (domainInputObj.Count > 0)
					{
						domain = domainInputObj.First().GetAttribute("value");
						loading = string.IsNullOrEmpty(domain);
					}
					ChkLoadingIndex++;
				}
				Delay(1000);

				if (loading)
				{
					this.log("Not Get Domain!");
					switchRegTabHandle();
					return null;
				}

				string email = username + "@" + domain;
				this.log("Email: " + email);

				switchRegTabHandle();
				return email;
			}
			catch (Exception ex)
			{
				this.log(ex);
				switchRegTabHandle();
				return null;
			}
		}

		private string GetSubDomain(Uri url)
		{

			if (url.HostNameType == UriHostNameType.Dns)
			{
				string host = url.Host;
				var nodes = host.Split('.');
				int startNode = 0;
				if (nodes[0] == "www") startNode = 1;
				return nodes[startNode];
			}

			return null;
		}

		private string getCloneEmailVerifyLink(string source = "mail.tm")
		{
			try
			{
				if (source == "emailfake.com")
				{
					return "";
				}

				if ((source == "mail.tm") || (source == "mail.gw"))
				{
					return getMailTMCloneVerifyLinkAsync().Result;
				}

				if (source == "10minutemail.net")
				{
					return get10MinuteMailCloneVerifyLinkAsync();
				}
				return "";
			}
			catch (Exception ex)
			{
				this.log(ex);
				switchRegTabHandle();
				return null;
			}
		}

		private string get10MinuteMailCloneVerifyLinkAsync()
		{
			try
			{
				string link = "";
				int ChkLoadingIndex = 1;
				while (string.IsNullOrEmpty(link) && (ChkLoadingIndex <= 6))
				{
					Delay(5000);
					MailContent[] messages = tenMinuteMail.GetEmails().Result;
					foreach (MailContent item in messages)
					{
						if (item.From.Contains("security@facebookmail.com"))
						{
							string htmls = item.Html.First();
							htmls = HttpUtility.HtmlDecode(htmls);
							Match rgMatch = Regex.Match(htmls, @"https:\/\/www\.facebook\.com\/confirmcontact.php([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
							if (rgMatch != null && rgMatch.Groups.Count > 1)
							{
								link = @"https://www.facebook.com/confirmcontact.php" + rgMatch.Groups[1].Value;
								break;
							}
						}
					}
					ChkLoadingIndex++;
				}

				return link;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private async Task<string> getMailTMCloneVerifyLinkAsync()
		{
			try
			{
				MessageInfo[] messages = await mailClient.GetAllMessages();
				string link = "";
				if (messages.Length > 0)
				{
					foreach (MessageInfo item in messages)
					{
						if (item.From.Name == "Facebook")
						{
							MessageSource source = await mailClient.GetMessageSource(item.Id);
							string rawMessage = source.Data;
							byte[] byteArray = Encoding.UTF8.GetBytes(rawMessage);
							MemoryStream stream = new MemoryStream(byteArray);
							MimeParser parser = new MimeParser(stream, MimeFormat.Entity);
							MimeMessage mimeMessage = parser.ParseMessage();
							string htmls = HttpUtility.HtmlDecode(mimeMessage.HtmlBody);
							Match rgMatch = Regex.Match(htmls, @"https:\/\/www\.facebook\.com\/confirmcontact.php([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
							if (rgMatch != null && rgMatch.Groups.Count > 1)
							{
								link = @"https://www.facebook.com/confirmcontact.php" + rgMatch.Groups[1].Value;
								break;
							}
						}
					}
				}

				int ChkLoadingIndex = 1;
				while (string.IsNullOrEmpty(link) && (ChkLoadingIndex <= 6))
				{
					Delay(5000);
					messages = await mailClient.GetAllMessages();
					foreach (MessageInfo item in messages)
					{
						if (item.From.Name == "Facebook")
						{
							MessageSource source = await mailClient.GetMessageSource(item.Id);
							string rawMessage = source.Data;
							byte[] byteArray = Encoding.UTF8.GetBytes(rawMessage);
							MemoryStream stream = new MemoryStream(byteArray);
							MimeParser parser = new MimeParser(stream, MimeFormat.Entity);
							MimeMessage mimeMessage = parser.ParseMessage();
							string htmls = HttpUtility.HtmlDecode(mimeMessage.HtmlBody);
							Match rgMatch = Regex.Match(htmls, @"https:\/\/www\.facebook\.com\/confirmcontact.php([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
							if (rgMatch != null && rgMatch.Groups.Count > 1)
							{
								link = @"https://www.facebook.com/confirmcontact.php" + rgMatch.Groups[1].Value;
								break;
							}
						}
					}
					ChkLoadingIndex++;
				}

				return link;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private bool RegisterFetchApi(string[] _tmpNameArr, string _randomEmail, string email_confirmation, string _passAcc, string ignore)
		{
			try
			{
				ReadOnlyCollection<IWebElement> jazoestElms = null;
				string jazoestElmStr = "", jazoest = "", lsd = "", ns = "", resData = "", error_code = "", body = "", fUrl = "";

				// jazoest
				jazoestElmStr = "//form[@id='reg']//input[@type='hidden' and @name='jazoest']";
				jazoestElms = this._driver.FindElements(By.XPath(jazoestElmStr));
				if (jazoestElms.Count == 0)
				{
					this.log("NOT jazoest !");
					return false;
				}
				jazoest = jazoestElms.First().GetAttribute("value");
				// this.log("jazoest: " + jazoest);

				// lsd
				string lsdElmStr = "//form[@id='reg']//input[@type='hidden' and @name='lsd']";
				ReadOnlyCollection<IWebElement> lsdElms = this._driver.FindElements(By.XPath(lsdElmStr));
				if (lsdElms.Count == 0)
				{
					this.log("NOT lsd !");
					return false;
				}
				lsd = lsdElms.First().GetAttribute("value");
				// this.log("lsd: " + lsd);

				// ns
				string nsElmStr = "//form[@id='reg']//input[@type='hidden' and @name='ns']";
				ReadOnlyCollection<IWebElement> nsElms = this._driver.FindElements(By.XPath(nsElmStr));
				if (nsElms.Count == 0)
				{
					this.log("NOT ns !");
					return false;
				}
				ns = nsElms.First().GetAttribute("value");
				// this.log("ns: " + ns);

				// ri
				string riElmStr = "//form[@id='reg']//input[@type='hidden' and @name='ri']";
				ReadOnlyCollection<IWebElement> riElms = this._driver.FindElements(By.XPath(riElmStr));
				if (riElms.Count == 0)
				{
					this.log("NOT ri !");
					return false;
				}
				string ri = riElms.First().GetAttribute("value");
				// this.log("ri: " + ri);

				// locale
				string localeElmStr = "//form[@id='reg']//input[@type='hidden' and @name='locale']";
				ReadOnlyCollection<IWebElement> localeElms = this._driver.FindElements(By.XPath(localeElmStr));
				if (localeElms.Count == 0)
				{
					this.log("NOT locale !");
					return false;
				}
				string locale = localeElms.First().GetAttribute("value");
				// this.log("locale: " + locale);

				// reg_instance
				string reg_instanceElmStr = "//form[@id='reg']//input[@type='hidden' and @name='reg_instance']";
				ReadOnlyCollection<IWebElement> reg_instanceElms = this._driver.FindElements(By.XPath(reg_instanceElmStr));
				if (reg_instanceElms.Count == 0)
				{
					this.log("NOT reg_instance !");
					return false;
				}
				string reg_instance = reg_instanceElms.First().GetAttribute("value");
				// this.log("reg_instance: " + reg_instance);

				// captcha_persist_data
				string captchaElmStr = "//form[@id='reg']//input[@type='hidden' and @name='captcha_persist_data']";
				ReadOnlyCollection<IWebElement> captchaElms = this._driver.FindElements(By.XPath(captchaElmStr));
				if (captchaElms.Count == 0)
				{
					this.log("NOT captcha !");
					return false;
				}
				string captcha = captchaElms.First().GetAttribute("value");
				// this.log("captcha: " + captcha);

				Random _random = new Random(Guid.NewGuid().GetHashCode());

				body = string.Concat(new string[]
				{
					"jazoest=",
					jazoest,
					"&lsd=",
					lsd,
					"&firstname=",
					_tmpNameArr[0],
					"&lastname=",
					_tmpNameArr[1],
					"&reg_email__=",
					_randomEmail,
					"&reg_email_confirmation__=",
					email_confirmation,
					"&reg_passwd__=",
					_passAcc,
					"&birthday_day=",
					_random.Next(10, 28).ToString(),
					"&birthday_month=",
					_random.Next(1, 12).ToString(),
					"&birthday_year=",
					_random.Next(1970, 1996).ToString(),
					"&birthday_age=&did_use_age=false&sex=",
					_random.Next(1, 2).ToString(),
					"&preferred_pronoun=&custom_gender=&referrer=&asked_to_login=0&use_custom_gender=&terms=on&ns=",
					ns,
					"&ri=",
					ri,
					"&action_dialog_shown=&ignore=",
					ignore,
					"&locale=",
					locale,
					"&reg_instance=",
					reg_instance,
					"&captcha_persist_data=",
					captcha,
					"&captcha_response=&__user=0&__a=1&__dyn=7xe6FomK36Q5E5ObwKBWo5O12wAxu13wqovzEdEc8uw9-3K4o1j8hwem0nCq1ewcG0KEswaq0yE5ufw5ZKdwnU1oU884y0lW0L8uw9O0RE2Jw8W1uwc-0lK3qaw4kwbS&__csr=&__req=f&__hs=19041.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005089944&__s=hpqjri%3Af201xn%3Arxlt9o&__hsi=7065950660809517706-0&__comet_req=0&__spin_r=1005089944&__spin_b=trunk&__spin_t=1645169840"
				});

				Uri theUri = new Uri(this._driver.Url);
				string subDomain = this.GetSubDomain(theUri);
				fUrl = "https://www.facebook.com/ajax/register.php";
				if (subDomain != "facebook")
				{
					fUrl = "https://" + subDomain + ".facebook.com/ajax/register.php";
				}
				resData = this.fetchApi(fUrl, "POST", body, "", lsd, this._driver.Url);
				if (resData == null)
				{
					resData = this.fetchApi(fUrl, "POST", body, "", lsd, this._driver.Url);
				}

				if (resData == null)
				{
					this.log("NOT Reg !");
					return false;
				}
				error_code = Regex.Match(resData, "\"error_code\":(.*?),").Groups[1].Value;
				if (!string.IsNullOrEmpty(error_code))
				{
					this.log("error_code: " + error_code);
					if (error_code == "1351050")
					{
						this.log("Hãy chờ làm lại ...");
						bool erroring = true;
						int ChkErrIndex = 1;
						while (erroring && (ChkErrIndex <= 5))
						{
							Delay(20000);
							this.log("Làm lại ...");
							resData = this.fetchApi(fUrl, "POST", body, "", lsd, this._driver.Url);
							if (resData != null)
							{
								error_code = Regex.Match(resData, "\"error_code\":(.*?),").Groups[1].Value;
								if (string.IsNullOrEmpty(error_code))
								{
									erroring = false;
								}
								else
								{
									if (error_code != "1351050")
									{
										erroring = false;
									}
								}
							}

							ChkErrIndex++;
						}
					}
				}

				if (!string.IsNullOrEmpty(error_code))
				{
					this.log("error_code: " + error_code);
					this.log("Ko đc!");
					string __html = Regex.Match(resData, "\"__html\":\"(.*?)\"").Groups[1].Value;
					this.log("MSG: " + __html);
					return false;
				}

				Delay(1000);
				WaitLoading();
				Delay(500);

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private bool RegisterAuto(string[] _tmpNameArr, string _randomEmail, string email_confirmation, string _passAcc)
		{
			try
			{
				if (!elmAutoFinAndSendkey("//input[@name='firstname']", _tmpNameArr[0]))
				{
					this.log("Not firstname_input!");
					return false;
				}

				if (!elmAutoFinAndSendkey("//input[@name='lastname']", _tmpNameArr[1]))
				{
					this.log("Not lastname_input!");
					return false;
				}

				if (!elmAutoFinAndSendkey("//input[@name='reg_email__']", _randomEmail))
				{
					this.log("Not reg_email__input!");
					return false;
				}

				if (!string.IsNullOrEmpty(email_confirmation))
				{
					if (!elmAutoFinAndSendkey("//input[@name='reg_email_confirmation__']", email_confirmation))
					{
						this.log("Not reg_email_confirmation__input!");
						return false;
					}
				}

				if (!elmAutoFinAndSendkey("//input[@name='reg_passwd__']", _passAcc))
				{
					this.log("Not reg_passwd__input!");
					return false;
				}

				Random _random = new Random(Guid.NewGuid().GetHashCode());

				SelectElement _selectDay = new SelectElement(this.getElement(By.Id("day")));
				_selectDay.SelectByIndex(_random.Next(1, 20));

				SelectElement _selectMonth = new SelectElement(this.getElement(By.Id("month")));
				_selectMonth.SelectByValue(_random.Next(1, 12).ToString());

				SelectElement _selectYear = new SelectElement(this.getElement(By.Id("year")));
				_selectYear.SelectByValue(_random.Next(1970, 1996).ToString());

				ReadOnlyCollection<IWebElement> _gender = this._driver.FindElements(By.CssSelector("input[name='sex']"));
				if (_gender.Count > 1)
				{
					int _ranPer = _random.Next(0, 100);
					if (_ranPer % 2 == 0)
					{
						_gender[0].Click();
					}
					else
					{
						_gender[1].Click();
					}
				}

				if (!elmAutoFinAndJsClick("//button[@type='submit' and @name='websubmit']"))
				{
					this.log("Not Submit Btn!");
					return false;
				}

				Delay(500);

				// Check loading
				bool regErro = false;
				string _erroElmXp = "//div[not(contains(@class, 'hidden_elem'))]/div[@id='reg_error_inner']";
				int ChkLoadingIndex = 1;
				bool loaded = this._driver.Url.Contains("confirmemail.php");
				while (!loaded && (ChkLoadingIndex <= 8))
				{
					Delay(5000);
					WaitLoading();
					Delay(500);
					loaded = this._driver.Url.Contains("confirmemail.php");
					if (!loaded)
					{
						loaded = this._driver.Url.Contains("facebook.com/checkpoint");
					}

					if (!loaded)
					{
						ReadOnlyCollection<IWebElement> _erroElm = this._driver.FindElements(By.XPath(_erroElmXp));
						if (_erroElm.Count > 0)
						{
							loaded = true;
							regErro = true;
						}
					}

					ChkLoadingIndex++;
				}
				Delay(1000);

				if (!loaded)
				{
					this.log("Submit lâu quá, ko đợi đc!");
					return false;
				}

				if (loaded)
				{
					if (this._driver.Url.Contains("https://www.facebook.com/checkpoint"))
					{
						this.log("Check point!");
						return false;
					}
				}

				ChkLoadingIndex = 1;
				while (regErro && (ChkLoadingIndex <= 5))
				{
					Delay(20000);
					this.log("Nhấn lại ...");
					elmAutoFinAndJsClick("//button[@type='submit' and @name='websubmit']");
					loaded = this._driver.Url.Contains("confirmemail.php");
					int ChkLoadingIndexTh = 1;
					while (!loaded && (ChkLoadingIndexTh <= 8))
					{
						Delay(5000);
						WaitLoading();
						Delay(500);
						loaded = this._driver.Url.Contains("confirmemail.php");
						if (!loaded)
						{
							loaded = this._driver.Url.Contains("facebook.com/checkpoint");
						}

						if (!loaded)
						{
							ReadOnlyCollection<IWebElement> _erroElm = this._driver.FindElements(By.XPath(_erroElmXp));
							if (_erroElm.Count > 0)
							{
								loaded = true;
								regErro = true;
							}
						}
						ChkLoadingIndexTh++;
					}

					if (loaded)
					{
						ReadOnlyCollection<IWebElement> _erroElm = this._driver.FindElements(By.XPath(_erroElmXp));
						if (_erroElm.Count == 0)
						{
							regErro = false;
						}
					}

					ChkLoadingIndex++;
				}
				Delay(1000);
				WaitLoading();

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private bool ChangeEmail(string newEmail, FbRegParam regParam)
		{
			try
			{
				WaitAjaxLoading(By.CssSelector("iframe[src*='facebook.com/confirmemail.php']"), 10);
				ReadOnlyCollection<IWebElement> iFrame = this._driver.FindElements(By.CssSelector("iframe[src*='facebook.com/confirmemail.php']"));
				if (iFrame.Count == 0)
				{
					this.log("Not Change phoneNumber iFrame!");
					return false;
				}
				this._driver.SwitchTo().Frame(iFrame.First());

				if (!elmAutoFinAndJsClick("//a[@role='button' and @rel='dialog' and contains(@href, 'change_contactpoint')]"))
				{
					this.log("Not Submit Btn!");
					return false;
				}

				Delay(1000);
				if (!elmAutoFinAndSendkey("//input[@name='contactpoint']", newEmail, 15))
				{
					this.log("Not contactpoint_input!");
					return false;
				}

				if (!elmAutoFinAndJsClick("//div[contains(@class, '_confirmationCliff__dialogFooter')]/button[@type='submit']"))
				{
					this.log("Not Contactpoint Submit Btn!");
					return false;
				}

				Delay(5000);
				WaitLoading();
				Delay(500);

				if (this._driver.Url.Contains("https://www.facebook.com/checkpoint"))
				{
					this.log("Check point!");
					return false;
				}

				this._driver.SwitchTo().DefaultContent();

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private string getVerifyCode(string verifyData)
		{
			try
			{
				this.log("Get Verify Code!");
				this.log("Nguồn: " + verifyData);
				if (verifyData == "emailfake.com")
				{
					return getEmailFakeComVerifyCode();
				}

				if (verifyData == "temp-mail.org")
				{
					return getTempMailComVerifyCode();
				}

				if ((verifyData == "mail.tm") || (verifyData == "mail.gw"))
				{
					return getMailTMVerifyCodeAsync().Result;
				}

				return "";
			}
			catch (Exception ex)
			{
				this.log(ex);
				switchRegTabHandle();
				return null;
			}
		}

		private string getTempMailComVerifyCode()
		{
			try
			{
				this._driver.SwitchTo().Window(this.shopifyHandle);
				Delay(1000);
				string _tmpCode = "";
				int _count = 0;
				string _code = "";

				for (; ; )
				{
					Delay(3000);
					this.log("Requet code -  " + _count);
					WaitAjaxLoading(By.CssSelector("div[class*='tm-content']"), 5);
					ReadOnlyCollection<IWebElement> tmcontent = this._driver.FindElements(By.CssSelector("div[class*='tm-content']"));
					if (tmcontent.Count > 0)
					{
						if (this.isValid(tmcontent[0]))
						{
							Actions actions = new Actions(this._driver);
							actions.MoveToElement(tmcontent[0]);
							Delay(2000);
							actions.Perform();
						}
					}

					WaitAjaxLoading(By.CssSelector("div[class*='beforeContentBannerBl']"), 5);
					Delay(1000);
					tmcontent = this._driver.FindElements(By.CssSelector("div[class*='beforeContentBannerBl']"));
					if (tmcontent.Count > 0)
					{
						if (this.isValid(tmcontent[0]))
						{
							Actions actions = new Actions(this._driver);
							actions.MoveToElement(tmcontent[0]);
							Delay(2000);
							actions.Perform();
						}

					}

					string inboxSubjectStr = "//span[contains(@class, 'nboxSubject')]//a[contains(@href, 'temp-mail.org/en/view/')]";
					WaitAjaxLoading(By.XPath(inboxSubjectStr), 5);
					ReadOnlyCollection<IWebElement> inboxSubjectObj = this._driver.FindElements(By.XPath(inboxSubjectStr));
					if (inboxSubjectObj.Count > 0)
					{
						string subject = inboxSubjectObj.First().GetAttribute("innerHTML");
						_tmpCode = Regex.Match(subject, "[0-9]+").ToString();
					}

					if (!string.IsNullOrEmpty(_tmpCode))
					{
						break;
					}

					_count++;
					if (_count >= 5)
					{
						goto Block_3;
					}

					_tmpCode = null;
				}

				_code = _tmpCode.Replace("FB-", "");

				Block_3:
				this.log("Code: " + _code);
				switchRegTabHandle();
				return _code;
			}
			catch (Exception ex)
			{
				this.log(ex);
				switchRegTabHandle();
				return null;
			}
		}

		private string getEmailFakeComVerifyCode()
		{
			try
			{
				this.log("Get Verify Code!");
				this._driver.SwitchTo().Window(this.shopifyHandle);
				this._driver.Navigate().Refresh();
				WaitLoading();
				Delay(500);

				string codeVrf = "";
				string _tmpCode = "";

				string elmConfirmString = "//td[@id='email_content']//span[@class='mb_text']//table//td";
				WaitAjaxLoading(By.XPath(elmConfirmString), 5);
				Delay(500);
				ReadOnlyCollection<IWebElement> elmObjs = this._driver.FindElements(By.XPath(elmConfirmString));
				if (elmObjs.Count > 0)
				{
					string innerHTML = elmObjs.First().Text;
					_tmpCode = Regex.Match(innerHTML, "[0-9]+").ToString();
				}

				int ChkLoadingIndex = 1;
				while (string.IsNullOrEmpty(_tmpCode) && (ChkLoadingIndex <= 6))
				{
					this._driver.Navigate().Refresh();
					WaitLoading();
					Delay(500);

					WaitAjaxLoading(By.XPath(elmConfirmString), 5);
					Delay(500);
					elmObjs = this._driver.FindElements(By.XPath(elmConfirmString));
					if (elmObjs.Count > 0)
					{
						string innerHTML = elmObjs.First().Text;
						_tmpCode = Regex.Match(innerHTML, "[0-9]+").ToString();
					}

					ChkLoadingIndex++;
				}

				if (string.IsNullOrEmpty(_tmpCode))
				{
					this.log("Not Get Code!");
					this._driver.SwitchTo().DefaultContent();
					switchRegTabHandle();
					return null;
				}
				codeVrf = _tmpCode.Replace("FB-", "");
				switchRegTabHandle();
				return codeVrf;
			}
			catch (Exception ex)
			{
				this.log(ex);
				switchRegTabHandle();
				return null;
			}
		}

		private async Task<string> getMailTMVerifyCodeAsync()
		{
			try
			{
				MessageInfo[] messages = await mailClient.GetAllMessages();
				string codeVrf = "";
				if (messages.Length > 0)
				{
					foreach (MessageInfo item in messages)
					{
						if (item.From.Name == "Facebook")
						{
							string subject = item.Subject;
							string _tmpCode = Regex.Match(subject, "FB[-][0-9]+").ToString();
							codeVrf = _tmpCode.Replace("FB-", "");
							break;
						}
					}
				}

				int ChkLoadingIndex = 1;
				while (string.IsNullOrEmpty(codeVrf) && (ChkLoadingIndex <= 6))
				{
					Delay(5000);
					messages = await mailClient.GetAllMessages();
					foreach (MessageInfo item in messages)
					{
						if (item.From.Name == "Facebook")
						{
							string subject = item.Subject;
							string _tmpCode = Regex.Match(subject, "FB[-][0-9]+").ToString();
							codeVrf = _tmpCode.Replace("FB-", "");
							break;
						}
					}
					ChkLoadingIndex++;
				}

				return codeVrf;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		public bool fbRegMb(FbRegParam regParam)
		{
			try
			{
				int timeDelay = regParam.time_delay * 1000;

				this._driver.SwitchTo().Window(this.regTabHandle);
				this._driver.Navigate().GoToUrl("https://m.facebook.com/reg/");
				Delay(3000);

				string checkStr = "//div[@class='_MCookieAccordionSection__container']/button";
				WaitAjaxLoading(By.XPath(checkStr), 7);
				ReadOnlyCollection<IWebElement> expandableObjs = this._driver.FindElements(By.XPath(checkStr));
				foreach (IWebElement item in expandableObjs)
				{
					Delay(1000);
					((IJavaScriptExecutor)this._driver).ExecuteScript("return arguments[0].scrollIntoView(true);", item);
					Delay(1000);
					MouseHoverAndClickByJavaScript(item);
					Delay(2000);
				}

				Delay(1000);
				elmAutoFinAndJsClick("//button[@type='submit' and (@id='accept-cookie-banner-label' or @data-cookiebanner='accept_button')]", 5);

				string localeSelectorDivStr = "//div[@id='locale-selector']/div/div";
				WaitAjaxLoading(By.XPath(localeSelectorDivStr));
				Delay(500);
				ReadOnlyCollection<IWebElement> localeSelectorDivObj = this._driver.FindElements(By.XPath(localeSelectorDivStr));
				if (localeSelectorDivObj.Count == 0)
				{
					this.log("Change locale 1 --- False!");
					this.log("End!");
					return false;
				}

				ReadOnlyCollection<IWebElement> localeSelectorItemObj = localeSelectorDivObj.Last().FindElements(By.CssSelector("a[href*='intl/save_locale']"));
				if (localeSelectorItemObj.Count == 0)
				{
					this.log("Change locale 2 --- False!");
					this.log("End!");
					return false;
				}

				MouseHoverAndClickByJavaScript(localeSelectorItemObj.First());
				WaitLoading();
				Delay(timeDelay);

				string[] _tmpNameArr = GenFakeInfoAsync();
				if (_tmpNameArr is null)
				{
					this.log("Gen Fake Info --- False!");
					this.log("End!");
					return false;
				}

				Re_login:

				if (!elmAutoFinAndSendkey("//input[@id='firstname_input']", _tmpNameArr[0]))
				{
					this.log("Not firstname_input!");
					return false;
				}
				Delay(timeDelay);
				if (!elmAutoFinAndSendkey("//input[@id='lastname_input']", _tmpNameArr[1]))
				{
					this.log("Not lastname_input!");
					return false;
				}
				Delay(timeDelay);
				if (!elmAutoFinAndJsClick("//button[@type='submit' and contains(@data-sigil, 'multi_step_next')]"))
				{
					this.log("Not multi_step_next Btn!");
					return false;
				}

				WaitLoading();
				Delay(timeDelay);
				// Birthday
				MouseHoverAndClickByJavaScript(this.getElement(By.Id("day")));
				SelectElement _selectDay = new SelectElement(this.getElement(By.Id("day")));
				Random _random = new Random(Guid.NewGuid().GetHashCode());
				_selectDay.SelectByIndex(_random.Next(1, 20));
				Delay(timeDelay);

				MouseHoverAndClickByJavaScript(this.getElement(By.Id("month")));
				SelectElement _selectMonth = new SelectElement(this.getElement(By.Id("month")));
				_selectMonth.SelectByValue(_random.Next(1, 12).ToString());
				Delay(timeDelay);

				MouseHoverAndClickByJavaScript(this.getElement(By.Id("year")));
				SelectElement _selectYear = new SelectElement(this.getElement(By.Id("year")));
				_selectYear.SelectByValue(_random.Next(1970, 1996).ToString());

				if (!elmAutoFinAndJsClick("//button[@type='submit' and contains(@data-sigil, 'multi_step_next')]"))
				{
					this.log("Not multi_step_next Btn!");
					return false;
				}
				WaitLoading();
				Delay(500);
				string email = this.getEmail(regParam.verifyData);
				this.log("Email: " + email);
				Delay(timeDelay);
				if (!elmAutoFinAndSendkey("//input[@id='contactpoint_step_input']", email))
				{
					this.log("Not contactpoint_step_input!");
					return false;
				}
				Delay(timeDelay);
				if (!elmAutoFinAndJsClick("//button[@type='submit' and contains(@data-sigil, 'multi_step_next')]"))
				{
					this.log("Not multi_step_next Btn!");
					return false;
				}
				WaitLoading();

				ReadOnlyCollection<IWebElement> _gender = this._driver.FindElements(By.CssSelector("input[name='sex']"));
				if (_gender.Count > 1)
				{
					int _ranPer = _random.Next(0, 100);
					if (_ranPer % 2 == 0)
					{
						MouseHoverAndClickByJavaScript(_gender[0]);
					}
					else
					{
						MouseHoverAndClickByJavaScript(_gender[1]);
					}
				}
				Delay(timeDelay);
				if (!elmAutoFinAndJsClick("//button[@type='submit' and contains(@data-sigil, 'multi_step_next')]"))
				{
					this.log("Not multi_step_next Btn!");
					return false;
				}
				WaitLoading();
				Delay(timeDelay);
				string _passAcc = "D20121992";
				this.log("Pass: " + _passAcc);
				IWebElement _pass = this.getElement(By.CssSelector("input[name='reg_passwd__']"));
				if (this.isValid(_pass))
				{
					MouseHoverAndClickByJavaScript(_pass);
					this.fillInput(_pass, _passAcc);
				}
				Delay(timeDelay);

				// Submit todo
				if (!elmAutoFinAndJsClick("//button[@type='submit' and contains(@data-sigil, 'multi_step_submit')]"))
				{
					this.log("Not multi_step_submit Btn 1!");
					return false;
				}
				Delay(timeDelay);
				WaitLoading();

				bool success = false;
				bool re_reg = false;
				int ChkSuccessIndex = 1;
				while (!success && (ChkSuccessIndex <= 8))
				{
					Delay(3000);
					bool checkCtn = true;
					if (this._driver.Url.Contains("facebook.com/error/index.php"))
					{
						this.log("facebook.com/error/index.php!");
						checkCtn = false;
						re_reg = true;
						break;
					}

					if (this._driver.Url.Contains("facebook.com/checkpoint"))
					{
						this.log("Check point!");
						checkCtn = false;
					}

					if (!checkCtn)
					{
						ChkSuccessIndex = 1000;
					}

					if (this._driver.Url.Contains("/login/save-device") || this._driver.Url.Contains("facebook.com/confirmemail.php") || this._driver.Url.Contains("facebook.com/privacy/consent_framework/prompt"))
					{
						success = true;
					}

					ChkSuccessIndex++;
				}
				WaitLoading();
				Delay(1000);

				if (re_reg)
				{
					this.log("ERROR! Check Re REG...");
					ReadOnlyCollection<IWebElement> reRegObj = this._driver.FindElements(By.CssSelector("a[href*='https://m.facebook.com/login.php']"));
					if (reRegObj.Count > 0)
					{
						string reHref = reRegObj.First().GetAttribute("href");
						this._driver.Navigate().GoToUrl(reHref);
						WaitLoading();
						Delay(1000);

						string signupButtonBtn = "//a[@id='signup-button']";
						if (elmAutoFinAndJsClick(signupButtonBtn))
						{
							goto Re_login;
						}
					}
					else
					{
						this.log("Re REG: FALSE!");
					}
				}

				if (!success)
				{
					string[] checkxp = new string[] { "//button[@type='submit' and contains(@data-sigil, 'multi_step_next')]" };
					string checkxpRes = this.checkXPath(checkxp);
					if (!string.IsNullOrEmpty(checkxpRes))
					{
						this.log("multi_step_submit Btn 2!");
						if (elmAutoFinAndJsClick(checkxpRes))
						{
							success = false;
							ChkSuccessIndex = 1;
							while (!success && (ChkSuccessIndex <= 8))
							{
								Delay(3000);
								bool checkCtn = true;
								if (this._driver.Url.Contains("facebook.com/error/index.php"))
								{
									this.log("Error!");
									checkCtn = false;
								}

								if (this._driver.Url.Contains("facebook.com/checkpoint"))
								{
									this.log("Check point!");
									checkCtn = false;
								}

								if (!checkCtn)
								{
									ChkSuccessIndex = 1000;
								}

								if (this._driver.Url.Contains("/login/save-device") || this._driver.Url.Contains("facebook.com/confirmemail.php") || this._driver.Url.Contains("facebook.com/privacy/consent_framework/prompt"))
								{
									success = true;
								}

								ChkSuccessIndex++;
							}
							WaitLoading();
							Delay(1000);
						}
					}
				}

				if (!success)
				{
					this.log("Not rl to get confirm mail!");
					return false;
				}

				if (this._driver.Url.Contains("/login/save-device"))
				{
					// Not Now
					if (!elmAutoFinAndJsClick("//a[@role='button' and contains(@href, 'login/save-device/cancel/?flow=interstitial_nux')]"))
					{
						this.log("Not Not Now Btn!");
						// return false;
					}
					WaitLoading();
				}

				// confirm account
				success = false;
				ChkSuccessIndex = 1;
				while (!success && (ChkSuccessIndex <= 8))
				{
					Delay(3000);
					if (this._driver.Url.Contains("facebook.com/confirmemail.php"))
					{
						success = true;
					}
					ChkSuccessIndex++;
				}
				WaitLoading();
				Delay(1000);
				if (!success)
				{
					this.log("Not Load Confirmemail Page!");
					return false;
				}

				// getVerifyCode
				string verifyCode = getVerifyCode(regParam.verifyData);
				MobileVerifyByCoderAuto(verifyCode);
				WaitLoading();

				success = false;
				ChkSuccessIndex = 1;
				while (!success && (ChkSuccessIndex <= 8))
				{
					Delay(3000);
					if (this._driver.Url.Contains("facebook.com/home.php") || this._driver.Url.Contains("privacy/consent_framework/prompt") || this._driver.Url.Contains("facebook.com/gettingstarted/"))
					{
						success = true;
					}
					ChkSuccessIndex++;
				}

				WaitLoading();
				Delay(1000);
				if (this._driver.Url.Contains("facebook.com/checkpoint"))
				{
					this.log("Check point!");
					return false;
				}

				if (!success)
				{
					this.log("Not Load home - gettingstarted Page!");
					_cookie = "";
					foreach (OpenQA.Selenium.Cookie _c in this._driver.Manage().Cookies.AllCookies)
					{
						if (_c.Name == "c_user")
						{
							_clone_uid = _c.Value;
						}
						_cookie = string.Concat(new string[] { _cookie, _c.Name, "=", _c.Value, "; " });
					}
					this.log("Cookie: " + _cookie);
					return false;
				}

				if (this._driver.Url.Contains("privacy/consent_framework/prompt"))
				{
					if (!elmAutoFinAndJsClick("//button[@name='primary_consent_button']"))
					{
						this.log("Not Allow All Cookies Btn!");
					}
					WaitLoading();
				}

				this._driver.Navigate().GoToUrl("https://m.facebook.com/home.php?ref=wizard&_rdr");
				WaitLoading();
				if (this._driver.Url.Contains("facebook.com/checkpoint"))
				{
					this.log("Check point!");
					return false;
				}

				_clone_pass = _passAcc;
				_clone_email = email;
				_clone_email_pass = mailPass;
				_cookie = "";
				foreach (OpenQA.Selenium.Cookie _c in this._driver.Manage().Cookies.AllCookies)
				{
					if (_c.Name == "c_user")
					{
						_clone_uid = _c.Value;
					}
					_cookie = string.Concat(new string[] { _cookie, _c.Name, "=", _c.Value, "; " });
				}

				_clone_data = _clone_uid + "|" + _passAcc + "|" + _location + "|" + _cookie;
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private async Task<string> getMailTMVerifyLinkAsync()
		{
			try
			{
				string linkVrf = "";
				int ChkLoadingIndex = 1;
				while (string.IsNullOrEmpty(linkVrf) && (ChkLoadingIndex <= 6))
				{
					Delay(5000);
					MessageInfo[] messages = await mailClient.GetAllMessages();
					foreach (MessageInfo item in messages)
					{
						if (item.From.Name == "Facebook")
						{
							MessageSource source = await mailClient.GetMessageSource(item.Id);
							string rawMessage = source.Data;
							byte[] byteArray = Encoding.UTF8.GetBytes(rawMessage);
							MemoryStream stream = new MemoryStream(byteArray);
							MimeParser parser = new MimeParser(stream, MimeFormat.Entity);
							MimeMessage mimeMessage = parser.ParseMessage();
							string htmls = HttpUtility.HtmlDecode(mimeMessage.HtmlBody);
							Match rgMatch = Regex.Match(htmls, @"https:\/\/www\.facebook.com\/n\/\?confirmemail.php([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
							if (rgMatch != null && rgMatch.Groups.Count > 1)
							{
								linkVrf = @"https://www.facebook.com/n/?confirmemail.php" + rgMatch.Groups[1].Value;
								break;
							}
						}
					}
					ChkLoadingIndex++;
				}

				return linkVrf;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private string getTempMailOrgVerifyLink()
		{
			try
			{
				// SwitchTo Mail
				this._driver.SwitchTo().Window(this.shopifyHandle);
				Delay(500);

				string linkVrf = "";
				int ChkLoadingIndex = 1;
				while (string.IsNullOrEmpty(linkVrf) && (ChkLoadingIndex <= 6))
				{
					Delay(5000);
					// string getData = this.tempMailFetchGet("https://web2.temp-mail.org/messages");
					// string email = Regex.Match(getData, "\"mailbox\":\"(.*?)\"").Groups[1].Value;



					ChkLoadingIndex++;
				}

				this._driver.SwitchTo().Window(this.regTabHandle);
				return linkVrf;
			}
			catch (Exception ex)
			{
				this.log(ex);
				this._driver.SwitchTo().Window(this.regTabHandle);
				return null;
			}
		}

		private string getVerifyLink(string verifyData)
		{
			try
			{
				this.log("Get Verify Link!");
				this.log("Nguồn: " + verifyData);
				if (verifyData == "mail.tm")
				{
					return getMailTMVerifyLinkAsync().Result;
				}

				if (verifyData == "temp-mail.org")
				{
					return getTempMailOrgVerifyLink();
				}

				return "";
			}
			catch (Exception ex)
			{
				this.log(ex);
				this._driver.SwitchTo().Window(this.regTabHandle);
				return null;
			}
		}

		private bool MobileVerifyByCoderAuto(string codeNumber)
		{
			try
			{
				if (!elmAutoFinAndSendkey("//form//input[@name='c']", codeNumber))
				{
					this.log("Not codeNumber_input!");
					return false;
				}

				if (!elmAutoFinAndJsClick("//div[@id='m_conf_cliff_root_id']//form//a"))
				{
					this.log("Not Submit Btn!");
					return false;
				}

				Delay(500);
				WaitLoading();
				Delay(500);

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool logoutAllSessions()
		{
			try
			{
				switchRegTabHandle();

				this._driver.Navigate().GoToUrl("https://www.facebook.com/settings?tab=security");
				WaitLoading();
				Delay(1000);

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string iframeStr = "iframe[src*='tab=security']";
				WaitAjaxLoading(By.CssSelector(iframeStr), 20);
				ReadOnlyCollection<IWebElement> iFrames = this._driver.FindElements(By.CssSelector(iframeStr));
				if (iFrames.Count > 0)
				{
					IWebElement iFrame = iFrames.First();
					string ifSrc = iFrame.GetAttribute("src");
					Uri theUri = new Uri(ifSrc);

					string cquick_token = HttpUtility.ParseQueryString(theUri.Query).Get("cquick_token");
					string ctarget = HttpUtility.ParseQueryString(theUri.Query).Get("ctarget");
					string cquick = HttpUtility.ParseQueryString(theUri.Query).Get("cquick");
					string refAv = ifSrc;

					string url = "https://www.facebook.com/security/settings/sessions/log_out_all/";
					string body = string.Concat(new string[] {
						"clear_all=false&__user=",
						_clone_uid,
						"&__a=1&__dyn=7xeUmBwjbgmwn8K2V4Wo5O12wAxu13wqovzEdEc8uxa0z8S2S4o14Ue8hwem0Ko2_CwjE28wgo2WxO0FE5_wEwlU-cw5MKdwnU5WcwOwsU9kbxS2218wc61uwZwaOfK6E28xe0RE5a2W2K0zE5W0HUvw4JwJwSyES0gq0Lo4K2e&__csr=&__req=d&__hs=19100.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005366754&__s=zb3qzs%3Ak97xne%3Ami8jt1&__hsi=7087748640522908866-0&__comet_req=0&cquick=",
						cquick,
						"&ctarget=",
						ctarget,
						"&cquick_token=",
						cquick_token,
						"&fb_dtsg=",
						fb_dtsg,
						"&jazoest=21962&lsd=",
						lsd,
						"&__spin_r=1005366754&__spin_b=trunk&__spin_t=1650245078"
					});

					string postData = this.fetchApi(url, "POST", body, "", lsd, refAv, "include", "54263816,54263819");
					string lid = Regex.Match(postData, "\"lid\":\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(lid))
					{
						this.log(postData);
						return false;
					}
				}
				else
				{
					this.log("KHÔNG LOGOUT ĐC DO LAYOUT KHÁC! GIỮ LẠI CHO NINI CLONE NÀY!");
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public string getTimeZoneId()
        {
			string res = "0";
			string url = "https://developers.facebook.com/docs/marketing-api/reference/ad-account/timezone-ids/v11.0";
			string data = this.GetData(null, url);
			HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
			doc.LoadHtml(data);

			List<List<string>> table = doc.DocumentNode.SelectSingleNode("//table")
			.Descendants("tr")
			.Where(tr => tr.Elements("td").Count() > 1)
			.Select(tr => tr.Elements("td").Select(td => td.InnerText.Trim()).ToList())
			.ToList();

			// America/Indianapolis
			// TZ_AMERICA_INDIANAPOLIS

			string timezonestr = @"TZ/" + this._time_zone;
			timezonestr = timezonestr.Replace("/","_").ToUpper();

			table.ForEach(x => {
                if (x[0] == timezonestr)
                {
					res = x[1];
				}
			});
			return res;
		}

		public bool switchRegTabHandle()
		{
			try
			{
				this._driver.SwitchTo().Window(this.regTabHandle);
				Delay(500);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool switchBusinessTabHandle()
		{
			try
			{
				this._driver.SwitchTo().Window(this.businessTabHandle);
				Delay(500);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool SetSecurity()
		{
			try
			{
				switchRegTabHandle();

				string url = "https://www.facebook.com/privacy/review/?review_id=573933453011661";
				string data = this.GetData(null, url, _cookie, this._userAgent);
				string value = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string value2 = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				url = "https://www.facebook.com/api/graphql/";
				string data2 = string.Concat(new string[]
				{
						"av=",
						_clone_uid,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgU765QdwSwAyU8EW0CEboG4E762S1DwUx609vCxS320om78-221Rwwwg8vy8465o-cw5MKdwGwFyE2ly87e2l2Utwwwi831wiEjwPyoowYwlE-UqwsUkxe2GewGwkUtxGm2SUnxq5olwUwgojUlDw-wUws9o8oy2a2-3a1PwyBwp8Gdwb622&__csr=&__req=g&__hs=18869.EXP2%3Acomet_pkg.2.1.0.0.0&dpr=1&__ccg=GOOD&__rev=1004326247&__s=li0fvf%3A04r8ge%3A8vsulg&__hsi=7002261123746414223-0&__comet_req=1&fb_dtsg=",
						value,
						"&jazoest=22000&lsd=",
						value2,
						"&__spin_r=1004326247&__spin_b=trunk&__spin_t=1630340964&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PrivacyCheckupLoginAlertsMutation&variables=%7B%22input%22%3A%7B%22set_email%22%3Atrue%2C%22set_messenger%22%3Atrue%2C%22set_notif%22%3Atrue%2C%22set_phone%22%3Atrue%2C%22source%22%3A%22TOPIC_SELECTION%22%2C%22actor_id%22%3A%22",
						_clone_uid,
						"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=2923481841029323&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%22%5D"
				});

				string postData = this.PostData(null, url, data2, this._contentType, this._userAgent, _cookie, value2);
				string alertID = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(alertID))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool SetCookieData()
		{
			try
			{
				_cookie = "";
				foreach (OpenQA.Selenium.Cookie _c in this._driver.Manage().Cookies.AllCookies)
				{
					if (_c.Name == "c_user")
					{
						_clone_uid = _c.Value;
					}
					_cookie = string.Concat(new string[] { _cookie, _c.Name, "=", _c.Value, "; " });
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private bool waitLoadingAction(string loadingElmString)
        {
			try
			{
				bool loading = true;
				int ChkLoadingIndex = 1;
				while (loading && (ChkLoadingIndex <= 8))
				{
					Delay(3000);
					ReadOnlyCollection<IWebElement> cplLoadingObjs = this._driver.FindElements(By.XPath(loadingElmString));
					if (cplLoadingObjs.Count > 0)
					{
						loading = true;
					}
					else
					{
						loading = false;
					}

					ChkLoadingIndex++;
				}
				Delay(1000);

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public string getMe(string fbid = "me")
		{
			try
			{
				switchRegTabHandle();
				this._driver.Navigate().GoToUrl("https://www.facebook.com/" + fbid + "/");
				WaitLoading();
				Delay(1000);
				return this._driver.Url.TrimEnd('/');
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return "";
			}
		}

		public bool addFriend(string viaid)
		{
			try
			{
				switchRegTabHandle();

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[]
				{
						"av=",
						_clone_uid,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgU76byQdwSwAyU8EW0CEboG4E762S1DwUx60gu0luq7oc81xoszU887m2210x-8wgolzUO0-E4a3aUS2G3i0Boy1PwBgK7o884y0Mo4G4UcUC68f85qfK6E7e58jwGzEaE5e7oqBwJK5Umxm5oe8464-5pUfEe872m7-8wywLwOwsU8FobodEGdwb622390&__csr=&__req=4a&__hs=18925.BP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004608348&__s=sx3426%3Abidny4%3A0hc2cy&__hsi=7022892724752852810-0&__comet_req=1&fb_dtsg=",
						token1,
						"&jazoest=22060&lsd=",
						token2,
						"&__spin_r=1004608348&__spin_b=trunk&__spin_t=1635144633&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=FriendingCometFriendRequestSendMutation&variables=%7B%22input%22%3A%7B%22friend_requestee_ids%22%3A%5B%22",
						viaid,
						"%22%5D%2C%22people_you_may_know_location%22%3A%22friends_center%22%2C%22refs%22%3A%5Bnull%5D%2C%22source%22%3A%22people_you_may_know%22%2C%22warn_ack_for_ids%22%3A%5B%5D%2C%22actor_id%22%3A%22",
						_clone_uid,
						"%22%2C%22client_mutation_id%22%3A%222%22%7D%2C%22scale%22%3A1%7D&server_timestamps=true&doc_id=6259411214133253"
				});

				string resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "FriendingCometFriendRequestSendMutation", token2, "https://www.facebook.com/friends/suggestions");
				string friendship_status = Regex.Match(resData, "\"friendship_status\":\"(.*?)\"").Groups[1].Value;
				if ("OUTGOING_REQUEST" == friendship_status)
				{
					return true;
				}

				return false;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public bool agreeFriends(string fbid)
		{
			try
			{
				switchRegTabHandle();

				// get facebook url
				string urlVia = "https://www.facebook.com/" + fbid;
				this._driver.Navigate().GoToUrl(urlVia);
				WaitLoading();
				Delay(1000);
				urlVia = this._driver.Url;

				this._driver.Navigate().GoToUrl("https://www.facebook.com/friends/requests");
				WaitLoading();
				Delay(1000);

				// check request
				bool check = false;
				int ChkIndex = 1;
				string reqStr = "a[href*='" + urlVia + "']";
				while (!check && (ChkIndex <= 8))
				{
					Delay(6000);
					ReadOnlyCollection<IWebElement> cplLoadingObjs = this._driver.FindElements(By.CssSelector(reqStr));
					if (cplLoadingObjs.Count > 0)
					{
						check = true;
					}
					this._driver.Navigate().GoToUrl("https://www.facebook.com/friends/requests");
					WaitLoading();
					ChkIndex++;
				}

				if (!check)
				{
					this.log("Not friend request!");
					return false;
				}

				string url = "https://www.facebook.com/friends/requests/?profile_id=" + _clone_uid;
				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[]
					{
						"av=",
						_clone_uid,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgU76byQdwSwAyU8EW0CEboG4E762S1DwUx60gu0luq7oc81xoszU887m2210x-8wgolzUO0-E4a3aUS2G3i0Boy1PwBgK7o884y0Mo4G4UcUC68f85qfK6E7e58jwGzEaE5e7oqBwJK5Umxm5oe8464-5pUfE98jws9ovUy2a2-3a1PwyBwJwSyES0Io88cA&__csr=&__req=2h&__hs=18925.BP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004608456&__s=6kwkph%3Aww9jv6%3Aiwvox5&__hsi=7022909990546188440-0&__comet_req=1&fb_dtsg=",
						token1,
						"&jazoest=21905&lsd=",
						token2,
						"&__spin_r=1004608456&__spin_b=trunk&__spin_t=1635148653&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=FriendingCometFriendRequestConfirmMutation&variables=%7B%22input%22%3A%7B%22friend_requester_id%22%3A%22",
						fbid,
						"%22%2C%22source%22%3A%22friends_tab%22%2C%22actor_id%22%3A%22",
						_clone_uid,
						"%22%2C%22client_mutation_id%22%3A%222%22%7D%2C%22scale%22%3A1%2C%22refresh_num%22%3A0%7D&server_timestamps=true&doc_id=6990867230938619&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%22%5D"
					});

				string resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "FriendingCometFriendRequestConfirmMutation", token2, url);
				string friendship_status = Regex.Match(resData, "\"friendship_status\":\"(.*?)\"").Groups[1].Value;
				if ("ARE_FRIENDS" != friendship_status)
				{
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool CheckFiend(string fbid)
		{
			try
			{
				switchRegTabHandle();

				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v12.0/",
					_clone_uid,
					"/friends?access_token=",
					_cloneAccessToken,
					"&pretty=0"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				MatchCollection matches = Regex.Matches(data, "\"id\":\"" + fbid + "\"");
				if (matches.Count == 0)
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool agreeAdminProPage(string proPageid, string profile_admin_invite_id)
		{
			try
			{
				switchRegTabHandle();
				string url = "https://www.facebook.com/profile.php?id=" + proPageid;
				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[]
				{
					"av=",
					_clone_uid,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHK4HwkEng5K8G6EjBWo2nDwAxu13wsongS3q2ibwyzE2qwJyEiwsobo6u3y4o0B-q7oc81xoswMwto88422y11xmfz83WwgEcHzoaEnxO0Boy1PwBgK7o6C0Mo4G4Ufo5m1mzXxG1Pxi4UaEW1-xS6FobrwKxm5oe8464-5pUfEe872m7-8wywdq2am2Sq2-azqwaW223908O3216AzUjwTwNxe&__csr=gaca_Z8jlh7spOEl4Yjqbfl9ZbkLIR9v4bb8JGGdAVbmkxaWOshFhleVkiiAQnV4ch4F7qrjFmVaQjGmAjWAVFQiLBV4QqmazpHKagDKmiQ8rXppeJaiUyt9oS4LCjx2t5ya_AC-mVXuiWwzBzRizFV468jGml2eEsz8qqzotx-fyE-ujK9GE-qdJ5yHRVt2FpWCx7yZu5EnhqGiuEmyoy2S8oLAzogwwCxCR-4UrzEW4UmK6UOqEtDDACyVo9EG9xuq4VeEO2O7UyEKUK6EpxHBwFxy9wnokDwjUjwxzonwZwa-2i2afwau6-5UyawQwZBG3lK10y8dU4y3q360RE0bsFE0PGq02ne2u01DiU0s1wcy0Rpe0pq3DidP5wWyk0A8dBgC9yE2-g6G08oyB4w5Xz80Au0217wwwtEd80d1Vo0mja&__req=1g&__hs=19183.HYP%3Acomet_pkg.2.1.0.2.0&dpr=1&__ccg=EXCELLENT&__rev=1005818386&__s=s13ap3%3Az6xmww%3Ay5vg5i&__hsi=7118706202406600152&__comet_req=15&fb_dtsg=",
					token1,
					"&jazoest=25632&lsd=",
					token2,
					"&__spin_r=1005818386&__spin_b=trunk&__spin_t=1657452947&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=ProfilePlusCometAcceptOrDeclineAdminInviteMutation&variables=%7B%22input%22%3A%7B%22client_mutation_id%22%3A%221%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22is_accept%22%3Atrue%2C%22profile_admin_invite_id%22%3A%22",
					profile_admin_invite_id,
					"%22%2C%22user_id%22%3A%22",
					proPageid,
					"%22%7D%2C%22scale%22%3A1%7D&server_timestamps=true&doc_id=7597590303647347&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%22%5D"
				});

				string resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "ProfilePlusCometAcceptOrDeclineAdminInviteMutation", token2, url);
				string profileid = Regex.Match(resData, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(profileid))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool ViaShareAdToClone(string viaAdId, string viaAccessToken)
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v10.0/act_" + viaAdId + "/users?_reqName=adaccount%2Fusers&access_token=" + viaAccessToken + "&method=post"
				});

				string data2 = string.Concat(new string[]
				{
					"_reqName=adaccount%2Fusers&_reqSrc=AdsPermissionDialogController&_sessionID=&account_id=" + viaAdId + "&include_headers=false&locale=en_US" +
						"&method=post&pretty=0&role=281423141961500&suppress_http_code=1&uid=" + _clone_uid + "&xref=f3c98a378475d8"
				});

				string postData = this.PostData(null, url, data2, this._contentType, this._userAgent);
				string success = Regex.Match(postData, "\"success\":(.*?),").Groups[1].Value;
				if ("true" != success)
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool shareAdsToVia(string viaid, bool wait)
		{
			try
			{
				switchRegTabHandle();

				string linkVia = this.getMe(viaid);
				this._driver.Manage().Window.Maximize();
				this._driver.Navigate().GoToUrl(linkVia);
				WaitLoading();
				Delay(1000);
				bool checkf = false;
				int ChkfIndex = 1;

				string reqStr = "//img[contains(@src, 'c9BbXR9AzI1.png') or contains(@src, '5nzjDogBZbf.png')]";
				while (!checkf && (ChkfIndex <= 8))
				{
					Delay(6000);
					ReadOnlyCollection<IWebElement> cplLoadingObjs = this._driver.FindElements(By.XPath(reqStr));
					if (cplLoadingObjs.Count > 0)
					{
						checkf = true;
					}

					this._driver.Navigate().Refresh();
					WaitLoading();
					ChkfIndex++;
				}

				if (!checkf)
				{
					this.log("Not Is Friend!");
					return false;
				}

				string url = "https://www.facebook.com/ads/manager/account_settings/information";
				string getData = this.fetchGet(url);
				_ads_id = Regex.Match(getData, "accountID:\"(.*?)\",accountName:\"(.*?)\"").Groups[1].Value;
				_sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
				_cloneAccessToken = Regex.Match(getData, "access_token:\"(.*?)\"").Groups[1].Value;
				// Role admin: 281423141961500
				url = "https://graph.facebook.com/v10.0/act_" + _ads_id + "/users?_reqName=adaccount%2Fusers&access_token=" + _cloneAccessToken + "&method=post";
				string body = "_reqName=adaccount%2Fusers&_reqSrc=AdsPermissionDialogController&_sessionID=" + _sessionID + "&account_id=" + _ads_id + "&include_headers=false&locale=en_US" +
					"&method=post&pretty=0&role=461336843905730&suppress_http_code=1&uid=" + viaid + "&xref=f3c98a378475d8";

				if (wait)
				{
					this.log("Waiting BillUp ...");
					bool waitBillUp = this.waitBillUp(20, "ad");
					if (!waitBillUp)
					{
						this.log("Waiting BillUp: NOT OK");
						return false;
					}
				}

				string resData = this.fetchApi(url, "POST", body, "", "", "https://www.facebook.com/");
				string success = Regex.Match(resData, "\"success\":(.*?),").Groups[1].Value;
				if ("true" != success)
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CreateWhatsAppBm(string whatsApp, string messagePath)
		{
			try
			{
				switchRegTabHandle();
				_wappPageID = pageID;
				_wappPageName = _rNamePage;
				string url = "https://www.facebook.com/" + _wappPageName.Replace(" ", "-") + "-" + _wappPageID + "/settings/?tab=whatsapp_management";
				if (!this._driver.Url.Contains(_rNamePage.Replace(" ", "-") + "-" + pageID + "/settings"))
				{
					_driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(1000);
				}

				string referrer = url;

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[] {
					"av=",
					pageID,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHK4HwBwIxt0mUyEqxenFw9uu2i5U4e2O14xt3odF8vyU8EW1twYwJyEiwsobo6u3y4o2Gwfi0LVEtwMw65xOfxOU7m221FwgolzUO0n2US2G3i0Boy1PwBgK7o884y0Mo4G4UfoowYwlE-UqwsUkxe2GewGwkUtxGm2SUbElxm3y3aexfxmu3W3y261eBx_y88EjwjE7eUsVobodEGdwp84O223908O321bw&__csr=&__req=3o&__hs=19069.HYP%3Acomet_pkg.2.1.0.2.&dpr=1&__ccg=GOOD&__rev=1005215872&__s=es68hx%3Avqt41y%3A8g2od0&__hsi=7076292036164186705-0&__comet_req=1&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22033&lsd=",
					lsd,
					"&__spin_r=1005215872&__spin_b=trunk&__spin_t=1647577629&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PageWhatsAppLinkingStartVerifyMutation&variables=%7B%22whatsapp_number%22%3A%22",
					whatsApp,
					"%22%2C%22page_id%22%3A%22",
					pageID,
					"%22%2C%22source%22%3A%22PAGE_SETTING%22%2C%22require_business_number%22%3Afalse%7D&server_timestamps=true&doc_id=3970879176294359&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D431626192%22%5D"
				});

				string postData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "PageWhatsAppLinkingStartVerifyMutation", lsd, referrer);
				string result = Regex.Match(postData, "\"result\":\"(.*?)\"").Groups[1].Value;
                if ("VERIFICATION_CODE_SEND_SUCCESS" != result)
                {
					this.log("VERIFICATION_CODE_SEND: NOT OK");
					return false;
				}

				this.log("GET VERIFICATION CODE: ...");
				string verifycode = "";
				bool checkfile = File.Exists(messagePath);
                if (!checkfile)
                {
					int ChkSuccessIndex = 1;
					while (!checkfile && (ChkSuccessIndex <= 8))
					{
						Delay(3000);
						checkfile = File.Exists(messagePath);
						ChkSuccessIndex++;
					}
				}

                if (!checkfile)
                {
					this.log("GET FILE VERIFICATION CODE: NOT OK");
					return false;
				}

				verifycode = File.ReadAllText(messagePath);
				if (string.IsNullOrEmpty(verifycode))
				{
					this.log("GET VERIFICATION CODE: NOT OK");
					return false;
				}

				this.log("Verify code: " + verifycode);
				this.log("VERIFY ...");
				body = string.Concat(new string[] {
					"av=",
					pageID,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHK4HwBwIxt0mUyEqxenFw9uu2i5U4e2O14xt3odF8vyU8EW1twYwJyEiwsobo6u3y4o2Gwfi0LVEtwMw65xOfxOU7m221FwgolzUO0n2US2G3i0Boy1PwBgK7o884y0Mo4G4UfoowYwlE-UqwsUkxe2GewGwkUtxGm2SUbElxm3y3aexfxmu3W3y261eBx_y88EjwjE7eUsVobodEGdwp84O223908O321bw&__csr=&__req=3t&__hs=19069.HYP%3Acomet_pkg.2.1.0.2.&dpr=1&__ccg=GOOD&__rev=1005215872&__s=es68hx%3Avqt41y%3A8g2od0&__hsi=7076292036164186705-0&__comet_req=1&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22033&lsd=",
					lsd,
					"&__spin_r=1005215872&__spin_b=trunk&__spin_t=1647577629&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PageAdminSettingsWhatsAppLinkAccountMutation&variables=%7B%22whatsapp_number%22%3A%22",
					whatsApp,
					"%22%2C%22page_id%22%3A%22",
					pageID,
					"%22%2C%22source%22%3A%22PAGE_SETTING%22%2C%22require_business_number%22%3Afalse%2C%22verification_code%22%3A%22",
					verifycode,
					"%22%2C%22register_wa_to_page%22%3Anull%7D&server_timestamps=true&doc_id=4017206958359178&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D431626192%22%5D"
				});
				postData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "PageAdminSettingsWhatsAppLinkAccountMutation", lsd, referrer);
				result = Regex.Match(postData, "\"whatsapp_display_number\":\"(.*?)\"").Groups[1].Value;
                if (string.IsNullOrEmpty(result))
                {
					string errocode = Regex.Match(postData, "\"code\":(.*?),").Groups[1].Value;
                    if (errocode == "3095003")
                    {
						string summary = Regex.Match(postData, "\"summary\":\"(.*?)\"").Groups[1].Value;
						this.log(summary);
						this.log("Đợi để kt BM...");
					}
                    else
                    {
						this.log(postData);
						return false;
					}
				}

				string gurl = "https://business.facebook.com/settings/people";
				if (!this._driver.Url.Contains("business.facebook.com/settings/people"))
				{
					this._driver.Navigate().GoToUrl(gurl);
					WaitLoading();
					Delay(500);
				}

				Uri theUri = new Uri(this._driver.Url);
				_bm_id = HttpUtility.ParseQueryString(theUri.Query).Get("business_id");
                if (string.IsNullOrEmpty(_bm_id))
                {
					this.log("Không check được BM!");
					return false;
				}
				_create_bm = true;
				this.log("BM: " + _bm_id);
				// this.getBmInfo();

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CreateScriptBm(string[] emailStepCode, bool verify = true)
		{
			try
			{
				createBmErrCode = "";
				this._driver.Navigate().GoToUrl(emailStepCode[2]);
				Delay(1000);

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string url = "https://business.facebook.com/business/create_account";

				int Timestamp = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
				string brand_name = "Nini" + Timestamp.ToString();

				string body = string.Concat(new string[] {
						"brand_name=",
						brand_name,
						"&first_name=",
						"Nini",
						"&last_name=",
						"Cuong",
						"&email=",
						emailBmConfirm,
						"&timezone_id=6&business_category=OTHER&city= &country=&state=&legal_name=&phone_number=&postal_code=&street1=&street2=&website_url=&is_b2b=false&__a=1&fb_dtsg=",
						fb_dtsg,
						"&jazoest=22058&lsd=",
						lsd,
						"&__spin_r=1004528366&__spin_b=trunk&__spin_t=1633753333&__jssesw=1"
					});

				string postData = this.fetchApi(url, "POST", body, "", lsd, emailStepCode[2], "include");
				string business_id = Regex.Match(postData, "business_id=(.*?)&").Groups[1].Value;
				//success
				if (string.IsNullOrEmpty(business_id))
				{
					string errorSummary = Regex.Match(postData, "\"errorSummary\":(.*?),").Groups[1].Value;
					this.log(errorSummary);
					// Limit
					if (errorSummary.Contains("Restricted") || errorSummary.Contains("restricted"))
                    {
						this.log("Clone bị hạn chế!");
						createBmErrCode = "Restricted";
						return false;
					}

					if (errorSummary.Contains("Limit") || errorSummary.Contains("limit"))
					{
						this.log("Clone đã tạo 2 bm!");
						createBmErrCode = "Restricted";
						return false;
					}

					return false;
				}
				_create_bm = true;

				string checkmail = "https://business.facebook.com/settings/people/?business_id=" + business_id;
				if (verify)
                {
					// Confirm email!
					checkmail = getConfirmBmFromEmail(brand_name);
					if (string.IsNullOrEmpty(checkmail))
					{
						Delay(5000);
						this.log("Check mail...");
						this.log("...");
						checkmail = getConfirmBmFromEmail(brand_name);
					}

					if (string.IsNullOrEmpty(checkmail))
					{
						Delay(5000);
						this.log("...");
						checkmail = getConfirmBmFromEmail(brand_name);
					}

					if (string.IsNullOrEmpty(checkmail))
					{
						Delay(5000);
						this.log("...");
						checkmail = getConfirmBmFromEmail(brand_name);
					}

					if (string.IsNullOrEmpty(checkmail))
					{
						Delay(5000);
						this.log("...");
						checkmail = getConfirmBmFromEmail(brand_name);
					}

					if (string.IsNullOrEmpty(checkmail))
					{
						Delay(5000);
						this.log("...");
						checkmail = getConfirmBmFromEmail(brand_name);
					}

					if (string.IsNullOrEmpty(checkmail))
					{
						Delay(5000);
						this.log("...");
						checkmail = getConfirmBmFromEmail(brand_name);
					}

					if (string.IsNullOrEmpty(checkmail))
					{
						Delay(5000);
						this.log("...");
						checkmail = getConfirmBmFromEmail(brand_name);
					}

					if (string.IsNullOrEmpty(checkmail))
					{
						Delay(5000);
						this.log("...");
						checkmail = getConfirmBmFromEmail(brand_name);
					}

					if (string.IsNullOrEmpty(checkmail))
					{
						this.log("Not mail confirm!");
						return false;
					}

					this.log("Verify email ok!");
                }
				
				this._driver.Navigate().GoToUrl(checkmail);
				WaitLoading();
				Delay(1000);
				Uri theUri = new Uri(this._driver.Url);
				_bm_id = HttpUtility.ParseQueryString(theUri.Query).Get("business_id");
				this.log("BM: " + _bm_id);

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool WhatsAppPageDisconect()
		{
			try
			{
				switchRegTabHandle();
				string url = "https://www.facebook.com/" + _wappPageName.Replace(" ", "-") + "-" + _wappPageID + "/settings/?tab=whatsapp_management";
				if (!this._driver.Url.Contains(_wappPageName.Replace(" ", "-") + "-" + _wappPageID + "/settings"))
				{
					_driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(1000);
				}

				string referrer = url;

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[] {
					"av=",
					_wappPageID,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHK4HwkECdg5KbxG4VuC0BVU98nwgU29gS3qi7UK361twYwJxS1NwJwpUe8hw2nVEtwMw65xO2O1Vwwwg8a8465o-cw5MKdwGwQw9m8wsU9kbxS2218wc61axe3S4Fof85qfK6E7e58jwGzEaE5e7oqBwJK2W5olwUwOzEjUlDw-wUws9ovUaU3qxWm2S3qazo2NwwwOg2cwMwhF8-4U&__csr=&__req=32&__hs=19081.HYP%3Acomet_pkg.2.1.0.2.&dpr=1&__ccg=EXCELLENT&__rev=1005268711&__s=3zrkoy%3Aszpb83%3Arars0a&__hsi=7080954119715044262-0&__comet_req=1&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22121&lsd=",
					lsd,
					"&__spin_r=1005268711&__spin_b=trunk&__spin_t=1648663105&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=CometPageAdminSettingsWhatsAppDisconnectNumberDialog_NumberDisconnectMutation&variables=%7B%22input%22%3A%7B%22client_mutation_id%22%3A%222%22%2C%22actor_id%22%3A%22",
					_wappPageID,
					"%22%2C%22page_id%22%3A%22",
					_wappPageID,
					"%22%2C%22referrer%22%3A%22page_setting%22%7D%7D&server_timestamps=true&doc_id=4023281384431126&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%2C431626192%22%5D"
				});

				string postData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "CometPageAdminSettingsWhatsAppDisconnectNumberDialog_NumberDisconnectMutation", lsd, referrer);
				string is_final = Regex.Match(postData, "\"is_final\":(.*?)}").Groups[1].Value;
				if ("true" != is_final)
				{
					this.log(postData);
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool WhatsAppBmDisconect(string phone)
		{
			try
			{
				switchBusinessTabHandle();
				if (string.IsNullOrEmpty(_bm_id))
				{
					return false;
				}

				if (string.IsNullOrEmpty(_accessToken))
				{
					return false;
				}

				if (string.IsNullOrEmpty(phone))
				{
					return false;
				}

				string url = "https://graph.facebook.com/v12.0/" + _bm_id + "/unlink_whatsapp_smb?access_token=" + _accessToken;
				string dataPost = string.Concat(new string[]
				{
					"_reqName=object%3Abusiness%2Funlink_whatsapp_smb&_reqSrc=WhatsAppAccountDataManager.brands&locale=en_US&method=post&phone_number=",
					phone.Replace("+", ""),
					"&pretty=0&suppress_http_code=1&xref=f1f78d1770f41"
				});
				string datapost = this.PostData(null, url, dataPost, this._contentType, this._userAgent, null);
				string status = Regex.Match(datapost, "success\": (.*?)}").Groups[0].Value;
				if ("true" != status)
				{
					this.log(datapost);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool cloneAdDeactive()
		{
			try
			{
                if (string.IsNullOrEmpty(_ads_id))
                {
					this.log("Không xác định Ad ID!");
					return false;
				}
				switchRegTabHandle();

				string referrer = "https://www.facebook.com/ads/manager/account_settings/information/?act=" + _ads_id + "&pid=p1&page=account_settings&tab=account_information";

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[]
				{
					"account_id=",
					_ads_id,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8aolJ28S2q3m9U8EJ4WqwOwCwgEpyA4WCG6UmCyEgwNxK4UKQ9wPGiidBxa7GzUK3G4Wxa6US2SfUgS4Ugwxwxx21FxG9y8Gdz8hwgocEaEcEixWq4o8E9EmwwCwXxKaCwTxqWBBwLjzu33hoC4U8RVodoKUryoiAwu8sgiCgOUa8lwWxe4oeUa8465udz8C1Mxu7o6-cwgHBDxWbwQwywWjy8gK7o46u2B0AgK7k48W2e2i3mbxOfwXxq1uK6S6UgyE9Ehjy88rwzzUuxe1dx-q4VEhwww9Oi2-fzobEaUiwrUK5Ue8Sp1G3WcwMzUkGum2ym2WEdEO8wl8hyVEKu9zawLCyKbwzwea1Pwhrw&__csr=&__req=o&__hs=19030.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005031848&__s=nr9842%3Az86zj2%3Abyq55y&__hsi=7061820981091549761-0&__comet_req=0&fb_dtsg=",
					token1,
					"&jazoest=21903&lsd=",
					token2,
					"&__spin_r=1005031848&__spin_b=trunk&__spin_t=1644208324"
				});

				string resData = this.fetchApi("https://www.facebook.com/ads/ajax/account_close/", "POST", body, "", token2, referrer);
				string rs_status = Regex.Match(resData, "\"consistency\":{(.*?)}").Groups[1].Value;
				if (string.IsNullOrEmpty(rs_status))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool regShopify()
		{
			try
			{
				this._driver.SwitchTo().Window(this.shopifyHandle);
				Delay(500);
				this._driver.Manage().Window.Maximize();
				this._driver.Navigate().GoToUrl("https://www.shopify.com/");
				WaitLoading();
				Delay(500);

				if (!elmAutoFinAndClick("//button[contains(@class, 'start-free-trial__button')]", 20))
				{
					this.log("Not Start free trial Btn!");
					return false;
				}

				if (!elmAutoFinAndClick("//div[@data-test-id='questionnaire']//button[@id='navigation-skip-button']", 7))
				{
					this.log("Not Skip 1 Btn!");
					return false;
				}

				if (!elmAutoFinAndClick("//div[@data-test-id='questionnaire']//button[@id='navigation-skip-button']", 7))
				{
					this.log("Not Skip 2 Btn!");
					return false;
				}

				if (!elmAutoFinAndClick("//div[@data-test-id='questionnaire']//button[@id='navigation-skip-button']", 7))
				{
					this.log("Not Skip 3 Btn!");
					return false;
				}

				if (!elmAutoFinAndClick("//div[@data-test-id='questionnaire']//button[@id='navigation-skip-button']", 7))
				{
					this.log("Not Skip 4 Btn!");
					return false;
				}

				int Timestamp = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
				string store_name = "ctc" + Timestamp.ToString();
				if (!elmAutoFinAndSendkey("//input[@id='storeName']", store_name))
				{
					this.log("Not storeName!");
					return false;
				}

				Delay(3000);
				if (!elmAutoFinAndClick("//div[@data-test-id='store-creator']//button[@id='navigation-next-button']", 7))
				{
					this.log("Not Next 1 Btn!");
					return false;
				}

				if (!elmAutoFinAndClick("//div[@data-test-id='country-selector']//button[@id='navigation-next-button']", 7))
				{
					this.log("Not Next 2 Btn!");
					return false;
				}

				bool loading = true;
				int loadIndex = 1;
				while (loading && (loadIndex <= 9))
				{
					Delay(3000);
					if (this._driver.Url.Contains("accounts.shopify.com/signup"))
					{
						loading = false;
					}
					loadIndex++;
				}
				Delay(1000);
				WaitLoading();
				Delay(2000);

				string signupStr = "a[href*='/signup?rid=']";
				WaitAjaxLoading(By.CssSelector(signupStr), 7);
				ReadOnlyCollection<IWebElement> iSignup = this._driver.FindElements(By.CssSelector(signupStr));
				if (iSignup.Count == 0)
				{
					this.log("Not signup 1!");
					return false;
				}
				iSignup.First().Click();
				Delay(1000);
				WaitLoading();
				Delay(2000);

				string emailstr = store_name + "@gmail.com";
				string pass = "hungcuongqn86";

				if (!elmAutoFinAndSendkey("//input[@name='account[email]']", emailstr))
				{
					this.log("Not email_input!");
					return false;
				}

				if (!elmAutoFinAndSendkey("//input[@name='account[password]']", pass))
				{
					this.log("Not password_input!");
					return false;
				}

				if (!elmAutoFinAndClick("//button[@name = 'commit' and @data-bind-disabled='captchaDisabled']"))
				{
					this.log("Not submit Btn!");
					return false;
				}

				bool success = false;
				int ChkSuccessIndex = 1;
				while (!success && (ChkSuccessIndex <= 20))
				{
					Delay(3000);
					if (this._driver.Url.Contains("myshopify.com/admin"))
					{
						success = true;
					}
					ChkSuccessIndex++;
				}
				Delay(1000);

				WaitLoading();
				Delay(2000);

				// Browse not supported by Shopify
				shopifyPassBrowserNotSupport();

				// Add app
				this._driver.Navigate().GoToUrl("https://apps.shopify.com/facebook?search_id=cf3b9db8-22a8-4f97-a11c-" + store_name + "&surface_detail=facebook&surface_inter_position=1&surface_intra_position=2&surface_type=search");
				WaitLoading();
				Delay(500);

				if (!elmAutoFinAndClick("//div[@id='app_pricing_and_actions-content']//input[@type='submit' and @value='Add app']"))
				{
					this.log("Not Add App Btn!");
					return false;
				}
				WaitLoading();
				Delay(500);

				success = false;
				ChkSuccessIndex = 1;
				while (!success && (ChkSuccessIndex <= 10))
				{
					Delay(3000);
					if (this._driver.Url.Contains("myshopify.com/admin/oauth/request_grant"))
					{
						success = true;
					}
					ChkSuccessIndex++;
				}
				Delay(1000);

				WaitLoading();
				Delay(2000);

				// Browse not supported by Shopify
				shopifyPassBrowserNotSupport();
				WaitLoading();
				Delay(2000);

				if (!elmAutoFinAndClick("//div[contains(@class, 'Polaris-Page-Header__RightAlign')]//div[contains(@class, 'Polaris-Page-Header__PrimaryActionWrapper')]//button[@type='button' and @aria-disabled='false']"))
				{
					this.log("Not Add sales channel Btn!");
					return false;
				}
				WaitLoading();
				Delay(500);

				success = false;
				ChkSuccessIndex = 1;
				while (!success && (ChkSuccessIndex <= 10))
				{
					Delay(3000);
					if (this._driver.Url.Contains("myshopify.com/admin/apps"))
					{
						success = true;
					}
					ChkSuccessIndex++;
				}
				Delay(1000);

				WaitLoading();
				Delay(2000);

				// Browse not supported by Shopify
				shopifyPassBrowserNotSupport();
				WaitLoading();
				Delay(2000);

				string iframeStr = "iframe[src*='commercepartnerhub.com/shopify_app/confirm_installation']";
				WaitAjaxLoading(By.CssSelector(iframeStr), 10);
				ReadOnlyCollection<IWebElement> iFrame = this._driver.FindElements(By.CssSelector(iframeStr));

				if (iFrame.Count == 0)
				{
					this.log("Not commercepartnerhub iFrame!");
					return false;
				}

				this._driver.SwitchTo().Frame(iFrame.First());
				Delay(500);

				if (!elmAutoFinAndClick("//div[contains(@class, 'Polaris-Stack--alignmentCenter')]//div[contains(@class, 'olaris-Stack__Item')]//button[@type='button' and contains(@class, 'Polaris-Button--primary')]", 30))
				{
					this.log("Not Start setup Btn!");
					this._driver.SwitchTo().DefaultContent();
					return false;
				}
				WaitLoading();
				Delay(500);

				this._driver.SwitchTo().DefaultContent();
				shopifyRegData = store_name + ".myshopify.com";
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private bool shopifyPassBrowserNotSupport()
		{
			try
			{
				// Browse not supported by Shopify
				WaitAjaxLoading(By.CssSelector("a[href*='/shopify-admin/supported-browsers']"), 7);
				ReadOnlyCollection<IWebElement> iSupported = this._driver.FindElements(By.CssSelector("a[href*='/shopify-admin/supported-browsers']"));
				if (iSupported.Count > 0)
				{
					if (!elmAutoFinAndJsClick("//button[@type='button']", 5))
					{
						this.log("Not Continue_Btn!");
						return false;
					}
				}
				WaitLoading();
				Delay(500);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private string getShopifyAccessToken(string fromTab)
		{
			try
			{
				string token = "";
				switchRegTabHandle();

				this._driver.Navigate().GoToUrl("https://www.facebook.com/dialog/oauth?app_id=699341854433150&scope=push_read%2Cpush_link&state=%7B%22parent_window_domain%22%3A%22www.commercepartnerhub.com%22%2C%22request_id%22%3A%2212e6a35b-1837-423b-b511-b744cdfa3cba%22%7D&redirect_uri=https%3A%2F%2Fbusiness.facebook.com%2Fshopify_app%2Fauthenticate%2F");
				WaitLoading();
				Delay(500);
				String pageSource = "";

				if (this._driver.Url.Contains("shopify_app/authenticate/?state="))
                {
					pageSource = _driver.PageSource;
					token = Regex.Match(pageSource, "\"EAAr(.*?)\"").Groups[1].Value;
                    if (string.IsNullOrEmpty(token))
                    {
						this._driver.SwitchTo().Window(fromTab);
						return null;
                    }
                    else
                    {
						this._driver.SwitchTo().Window(fromTab);
						return "EAAr" + token;
					}
				}

				if (this._driver.Url.Contains("privacy/consent/user_cookie_choice"))
				{
					elmAutoFinAndJsClick("//div[@role='button' and contains(@aria-label, 'llow all cookie') and not(@aria-disabled='true')]", 10);
					Delay(500);
					WaitLoading();
					Delay(500);
				}

				if (!this._driver.Url.Contains("dialog/oauth?app_id="))
				{
					this._driver.SwitchTo().Window(fromTab);
					return null;
				}

				if (elmAutoFinAndJsClick("//div[@role='button' and not(@aria-disabled='true')]", 10))
                {
					Delay(500);
					WaitLoading();
					Delay(500);
					if (this._driver.Url.Contains("dialog/oauth/push/read"))
                    {
						if (elmAutoFinAndJsClick("//div[@role='button' and not(@aria-disabled='true')]", 10))
                        {
							if (this._driver.Url.Contains("dialog/oauth/push/link"))
                            {
								elmAutoFinAndJsClick("//div[@role='button' and not(@aria-disabled='true')]", 10);
								Delay(500);
								WaitLoading();
							}
						}
					}
				}

				if (!this._driver.Url.Contains("shopify_app/authenticate/?state="))
				{
					this._driver.SwitchTo().Window(fromTab);
					return null;
				}

				pageSource = _driver.PageSource;
				token = Regex.Match(pageSource, "\"EAAr(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(token))
				{
					this._driver.SwitchTo().Window(fromTab);
					return null;
				}
				else
				{
					this._driver.SwitchTo().Window(fromTab);
					return "EAAr" + token;
				}
			}
			catch (Exception ex)
			{
				this.log(ex);
				this._driver.SwitchTo().Window(fromTab);
				return null;
			}
		}

		private string shopifySessionTokenFetch(string url, string body, string csrf = "")
		{
			try
			{
				string sec = "\\\"Chromium\\\";v=\\\"94\\\", \\\"Google Chrome\\\";v=\\\"94\\\", \\\";Not A Brand\\\";v=\\\"99\\\"";
				string win = "\\\"Windows\\\"";

				string csrfStr = "";
				if (!string.IsNullOrEmpty(csrf))
				{
					csrfStr = "\"x-csrf-token\": \"" + csrf + "\"";
				}

				string jsstr = "let response = await fetch(\"" + url + "\", {" +
									"\"headers\": {" +
										"\"accept\": \"application/json\"," +
										"\"accept-language\": \"en-US,en;q=0.9\"," +
										"\"content-type\": \"application/json\"," +
										"\"sec-ch-ua\": \"" + sec + "\"," +
										"\"sec-ch-ua-mobile\": \"?0\"," +
										"\"sec-ch-ua-platform\": \"" + win + "\"," +
										"\"sec-fetch-dest\": \"empty\"," +
										"\"sec-fetch-mode\": \"cors\"," +
										"\"sec-fetch-site\": \"same-origin\"," +
										"\"x-shopify-web-force-proxy\": \"1\"," +
										csrfStr +
									"}," +
									"\"referrerPolicy\": \"no-referrer\"," +
									"\"body\": \"" + body + "\"," +
									"\"method\": \"POST\"," +
									"\"mode\": \"cors\"," +
									"\"credentials\": \"include\"" +
								"}); return await response.text();";
				return (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private string getShopifySessionToken(string storeName)
		{
			try
			{
				String pageSource = _driver.PageSource;
				string csrf = Regex.Match(pageSource, "\"csrf-token\">\"(.*?)\"").Groups[1].Value;

				string url = "https://" + storeName + "/admin/internal/web/graphql/core?operation=GenerateSessionToken&type=mutation";
				string body = string.Concat(new string[] {
					"{\\\"operationName\\\":\\\"GenerateSessionToken\\\",\\\"variables\\\":{\\\"appId\\\":\\\"gid://shopify/App/2329312\\\"},\\\"query\\\":\\\"mutation GenerateSessionToken($appId: ID!) {\\\\n  adminGenerateSession(appId: $appId) {\\\\n    session\\\\n    __typename\\\\n  }\\\\n}\\\\n\\\"}"
				});

				string postData = shopifySessionTokenFetch(url, body, csrf);
                if (string.IsNullOrEmpty(postData))
                {
					this.log(postData);
					return null;
				}
				return Regex.Match(postData, "\"session\":\"(.*?)\"").Groups[1].Value;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private string shopifyCreateBmFetch(string url, string body, string session_token, string lsd, string referrer)
		{
			try
			{
				string sec = "\\\"Chromium\\\";v=\\\"94\\\", \\\"Google Chrome\\\";v=\\\"94\\\", \\\";Not A Brand\\\";v=\\\"99\\\"";
				string win = "\\\"Windows\\\"";
				string jsstr = "let response = await fetch(\"" + url + "\", {" +
									"\"headers\": {" +
										"\"accept\": \"*/*\"," +
										"\"accept-language\": \"en-US,en;q=0.9\"," +
										"\"authorization\": \"Bearer " + session_token + "\"," +
										"\"content-type\": \"application/x-www-form-urlencoded\"," +
										"\"sec-ch-ua\": \"" + sec + "\"," +
										"\"sec-ch-ua-mobile\": \"?0\"," +
										"\"sec-ch-ua-platform\": \"" + win + "\"," +
										"\"sec-fetch-dest\": \"empty\"," +
										"\"sec-fetch-mode\": \"cors\"," +
										"\"sec-fetch-site\": \"same-origin\"," +
										"\"x-fb-friendly-name\": \"ShopifyOnboardingConfirmPageSelectionMutation\"," +
										"\"x-fb-lsd\": \"" + lsd + "\"" +
									"}," +
									"\"referrer\": \"" + referrer + "\"," +
									"\"referrerPolicy\": \"origin-when-cross-origin\"," +
									"\"body\": \"" + body + "\"," +
									"\"method\": \"POST\"," +
									"\"mode\": \"cors\"," +
									"\"credentials\": \"include\"" +
								"}); return await response.text();";
				return (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private string shopifyCreateBm(string session_token, string access_token, string applicationInstallationID, string lsd)
		{
			try
			{
				switchBusinessTabHandle();
				this._driver.Navigate().GoToUrl("https://www.commercepartnerhub.com/shopify_app/commerce_onboard");
				WaitLoading();
				Delay(500);

				string url = "https://www.commercepartnerhub.com/shopify_app/graphql/";
				string body = string.Concat(new string[] {
					"locale=en&session_token=",
					session_token,
					"&access_token=",
					access_token,
					"&__user=0&__a=1&__dyn=7xeUmxa3-Q8zo5ObwKBWobVo9E4a2i5U4e1Fx-ewSAAzooxW4E2qwJw5ux60Vo1upE4W1Zg4K2C0A8swIwuo9oeUa8465o-0nSUS1vw4iwBgao882DwoE5W3S12xq1izUuw9O0RE5a1qxa1XwnE2Lxiaw820H8bodEGdw46wHwAwr84K2e1FwsXxeu2yi4E&__csr=&__req=m&__hs=19302.BP%3ADEFAULT.2.0.0.0.0&dpr=1&__ccg=EXCELLENT&__rev=1006554238&__s=%3Ay72my2%3Adh4yx4&__hsi=7162811815342856823&__comet_req=0&lsd=",
					lsd,
					"&jazoest=21860&__spin_r=1006554238&__spin_b=trunk&__spin_t=1667722084&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=ShopifyOnboardingConfirmPageSelectionMutation&variables=%7B%22input%22%3A%7B%22client_mutation_id%22%3A%221%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22business_id%22%3Anull%2C%22page_id%22%3A%22",
					pageID,
					"%22%2C%22shopify_installation_id%22%3A%22",
					applicationInstallationID,
					"%22%7D%7D&server_timestamps=true&doc_id=5328709937148025&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D975770238%22%5D"
				});

				string postData = shopifyCreateBmFetch(url, body, session_token, lsd, _driver.Url);
				if (string.IsNullOrEmpty(postData))
				{
					this.log(postData);
					return null;
				}
				return Regex.Match(postData, "\"owner_business\":{\"id\":\"(.*?)\"").Groups[1].Value;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		public bool CreateShopifyBm(string storeName, string[] stepcode = null)
		{
			try
			{
				string storeUrl = "https://" + storeName + "/admin/apps/facebook-ads/shopify_app/commerce_onboard";
				this._driver.SwitchTo().Window(this.shopifyHandle);
				Delay(500);
				this._driver.Navigate().GoToUrl(storeUrl);
				WaitLoading();
				Delay(500);
				shopifyPassBrowserNotSupport();

				// applicationInstallationID
				string iframeStr = stepcode[0];
				WaitAjaxLoading(By.CssSelector(iframeStr), 10);
				ReadOnlyCollection<IWebElement> iFrame = this._driver.FindElements(By.CssSelector(iframeStr));

				if (iFrame.Count == 0)
                {
					this._driver.Navigate().Refresh();
					WaitLoading();
					Delay(500);
					shopifyPassBrowserNotSupport();

					WaitAjaxLoading(By.CssSelector(iframeStr), 10);
					iFrame = this._driver.FindElements(By.CssSelector(iframeStr));
				}

				if (iFrame.Count == 0)
				{
					this.log("Not iFrame!");
					return false;
				}

				this._driver.SwitchTo().Frame(iFrame.First());
				Delay(500);
				String pageSource = _driver.PageSource;
				string applicationInstallationID = Regex.Match(pageSource, "\"applicationInstallationID\":\"(.*?)\"").Groups[1].Value;

				int ChkIndex = 1;
				while (string.IsNullOrEmpty(applicationInstallationID) && (ChkIndex <= 10))
				{
					Delay(3000);
					pageSource = _driver.PageSource;
					applicationInstallationID = Regex.Match(pageSource, "\"applicationInstallationID\":\"(.*?)\"").Groups[1].Value;
					ChkIndex++;
				}
				
				string lsd = Regex.Match(pageSource, "\"token\":\"(.*?)\"").Groups[1].Value;
                if (string.IsNullOrEmpty(applicationInstallationID))
                {
					this.log("Không lấy được applicationInstallationID!");
					this._driver.SwitchTo().DefaultContent();
					return false;
				}
				if (string.IsNullOrEmpty(lsd))
				{
					this.log("Không lấy được lsd!");
					this._driver.SwitchTo().DefaultContent();
					return false;
				}
				this.log("applicationInstallationID: " + applicationInstallationID);
				this._driver.SwitchTo().DefaultContent();

				// Lay fbAuthen access_token=EAAr...
				string shopifyAccessToken = getShopifyAccessToken(this.shopifyHandle);
				if (string.IsNullOrEmpty(shopifyAccessToken))
				{
					this.log("Không lấy được shopifyAccessToken!");
					return false;
				}
				this.log("shopifyAccessToken: " + shopifyAccessToken);
				// Lay session token
				string shopifySessionToken = getShopifySessionToken(storeName);
				if (string.IsNullOrEmpty(shopifySessionToken))
				{
					this.log("Không lấy được shopifySessionToken!");
					return false;
				}
				this.log("shopifySessionToken: " + shopifySessionToken);
				// Tao BM
				string bmId = shopifyCreateBm(shopifySessionToken, shopifyAccessToken, applicationInstallationID, lsd);
				if (string.IsNullOrEmpty(bmId))
				{
					return false;
				}

				_bm_id = bmId;
				_create_bm = true;
				this.log("BM: " + _bm_id);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public string getConfirmBmFromEmail(string bmName)
		{
			string confirmUrl = "";
			try
			{
				using (var client = new ImapClient())
				{
					using (var cancel = new CancellationTokenSource())
					{
						client.Connect("imap.gmail.com", 993, true, cancel.Token);

						// If you want to disable an authentication mechanism,
						// you can do so by removing the mechanism like this:
						client.AuthenticationMechanisms.Remove("XOAUTH");

						client.Authenticate(emailBmConfirm, emailPassBmConfirm, cancel.Token);

						// The Inbox folder is always available...
						var inbox = client.Inbox;
						inbox.Open(FolderAccess.ReadOnly, cancel.Token);

						DateTime date = DateTime.Today.AddMinutes(-15);

						// let's try searching for some messages...
						var query = SearchQuery.DeliveredAfter(date)
							.And(SearchQuery.FromContains("notification@facebookmail.com"))
							.And(SearchQuery.BodyContains(bmName));

						foreach (var uid in inbox.Search(query, cancel.Token))
						{
							var message = inbox.GetMessage(uid, cancel.Token);
							string token = Regex.Match(message.HtmlBody, "token=(.*?)\"").Groups[1].Value;
							if (!string.IsNullOrEmpty(token))
							{
								confirmUrl = "https://www.facebook.com/verify/email/checkpoint/?token=" + token;
								break;
							}
						}

						client.Disconnect(true, cancel.Token);
					}
				}

				return confirmUrl;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return "";
			}
		}

		public bool getPageInfo()
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v12.0/",
					pageID,
					"?access_token=",
					_accessToken,
					"&fields=%5B%22instagram_accounts%22%2C%22page_backed_instagram_accounts%22%2C%22name%22%5D&include_headers=false&pretty=0"
				});

				string resData = this.GetData(null, url, _cookie, this._userAgent);
				page pageData = JsonConvert.DeserializeObject<page>(resData);
				_rNamePage = pageData.name;
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public bool getBmInfo()
		{
			try
			{
				if (string.IsNullOrEmpty(_bm_id))
				{
					this.log("NOT BM!");
					return false;
				}

				string url = "https://business.facebook.com/settings/ad-accounts/?business_id=" + _bm_id;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					_driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(1000);
				}

				String pageSource = _driver.PageSource;
				_accessToken = Regex.Match(pageSource, "\"accessToken\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(_accessToken))
				{
					_accessToken = Regex.Match(pageSource, "\"accessToken\":\"(.*?)\"").NextMatch().Groups[1].Value;
				}

				if (string.IsNullOrEmpty(_accessToken))
				{
					this.log("NOT BM TOKEN!");
					return false;
				}
				this.log("Token: " + _accessToken);

				_business_user_id = Regex.Match(pageSource, "\"business_user_id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(_business_user_id))
				{
					this.log("NOT Business User ID!");
					return false;
				}
				this.log("Business User ID: " + _business_user_id);

				// Get info
				url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v11.0/",
					_bm_id,
					"?access_token=",
					_accessToken,
					"&_reqName=object%3Abrand&_reqSrc=BrandResourceRequests.brands&date_format=U&fields=%5B%22id%22%2C%22name%22%2C%22vertical_id%22%2C%22timezone_id%22%2C%22picture.type(square)%22%2C%22primary_page.fields(name%2C%20picture%2C%20link)%22%2C%22payment_account_id%22%2C%22link%22%2C%22created_time%22%2C%22created_by.fields(name)%22%2C%22updated_time%22%2C%22updated_by.fields(name)%22%2C%22extended_updated_time%22%2C%22two_factor_type%22%2C%22allow_page_management_in_www%22%2C%22eligible_app_id_for_ami_initiation%22%2C%22verification_status%22%2C%22sharing_eligibility_status%22%2C%22can_create_ad_account%22%2C%22can_use_extended_credit%22%2C%22is_business_verification_eligible%22%2C%22is_tier_restricted%22%2C%22is_tier_pending_compromise_review%22%2C%22is_non_discrimination_certified%22%2C%22can_add_or_create_page%22%2C%22has_opted_out_business_verification%22%2C%22business_verification_directional_exp_bucket%22%5D&locale=en_US&method=get&pretty=0&suppress_http_code=1&xref=f34b13d995ffc84"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				_payment_account_id = Regex.Match(data, "\"payment_account_id\":\"(.*?)\"").Groups[1].Value;
				this.log("payment_account_id: " + _payment_account_id);
				if (string.IsNullOrEmpty(_payment_account_id))
				{
					this.log("NOT BM payment_account_id!");
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool adPixelShare()
		{
			try
			{
				return this.sharePixel(_ads_id);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool BmAdPixelShare()
		{
			try
			{
				return this.sharePixel(_bm_ads_id);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private bool sharePixel(string ad)
		{
			try
			{
				if (string.IsNullOrEmpty(ad))
				{
					return false;
				}

				string url = "http://nguyenlieu.site/api/public/api/v1/tools/sharepixel";
				var reqParams = new RequestParams();
				reqParams["ad_id"] = ad;
				string datapost = this.autoRequest.PostToStoreData(url, reqParams);
				string status = Regex.Match(datapost, "\"success\":(.*?)}").Groups[1].Value;
				if ("true" != status)
				{
					status = Regex.Match(datapost, "\"success\":(.*?),").Groups[1].Value;
				}
				if ("true" != status)
				{
					this.log("ad_id: " + ad);
					this.log("datapost: " + datapost);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool selfCert()
		{
			try
			{
				switchRegTabHandle();

				if (!this._driver.Url.Contains("https://www.facebook.com"))
				{
					_driver.Navigate().GoToUrl("https://www.facebook.com");
					WaitLoading();
					Delay(500);
				}

				string getData = _driver.PageSource;
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				string self_cert_url = "https://www.facebook.com/ads/integrity/user/self_cert/ajax/";
				string self_cert_referrer = "https://www.facebook.com/adsmanager/manage/campaigns?nav_entry_point=bm_global_nav_shortcut";
				string self_cert_body = string.Concat(new string[] {
						"source=boosted_component_create&__usid=&__user=",
						_clone_uid,
						"&__a=1&__dyn=7xeUmBz8fXgydwCwRyU8EKnFGm4oqwCwgE98kGqErG6EG48corxebzEcuF98SmcBxWE-16UkxaczES2SfUg-mdx22q78gw8i9y8G6EhwgpUtz8aE4mewEwyg9poe8S2S6UGq1eKFprzUrgS-2TlaECfl0zlBwyxabK6UC4F87y78jCgOVojzE-mdwIxe4oeUS6E465ubUO9wJwmE2dz8txSqUS7EK2ifBwlUO4o8onwFDyovg942B2V8W2efxi11xOfwXxq1uxZyAfx22Oi4p8y26U8U-ezUWi2y2i3G6awl86mi3-dwKwHxa2i4AUryrxi5Ue8Su6E2cxi9hoqwZCg-3ecyUDwDx24oiyVUCcG225oGcgjgW4E4Ocwtp-1ewnKV8&__csr=&__req=18&__hs=19032.BP%3Aads_manager_pkg.2.0.0.0.&dpr=1&__ccg=UNKNOWN&__rev=1005043881&__s=hhgn1x%3Avg5v43%3Acdstl7&__hsi=7062597305439533608-0&__comet_req=0&fb_dtsg=",
						fb_dtsg,
						"&jazoest=21897&lsd=",
						lsd,
						"&__spin_r=1005043881&__spin_b=trunk&__spin_t=1644389076&__jssesw=1"
				});
				string self_cert_postData = this.PostData(null, self_cert_url, self_cert_body, this._contentType, this._userAgent, _cookie, lsd, null, self_cert_referrer);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private void Unhide(IWebDriver driver, IWebElement element)
		{
			try
			{
				String script = "arguments[0].style.opacity=1;"
				  + "arguments[0].style['transform']='translate(0px, 0px) scale(1)';"
				  + "arguments[0].style['MozTransform']='translate(0px, 0px) scale(1)';"
				  + "arguments[0].style['WebkitTransform']='translate(0px, 0px) scale(1)';"
				  + "arguments[0].style['msTransform']='translate(0px, 0px) scale(1)';"
				  + "arguments[0].style['OTransform']='translate(0px, 0px) scale(1)';"
				  + "return true;";
				((IJavaScriptExecutor)driver).ExecuteScript(script, element);
			}
			catch (Exception error)
			{
				Console.WriteLine(error.Message.ToString());
			}
		}

		public bool peLaunchingCamp()
		{
			try
			{
				switchRegTabHandle();

				string gurl = "https://www.facebook.com/adsmanager/?act=" + _ads_id + "&tool=MANAGE_ADS&nav_entry_point=bm_global_nav_shortcut&nav_source=flyout_menu";
				this._driver.Navigate().GoToUrl(gurl);
				WaitLoading();
				Delay(500);
				string getData = _driver.PageSource;

				string addraft = Regex.Match(getData, "\"draft_version\":\"1\",\"id\":\"(.*?)\"").Groups[1].Value;
				string app_id = Regex.Match(getData, "\"app_id\":\"(.*?)\"").Groups[1].Value;
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;

				string campAccessToken = Regex.Match(getData, "__accessToken=\"(.*?)\"").Groups[1].Value;
				string async_get_token = Regex.Match(getData, "\"async_get_token\":\"(.*?)\"").Groups[1].Value;

				// Self Cert
				string self_cert_url = "https://www.facebook.com/ads/integrity/user/self_cert/ajax/";
				string self_cert_referrer = "https://www.facebook.com/adsmanager/manage/campaigns?act=" + _ads_id + "&nav_entry_point=bm_global_nav_shortcut";
				string self_cert_body = string.Concat(new string[] {
					"source=boosted_component_create&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8fXgydwCwRyU8EKnFGm4oqwCwgE98kGqErG6EG48corxebzEcuF98SmcBxWE-16UkxaczES2SfUg-mdx22q78gw8i9y8G6EhwgpUtz8aE4mewEwyg9poe8S2S6UGq1eKFprzUrgS-2TlaECfl0zlBwyxabK6UC4F87y78jCgOVojzE-mdwIxe4oeUS6E465ubUO9wJwmE2dz8txSqUS7EK2ifBwlUO4o8onwFDyovg942B2V8W2efxi11xOfwXxq1uxZyAfx22Oi4p8y26U8U-ezUWi2y2i3G6awl86mi3-dwKwHxa2i4AUryrxi5Ue8Su6E2cxi9hoqwZCg-3ecyUDwDx24oiyVUCcG225oGcgjgW4E4Ocwtp-1ewnKV8&__csr=&__req=18&__hs=19032.BP%3Aads_manager_pkg.2.0.0.0.&dpr=1&__ccg=UNKNOWN&__rev=1005043881&__s=hhgn1x%3Avg5v43%3Acdstl7&__hsi=7062597305439533608-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=21897&lsd=",
					lsd,
					"&__spin_r=1005043881&__spin_b=trunk&__spin_t=1644389076&__jssesw=1"
				});

				string self_cert_postData = this.fetchApi(self_cert_url, "POST", self_cert_body, "", lsd, self_cert_referrer);

				// check Fragments
				List<string> allFragments = this.getAddraftFragments(addraft, campAccessToken);
                if (allFragments.Count == 0)
                {
					this.log("Import: ...");
					string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
					string dataPath = desktopPath + "\\" + _dataDir;
					string campPath = dataPath + "\\camp\\camp_temp.txt";

					string campTemp = File.ReadAllText(campPath);
					String[] postIdData = postID.Split('_');
					if (postIdData.Length > 1)
					{
						campTemp = campTemp.Replace("#page_id#", postIdData[0]);
						campTemp = campTemp.Replace("#post_id#", postIdData[1]);
					}

					campTemp = Uri.EscapeDataString(campTemp);

					string purl = "https://www.facebook.com/adsmanager/loadtsv/";
					string referrer = "https://www.facebook.com/adsmanager/manage/campaigns?act=" + _ads_id + "&nav_entry_point=bm_global_nav_shortcut";
					string body = string.Concat(new string[] {
						"account_id=",
						_ads_id,
						"&app_id=",
						app_id,
						"&draft_id=",
						addraft,
						"&image_mapping&video_mapping&import_session_token=f2cea62fabaf5e&tsv=",
						campTemp,
						"&__usid=&__user=",
						_clone_uid,
						"&__a=1&__dyn=7AgSXghLzaxd2um5rgydg9omoiyoK6FVpkjFGm4rxq2q12wABzGCGq5axeqaScCCG225pojACjyocuF98SmqnK7GzUuwDUkxaczESbwxKbUCrVoS489EsGdzAayU4a5AEC8yEScx6bxW5FQ4Vbz8ix2q9hUhzoizE-Hx6exd0BAggwwCzoO69UryFE4eaKFprzu6QUCZ0IRiG9DDl0zlBxabKdAyXzAbyoiAyU6O78jCgOVp8W9AylmnyUb8jz98eUS48C11xny-cyotzU4a5UiGm1hguz478shHBDx6cyU98-m79UcF9Q4bxS5UapUC4UJ164AbxR2V8W5Hy8-4Q3mbzryE-5o9ohAwkEKUroF3UgwIAx5e8xSXzUC9zUWfzF8a898O4FEjCx6EO48dE9Ufp8bU-dwKUuyEiwAgCnjxK9K8yUnwUzpUqwWKaUjxyU-5aChoCUOdBwDQZ3UcUyUK9Uixi48hyVEKu9DAm22eCyKqQfJ3Eiwj9pE7mvxu9wIwnKV9E&__csr=&__req=1l&__hs=19029.BP%3Aads_manager_pkg.2.0.0.0.&dpr=1&__ccg=UNKNOWN&__rev=1005031593&__s=vzp2s0%3Aq7jgft%3Azeosex&__hsi=7061608490789623790-0&__comet_req=0&fb_dtsg=",
						fb_dtsg,
						"&jazoest=21869&lsd=",
						lsd,
						"&__spin_r=1005031593&__spin_b=trunk&__spin_t=1644158850&__jssesw=1"
					});

					string postData = this.fetchApi(purl, "POST", body, "", lsd, referrer, "include", "270216139");
					string async_session_id = Regex.Match(postData, "\"async_session_id\":\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(async_session_id))
					{
						this.log("Import: FALSE!");
						return false;
					}
					this.log("Import: OK!");
				}

				// Check Fragments
				this.log("Check Fragments: ...");
				bool checkImport = false;
				
				int ChkImportIndex = 1;
				while ((!checkImport) && (ChkImportIndex <= 8))
				{
					Delay(3000);
					allFragments = this.getAddraftFragments(addraft, campAccessToken);
					if (allFragments.Count > 2)
					{
						checkImport = true;
					}

					ChkImportIndex++;
				}

				if (!checkImport)
				{
					this.log("Fragments Data: " + allFragments.ToString());
					this.log("Fragments: FALSE!");
					return false;
				}

				// Publish
				string fragmentsStr = "%5B%22" + String.Join("%22%2C%22", allFragments.ToArray()) + "%22%5D";

				string publishurl = string.Concat(new string[] {
					"https://graph.facebook.com/v11.0/",
					addraft,
					"/publish?_app=ADS_MANAGER&_reqName=object%3Adraft_id%2Fpublish&access_token=",
					campAccessToken,
					"&method=post&qpl_active_flow_ids=270208286%2C270216423&qpl_active_flow_instance_ids=270216423_f1ab840000000&__cppo=1"
				});
				// this.log(publishurl);

				string publishreferrer = "https://www.facebook.com/";
				string publishbody = string.Concat(new string[] {
					"__activeScenarioIDs=%5B%22%22%5D&__activeScenarios=%5B%22review_and_publish%22%5D&_app=ADS_MANAGER&_reqName=object%3Adraft_id%2Fpublish&_reqSrc=AdsDraftPublishDataManager&_sessionID=",
					sessionID,
					"&append=false&fragments=",
					fragmentsStr,
					"&ignore_errors=true&include_fragment_statuses=true&include_headers=false&locale=en_US&method=post&pretty=0&qpl_active_flow_ids=270208286%2C270216423&qpl_active_flow_instance_ids=270216423_f1ab840000000&suppress_http_code=1&xref=f1557172faee02"
				});

				// this.log(publishbody);
				string publishData = this.fetchApi(publishurl, "POST", publishbody, "", "", publishreferrer, "omit");
				string success = Regex.Match(publishData, "\"success\":(.*?),").Groups[1].Value;
				//success
				if (success != "true")
				{
					this.log(publishData);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public List<string> getAddraftFragments(string addraft, string campAccessToken)
		{
			try
			{
				string url = string.Concat(new string[]
					{
						"https://graph.facebook.com/v15.0/addraft_",
						addraft,
						"/addraft_fragments?access_token=",
						campAccessToken,
						"&include_headers=false&limit=3&locale=en_US&method=get&pretty=0"
					});
				string resData = this.fetchGet(url);
				MatchCollection matches = Regex.Matches(resData, "\"id\":\"(.*?)\"");
				List<string> allFragments = new List<string> { };
				if (matches.Count > 0)
				{
					foreach (Match match in matches)
					{
						try
						{
							allFragments.Add(match.Groups[1].Value);
						}
						catch { }
					}
				}

				return allFragments;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return new List<string> { };
			}
		}

		private string getVideo(string id, string accessToken)
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v12.0/",
					id,
					"?access_token=",
					accessToken,
					"&fields=%5B%22captions%22%2C%22length%22%2C%22video_asset_id%22%5D&include_headers=false&locale=en_US&method=get&pretty=0"
				});
				return this.fetchGet(url);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private string getVideoContent(string id, string accessToken)
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v14.0/media_manager/contents?access_token=",
					accessToken,
					"&content_list=%5B%22",
					id,
					"%22%5D&fields=%5B%22video_asset_id%22%2C%22video_container_post_id%22%5D&locale=en_US&method=get&page_list=%5B%5D&pretty=0&suppress_http_code=1&video_collaborator_type=PRIMARY&xref=ff9f112075d6d8"
				});
				return this.fetchGet(url);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		public bool TKBMPeLaunchingCamp(string nlCampId, string pixel_id = "", bool off_ads = false)
		{
			try
			{
				switchBusinessTabHandle();

				string rurl = "https://business.facebook.com/adsmanager/manage/campaigns?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&global_scope_id=" + _bm_id + "&tool=MANAGE_ADS&nav_entry_point=bm_global_nav_shortcut&nav_source=flyout_menu&nav_id=1565291292about%3Ablank";
				string getData = this.fetchGet(rurl);
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(fb_dtsg))
				{
					rurl = "https://business.facebook.com/adsmanager/manage/accounts?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					getData = this.fetchGet(rurl);
					fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				}
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string addraft = Regex.Match(getData, "\"ad_draft_id\":\"(.*?)\"").Groups[1].Value;

				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"1\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"2\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"3\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "addraft_(.*?){\"fields\"").Groups[1].Value;
				}

				if (string.IsNullOrEmpty(addraft))
				{
					this.log("Không thấy bản thảo!");
					return false;
				}

				string sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
				string campAccessToken = Regex.Match(getData, "__accessToken=\"(.*?)\"").Groups[1].Value;

				// Self Cert
				string self_cert_url = "https://business.facebook.com/ads/integrity/user/self_cert/ajax/";
				string self_cert_referrer = rurl;
				string self_cert_body = string.Concat(new string[] {
					"source=ads_manager_dialog&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHJ16-aUmgDxKQ8zk2m2K9yUqDyQjFGEkxim2qdwQxCagjGqErxqqax2qqEboym4UJe9wNWAAzppFXxWAewhK58ixKdwJKbUg-mdx22q78gxO1cxuEC8yEqx611Dm3G49HwwwwUbFEoyoaUcFES2SUnyFE4WWBBKdxyhedU88OJaECfiwzlBwyxabK6UC4F8nwRwGUkBzXCggyEtxnx-68jz98eUuyo46mfULxq0y8O48bEqwBgKfx26pXzouyU98-1tgJ12262222u9xZ0AgK7kbAzE8U8Q11x6azUeUhAwnEvorx2364kUy58yXzUjzXxG9HAxe48gxu5ogAzEoG7odUnwj8rwEzobK7EG4E9ohjxK2a5Ue8Su6E7y4UkyFp9bwiVE4ecxm228x24oiyVV98OEdEGdwzxa1iwi8cV-1bG13xOUmwQw&__csr=&__req=1q&__hs=19061.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1005176015&__s=vvpeu4%3Ab40pzd%3Ag2rtl3&__hsi=7073397991601009765-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=21872&lsd=",
					lsd,
					"&__spin_r=1005176015&__spin_b=trunk&__spin_t=1646903807&__jssesw=1"
				});

				string self_cert_postData = this.fetchApi(self_cert_url, "POST", self_cert_body, "", lsd, self_cert_referrer);

				// check Fragments
				List<string> allFragments = this.getAddraftFragments(addraft, campAccessToken);
				if (allFragments.Count == 0)
				{
					// Upload picture file
					this.log("Upload: ...");
					string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
					string dataPath = desktopPath + "\\" + _dataDir + "\\camp2.0\\" + nlCampId + "\\";
					string imgPath = dataPath + @"img_temp.jpg";
					if (!File.Exists(imgPath))
					{
						this.log("Không thấy file ảnh!");
						return false;
					}
					byte[] imageArray = System.IO.File.ReadAllBytes(imgPath);
					string base64ImageRepresentation = Convert.ToBase64String(imageArray);
					base64ImageRepresentation = HttpUtility.UrlEncode(base64ImageRepresentation);

					string uploadUrl = "https://graph.facebook.com/v15.0/act_" + _bm_ads_id + "/adimages?_app=ADS_MANAGER&_reqName=path%3A%2Fact_" + _bm_ads_id + "%2Fadimages&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270206671%2C270212441%2C270213183%2C270213890&qpl_active_flow_instance_ids=270206671_b8fbde6f140c93a4%2C27021288800_b8f218ec33ae9279%2C270213183_b8f25fde5107dc2a8%2C270213890_b8f2a5acf8b0c9894&__cppo=1";
					string upload_body = string.Concat(new string[] {
						"__activeScenarioIDs=%5B%22f22c28f12953a2c_1664363271627.8%22%2C%22a7b75a42-80cb-423d-b68c-be3a2bf39069%22%5D&__activeScenarios=%5B%22am%3Aedit_ads%3Aupload_asset_in_media_dialog%22%2C%22am.edit_ads.upload_asset_in_media_dialog%22%5D&__business_id=",
						_bm_id,
						"&_app=ADS_MANAGER&_reqName=path%3A%2Fact_",
						_bm_ads_id,
						"%2Fadimages&_reqSrc=adsDaoGraphDataMutator&_sessionID=",
						sessionID,
						"&accountId=",
						_bm_ads_id,
						"&bytes=",
						base64ImageRepresentation,
						"&encoding=data%3Aimage%2Fjpeg%3Bbase64&endpoint=%2Fact_",
						_bm_ads_id,
						"%2Fadimages&include_headers=false&locale=en_US&method=post&name=img_temp.jpg&pretty=0&qpl_active_flow_ids=270206671%2C270212441%2C270213183%2C270213890&qpl_active_flow_instance_ids=270206671_b8fbde6f140c93a4%2C27021288800_b8f218ec33ae9279%2C270213183_b8f25fde5107dc2a8%2C270213890_b8f2a5acf8b0c9894&suppress_http_code=1&xref=f1515ab8a190b58"
					});
					string upload_postData = this.fetchApi(uploadUrl, "POST", upload_body, "", "", "https://business.facebook.com/");
					if (string.IsNullOrEmpty(upload_postData))
					{
						this.log("Upload: FALSE!");
						return false;
					}
					else
					{
						string hashcode = Regex.Match(upload_postData, "\"hash\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(hashcode))
						{
							this.log("Upload: FALSE!");
							return false;
						}

						// Todo get temp
						string jsonfile = dataPath + @"pe_camp_temp.json";
						if (!File.Exists(jsonfile))
						{
							this.log("Not camp temp file!");
							return false;
						}
						string _json = File.ReadAllText(jsonfile);
						PeCampParam peCampParam = JsonConvert.DeserializeObject<PeCampParam>(_json);

						string daily_budget = peCampParam.daily_budget;
						string budget_remaining = peCampParam.budget_remaining;
						string age_max = peCampParam.age_max;
						string age_min = peCampParam.age_min;
						string genders = peCampParam.genders;
						string conversion_domain = "";
						if (!string.IsNullOrEmpty(peCampParam.conversion_domain))
						{

							conversion_domain = "%2C%7B%22field%22%3A%22conversion_domain%22%2C%22new_value%22%3A%22" + peCampParam.conversion_domain + "%22%7D";
						}
						string message = Uri.EscapeDataString(peCampParam.message);
						string link = Uri.EscapeDataString(peCampParam.link);
						string page_id = pageID;
                        if (!string.IsNullOrEmpty(peCampParam.page_id))
                        {
							page_id = peCampParam.page_id;
						}

						string countries = "null";
						string countriesInput = peCampParam.countries;
						if (!string.IsNullOrEmpty(countriesInput))
						{
							countriesInput = countriesInput.Replace(",", "%22%2C%22");
							countries = "%5B%22" + countriesInput + "%22%5D";
						}

						string start_time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz").Remove(22, 1);
						start_time = Uri.EscapeDataString(start_time);

						string qpl_active_flow_instance_ids = "270209052_b8f1070d8788c3e9%2C270000000_b8f29601f3505941c%2C270220209_b8f1649ca93334e68";

						// Create Fragment 1
						long milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
						string fragment_url = "https://graph.facebook.com/v15.0/" + addraft + "/addraft_fragments?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=" + qpl_active_flow_instance_ids + "&__cppo=1";
						string fragment_referrer = "https://business.facebook.com/";
						string fragment__body = string.Concat(new string[] {
							"__activeScenarioIDs=%5B%22f3cbe43d301cee8_1664360769044.7%22%2C%222ecd0851-c185-499c-9250-c69fcd84246a%22%5D&__activeScenarios=%5B%22ad_object_draft_creation%22%2C%22am.draft.create_draft%22%5D&__business_id=",
							_bm_id,
							"&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
							sessionID,
							"&account_id=",
							_bm_ads_id,
							"&action=add&ad_draft_id=",
							addraft,
							"&ad_object_type=campaign&include_headers=false&is_archive=false&is_delete=false&locale=en_US&method=post&pretty=0&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=",
							qpl_active_flow_instance_ids,
							"&source=click_quick_create&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22adlabels%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22pacing_type%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%22standard%22%5D%7D%2C%7B%22field%22%3A%22start_time%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22stop_time%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22split_test_config%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22can_use_spend_cap%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Atrue%7D%2C%7B%22field%22%3A%22daily_budget%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A",
							daily_budget,
							"%7D%2C%7B%22field%22%3A%22name%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22New%20Sales%20Campaign%22%7D%2C%7B%22field%22%3A%22metrics_metadata%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22campaign_group_creation_source%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22click_quick_create%22%7D%2C%7B%22field%22%3A%22smart_promotion_type%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22account_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							_bm_ads_id,
							"%22%7D%2C%7B%22field%22%3A%22is_odax_campaign_group%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Atrue%7D%2C%7B%22field%22%3A%22tempID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A-",
							milliseconds.ToString(),
							"%7D%2C%7B%22field%22%3A%22boosted_component_product%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22incremental_conversion_optimization_config%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22spend_cap%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22topline_id%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22special_ad_categories%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%22NONE%22%5D%7D%2C%7B%22field%22%3A%22status%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22ACTIVE%22%7D%2C%7B%22field%22%3A%22special_ad_category%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22NONE%22%7D%2C%7B%22field%22%3A%22bid_strategy%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22LOWEST_COST_WITHOUT_CAP%22%7D%2C%7B%22field%22%3A%22objective%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22OUTCOME_SALES%22%7D%2C%7B%22field%22%3A%22is_autobid%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Atrue%7D%2C%7B%22field%22%3A%22promoted_object%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22lifetime_budget%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22budget_remaining%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22is_average_price_pacing%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Afalse%7D%2C%7B%22field%22%3A%22buying_type%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22AUCTION%22%7D%2C%7B%22field%22%3A%22collaborative_ads_partner_info%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22is_using_l3_schedule%22%2C%22old_value%22%3Anull%7D%5D&xref=f397af022d787"
						});

						string fragment__postData = this.fetchApi(fragment_url, "POST", fragment__body, "", "", fragment_referrer);
						if (string.IsNullOrEmpty(fragment__postData))
						{
							this.log("Tạo camp: FALSE!");
							return false;
						}

						string campId = Regex.Match(fragment__postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(campId))
						{
							this.log("Tạo camp: FALSE!");
							return false;
						}

						// Create Fragment 2
						milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
						qpl_active_flow_instance_ids = "270209052_b8f1070d8788c3e9%2C270209052_b8f29601f3505941c%2C270220108_b8f113d35422b6254%2C200000000_b8f3a2a766f69ef6c%2C270220209_b8f1649ca93334e68";
						fragment_url = "https://graph.facebook.com/v15.0/" + addraft + "/addraft_fragments?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=" + qpl_active_flow_instance_ids + "&__cppo=1";
						fragment__body = string.Concat(new string[] {
							"__activeScenarioIDs=%5B%22f3cbe43d301cee8_1664360769044.7%22%2C%22fa02ea1093832_1664360770188.1%22%2C%222ecd0851-c185-499c-9250-c69fcd84246a%22%2C%223d80b2cd-dd67-4671-946a-d01939fcdbde%22%5D&__activeScenarios=%5B%22ad_object_draft_creation%22%2C%22table_insights_footer_dd%22%2C%22am.draft.create_draft%22%2C%22am.table_data_display.footer%22%5D&__business_id=",
							_bm_id,
							"&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
							sessionID,
							"&account_id=",
							_bm_ads_id,
							"&action=add&ad_draft_id=",
							addraft,
							"&ad_object_type=ad_set&include_headers=false&is_archive=false&is_delete=false&locale=en_US&method=post&parent_ad_object_id=",
							campId,
							"&pretty=0&qpl_active_flow_ids=270209052%2C270220108%2C270220209&qpl_active_flow_instance_ids=",
							qpl_active_flow_instance_ids,
							"&source=click_quick_create&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22optimization_goal%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22OFFSITE_CONVERSIONS%22%7D%2C%7B%22field%22%3A%22placement%22%2C%22new_value%22%3A%7B%22user_device%22%3A%5B%5D%2C%22excluded_publisher_list_ids%22%3A%5B%5D%2C%22facebook_positions%22%3A%5B%22instant_article%22%2C%22feed%22%2C%22search%22%5D%2C%22excluded_brand_safety_content_types%22%3A%5B%5D%2C%22oculus_positions%22%3A%5B%5D%2C%22excluded_user_device%22%3A%5B%5D%2C%22wireless_carrier%22%3A%5B%5D%2C%22device_platforms%22%3A%5B%22mobile%22%2C%22desktop%22%5D%2C%22user_os%22%3A%5B%5D%2C%22brand_safety_content_filter_levels%22%3A%5B%22FACEBOOK_STANDARD%22%5D%2C%22publisher_platforms%22%3A%5B%22facebook%22%5D%7D%7D%2C%7B%22field%22%3A%22targeting_as_signal%22%2C%22new_value%22%3A3%7D%2C%7B%22field%22%3A%22parentAdObjectID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							campId,
							"%22%7D%2C%7B%22field%22%3A%22start_time%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							start_time,							
							"%22%7D%2C%7B%22field%22%3A%22campaign_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							campId,
							"%22%7D%2C%7B%22field%22%3A%22name%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22New%20Sales%20Ad%20Set%22%7D%2C%7B%22field%22%3A%22campaign_creation_source%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22click_quick_create%22%7D%2C%7B%22field%22%3A%22account_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							_bm_ads_id,
							"%22%7D%2C%7B%22field%22%3A%22tempID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A-",
							milliseconds.ToString(),
							"%7D%2C%7B%22field%22%3A%22targeting%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%7B%22age_max%22%3A",
							age_max,
							"%2C%22user_device%22%3A%5B%5D%2C%22excluded_publisher_list_ids%22%3A%5B%5D%2C%22geo_locations%22%3A%7B%22countries%22%3A",
							countries,
							"%2C%22location_types%22%3A%5B%22home%22%2C%22recent%22%5D%7D%2C%22facebook_positions%22%3A%5B%22instant_article%22%2C%22feed%22%2C%22search%22%5D%2C%22genders%22%3A%5B",
							genders,
							"%5D%2C%22age_min%22%3A",
							age_min,
							"%2C%22excluded_brand_safety_content_types%22%3A%5B%5D%2C%22oculus_positions%22%3A%5B%5D%2C%22excluded_user_device%22%3A%5B%5D%2C%22wireless_carrier%22%3A%5B%5D%2C%22device_platforms%22%3A%5B%22mobile%22%2C%22desktop%22%5D%2C%22user_os%22%3A%5B%5D%2C%22brand_safety_content_filter_levels%22%3A%5B%22FACEBOOK_STANDARD%22%5D%2C%22publisher_platforms%22%3A%5B%22facebook%22%5D%7D%7D%2C%7B%22field%22%3A%22promoted_object%22%2C%22new_value%22%3A%7B%22pixel_id%22%3A%22",
							pixel_id,
							"%22%2C%22custom_event_type%22%3A%22PURCHASE%22%7D%7D%2C%7B%22field%22%3A%22status%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22ACTIVE%22%7D%2C%7B%22field%22%3A%22frequency_control_specs%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22billing_event%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22IMPRESSIONS%22%7D%2C%7B%22field%22%3A%22full_funnel_exploration_mode%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22NONE_EXPLORATION%22%7D%2C%7B%22field%22%3A%22attribution_spec%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%7B%22event_type%22%3A%22CLICK_THROUGH%22%2C%22window_days%22%3A7%7D%2C%7B%22event_type%22%3A%22VIEW_THROUGH%22%2C%22window_days%22%3A1%7D%5D%7D%2C%7B%22field%22%3A%22budget_remaining%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A",
							budget_remaining,
							"%7D%5D&xref=f30c3625055cd7c"
						});

						fragment__postData = this.fetchApi(fragment_url, "POST", fragment__body, "", "", fragment_referrer);
						if (string.IsNullOrEmpty(fragment__postData))
						{
							this.log("Tạo Ads Set: FALSE!");
							return false;
						}

						string adSetId = Regex.Match(fragment__postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(adSetId))
						{
							this.log("Tạo Ads Set: FALSE!");
							return false;
						}

						// Create Fragment 3
						string adsStatus = "ACTIVE";
                        if (off_ads)
                        {
							adsStatus = "PAUSED";
						}
						milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
						qpl_active_flow_instance_ids = "270209052_b8f1070d8788c3e9%2C270209052_b8f29601f3505941c%2C200000000_b8f1649ca93334e68";
						fragment_url = "https://graph.facebook.com/v15.0/" + addraft + "/addraft_fragments?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=270209052_b8f1070d8788c3e9%2C270000000_b8f29601f3505941c%2C270220209_b8f1649ca93334e68&__cppo=1";
						fragment__body = string.Concat(new string[] {
							"__activeScenarioIDs=%5B%22f3cbe43d301cee8_1664360769044.7%22%2C%222ecd0851-c185-499c-9250-c69fcd84246a%22%5D&__activeScenarios=%5B%22ad_object_draft_creation%22%2C%22am.draft.create_draft%22%5D&__business_id=",
							_bm_id,
							"&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
							sessionID,
							"&account_id=",
							_bm_ads_id,
							"&action=add&ad_draft_id=",
							addraft,
							"&ad_object_type=ad&include_headers=false&is_archive=false&is_delete=false&locale=en_US&method=post&parent_ad_object_id=",
							adSetId,
							"&pretty=0&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=",
							qpl_active_flow_instance_ids,
							"&source=click_quick_create&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22parentAdObjectID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							adSetId,
							"%22%7D",
							conversion_domain,
							"%2C%7B%22field%22%3A%22campaign_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							campId,
							"%22%7D%2C%7B%22field%22%3A%22name%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22New%20Sales%20Ad%22%7D%2C%7B%22field%22%3A%22account_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							_bm_ads_id,
							"%22%7D%2C%7B%22field%22%3A%22creative%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%7B%22object_story_spec%22%3A%7B%22link_data%22%3A%7B%22call_to_action%22%3A%7B%22value%22%3A%7B%22link%22%3Anull%7D%2C%22type%22%3A%22SHOP_NOW%22%7D%2C%22image_hash%22%3A%22",
							hashcode,
							"%22%2C%22message%22%3A%22",
							message,
							"%22%2C%22link%22%3A%22",
							link,
							"%22%7D%2C%22page_id%22%3A%22",
							page_id,
							"%22%7D%2C%22degrees_of_freedom_spec%22%3A%7B%22creative_features_spec%22%3A%7B%22standard_enhancements%22%3A%7B%22action_metadata%22%3A%7B%22type%22%3A%22DEFAULT%22%7D%2C%22enroll_status%22%3A%22OPT_OUT%22%7D%7D%2C%22degrees_of_freedom_type%22%3A%22USER_ENROLLED_NON_DCO%22%2C%22text_transformation_types%22%3A%5B%22TEXT_LIQUIDITY%22%5D%2C%22image_transformation_types%22%3A%5B%22CROPPING%22%2C%22ENHANCEMENT%22%5D%7D%2C%22object_type%22%3A%22SHARE%22%2C%22asset_feed_spec%22%3A%7B%22badge_sets%22%3A%5B%7B%22social_cues_from_profile%22%3A%7B%22selected_social_cues_options%22%3A%5B%22ig_account_followers%22%2C%22page_check_ins%22%2C%22page_follows%22%2C%22page_likes%22%2C%22page_messenger_response_time%22%2C%22page_ratings%22%2C%22page_recent_check_ins%22%5D%7D%2C%22business_info_from_profile%22%3A%7B%22selected_business_info_options%22%3A%5B%22ig_account_joining_date%22%2C%22page_operating_hours%22%2C%22page_price_range%22%2C%22page_store_location%22%5D%7D%7D%5D%7D%7D%7D%2C%7B%22field%22%3A%22tempID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A-",
							milliseconds.ToString(),
							"%7D%2C%7B%22field%22%3A%22tracking_specs%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%7B%22action.type%22%3A%5B%22offsite_conversion%22%5D%2C%22fb_pixel%22%3A%5B%22",
							pixel_id,
							"%22%5D%7D%5D%7D%2C%7B%22field%22%3A%22status%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							adsStatus,
							"%22%7D%2C%7B%22field%22%3A%22adset_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							adSetId,
							"%22%7D%2C%7B%22field%22%3A%22display_sequence%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A0%7D%2C%7B%22field%22%3A%22ad_creation_source%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22click_quick_create%22%7D%5D&xref=f37b38bbe65fc5c"
						});

						fragment__postData = this.fetchApi(fragment_url, "POST", fragment__body, "", "", fragment_referrer);
						if (string.IsNullOrEmpty(fragment__postData))
						{
							this.log("Tạo Ad: FALSE!");
							return false;
						}

						string adId = Regex.Match(fragment__postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(adId))
						{
							this.log("Tạo Ad: FALSE!");
							return false;
						}
					}
				}

				// Check Fragments
				this.log("Check Fragments: ...");
				bool checkImport = false;

				int ChkImportIndex = 1;
				while ((!checkImport) && (ChkImportIndex <= 3))
				{
					Delay(3000);
					allFragments = this.getAddraftFragments(addraft, campAccessToken);
					if (allFragments.Count > 2)
					{
						checkImport = true;
					}

					ChkImportIndex++;
				}

				if (!checkImport)
				{
					this.log("Fragments Data: " + allFragments.ToString());
					this.log("Fragments: FALSE!");
					return false;
				}

				// Publish
				string fragmentsStr = "%5B%22" + String.Join("%22%2C%22", allFragments.ToArray()) + "%22%5D";
				string flow_instance_ids = "270208286_b0f2ef14b25cae6b%2C270208286_b0f398c3ea47bd7f8%2C279999999_" + sessionID;
				string publishurl = string.Concat(new string[] {
					"https://graph.facebook.com/v15.0/",
					addraft,
					"/publish?_app=ADS_MANAGER&_reqName=object%3Adraft_id%2Fpublish&access_token=",
					campAccessToken,
					"&method=post&qpl_active_flow_ids=270208286%2C270216423&qpl_active_flow_instance_ids=",
					flow_instance_ids,
					"&__cppo=1",
				});

				string publishreferrer = "https://business.facebook.com/";
				string publishbody = string.Concat(new string[] {
					"__activeScenarioIDs=%5B%22f3a35cd1611978c_1664513102035.7%22%2C%223d7dfed9-073c-4bcc-a8c3-f55e0b49bcbe%22%5D&__activeScenarios=%5B%22review_and_publish%22%2C%22am.publish_ads.in_review_and_publish%22%5D&__business_id=",
					_bm_id,
					"&_app=ADS_MANAGER&_reqName=object%3Adraft_id%2Fpublish&_reqSrc=AdsDraftPublishDataManager&_sessionID=",
					sessionID,
					"&fragments=",
					fragmentsStr,
					"&ignore_errors=true&include_fragment_statuses=true&include_headers=false&locale=en_US&method=post&pretty=0&qpl_active_flow_ids=270208286%2C270216423&qpl_active_flow_instance_ids=",
					flow_instance_ids,
					"&suppress_http_code=1&xref=f7cd5b9852c4b4"
				});

				string publishData = this.fetchApi(publishurl, "POST", publishbody, "", "", publishreferrer);
				string success = Regex.Match(publishData, "\"success\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(success))
				{
					success = Regex.Match(publishData, "\"success\":(.*?)}").Groups[1].Value;
				}

				if (success != "true")
				{
					this.log(publishData);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool TKBMPeVideoLaunchingCamp(string pixel_id = "")
		{
			try
			{
				switchBusinessTabHandle();

				string rurl = "https://business.facebook.com/adsmanager/manage/campaigns?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&global_scope_id=" + _bm_id + "&tool=MANAGE_ADS&nav_entry_point=bm_global_nav_shortcut&nav_source=flyout_menu&nav_id=1565291292about%3Ablank";
				string getData = this.fetchGet(rurl);
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(fb_dtsg))
				{
					rurl = "https://business.facebook.com/adsmanager/manage/accounts?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					getData = this.fetchGet(rurl);
					fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				}
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string addraft = Regex.Match(getData, "\"ad_draft_id\":\"(.*?)\"").Groups[1].Value;

				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"1\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"2\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"3\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "addraft_(.*?){\"fields\"").Groups[1].Value;
				}

				if (string.IsNullOrEmpty(addraft))
				{
					this.log("Không thấy bản thảo!");
					return false;
				}

				string sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
				string campAccessToken = Regex.Match(getData, "__accessToken=\"(.*?)\"").Groups[1].Value;

				// Self Cert
				string self_cert_url = "https://business.facebook.com/ads/integrity/user/self_cert/ajax/";
				string self_cert_referrer = rurl;
				string self_cert_body = string.Concat(new string[] {
					"source=ads_manager_dialog&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHJ16-aUmgDxKQ8zk2m2K9yUqDyQjFGEkxim2qdwQxCagjGqErxqqax2qqEboym4UJe9wNWAAzppFXxWAewhK58ixKdwJKbUg-mdx22q78gxO1cxuEC8yEqx611Dm3G49HwwwwUbFEoyoaUcFES2SUnyFE4WWBBKdxyhedU88OJaECfiwzlBwyxabK6UC4F8nwRwGUkBzXCggyEtxnx-68jz98eUuyo46mfULxq0y8O48bEqwBgKfx26pXzouyU98-1tgJ12262222u9xZ0AgK7kbAzE8U8Q11x6azUeUhAwnEvorx2364kUy58yXzUjzXxG9HAxe48gxu5ogAzEoG7odUnwj8rwEzobK7EG4E9ohjxK2a5Ue8Su6E7y4UkyFp9bwiVE4ecxm228x24oiyVV98OEdEGdwzxa1iwi8cV-1bG13xOUmwQw&__csr=&__req=1q&__hs=19061.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1005176015&__s=vvpeu4%3Ab40pzd%3Ag2rtl3&__hsi=7073397991601009765-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=21872&lsd=",
					lsd,
					"&__spin_r=1005176015&__spin_b=trunk&__spin_t=1646903807&__jssesw=1"
				});

				string self_cert_postData = this.fetchApi(self_cert_url, "POST", self_cert_body, "", lsd, self_cert_referrer);

				// check Fragments
				List<string> allFragments = this.getAddraftFragments(addraft, campAccessToken);
				if (allFragments.Count == 0)
				{
					// Upload thumb file
					this.log("Upload: ...");
					string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
					string dataPath = desktopPath + "\\" + _dataDir;
					string imgPath = dataPath + @"\post\video_thumb.jpg";
					if (!File.Exists(imgPath))
					{
						this.log("Không thấy file video_thumb.jpg!");
						return false;
					}
					byte[] imageArray = System.IO.File.ReadAllBytes(imgPath);
					string base64ImageRepresentation = Convert.ToBase64String(imageArray);
					base64ImageRepresentation = HttpUtility.UrlEncode(base64ImageRepresentation);

					string uploadUrl = "https://graph.facebook.com/v13.0/act_" + _bm_ads_id + "/adimages?_app=ADS_MANAGER&_reqName=path%3A%2Fact_" + _bm_ads_id + "%2Fadimages&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270206671%2C270212441%2C270213183%2C270213890&qpl_active_flow_instance_ids=270206671_b8fbde6f140c93a4%2C27021288800_b8f218ec33ae9279%2C270213183_b8f25fde5107dc2a8%2C270213890_b8f2a5acf8b0c9894&__cppo=1";
					string upload_body = string.Concat(new string[] {
						"__activeScenarioIDs=%5B%22f22c28f12953a2c_1664363271627.8%22%2C%22a7b75a42-80cb-423d-b68c-be3a2bf39069%22%5D&__activeScenarios=%5B%22am%3Aedit_ads%3Aupload_asset_in_media_dialog%22%2C%22am.edit_ads.upload_asset_in_media_dialog%22%5D&__business_id=",
						_bm_id,
						"&_app=ADS_MANAGER&_reqName=path%3A%2Fact_",
						_bm_ads_id,
						"%2Fadimages&_reqSrc=adsDaoGraphDataMutator&_sessionID=",
						sessionID,
						"&accountId=",
						_bm_ads_id,
						"&bytes=",
						base64ImageRepresentation,
						"&encoding=data%3Aimage%2Fjpeg%3Bbase64&endpoint=%2Fact_",
						_bm_ads_id,
						"%2Fadimages&include_headers=false&locale=en_US&method=post&name=img_temp.jpg&pretty=0&qpl_active_flow_ids=270206671%2C270212441%2C270213183%2C270213890&qpl_active_flow_instance_ids=270206671_b8fbde6f140c93a4%2C27021288800_b8f218ec33ae9279%2C270213183_b8f25fde5107dc2a8%2C270213890_b8f2a5acf8b0c9894&suppress_http_code=1&xref=f1515ab8a190b58"
					});
					string upload_postData = this.fetchApi(uploadUrl, "POST", upload_body, "", "", "https://business.facebook.com/");
					if (string.IsNullOrEmpty(upload_postData))
					{
						this.log("Upload Thumb: FALSE!");
						return false;
					}
					else
					{
						string thumbUrl = Regex.Match(upload_postData, "\"url\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(thumbUrl))
						{
							this.log("Upload Thumb: FALSE!");
							return false;
						}

						thumbUrl = thumbUrl.Replace("\\/", "/");
						thumbUrl = Uri.EscapeDataString(thumbUrl);

						// Todo get temp
						string jsonfile = dataPath + @"\camp\pe_camp_temp.json";
						if (!File.Exists(jsonfile))
						{
							this.log("Not camp temp file!");
							return false;
						}
						string _json = File.ReadAllText(jsonfile);
						PeCampParam peCampParam = JsonConvert.DeserializeObject<PeCampParam>(_json);

						string daily_budget = peCampParam.daily_budget;
						string budget_remaining = peCampParam.budget_remaining;
						string age_max = peCampParam.age_max;
						string age_min = peCampParam.age_min;
						string genders = peCampParam.genders;
						string conversion_domain = "";
						if (!string.IsNullOrEmpty(peCampParam.conversion_domain))
						{
							conversion_domain = "%2C%7B%22field%22%3A%22conversion_domain%22%2C%22new_value%22%3A%22" + peCampParam.conversion_domain + "%22%7D";
						}
						string message = Uri.EscapeDataString(peCampParam.message);
						string link = Uri.EscapeDataString(peCampParam.link);
						string page_id = pageID;
						if (!string.IsNullOrEmpty(peCampParam.page_id))
						{
							page_id = peCampParam.page_id;
						}
						string video_id = peCampParam.video_id;
						if (string.IsNullOrEmpty(video_id) && !string.IsNullOrEmpty(peCampParam.video_s_url))
						{
							// Upload video
							this.log("Upload video...!");
							uploadUrl = "https://graph.facebook.com/v13.0/act_" + _bm_ads_id + "/advideos?title=nini2bi&access_token=" + campAccessToken;
							upload_body = string.Concat(new string[] {
								"------WebKitFormBoundaryabkJ1dWwF1Okeym1\\r\\nContent-Disposition: form-data; name=\\\"waterfall_id\\\"\\r\\n\\r\\n64815d5db424ca323ea9edf678944a43\\r\\n------WebKitFormBoundaryabkJ1dWwF1Okeym1\\r\\nContent-Disposition: form-data; name=\\\"name\\\"\\r\\n\\r\\nninicheck\\r\\n------WebKitFormBoundaryabkJ1dWwF1Okeym1\\r\\nContent-Disposition: form-data; name=\\\"file_url\\\"\\r\\n\\r\\n",
								peCampParam.video_s_url,
								"\\r\\n------WebKitFormBoundaryabkJ1dWwF1Okeym1--\\r\\n"
							});
							upload_postData = this.formDataFetchApi(uploadUrl, "POST", upload_body, "", "", "https://business.facebook.com/");
                            if (!string.IsNullOrEmpty(upload_postData))
                            {
								video_id = Regex.Match(upload_postData, "\"id\": \"(.*?)\"").Groups[1].Value;
                                if (!string.IsNullOrEmpty(video_id))
                                {
									this.log("Check video...!");
									bool checkVideo = false;
									int ChkVideoIndex = 1;
									while ((!checkVideo) && (ChkVideoIndex <= 8))
									{
										Delay(3000);
										string videodata = getVideo(video_id, campAccessToken);
										string length = Regex.Match(videodata, "\"length\":(.*?),").Groups[1].Value;
                                        if (string.IsNullOrEmpty(length))
                                        {
											checkVideo = true;
										}
										ChkVideoIndex++;
									}
								}
							}
						}

						if (string.IsNullOrEmpty(page_id))
						{
							this.log("Không thấy Page ID!");
							return false;
						}

						if (string.IsNullOrEmpty(video_id))
						{
							this.log("Không thấy Video ID!");
							return false;
						}

						string link_description = Uri.EscapeDataString(peCampParam.link_description);
						string title = Uri.EscapeDataString(peCampParam.title);

						string countries = "null";
						string countriesInput = peCampParam.countries;
						if (!string.IsNullOrEmpty(countriesInput))
						{
							countriesInput = countriesInput.Replace(",", "%22%2C%22");
							countries = "%5B%22" + countriesInput + "%22%5D";
						}

						string start_time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz").Remove(22, 1);
						start_time = Uri.EscapeDataString(start_time);

						string qpl_active_flow_instance_ids = "270209052_b8f1070d8788c3e9%2C270000000_b8f29601f3505941c%2C270220209_b8f1649ca93334e68";

						// Create Fragment 1
						long milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
						string fragment_url = "https://graph.facebook.com/v13.0/" + addraft + "/addraft_fragments?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=" + qpl_active_flow_instance_ids + "&__cppo=1";
						string fragment_referrer = "https://business.facebook.com/";
						string fragment__body = string.Concat(new string[] {
							"__activeScenarioIDs=%5B%22f3cbe43d301cee8_1664360769044.7%22%2C%222ecd0851-c185-499c-9250-c69fcd84246a%22%5D&__activeScenarios=%5B%22ad_object_draft_creation%22%2C%22am.draft.create_draft%22%5D&__business_id=",
							_bm_id,
							"&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
							sessionID,
							"&account_id=",
							_bm_ads_id,
							"&action=add&ad_draft_id=",
							addraft,
							"&ad_object_type=campaign&include_headers=false&is_archive=false&is_delete=false&locale=en_US&method=post&pretty=0&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=",
							qpl_active_flow_instance_ids,
							"&source=click_quick_create&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22adlabels%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22pacing_type%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%22standard%22%5D%7D%2C%7B%22field%22%3A%22start_time%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22stop_time%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22split_test_config%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22can_use_spend_cap%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Atrue%7D%2C%7B%22field%22%3A%22daily_budget%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A",
							daily_budget,
							"%7D%2C%7B%22field%22%3A%22name%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22New%20Sales%20Campaign%22%7D%2C%7B%22field%22%3A%22metrics_metadata%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22campaign_group_creation_source%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22click_quick_create%22%7D%2C%7B%22field%22%3A%22smart_promotion_type%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22account_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							_bm_ads_id,
							"%22%7D%2C%7B%22field%22%3A%22is_odax_campaign_group%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Atrue%7D%2C%7B%22field%22%3A%22tempID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A-",
							milliseconds.ToString(),
							"%7D%2C%7B%22field%22%3A%22boosted_component_product%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22incremental_conversion_optimization_config%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22spend_cap%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22topline_id%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22special_ad_categories%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%22NONE%22%5D%7D%2C%7B%22field%22%3A%22status%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22ACTIVE%22%7D%2C%7B%22field%22%3A%22special_ad_category%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22NONE%22%7D%2C%7B%22field%22%3A%22bid_strategy%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22LOWEST_COST_WITHOUT_CAP%22%7D%2C%7B%22field%22%3A%22objective%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22OUTCOME_SALES%22%7D%2C%7B%22field%22%3A%22is_autobid%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Atrue%7D%2C%7B%22field%22%3A%22promoted_object%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22lifetime_budget%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22budget_remaining%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22is_average_price_pacing%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Afalse%7D%2C%7B%22field%22%3A%22buying_type%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22AUCTION%22%7D%2C%7B%22field%22%3A%22collaborative_ads_partner_info%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22is_using_l3_schedule%22%2C%22old_value%22%3Anull%7D%5D&xref=f397af022d787"
						});

						string fragment__postData = this.fetchApi(fragment_url, "POST", fragment__body, "", "", fragment_referrer);
						if (string.IsNullOrEmpty(fragment__postData))
						{
							this.log("Tạo camp: FALSE!");
							return false;
						}

						string campId = Regex.Match(fragment__postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(campId))
						{
							this.log("Tạo camp: FALSE!");
							return false;
						}

						// Create Fragment 2
						milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
						qpl_active_flow_instance_ids = "270209052_b8f1070d8788c3e9%2C270209052_b8f29601f3505941c%2C270220108_b8f113d35422b6254%2C200000000_b8f3a2a766f69ef6c%2C270220209_b8f1649ca93334e68";
						fragment_url = "https://graph.facebook.com/v13.0/" + addraft + "/addraft_fragments?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=" + qpl_active_flow_instance_ids + "&__cppo=1";
						fragment__body = string.Concat(new string[] {
							"__activeScenarioIDs=%5B%22f3cbe43d301cee8_1664360769044.7%22%2C%22fa02ea1093832_1664360770188.1%22%2C%222ecd0851-c185-499c-9250-c69fcd84246a%22%2C%223d80b2cd-dd67-4671-946a-d01939fcdbde%22%5D&__activeScenarios=%5B%22ad_object_draft_creation%22%2C%22table_insights_footer_dd%22%2C%22am.draft.create_draft%22%2C%22am.table_data_display.footer%22%5D&__business_id=",
							_bm_id,
							"&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
							sessionID,
							"&account_id=",
							_bm_ads_id,
							"&action=add&ad_draft_id=",
							addraft,
							"&ad_object_type=ad_set&include_headers=false&is_archive=false&is_delete=false&locale=en_US&method=post&parent_ad_object_id=",
							campId,
							"&pretty=0&qpl_active_flow_ids=270209052%2C270220108%2C270220209&qpl_active_flow_instance_ids=",
							qpl_active_flow_instance_ids,
							"&source=click_quick_create&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22optimization_goal%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22OFFSITE_CONVERSIONS%22%7D%2C%7B%22field%22%3A%22placement%22%2C%22new_value%22%3A%7B%22user_device%22%3A%5B%5D%2C%22excluded_publisher_list_ids%22%3A%5B%5D%2C%22facebook_positions%22%3A%5B%22instant_article%22%2C%22feed%22%2C%22search%22%5D%2C%22excluded_brand_safety_content_types%22%3A%5B%5D%2C%22oculus_positions%22%3A%5B%5D%2C%22excluded_user_device%22%3A%5B%5D%2C%22wireless_carrier%22%3A%5B%5D%2C%22device_platforms%22%3A%5B%22mobile%22%2C%22desktop%22%5D%2C%22user_os%22%3A%5B%5D%2C%22brand_safety_content_filter_levels%22%3A%5B%22FACEBOOK_STANDARD%22%5D%2C%22publisher_platforms%22%3A%5B%22facebook%22%5D%7D%7D%2C%7B%22field%22%3A%22targeting_as_signal%22%2C%22new_value%22%3A3%7D%2C%7B%22field%22%3A%22parentAdObjectID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							campId,
							"%22%7D%2C%7B%22field%22%3A%22start_time%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							start_time,
							"%22%7D%2C%7B%22field%22%3A%22campaign_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							campId,
							"%22%7D%2C%7B%22field%22%3A%22name%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22New%20Sales%20Ad%20Set%22%7D%2C%7B%22field%22%3A%22campaign_creation_source%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22click_quick_create%22%7D%2C%7B%22field%22%3A%22account_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							_bm_ads_id,
							"%22%7D%2C%7B%22field%22%3A%22tempID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A-",
							milliseconds.ToString(),
							"%7D%2C%7B%22field%22%3A%22targeting%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%7B%22age_max%22%3A",
							age_max,
							"%2C%22user_device%22%3A%5B%5D%2C%22excluded_publisher_list_ids%22%3A%5B%5D%2C%22geo_locations%22%3A%7B%22countries%22%3A",
							countries,
							"%2C%22location_types%22%3A%5B%22home%22%2C%22recent%22%5D%7D%2C%22facebook_positions%22%3A%5B%22instant_article%22%2C%22feed%22%2C%22search%22%5D%2C%22genders%22%3A%5B",
							genders,
							"%5D%2C%22age_min%22%3A",
							age_min,
							"%2C%22excluded_brand_safety_content_types%22%3A%5B%5D%2C%22oculus_positions%22%3A%5B%5D%2C%22excluded_user_device%22%3A%5B%5D%2C%22wireless_carrier%22%3A%5B%5D%2C%22device_platforms%22%3A%5B%22mobile%22%2C%22desktop%22%5D%2C%22user_os%22%3A%5B%5D%2C%22brand_safety_content_filter_levels%22%3A%5B%22FACEBOOK_STANDARD%22%5D%2C%22publisher_platforms%22%3A%5B%22facebook%22%5D%7D%7D%2C%7B%22field%22%3A%22promoted_object%22%2C%22new_value%22%3A%7B%22pixel_id%22%3A%22",
							pixel_id,
							"%22%2C%22custom_event_type%22%3A%22PURCHASE%22%7D%7D%2C%7B%22field%22%3A%22status%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22ACTIVE%22%7D%2C%7B%22field%22%3A%22frequency_control_specs%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22billing_event%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22IMPRESSIONS%22%7D%2C%7B%22field%22%3A%22full_funnel_exploration_mode%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22NONE_EXPLORATION%22%7D%2C%7B%22field%22%3A%22attribution_spec%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%7B%22event_type%22%3A%22CLICK_THROUGH%22%2C%22window_days%22%3A7%7D%2C%7B%22event_type%22%3A%22VIEW_THROUGH%22%2C%22window_days%22%3A1%7D%5D%7D%2C%7B%22field%22%3A%22budget_remaining%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A",
							budget_remaining,
							"%7D%5D&xref=f30c3625055cd7c"
						});

						fragment__postData = this.fetchApi(fragment_url, "POST", fragment__body, "", "", fragment_referrer);
						if (string.IsNullOrEmpty(fragment__postData))
						{
							this.log("Tạo Ads Set: FALSE!");
							return false;
						}

						string adSetId = Regex.Match(fragment__postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(adSetId))
						{
							this.log("Tạo Ads Set: FALSE!");
							return false;
						}

						// Create Fragment 3
						milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
						qpl_active_flow_instance_ids = "270209052_b8f1070d8788c3e9%2C270209052_b8f29601f3505941c%2C200000000_b8f1649ca93334e68";
						fragment_url = "https://graph.facebook.com/v13.0/" + addraft + "/addraft_fragments?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=270209052_b8f1070d8788c3e9%2C270000000_b8f29601f3505941c%2C270220209_b8f1649ca93334e68&__cppo=1";
						fragment__body = string.Concat(new string[] {
							"__activeScenarioIDs=%5B%22f3cbe43d301cee8_1664360769044.7%22%2C%222ecd0851-c185-499c-9250-c69fcd84246a%22%5D&__activeScenarios=%5B%22ad_object_draft_creation%22%2C%22am.draft.create_draft%22%5D&__business_id=",
							_bm_id,
							"&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
							sessionID,
							"&account_id=",
							_bm_ads_id,
							"&action=add&ad_draft_id=",
							addraft,
							"&ad_object_type=ad&include_headers=false&is_archive=false&is_delete=false&locale=en_US&method=post&parent_ad_object_id=",
							adSetId,
							"&pretty=0&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=",
							qpl_active_flow_instance_ids,
							"&source=click_quick_create&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22parentAdObjectID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							adSetId,
							"%22%7D",
							conversion_domain,
							"%2C%7B%22field%22%3A%22campaign_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							campId,
							"%22%7D%2C%7B%22field%22%3A%22name%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22New%20Sales%20Ad%22%7D%2C%7B%22field%22%3A%22account_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							_bm_ads_id,
							"%22%7D",
							"%2C%7B%22field%22%3A%22creative%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%7B%22object_story_spec%22%3A%7B%22video_data%22%3A%7B%22message%22%3A%22%",
							message,
							"%22%2C%22image_url%22%3A%22",
							thumbUrl,
							"%22%2C%22video_thumbnail_source%22%3A%22custom%22%2C%22link_description%22%3A%22",
							link_description,
							"%22%2C%22title%22%3A%22",
							title,
							"%22%2C%22call_to_action%22%3A%7B%22value%22%3A%7B%22link%22%3A%22",
							link,
							"%22%7D%2C%22type%22%3A%22SHOP_NOW%22%7D%2C%22caption_ids%22%3Anull%2C%22video_id%22%3A%22",
							video_id,
							"%22%7D%2C%22page_id%22%3A%22",
							page_id,
							"%22%7D%2C%22degrees_of_freedom_spec%22%3A%7B%22text_transformation_types%22%3A%5B%22TEXT_LIQUIDITY%22%5D%2C%22degrees_of_freedom_type%22%3A%22USER_ENROLLED_NON_DCO%22%7D%2C%22thumbnail_url%22%3A%22",
							thumbUrl,
							"%22%2C%22object_type%22%3A%22VIDEO%22%2C%22asset_feed_spec%22%3A%7B%22badge_sets%22%3A%5B%7B%22social_cues_from_profile%22%3A%7B%22selected_social_cues_options%22%3A%5B%22ig_account_followers%22%2C%22page_check_ins%22%2C%22page_follows%22%2C%22page_likes%22%2C%22page_messenger_response_time%22%2C%22page_ratings%22%2C%22page_recent_check_ins%22%5D%7D%2C%22business_info_from_profile%22%3A%7B%22selected_business_info_options%22%3A%5B%22ig_account_joining_date%22%2C%22page_operating_hours%22%2C%22page_price_range%22%2C%22page_store_location%22%5D%7D%7D%5D%7D%7D%7D",
							"%2C%7B%22field%22%3A%22tempID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A-",
							milliseconds.ToString(),
							"%7D%2C%7B%22field%22%3A%22tracking_specs%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%7B%22action.type%22%3A%5B%22offsite_conversion%22%5D%2C%22fb_pixel%22%3A%5B%22",
							pixel_id,
							"%22%5D%7D%5D%7D%2C%7B%22field%22%3A%22status%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22ACTIVE%22%7D%2C%7B%22field%22%3A%22adset_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							adSetId,
							"%22%7D%2C%7B%22field%22%3A%22display_sequence%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A0%7D%2C%7B%22field%22%3A%22ad_creation_source%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22click_quick_create%22%7D%5D&xref=f37b38bbe65fc5c"
						});

						fragment__postData = this.fetchApi(fragment_url, "POST", fragment__body, "", "", fragment_referrer);
						if (string.IsNullOrEmpty(fragment__postData))
						{
							this.log("Tạo Ad: FALSE!");
							return false;
						}

						string adId = Regex.Match(fragment__postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(adId))
						{
							this.log("Tạo Ad: FALSE!");
							return false;
						}
					}
				}

				// Check Fragments
				this.log("Check Fragments: ...");
				bool checkImport = false;

				int ChkImportIndex = 1;
				while ((!checkImport) && (ChkImportIndex <= 3))
				{
					Delay(3000);
					allFragments = this.getAddraftFragments(addraft, campAccessToken);
					if (allFragments.Count > 2)
					{
						checkImport = true;
					}

					ChkImportIndex++;
				}

				if (!checkImport)
				{
					this.log("Fragments Data: " + allFragments.ToString());
					this.log("Fragments: FALSE!");
					return false;
				}

				// Publish
				string fragmentsStr = "%5B%22" + String.Join("%22%2C%22", allFragments.ToArray()) + "%22%5D";
				string flow_instance_ids = "270208286_b0f2ef14b25cae6b%2C270208286_b0f398c3ea47bd7f8%2C279999999_" + sessionID;
				string publishurl = string.Concat(new string[] {
					"https://graph.facebook.com/v13.0/",
					addraft,
					"/publish?_app=ADS_MANAGER&_reqName=object%3Adraft_id%2Fpublish&access_token=",
					campAccessToken,
					"&method=post&qpl_active_flow_ids=270208286%2C270216423&qpl_active_flow_instance_ids=",
					flow_instance_ids,
					"&__cppo=1",
				});

				string publishreferrer = "https://business.facebook.com/";
				string publishbody = string.Concat(new string[] {
					"__activeScenarioIDs=%5B%22f3a35cd1611978c_1664513102035.7%22%2C%223d7dfed9-073c-4bcc-a8c3-f55e0b49bcbe%22%5D&__activeScenarios=%5B%22review_and_publish%22%2C%22am.publish_ads.in_review_and_publish%22%5D&__business_id=",
					_bm_id,
					"&_app=ADS_MANAGER&_reqName=object%3Adraft_id%2Fpublish&_reqSrc=AdsDraftPublishDataManager&_sessionID=",
					sessionID,
					"&fragments=",
					fragmentsStr,
					"&ignore_errors=true&include_fragment_statuses=true&include_headers=false&locale=en_US&method=post&pretty=0&qpl_active_flow_ids=270208286%2C270216423&qpl_active_flow_instance_ids=",
					flow_instance_ids,
					"&suppress_http_code=1&xref=f7cd5b9852c4b4"
				});

				string publishData = this.fetchApi(publishurl, "POST", publishbody, "", "", publishreferrer);
				string success = Regex.Match(publishData, "\"success\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(success))
				{
					success = Regex.Match(publishData, "\"success\":(.*?)}").Groups[1].Value;
				}

				if (success != "true")
				{
					this.log(publishData);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool TKCNPeLaunchingCamp(string nlCampId, string pixel_id = "", bool off_ads = false)
		{
			try
			{
				switchRegTabHandle();

				if (!this._driver.Url.Contains("www.facebook.com"))
				{
					this._driver.Navigate().GoToUrl("https://www.facebook.com/ads/manager/account_settings/account_pages");
					WaitLoading();
					Delay(500);
				}

				string rurl = "https://www.facebook.com/adsmanager/manage/campaigns?act=" + _ads_id + "&tool=MANAGE_ADS&nav_entry_point=bm_global_nav_shortcut&nav_source=flyout_menu&nav_id=3206005184";
				string getData = this.fetchGet(rurl);
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(fb_dtsg))
				{
					rurl = "https://www.facebook.com/adsmanager/manage/accounts?act=" + _ads_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					getData = this.fetchGet(rurl);
					fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				}
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string addraft = Regex.Match(getData, "\"ad_draft_id\":\"(.*?)\"").Groups[1].Value;

				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"1\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"2\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"3\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "addraft_(.*?){\"fields\"").Groups[1].Value;
				}

				if (string.IsNullOrEmpty(addraft))
				{
					this.log("Không thấy bản thảo!");
					return false;
				}

				string sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
				string campAccessToken = Regex.Match(getData, "__accessToken=\"(.*?)\"").Groups[1].Value;

				// Self Cert
				string self_cert_url = "https://www.facebook.com/ads/integrity/user/self_cert/ajax/";
				string self_cert_referrer = rurl;
				string self_cert_body = string.Concat(new string[] {
					"source=ads_manager_dialog&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHJ16-aUmgDxKQ8zk2m2K9yUqDyQjFGEkxim2qdwQxCagjGqErxqqax2qqEboym4UJe9wNWAAzppFXxWAewhK58ixKdwJKbUg-mdx22q78gxO1cxuEC8yEqx611Dm3G49HwwwwUbFEoyoaUcFES2SUnyFE4WWBBKdxyhedU88OJaECfiwzlBwyxabK6UC4F8nwRwGUkBzXCggyEtxnx-68jz98eUuyo46mfULxq0y8O48bEqwBgKfx26pXzouyU98-1tgJ12262222u9xZ0AgK7kbAzE8U8Q11x6azUeUhAwnEvorx2364kUy58yXzUjzXxG9HAxe48gxu5ogAzEoG7odUnwj8rwEzobK7EG4E9ohjxK2a5Ue8Su6E7y4UkyFp9bwiVE4ecxm228x24oiyVV98OEdEGdwzxa1iwi8cV-1bG13xOUmwQw&__csr=&__req=1q&__hs=19061.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1005176015&__s=vvpeu4%3Ab40pzd%3Ag2rtl3&__hsi=7073397991601009765-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=21872&lsd=",
					lsd,
					"&__spin_r=1005176015&__spin_b=trunk&__spin_t=1646903807&__jssesw=1"
				});
				string self_cert_postData = this.fetchApi(self_cert_url, "POST", self_cert_body, "", lsd, self_cert_referrer);

				// check Fragments
				List<string> allFragments = this.getAddraftFragments(addraft, campAccessToken);
				if (allFragments.Count == 0)
				{
					// Upload picture file
					this.log("Upload: ...");
					string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
					string dataPath = desktopPath + "\\" + _dataDir + "\\camp2.0\\" + nlCampId + "\\";
					string imgPath = dataPath + @"img_temp.jpg";
					if (!File.Exists(imgPath))
					{
						this.log("Không thấy file ảnh!");
						return false;
					}
					byte[] imageArray = System.IO.File.ReadAllBytes(imgPath);
					string base64ImageRepresentation = Convert.ToBase64String(imageArray);
					base64ImageRepresentation = HttpUtility.UrlEncode(base64ImageRepresentation);

					string qpl_active_flow_instance_ids = "270206671_b8fbde6f140c93a4%2C27021288800_b8f218ec33ae9279%2C270213183_b8f25fde5107dc2a8%2C200000000_b8f2a5acf8b0c9894";
					string uploadUrl = "https://graph.facebook.com/v15.0/act_" + _ads_id + "/adimages?_app=ADS_MANAGER&_reqName=path%3A%2Fact_" + _ads_id + "%2Fadimages&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270206671%2C270211726%2C270212441%2C270213183%2C270213890%2C270216430&qpl_active_flow_instance_ids=" + qpl_active_flow_instance_ids + "&__cppo=1";
					string upload_body = string.Concat(new string[] {
						"__activeScenarioIDs=%5B%22f17229252d211d8_1664609394536.6%22%2C%22bdd1ac8e-c416-485f-a537-93cbe10cb3e1%22%5D&__activeScenarios=%5B%22am%3Aedit_ads%3Aupload_asset_in_media_dialog%22%2C%22am.edit_ads.upload_asset_in_media_dialog%22%5D&_app=ADS_MANAGER&_reqName=path%3A%2Fact_",
						_ads_id,
						"%2Fadimages&_reqSrc=adsDaoGraphDataMutator&_sessionID=",
						sessionID,
						"&accountId=",
						_ads_id,
						"&bytes=",
						base64ImageRepresentation,
						"&encoding=data%3Aimage%2Fjpeg%3Bbase64&endpoint=%2Fact_",
						_ads_id,
						"%2Fadimages&include_headers=false&locale=en_US&method=post&name=img_temp.jpg&pretty=0&qpl_active_flow_ids=270206671%2C270211726%2C270212441%2C270213183%2C270213890%2C270216430&qpl_active_flow_instance_ids=",
						qpl_active_flow_instance_ids,
						"&suppress_http_code=1&xref=f267b9e7e07dbfc"
					});
					string upload_postData = this.fetchApi(uploadUrl, "POST", upload_body, "", "", "https://www.facebook.com/");
					if (string.IsNullOrEmpty(upload_postData))
					{
						this.log("Upload: FALSE!");
						return false;
					}
					else
					{
						string hashcode = Regex.Match(upload_postData, "\"hash\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(hashcode))
						{
							this.log("Upload: FALSE!");
							return false;
						}

						// Todo get temp
						string jsonfile = dataPath + @"pe_camp_temp.json";
						if (!File.Exists(jsonfile))
						{
							this.log("Not camp temp file!");
							return false;
						}
						string _json = File.ReadAllText(jsonfile);
						PeCampParam peCampParam = JsonConvert.DeserializeObject<PeCampParam>(_json);

						string daily_budget = peCampParam.daily_budget;
						string budget_remaining = peCampParam.budget_remaining;
						string age_max = peCampParam.age_max;
						string age_min = peCampParam.age_min;
						string genders = peCampParam.genders;
						string conversion_domain = "";
						if (!string.IsNullOrEmpty(peCampParam.conversion_domain))
						{
							conversion_domain = "%2C%7B%22field%22%3A%22conversion_domain%22%2C%22new_value%22%3A%22" + peCampParam.conversion_domain + "%22%7D";
						}
						string message = Uri.EscapeDataString(peCampParam.message);
						string link = Uri.EscapeDataString(peCampParam.link);
						string page_id = pageID;
						if (!string.IsNullOrEmpty(peCampParam.page_id))
						{
							page_id = peCampParam.page_id;
						}

						string countries = "null";
						string countriesInput = peCampParam.countries;
						if (!string.IsNullOrEmpty(countriesInput))
						{
							countriesInput = countriesInput.Replace(",", "%22%2C%22");
							countries = "%5B%22" + countriesInput + "%22%5D";
						}

						string start_time = DateTime.Now.ToString("yyyy-MM-ddTHH:mm:sszzz").Remove(22, 1);
						start_time = Uri.EscapeDataString(start_time);
						qpl_active_flow_instance_ids = "270209052_b8f1070d8788c3e9%2C270000000_b8f29601f3505941c%2C270220209_b8f1649ca93334e68";

						// Create Fragment 1
						long milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
						string fragment_url = "https://graph.facebook.com/v15.0/" + addraft + "/addraft_fragments?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=" + qpl_active_flow_instance_ids + "&__cppo=1";
						string fragment_referrer = "https://www.facebook.com/";
						string fragment__body = string.Concat(new string[] {
							"__activeScenarioIDs=%5B%22f1688ad4b2150e8_1664609140232.2%22%2C%22a98cb162-5406-484b-b50a-f720ccdde91e%22%5D&__activeScenarios=%5B%22ad_object_draft_creation%22%2C%22am.draft.create_draft%22%5D&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
							sessionID,
							"&account_id=",
							_ads_id,
							"&action=add&ad_draft_id=",
							addraft,
							"&ad_object_type=campaign&include_headers=false&is_archive=false&is_delete=false&locale=en_US&method=post&pretty=0&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=",
							qpl_active_flow_instance_ids,
							"&source=click_quick_create&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22adlabels%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22pacing_type%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%22standard%22%5D%7D%2C%7B%22field%22%3A%22start_time%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22stop_time%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22split_test_config%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22can_use_spend_cap%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Atrue%7D%2C%7B%22field%22%3A%22daily_budget%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A",
							daily_budget,
							"%7D%2C%7B%22field%22%3A%22name%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22New%20Sales%20Campaign%22%7D%2C%7B%22field%22%3A%22metrics_metadata%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22campaign_group_creation_source%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22click_quick_create%22%7D%2C%7B%22field%22%3A%22smart_promotion_type%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22account_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							_ads_id,
							"%22%7D%2C%7B%22field%22%3A%22is_odax_campaign_group%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Atrue%7D%2C%7B%22field%22%3A%22tempID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A-",
							milliseconds.ToString(),
							"%7D%2C%7B%22field%22%3A%22boosted_component_product%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22incremental_conversion_optimization_config%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22spend_cap%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22topline_id%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22special_ad_categories%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%22NONE%22%5D%7D%2C%7B%22field%22%3A%22status%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22ACTIVE%22%7D%2C%7B%22field%22%3A%22special_ad_category%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22NONE%22%7D%2C%7B%22field%22%3A%22bid_strategy%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22LOWEST_COST_WITHOUT_CAP%22%7D%2C%7B%22field%22%3A%22objective%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22OUTCOME_SALES%22%7D%2C%7B%22field%22%3A%22is_autobid%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Atrue%7D%2C%7B%22field%22%3A%22promoted_object%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22lifetime_budget%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22budget_remaining%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22is_average_price_pacing%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Afalse%7D%2C%7B%22field%22%3A%22buying_type%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22AUCTION%22%7D%2C%7B%22field%22%3A%22collaborative_ads_partner_info%22%2C%22old_value%22%3Anull%7D%2C%7B%22field%22%3A%22is_using_l3_schedule%22%2C%22old_value%22%3Anull%7D%5D&xref=f397af022d787"
						});

						string fragment__postData = this.fetchApi(fragment_url, "POST", fragment__body, "", "", fragment_referrer);
						if (string.IsNullOrEmpty(fragment__postData))
						{
							this.log("Tạo camp: FALSE!");
							return false;
						}

						string campId = Regex.Match(fragment__postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(campId))
						{
							this.log("Tạo camp: FALSE!");
							return false;
						}

						// Create Fragment 2
						milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
						qpl_active_flow_instance_ids = "270209052_b8f1070d8788c3e9%2C270209052_b8f29601f3505941c%2C270220108_b8f113d35422b6254%2C200000000_b8f3a2a766f69ef6c%2C270220209_b8f1649ca93334e68";
						fragment_url = "https://graph.facebook.com/v15.0/" + addraft + "/addraft_fragments?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=" + qpl_active_flow_instance_ids + "&__cppo=1";
						fragment__body = string.Concat(new string[] {
							"__activeScenarioIDs=%5B%22f1688ad4b2150e8_1664609140232.2%22%2C%22f3b5e33a8ee8eec_1664609141017.6%22%2C%22a98cb162-5406-484b-b50a-f720ccdde91e%22%2C%2245eb5ae9-d639-4d42-ac7d-2106fc1c954c%22%5D&__activeScenarios=%5B%22ad_object_draft_creation%22%2C%22table_insights_footer_dd%22%2C%22am.draft.create_draft%22%2C%22am.table_data_display.footer%22%5D&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
							sessionID,
							"&account_id=",
							_ads_id,
							"&action=add&ad_draft_id=",
							addraft,
							"&ad_object_type=ad_set&include_headers=false&is_archive=false&is_delete=false&locale=en_US&method=post&parent_ad_object_id=",
							campId,
							"&pretty=0&qpl_active_flow_ids=270209052%2C270220108%2C270220209&qpl_active_flow_instance_ids=",
							qpl_active_flow_instance_ids,
							"&source=click_quick_create&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22optimization_goal%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22OFFSITE_CONVERSIONS%22%7D%2C%7B%22field%22%3A%22placement%22%2C%22new_value%22%3A%7B%22user_device%22%3A%5B%5D%2C%22excluded_publisher_list_ids%22%3A%5B%5D%2C%22facebook_positions%22%3A%5B%22instant_article%22%2C%22feed%22%2C%22search%22%5D%2C%22excluded_brand_safety_content_types%22%3A%5B%5D%2C%22oculus_positions%22%3A%5B%5D%2C%22excluded_user_device%22%3A%5B%5D%2C%22wireless_carrier%22%3A%5B%5D%2C%22device_platforms%22%3A%5B%22mobile%22%2C%22desktop%22%5D%2C%22user_os%22%3A%5B%5D%2C%22brand_safety_content_filter_levels%22%3A%5B%22FACEBOOK_STANDARD%22%5D%2C%22publisher_platforms%22%3A%5B%22facebook%22%5D%7D%7D%2C%7B%22field%22%3A%22targeting_as_signal%22%2C%22new_value%22%3A3%7D%2C%7B%22field%22%3A%22parentAdObjectID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							campId,
							"%22%7D%2C%7B%22field%22%3A%22start_time%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							start_time,
							"%22%7D%2C%7B%22field%22%3A%22campaign_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							campId,
							"%22%7D%2C%7B%22field%22%3A%22name%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22New%20Sales%20Ad%20Set%22%7D%2C%7B%22field%22%3A%22campaign_creation_source%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22click_quick_create%22%7D%2C%7B%22field%22%3A%22account_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							_ads_id,
							"%22%7D%2C%7B%22field%22%3A%22tempID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A-",
							milliseconds.ToString(),
							"%7D%2C%7B%22field%22%3A%22targeting%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%7B%22age_max%22%3A",
							age_max,
							"%2C%22user_device%22%3A%5B%5D%2C%22excluded_publisher_list_ids%22%3A%5B%5D%2C%22geo_locations%22%3A%7B%22countries%22%3A",
							countries,
							"%2C%22location_types%22%3A%5B%22home%22%2C%22recent%22%5D%7D%2C%22facebook_positions%22%3A%5B%22instant_article%22%2C%22feed%22%2C%22search%22%5D%2C%22genders%22%3A%5B",
							genders,
							"%5D%2C%22age_min%22%3A",
							age_min,
							"%2C%22excluded_brand_safety_content_types%22%3A%5B%5D%2C%22oculus_positions%22%3A%5B%5D%2C%22excluded_user_device%22%3A%5B%5D%2C%22wireless_carrier%22%3A%5B%5D%2C%22device_platforms%22%3A%5B%22mobile%22%2C%22desktop%22%5D%2C%22user_os%22%3A%5B%5D%2C%22brand_safety_content_filter_levels%22%3A%5B%22FACEBOOK_STANDARD%22%5D%2C%22publisher_platforms%22%3A%5B%22facebook%22%5D%7D%7D%2C%7B%22field%22%3A%22promoted_object%22%2C%22new_value%22%3A%7B%22pixel_id%22%3A%22",
							pixel_id,
							"%22%2C%22custom_event_type%22%3A%22PURCHASE%22%7D%7D%2C%7B%22field%22%3A%22status%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22ACTIVE%22%7D%2C%7B%22field%22%3A%22frequency_control_specs%22%2C%22old_value%22%3Anull%2C%22new_value%22%3Anull%7D%2C%7B%22field%22%3A%22billing_event%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22IMPRESSIONS%22%7D%2C%7B%22field%22%3A%22full_funnel_exploration_mode%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22NONE_EXPLORATION%22%7D%2C%7B%22field%22%3A%22attribution_spec%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%7B%22event_type%22%3A%22CLICK_THROUGH%22%2C%22window_days%22%3A7%7D%2C%7B%22event_type%22%3A%22VIEW_THROUGH%22%2C%22window_days%22%3A1%7D%5D%7D%2C%7B%22field%22%3A%22budget_remaining%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A",
							budget_remaining,
							"%7D%5D&xref=f30c3625055cd7c"
						});

						fragment__postData = this.fetchApi(fragment_url, "POST", fragment__body, "", "", fragment_referrer);
						if (string.IsNullOrEmpty(fragment__postData))
						{
							this.log("Tạo Ads Set: FALSE!");
							return false;
						}

						string adSetId = Regex.Match(fragment__postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(adSetId))
						{
							this.log("Tạo Ads Set: FALSE!");
							return false;
						}

						// Create Fragment 3
						string adsStatus = "ACTIVE";
						if (off_ads)
						{
							adsStatus = "PAUSED";
						}
						milliseconds = DateTimeOffset.Now.ToUnixTimeMilliseconds();
						qpl_active_flow_instance_ids = "270209052_b8f1070d8788c3e9%2C270209052_b8f29601f3505941c%2C200000000_b8f1649ca93334e68";
						fragment_url = "https://graph.facebook.com/v15.0/" + addraft + "/addraft_fragments?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=270209052_b8f1070d8788c3e9%2C270000000_b8f29601f3505941c%2C270220209_b8f1649ca93334e68&__cppo=1";
						fragment__body = string.Concat(new string[] {
							"__activeScenarioIDs=%5B%22f1688ad4b2150e8_1664609140232.2%22%2C%22a98cb162-5406-484b-b50a-f720ccdde91e%22%5D&__activeScenarios=%5B%22ad_object_draft_creation%22%2C%22am.draft.create_draft%22%5D&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
							sessionID,
							"&account_id=",
							_ads_id,
							"&action=add&ad_draft_id=",
							addraft,
							"&ad_object_type=ad&include_headers=false&is_archive=false&is_delete=false&locale=en_US&method=post&parent_ad_object_id=",
							adSetId,
							"&pretty=0&qpl_active_flow_ids=270209052%2C270220209&qpl_active_flow_instance_ids=",
							qpl_active_flow_instance_ids,
							"&source=click_quick_create&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22parentAdObjectID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							adSetId,
							"%22%7D",
							conversion_domain,
							"%2C%7B%22field%22%3A%22campaign_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							campId,
							"%22%7D%2C%7B%22field%22%3A%22name%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22New%20Sales%20Ad%22%7D%2C%7B%22field%22%3A%22account_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							_ads_id,
							"%22%7D%2C%7B%22field%22%3A%22creative%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%7B%22object_story_spec%22%3A%7B%22link_data%22%3A%7B%22call_to_action%22%3A%7B%22value%22%3A%7B%22link%22%3Anull%7D%2C%22type%22%3A%22SHOP_NOW%22%7D%2C%22image_hash%22%3A%22",
							hashcode,
							"%22%2C%22message%22%3A%22",
							message,
							"%22%2C%22link%22%3A%22",
							link,
							"%22%7D%2C%22page_id%22%3A%22",
							page_id,
							"%22%7D%2C%22degrees_of_freedom_spec%22%3A%7B%22creative_features_spec%22%3A%7B%22standard_enhancements%22%3A%7B%22action_metadata%22%3A%7B%22type%22%3A%22DEFAULT%22%7D%2C%22enroll_status%22%3A%22OPT_OUT%22%7D%7D%2C%22degrees_of_freedom_type%22%3A%22USER_ENROLLED_NON_DCO%22%2C%22text_transformation_types%22%3A%5B%22TEXT_LIQUIDITY%22%5D%2C%22image_transformation_types%22%3A%5B%22CROPPING%22%2C%22ENHANCEMENT%22%5D%7D%2C%22object_type%22%3A%22SHARE%22%2C%22asset_feed_spec%22%3A%7B%22badge_sets%22%3A%5B%7B%22social_cues_from_profile%22%3A%7B%22selected_social_cues_options%22%3A%5B%22ig_account_followers%22%2C%22page_check_ins%22%2C%22page_follows%22%2C%22page_likes%22%2C%22page_messenger_response_time%22%2C%22page_ratings%22%2C%22page_recent_check_ins%22%5D%7D%2C%22business_info_from_profile%22%3A%7B%22selected_business_info_options%22%3A%5B%22ig_account_joining_date%22%2C%22page_operating_hours%22%2C%22page_price_range%22%2C%22page_store_location%22%5D%7D%7D%5D%7D%7D%7D%2C%7B%22field%22%3A%22tempID%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A-",
							milliseconds.ToString(),
							"%7D%2C%7B%22field%22%3A%22tracking_specs%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%5B%7B%22action.type%22%3A%5B%22offsite_conversion%22%5D%2C%22fb_pixel%22%3A%5B%22",
							pixel_id,
							"%22%5D%7D%5D%7D%2C%7B%22field%22%3A%22status%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							adsStatus,
							"%22%7D%2C%7B%22field%22%3A%22adset_id%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22",
							adSetId,
							"%22%7D%2C%7B%22field%22%3A%22display_sequence%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A0%7D%2C%7B%22field%22%3A%22ad_creation_source%22%2C%22old_value%22%3Anull%2C%22new_value%22%3A%22click_quick_create%22%7D%5D&xref=f37b38bbe65fc5c"
						});

						fragment__postData = this.fetchApi(fragment_url, "POST", fragment__body, "", "", fragment_referrer);
						if (string.IsNullOrEmpty(fragment__postData))
						{
							this.log("Tạo Ad: FALSE!");
							return false;
						}

						string adId = Regex.Match(fragment__postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(adId))
						{
							this.log("Tạo Ad: FALSE!");
							return false;
						}
					}
				}

				// Check Fragments
				this.log("Check Fragments: ...");
				bool checkImport = false;

				int ChkImportIndex = 1;
				while ((!checkImport) && (ChkImportIndex <= 3))
				{
					Delay(3000);
					allFragments = this.getAddraftFragments(addraft, campAccessToken);
					if (allFragments.Count > 2)
					{
						checkImport = true;
					}

					ChkImportIndex++;
				}

				if (!checkImport)
				{
					this.log("Fragments Data: " + allFragments.ToString());
					this.log("Fragments: FALSE!");
					return false;
				}

				// Publish
				string fragmentsStr = "%5B%22" + String.Join("%22%2C%22", allFragments.ToArray()) + "%22%5D";
				string flow_instance_ids = "270208286_b0f2ef14b25cae6b%2C270208286_b0f398c3ea47bd7f8%2C279999999_" + sessionID;

				string publishurl = string.Concat(new string[] {
					"https://graph.facebook.com/v15.0/",
					addraft,
					"/publish?_app=ADS_MANAGER&_reqName=object%3Adraft_id%2Fpublish&access_token=",
					campAccessToken,
					"&method=post&qpl_active_flow_ids=270208286%2C270216423&qpl_active_flow_instance_ids=",
					flow_instance_ids,
					"&__cppo=1"
				});

				string publishreferrer = "https://www.facebook.com/";
				string publishbody = string.Concat(new string[] {
					"__activeScenarioIDs=%5B%22%22%5D&__activeScenarios=%5B%22review_and_publish%22%5D&_app=ADS_MANAGER&_reqName=object%3Adraft_id%2Fpublish&_reqSrc=AdsDraftPublishDataManager&_sessionID=",
					sessionID,
					"&append=false&fragments=",
					fragmentsStr,
					"&ignore_errors=true&include_fragment_statuses=true&include_headers=false&locale=en_US&method=post&pretty=0&qpl_active_flow_ids=270208286%2C270216423&qpl_active_flow_instance_ids=",
					flow_instance_ids,
					"&suppress_http_code=1&xref=f1557172faee02"
				});

				string publishData = this.fetchApi(publishurl, "POST", publishbody, "", "", publishreferrer);
				string success = Regex.Match(publishData, "\"success\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(success))
				{
					success = Regex.Match(publishData, "\"success\":(.*?)}").Groups[1].Value;
				}

				if (success != "true")
				{
					this.log(publishData);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool TKCNSummaryLaunchingCamp(string pixel_id = "")
		{
			try
			{
				switchRegTabHandle();

				if (!this._driver.Url.Contains("www.facebook.com"))
				{
					this._driver.Navigate().GoToUrl("https://www.facebook.com/ads/manager/account_settings/account_pages");
					WaitLoading();
					Delay(500);
				}

				string rurl = "https://www.facebook.com/adsmanager/manage/campaigns?act=" + _ads_id + "&tool=MANAGE_ADS&nav_entry_point=bm_global_nav_shortcut&nav_source=flyout_menu&nav_id=3206005184";
				string getData = this.fetchGet(rurl);
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(fb_dtsg))
				{
					rurl = "https://www.facebook.com/adsmanager/manage/accounts?act=" + _ads_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					getData = this.fetchGet(rurl);
					fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				}
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				string sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
				string campAccessToken = Regex.Match(getData, "__accessToken=\"(.*?)\"").Groups[1].Value;

				// Upload picture file
				this.log("Upload: ...");
				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir;
				string imgPath = dataPath + @"\post\img_temp.jpg";
				if (!File.Exists(imgPath))
				{
					this.log("Không thấy file ảnh!");
					return false;
				}
				byte[] imageArray = System.IO.File.ReadAllBytes(imgPath);
				string base64ImageRepresentation = Convert.ToBase64String(imageArray);
				base64ImageRepresentation = HttpUtility.UrlEncode(base64ImageRepresentation);

				string qpl_active_flow_instance_ids = "270206671_b8fbde6f140c93a4%2C27021288800_b8f218ec33ae9279%2C270213183_b8f25fde5107dc2a8%2C200000000_b8f2a5acf8b0c9894";
				string uploadUrl = "https://graph.facebook.com/v13.0/act_" + _ads_id + "/adimages?_app=ADS_MANAGER&_reqName=path%3A%2Fact_" + _ads_id + "%2Fadimages&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270206671%2C270211726%2C270212441%2C270213183%2C270213890%2C270216430&qpl_active_flow_instance_ids=" + qpl_active_flow_instance_ids + "&__cppo=1";
				string upload_body = string.Concat(new string[] {
						"__activeScenarioIDs=%5B%22f17229252d211d8_1664609394536.6%22%2C%22bdd1ac8e-c416-485f-a537-93cbe10cb3e1%22%5D&__activeScenarios=%5B%22am%3Aedit_ads%3Aupload_asset_in_media_dialog%22%2C%22am.edit_ads.upload_asset_in_media_dialog%22%5D&_app=ADS_MANAGER&_reqName=path%3A%2Fact_",
						_ads_id,
						"%2Fadimages&_reqSrc=adsDaoGraphDataMutator&_sessionID=",
						sessionID,
						"&accountId=",
						_ads_id,
						"&bytes=",
						base64ImageRepresentation,
						"&encoding=data%3Aimage%2Fjpeg%3Bbase64&endpoint=%2Fact_",
						_ads_id,
						"%2Fadimages&include_headers=false&locale=en_US&method=post&name=img_temp.jpg&pretty=0&qpl_active_flow_ids=270206671%2C270211726%2C270212441%2C270213183%2C270213890%2C270216430&qpl_active_flow_instance_ids=",
						qpl_active_flow_instance_ids,
						"&suppress_http_code=1&xref=f267b9e7e07dbfc"
					});
				string upload_postData = this.fetchApi(uploadUrl, "POST", upload_body, "", "", "https://www.facebook.com/");
				if (string.IsNullOrEmpty(upload_postData))
				{
					this.log("Upload: FALSE!");
					return false;
				}

				string imgUrl = Regex.Match(upload_postData, "\"url\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(imgUrl))
				{
					this.log("NOT imgUrl!");
					return false;
				}

				imgUrl = imgUrl.Replace("\\/", "/");
				imgUrl = Uri.EscapeDataString(imgUrl);
				this.log("imgUrl: " + imgUrl);

				// Todo get temp
				string jsonfile = dataPath + @"\camp\summary_camp_temp.json";
				if (!File.Exists(jsonfile))
				{
					this.log("Not summary_camp_temp.json file!");
					return false;
				}
				string _json = File.ReadAllText(jsonfile);
				PeCampParam peCampParam = JsonConvert.DeserializeObject<PeCampParam>(_json);

				string daily_budget = peCampParam.daily_budget;
				string age_max = peCampParam.age_max;
				string age_min = peCampParam.age_min;
				string genders = peCampParam.genders;
				string message = Uri.EscapeDataString(peCampParam.message);
				string link = Uri.EscapeDataString(peCampParam.link);
				string title = Uri.EscapeDataString(peCampParam.title);
				string page_id = pageID;
				if (!string.IsNullOrEmpty(peCampParam.page_id))
				{
					page_id = peCampParam.page_id;
				}

				string currency = peCampParam.currency;
				string countries = "null";
				string countriesInput = peCampParam.countries;
				if (!string.IsNullOrEmpty(countriesInput))
				{
					countriesInput = countriesInput.Replace(",", "%5C%22%2C%5C%22");
					countries = "%5B%5C%22" + countriesInput + "%5C%22%5D";
				}

				string camp_referrer = "https://business.facebook.com/latest/ad_center/ads_summary?asset_id=" + page_id + "&nav_ref=profile_plus_profile_left_nav_button";
				this._driver.Navigate().GoToUrl(camp_referrer);
				WaitLoading();
				Delay(500);

				getData = this._driver.PageSource;
				fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string camp_url = "https://business.facebook.com/api/graphql/";
				string camp_body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__usid=6-Trjyjwp1b9u6nu%3APrjyjwo1imynhu%3A0-Arjyjjpqskq56-RV%3D6%3AF%3D&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmxa3-Q8zo5ObwKBWobVo9E4a2i5U4e2O3O4UKewSAAzooxW4E5S2WdwJwiU6d1q1twUx60Vo1087yq10zo7R0iUao2gxO2O1VwwAwXwEwgolzU1vrzo5-1uwbe2l0Fwwwau1ywnEfogwNxaU5afK6E28xe3C2au1NwkEbEaUiwm88E5W3e1XxiawbW1Kxe6odEGdw46wbS2m2m2e3u362-0z8&__csr=&__req=39&__hs=19283.BP%3Abizweb_pkg.2.0.0.0.0&dpr=1&__ccg=EXCELLENT&__rev=1006411079&__s=zoy75n%3A1dd9dw%3Ahteka2&__hsi=7155894659387628571&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25203&lsd=",
					lsd,
					"&__aaid=",
					_ads_id,
					"&__spin_r=1006411079&__spin_b=trunk&__spin_t=1666111559&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=LWICometCreateBoostedComponentMutation&variables=%7B%22input%22%3A%7B%22boost_id%22%3Anull%2C%22creation_spec%22%3A%7B%22ads_lwi_goal%22%3A%22GET_PURCHASES%22%2C%22audience_option%22%3A%22AUTO_LOOKALIKE%22%2C%22auto_boost_settings_id%22%3Anull%2C%22auto_targeting_sources%22%3Anull%2C%22billing_event%22%3A%22IMPRESSIONS%22%2C%22budget%22%3A",
					daily_budget,
					"%2C%22budget_type%22%3A%22DAILY_BUDGET%22%2C%22currency%22%3A%22",
					currency,
					"%22%2C%22duration_in_days%22%3A-1%2C%22is_automatic_goal%22%3Afalse%2C%22legacy_ad_account_id%22%3A%22",
					_ads_id,
					"%22%2C%22legacy_entry_point%22%3A%22bizweb_ad_center_header%22%2C%22logging_spec%22%3A%7B%22spec_history%22%3A%5B%7B%22budget%22%3A",
					daily_budget,
					"%2C%22currency%22%3A%22",
					currency,
					"%22%7D%5D%7D%2C%22messenger_welcome_message%22%3Anull%2C%22pixel_event_type%22%3Anull%2C%22pixel_id%22%3A%22",
					pixel_id,
					"%22%2C%22placement_spec%22%3A%7B%22publisher_platforms%22%3A%5B%22FACEBOOK%22%2C%22MESSENGER%22%5D%7D%2C%22regulated_categories%22%3A%5B%5D%2C%22regulated_category%22%3A%22NONE%22%2C%22retargeting_enabled%22%3Afalse%2C%22run_continuously%22%3Atrue%2C%22saved_audience_id%22%3Anull%2C%22special_ad_category_countries%22%3A%5B%5D%2C%22start_time%22%3Anull%2C%22surface%22%3A%22BIZ_WEB%22%2C%22targeting_spec_string%22%3A%22%7B%5C%22age_min%5C%22%3A",
					age_min,
					"%2C%5C%22age_max%5C%22%3A",
					age_max,
					"%2C%5C%22geo_locations%5C%22%3A%7B%5C%22countries%5C%22%3A",
					countries,
					"%2C%5C%22location_types%5C%22%3A%5B%5C%22home%5C%22%5D%7D%2C%5C%22targeting_optimization%5C%22%3A%5C%22expansion_all%5C%22%7D%22%2C%22adgroup_specs%22%3A%5B%7B%22creative%22%3A%7B%22degrees_of_freedom_spec%22%3A%7B%7D%2C%22instagram_branded_content%22%3A%7B%7D%2C%22object_story_spec%22%3A%7B%22link_data%22%3A%7B%22call_to_action%22%3A%7B%22type%22%3A%22SHOP_NOW%22%2C%22value%22%3A%7B%22link%22%3A%22",
					link,
					"%22%7D%7D%2C%22link%22%3A%22",
					link,
					"%22%2C%22message%22%3A%22",
					message,
					"%22%2C%22name%22%3A%22",
					title,
					"%22%2C%22picture%22%3A%22",
					imgUrl,
					"%22%2C%22use_flexible_image_aspect_ratio%22%3Atrue%7D%2C%22page_id%22%3A%22",
					page_id,
					"%22%7D%2C%22use_page_actor_override%22%3Anull%7D%7D%5D%2C%22cta_data%22%3Anull%2C%22objective%22%3A%22WEBSITE_CONVERSIONS%22%7D%2C%22external_dependent_ent_id%22%3Anull%2C%22flow_id%22%3A%22d996e4d5-dec8-447f-a632-fea99e60d5f3%22%2C%22manual_review_requested%22%3Afalse%2C%22page_id%22%3A%22",
					page_id,
					"%22%2C%22product%22%3A%22BOOSTED_PURCHASE%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=4887866407975731"
				});

				string camp_postData = this.fetchApi(camp_url, "POST", camp_body, "LWICometCreateBoostedComponentMutation", lsd, camp_referrer);
				if (string.IsNullOrEmpty(camp_postData))
				{
					this.log("camp_postData: " + camp_postData);
					return false;
				}

				string boosting_status = Regex.Match(camp_postData, "\"boosting_status\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(boosting_status) || ("CREATING" != boosting_status))
				{
					this.log("camp_postData: " + camp_postData);
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool TKBMCampOnOff(string old_value, string new_value)
		{
			try
			{
				switchBusinessTabHandle();

				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					string current_url = "https://business.facebook.com/settings/people?business_id=" + _bm_id;
					this._driver.Navigate().GoToUrl(current_url);
					WaitLoading();
					Delay(1000);
				}

				string rurl = "https://business.facebook.com/adsmanager/manage/campaigns?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&global_scope_id=" + _bm_id + "&tool=MANAGE_ADS&nav_entry_point=bm_global_nav_shortcut&nav_source=flyout_menu&nav_id=1565291292about%3Ablank";
				string getData = this.fetchGet(rurl);
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(fb_dtsg))
				{
					rurl = "https://business.facebook.com/adsmanager/manage/accounts?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					getData = this.fetchGet(rurl);
					fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				}

				string addraft = Regex.Match(getData, "\"ad_draft_id\":\"(.*?)\"").Groups[1].Value;

				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"1\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"2\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"3\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "addraft_(.*?){\"fields\"").Groups[1].Value;
				}

				if (string.IsNullOrEmpty(addraft))
				{
					this.log("Không thấy bản thảo!");
					return false;
				}

				string sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
				string campAccessToken = Regex.Match(getData, "__accessToken=\"(.*?)\"").Groups[1].Value;

				// Get camp data
				string aurl = "https://graph.facebook.com/v12.0/act_" + _bm_ads_id + "?access_token=" + campAccessToken + "&fields=[%22id%22,%22name%22,%22ads.limit(1)%22,%22adsets.limit(1)%22]&method=get&pretty=0";
				string resrq = this.fetchGet(aurl);
				// string resrq = GetData(null, aurl, _cookie);
				AdAccounts actData = JsonConvert.DeserializeObject<AdAccounts>(resrq);
				if ((actData == null) || (actData.ads == null) || (actData.ads.data == null) || (actData.ads.data.Count() == 0)
					|| (actData.adsets == null) || (actData.adsets.data == null) || (actData.adsets.data.Count() == 0))
				{
					this.log("Không có DL ads!");
					return false;
				}

				// Bat cam
				string Ad_object_id = actData.ads.data.First().id;
				string Parent_ad_object_id = actData.adsets.data.First().id;

				string referrerUrl = "https://business.facebook.com/";
				string url = "https://graph.facebook.com/v12.0/" + addraft + "/addraft_fragments_with_publish?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments_with_publish&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270206350%2C270214832&qpl_active_flow_instance_ids=270206350_84f1601bdc1b086c&__cppo=1";
				string body = string.Concat(new string[] {
					"__activeScenarioIDs=%5B%22f1ce31407940674_1651037239094.8%22%5D&__activeScenarios=%5B%22instant_publish_rolldown%22%5D&__business_id=",
					_bm_id,
					"&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments_with_publish&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
					sessionID,
					"&account_id=",
					_bm_ads_id,
					"&action=modify&ad_object_id=",
					Ad_object_id,
					"&ad_object_type=ad&include_headers=false&locale=en_US&method=post&parent_ad_object_id=",
					Parent_ad_object_id,
					"&pretty=0&qpl_active_flow_ids=270206350%2C270214832&qpl_active_flow_instance_ids=270206350_84f1601bdc1b086c&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22status%22%2C%22old_value%22%3A%22",
					old_value,
					"%22%2C%22new_value%22%3A%22",
					new_value,
					"%22%7D%5D&xref=f1f064d03028c48"
				});

				string postData = PostData(null, url, body, null, null, _cookie, null, null, referrerUrl);
				string success = Regex.Match(postData, "\"success\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(success))
				{
					success = Regex.Match(postData, "\"success\":(.*?)}").Groups[1].Value;
				}

				if (success != "true")
				{
					this.log(body);
					this.log(postData);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool TKCNCampOnOff(string old_value, string new_value)
		{
			try
			{
				switchRegTabHandle();

				if (!this._driver.Url.Contains("www.facebook.com"))
				{
					this._driver.Navigate().GoToUrl("https://www.facebook.com/ads/manager/account_settings/account_pages");
					WaitLoading();
					Delay(500);
				}

				string rurl = "https://www.facebook.com/adsmanager/manage/campaigns?act=" + _ads_id + "&tool=MANAGE_ADS&nav_entry_point=bm_global_nav_shortcut&nav_source=flyout_menu&nav_id=3206005184";
				string getData = this.fetchGet(rurl);
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(fb_dtsg))
				{
					rurl = "https://www.facebook.com/adsmanager/manage/accounts?act=" + _ads_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					getData = this.fetchGet(rurl);
					fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				}

				string addraft = Regex.Match(getData, "\"ad_draft_id\":\"(.*?)\"").Groups[1].Value;

				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"1\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"2\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "\"draft_version\":\"3\",\"id\":\"(.*?)\"").Groups[1].Value;
				}
				if (string.IsNullOrEmpty(addraft))
				{
					addraft = Regex.Match(getData, "addraft_(.*?){\"fields\"").Groups[1].Value;
				}

				if (string.IsNullOrEmpty(addraft))
				{
					this.log("Không thấy bản thảo!");
					return false;
				}

				string sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
				string campAccessToken = Regex.Match(getData, "__accessToken=\"(.*?)\"").Groups[1].Value;

				// Get camp data
				string aurl = "https://graph.facebook.com/v12.0/act_" + _ads_id + "?access_token=" + campAccessToken + "&fields=[%22id%22,%22name%22,%22ads.limit(1)%22,%22adsets.limit(1)%22]&method=get&pretty=0";
				string resrq = GetData(null, aurl, _cookie);
				AdAccounts actData = JsonConvert.DeserializeObject<AdAccounts>(resrq);
				if ((actData == null) || (actData.ads == null) || (actData.ads.data == null) || (actData.ads.data.Count() == 0)
					|| (actData.adsets == null) || (actData.adsets.data == null) || (actData.adsets.data.Count() == 0))
				{
					this.log("Không có DL ads!");
					return false;
				}

				// Bat cam
				string Ad_object_id = actData.ads.data.First().id;
				string Parent_ad_object_id = actData.adsets.data.First().id;

				string referrerUrl = "https://business.facebook.com/";
				string url = "https://graph.facebook.com/v12.0/" + addraft + "/addraft_fragments_with_publish?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments_with_publish&access_token=" + campAccessToken + "&method=post&qpl_active_flow_ids=270206350%2C270214832&qpl_active_flow_instance_ids=270206350_84f1601bdc1b086c&__cppo=1";
				string body = string.Concat(new string[] {
					"__activeScenarioIDs=%5B%22f1ce31407940674_1651037239094.8%22%5D&__activeScenarios=%5B%22instant_publish_rolldown%22%5D&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments_with_publish&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
					sessionID,
					"&account_id=",
					_ads_id,
					"&action=modify&ad_object_id=",
					Ad_object_id,
					"&ad_object_type=ad&include_headers=false&locale=en_US&method=post&parent_ad_object_id=",
					Parent_ad_object_id,
					"&pretty=0&qpl_active_flow_ids=270206350%2C270214832&qpl_active_flow_instance_ids=270206350_84f1601bdc1b086c&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22status%22%2C%22old_value%22%3A%22",
					old_value,
					"%22%2C%22new_value%22%3A%22",
					new_value,
					"%22%7D%5D&xref=f1f064d03028c48"
				});

				string postData = PostData(null, url, body, null, null, _cookie, null, null, referrerUrl);
				string success = Regex.Match(postData, "\"success\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(success))
				{
					success = Regex.Match(postData, "\"success\":(.*?)}").Groups[1].Value;
				}

				if (success != "true")
				{
					this.log(body);
					this.log(postData);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool waitAdsCampError(int checkCount = 10)
		{
			try
			{
				switchRegTabHandle();

				if (!this._driver.Url.Contains("www.facebook.com"))
				{
					string current_url = "https://www.facebook.com/ads/manager/account_settings/information/?act=" + _ads_id;
					this._driver.Navigate().GoToUrl(current_url);
					WaitLoading();
					Delay(1000);
				}

				string rurl = "https://www.facebook.com/adsmanager/manage/campaigns?act=" + _ads_id + "&nav_entry_point=bm_global_nav_shortcut";
				string getData = this.fetchGet(rurl);
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(fb_dtsg))
				{
					rurl = "https://www.facebook.com/adsmanager/manage/accounts?act=" + _ads_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					getData = this.fetchGet(rurl);
					fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				}

				string sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
				string campAccessToken = Regex.Match(getData, "__accessToken=\"(.*?)\"").Groups[1].Value;

				// Get camp data
				string aurl = "https://graph.facebook.com/v12.0/act_" + _ads_id + "?access_token=" + campAccessToken + "&fields=[%22id%22,%22name%22,%22ads.limit(1)%22,%22adsets.limit(1)%22]&method=get&pretty=0";
				string resrq = GetData(null, aurl, _cookie);
				AdAccounts actData = JsonConvert.DeserializeObject<AdAccounts>(resrq);
				if ((actData == null) || (actData.ads == null) || (actData.ads.data == null) || (actData.ads.data.Count() == 0)
					|| (actData.adsets == null) || (actData.adsets.data == null) || (actData.adsets.data.Count() == 0))
				{
					this.log("Không có DL ads!");
					return false;
				}

				string Ad_object_id = actData.ads.data.First().id;
				bool isError = false;

				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v14.0/?access_token=",
					campAccessToken,
					"&__cppo=1&__activeScenarioIDs=%5B%5D&__activeScenarios=%5B%5D&__business_id=",
					_bm_id,
					"&__interactionsMetadata=%5B%5D&_app=ADS_MANAGER&_reqName=objects%3Adynamic_campaign_group&_reqSrc=AdsDynamicAdObjectDataManager&_sessionID=",
					sessionID,
					"&am_call_tags=%7B%22data_manager%22%3A%22AdsDynamicAdObjectDataManager%22%7D&fields=%5B%22delivery_status%7Bextra_data%2Cstatus%2Csubstatuses%7D%22%5D&ids=",
					Ad_object_id,
					"&include_headers=false&locale=en_US&method=get&pretty=0&suppress_http_code=1&xref=f1d524fe5ef156c"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				string status = Regex.Match(data, "\"status\":\"(.*?)\"").Groups[1].Value;
				if (status == "error")
				{
					isError = true;
				}

				int ChkIndex = 1;

				this.log("Waiting...");
				while (!isError && (ChkIndex <= checkCount))
				{
					Delay(5000);
					data = this.GetData(null, url, _cookie, this._userAgent);
					status = Regex.Match(data, "\"status\":\"(.*?)\"").Groups[1].Value;
					if (status == "error")
					{
						isError = true;
					}

					ChkIndex++;
				}

				return isError;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool waitBMAdsCampError(int checkCount = 10)
		{
			try
			{
				switchBusinessTabHandle();

				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					string current_url = "https://business.facebook.com/settings/people?business_id=" + _bm_id;
					this._driver.Navigate().GoToUrl(current_url);
					WaitLoading();
					Delay(1000);
				}

				string rurl = "https://business.facebook.com/adsmanager/manage/campaigns?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&global_scope_id=" + _bm_id + "&tool=MANAGE_ADS&nav_entry_point=bm_global_nav_shortcut&nav_source=flyout_menu&nav_id=1565291292about%3Ablank";
				string getData = this.fetchGet(rurl);
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(fb_dtsg))
				{
					rurl = "https://business.facebook.com/adsmanager/manage/accounts?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					getData = this.fetchGet(rurl);
					fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				}

				string sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
				string campAccessToken = Regex.Match(getData, "__accessToken=\"(.*?)\"").Groups[1].Value;

				// Get camp data
				string aurl = "https://graph.facebook.com/v12.0/act_" + _bm_ads_id + "?access_token=" + campAccessToken + "&fields=[%22id%22,%22name%22,%22ads.limit(1)%22,%22adsets.limit(1)%22]&method=get&pretty=0";
				string resrq = GetData(null, aurl, _cookie);
				AdAccounts actData = JsonConvert.DeserializeObject<AdAccounts>(resrq);
				if ((actData == null) || (actData.ads == null) || (actData.ads.data == null) || (actData.ads.data.Count() == 0)
					|| (actData.adsets == null) || (actData.adsets.data == null) || (actData.adsets.data.Count() == 0))
				{
					this.log("Không có DL ads!");
					return false;
				}

				string Ad_object_id = actData.ads.data.First().id;
				bool isError = false;

				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v14.0/?access_token=",
					campAccessToken,
					"&__cppo=1&__activeScenarioIDs=%5B%5D&__activeScenarios=%5B%5D&__business_id=",
					_bm_id,
					"&__interactionsMetadata=%5B%5D&_app=ADS_MANAGER&_reqName=objects%3Adynamic_campaign_group&_reqSrc=AdsDynamicAdObjectDataManager&_sessionID=",
					sessionID,
					"&am_call_tags=%7B%22data_manager%22%3A%22AdsDynamicAdObjectDataManager%22%7D&fields=%5B%22delivery_status%7Bextra_data%2Cstatus%2Csubstatuses%7D%22%5D&ids=",
					Ad_object_id,
					"&include_headers=false&locale=en_US&method=get&pretty=0&suppress_http_code=1&xref=f1d524fe5ef156c"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				string status = Regex.Match(data, "\"status\":\"(.*?)\"").Groups[1].Value;
				if (status == "error")
				{
					isError = true;
				}

				int ChkIndex = 1;

				this.log("Waiting...");
				while (!isError && (ChkIndex <= checkCount))
				{
					Delay(5000);
					data = this.GetData(null, url, _cookie, this._userAgent);
					status = Regex.Match(data, "\"status\":\"(.*?)\"").Groups[1].Value;
					if (status == "error")
					{
						isError = true;
					}

					ChkIndex++;
				}

				return isError;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public IntData getInstagram(string pageId, string accessToken)
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v12.0/",
					pageId,
					"?access_token=",
					accessToken,
					"&fields=%5B%22instagram_accounts%22%2C%22page_backed_instagram_accounts%22%2C%22instagram_business_account%22%5D&include_headers=false&pretty=0"
				});

				// string resData = this.GetData(null, url, _cookie, this._userAgent);
				string resData = this.fetchGet(url);
				return JsonConvert.DeserializeObject<IntData>(resData);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		public bool checkHasPost()
		{
			try
			{
				string url = "";
				if (!string.IsNullOrEmpty(_bm_id))
				{
					url = "https://graph.facebook.com/v11.0/" + _bm_id + "/owned_pages?access_token=" + _accessToken + "&limit=1&fields=[%22posts%22]&include_headers=false&pretty=0";
				}
				else
				{
					url = "https://graph.facebook.com/v11.0/" + _clone_uid + "/facebook_pages?access_token=" + _cloneAccessToken + "&limit=1&fields=[%22posts%22]&include_headers=false&pretty=0";
				}

				string data = this.GetData(null, url, _cookie, this._userAgent);
				string posts_data = Regex.Match(data, "\"posts\":{\"data\":(.*?)}").Groups[1].Value;
				if (string.IsNullOrEmpty(posts_data))
				{
					this.log("POST EMPTY!");
					return false;
				}

				postID = Regex.Match(posts_data, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(postID))
				{
					this.log("postID EMPTY!");
					return false;
				}

				String[] postIdData = postID.Split('_');
				if (postIdData.Length < 2)
				{
					this.log("postID INVALID!");
					return false;
				}

				if (pageID != postIdData[0])
				{
					this.log("PAGE FALSE!");
					return false;
				}

				this.log("Post id: " + postID);
				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		private void handleUnexpectedAlert()
		{
			try
			{
				IAlert alert = this._driver.SwitchTo().Alert();
				alert.Accept();
			}
			catch (NoAlertPresentException ignore)
			{
				// This is the way to know it wasn't there
				this.log(ignore.Message);
				this.log("Not Alert!");
			}
		}

		public bool CreatePostAPI(string imgPath, string message)
        {
			try
			{
				switchRegTabHandle();

				this.log("CREATE Post...");
				this.log("Page ID: " + pageID);
				string url = "https://www.facebook.com/" + pageID;

				if (!File.Exists(imgPath))
				{
					this.log("Không thấy file img_temp.jpg!");
					return false;
				}
				byte[] imageArray = System.IO.File.ReadAllBytes(imgPath);
				string base64ImageRepresentation = Convert.ToBase64String(imageArray);

				string getData = _driver.PageSource;
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				url = string.Concat(new string[]
				{
				"https://upload.facebook.com/ajax/react_composer/attachments/photo/upload?av=",
				pageID,
				"&__user=",
				_clone_uid,
				"&__a=1&__dyn=7AzHxqUW13xt0mUyEqxenFwLBwopU98nwgU765QdwSAxacyUco5S3O2Saxa1NwJwpUe8hw6vwb-q7oc81xoswIK1Rwwwqo465o-cw5MKdwGxu782lwv89kbxS2218wc61axe3S1lwlE-UqwsUkxe2GewyDwkUtxGm2SUbElxm3y11xfxmu3W3y1MBx_wHwdG7FobpEbUGdwb6223908O3216AzUjwTwNwLwFg662S26&__csr=g_4PiNG8D9WT9d6tNkj6bvklO4dkGE8baBOFLsYg8dKxvYhWQ-9VAVybjHOGGIWmbRKcZkFkbBBWzVA8AqOblykZ1eEhKnV4qnuF8KjKGGiVBDyWBBXzQ9K6HAGXK9gyubAye49GBihFVEG8-fJd6yaGmEizaxa8BypFoyiEJ4ypV8oAUgJ2UjyVFUCt3aAzGAzQEsKaKF4Ulz8hy899E8u3swuG32EGdAxh38zUSbwFAV8S8xJ0QgrAyqzuEfEjzqGdwwwUByorwHzoow-yEgG1dyKewi86m3W2rg4K2m9xuXUmU89UkyEhw_xe1Qy84um1nw2TE08yz28S03PS01VGw1Pe08jDBDDzLHSGCGfK9hkJqzohG8GJoceu5qy8-cGqEkzUbEIsK7k_yUChHzJ5gowfCHWvjV8h952kuVWBG9HG0BVo2hwoo1x8jK3e2eu0OGwsU6e073u0csU2m81Zz8BuU7i2t0IjJ7w5Qw_g0zfo1Goy9GE2nwdO2u17wee0xUco39x62vCw41zqFa0Q84m0yd1G04lU0t1wDw1mq0QE0wyE0P20i2h0gE2rw5gw5qw3v4m1Hw5LxC&__req=1h&__hs=19305.HYP%3Acomet_pkg.2.1.0.2.1&dpr=1&__ccg=EXCELLENT&__rev=1006573260&__s=liaktn%3Anfcyk1%3A1gi4wv&__hsi=7163919921738016707&__comet_req=15&fb_dtsg=",
				fb_dtsg,
				"&jazoest=25696&lsd=",
				lsd,
				"&__aaid=",
				_ads_id,
				"&__spin_r=1006573260&__spin_b=trunk&__spin_t=1667980086"
				});

				string referrerStr = "\"referrer\": \"https://www.facebook.com/\",";

				string jsstr = "";
				jsstr += "const datatoBlob = (imgbase64) => {let byteString = atob(imgbase64); let ab = new ArrayBuffer(byteString.length); let ia = new Uint8Array(ab); ";
				jsstr += "for (let i = 0; i < byteString.length; i++) { ia[i] = byteString.charCodeAt(i);} return new Blob([ia], {type: 'image/jpeg'});}; ";

				jsstr += "let blob = datatoBlob(\"" + base64ImageRepresentation + "\");";

				jsstr += "let formData = new FormData();";
				jsstr += "formData.append(\"source\", \"8\");";
				jsstr += "formData.append(\"profile_id\", \"" + _clone_uid + "\");";
				jsstr += "formData.append(\"waterfallxapp\", \"comet\");";
				jsstr += "formData.append(\"farr\", blob, \"img_temp.jpg\");";
				jsstr += "formData.append(\"upload_id\", \"jsc_c_2e\");";

				jsstr += "let response = await fetch(\"" + url + "\", {" + referrerStr +
					"\"referrerPolicy\": \"strict-origin-when-cross-origin\"," +
					"\"body\": formData," +
					"\"method\": \"POST\"," +
					"\"mode\": \"cors\"," +
					"\"credentials\": \"include\"" +
				"}); return await response.text();";
				string postData = (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
                if (string.IsNullOrEmpty(postData))
                {
					return false;
				}

				// this.log(postData);
				string photoID = Regex.Match(postData, "\"photoID\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(photoID))
				{
					return false;
				}

				message = Uri.EscapeDataString(message);

				// Create Post
				url = "https://www.facebook.com/api/graphql/";
				string publishbody = string.Concat(new string[] {
					"av=",
					pageID,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHxqUW13xt0mUyEqxenFwLBwopU98nwgU765QdwSAxacyUco5S3O2Saxa1NwJwpUe8hw6vwb-q7oc81xoswIK1Rwwwqo465o-cw5MKdwGxu782lwv89kbxS2218wc61axe3S1lwlE-UqwsUkxe2GewyDwkUtxGm2SUbElxm3y11xfxmu3W3y1MBx_wHwdG7FobpEbUGdwb6223908O3216AzUjwTwNwLwFg662S26&__csr=g_4PiNG8DdHsAQpT5hcmlZhn8gRiGwwIGnaCZPN0wSW5_N7HjUDCjC8JeLaGGYGmbRKcZkFkbBBWzVA8AqOblykZ1eEhKnV4qnuF8KjKGG8DVUKFpuUZ2rxWiqXyk8KUKi8UgCGl96DCyEzU-QQq8GFqxacG4Eym9CBy9ayQi9DAxyjx2QbxebCDypQcGieGi4UsKeGhe5oO4oy2iq27wT87GwMGazp8kgO8-dyUapeidy8rgd46ui9GdWw-xedGES223W9xK2K2q3Wax2E4SaUW18wai2rg4K2m9xuXUmU89UkxO3-4U7i8whVo5u0buw0yac8zo0ffo07CG07cU0xeumuue-LqGqE-UB5iRGdx6EyGRwMVUlG8zUOFGxifwKyNOUtj-byp6KeQl1y0-qLFZfAx4Ak9hXDGmECKE2nBw961xw64xeUcU8VU3aG1PwoU0sdU0NPw9ow7ScylXwt89Q2NeQu0ni3Z02cZw6Fy8CGw9u0T89U4u0UU27wNwcC4o9-q0g6dGAE3gwho28Q6E0hnw1Q62u05pE3iw22aw3c8189412w9K0l20lG0dYho6K0m-6o&__req=15&__hs=19305.HYP%3Acomet_pkg.2.1.0.2.1&dpr=1&__ccg=EXCELLENT&__rev=1006573289&__s=j7gjjl%3Aizad9o%3Afb76pg&__hsi=7163958369868301431&__comet_req=15&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25348&lsd=",
					lsd,
					"&__aaid=",
					_ads_id,
					"&__spin_r=1006573289&__spin_b=trunk&__spin_t=1667989038&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=ComposerStoryCreateMutation&variables=%7B%22input%22%3A%7B%22source%22%3A%22WWW%22%2C%22logging%22%3A%7B%22composer_session_id%22%3A%2248abe42d-8985-4c4f-98dd%22%2C%22referral_code%22%3Anull%7D%2C%22attachments%22%3A%5B%7B%22photo%22%3A%7B%22id%22%3A%22",
					photoID,
					"%22%7D%7D%5D%2C%22audience%22%3A%7B%22privacy%22%3A%7B%22allow%22%3A%5B%5D%2C%22base_state%22%3A%22EVERYONE%22%2C%22deny%22%3A%5B%5D%2C%22tag_expansion_state%22%3A%22UNSPECIFIED%22%7D%7D%2C%22message%22%3A%7B%22ranges%22%3A%5B%5D%2C%22text%22%3A%22",
					message,
					"%22%7D%2C%22with_tags_ids%22%3A%5B%5D%2C%22inline_activities%22%3A%5B%5D%2C%22explicit_place_id%22%3A%220%22%2C%22text_format_preset_id%22%3A%220%22%2C%22navigation_data%22%3A%7B%22attribution_id_v2%22%3A%22PagesCometAdminSelfViewRoot.react%2Ccomet.page.admin.self_view%2Cvia_cold_start%2C1667989039171%2C185243%2C250100865708545%2C%22%7D%2C%22actor_id%22%3A%22",
					pageID,
					"%22%2C%22client_mutation_id%22%3A%223%22%7D%2C%22displayCommentsFeedbackContext%22%3Anull%2C%22displayCommentsContextEnableComment%22%3Anull%2C%22displayCommentsContextIsAdPreview%22%3Anull%2C%22displayCommentsContextIsAggregatedShare%22%3Anull%2C%22displayCommentsContextIsStorySet%22%3Anull%2C%22feedLocation%22%3A%22PAGE_TIMELINE%22%2C%22feedbackSource%22%3A22%2C%22focusCommentID%22%3Anull%2C%22gridMediaWidth%22%3Anull%2C%22groupID%22%3Anull%2C%22scale%22%3A1%2C%22privacySelectorRenderLocation%22%3A%22COMET_STREAM%22%2C%22renderLocation%22%3A%22timeline%22%2C%22useDefaultActor%22%3Afalse%2C%22inviteShortLinkKey%22%3Anull%2C%22isFeed%22%3Afalse%2C%22isFundraiser%22%3Afalse%2C%22isFunFactPost%22%3Afalse%2C%22isGroup%22%3Afalse%2C%22isEvent%22%3Afalse%2C%22isTimeline%22%3Atrue%2C%22isSocialLearning%22%3Afalse%2C%22isPageNewsFeed%22%3Afalse%2C%22isProfileReviews%22%3Afalse%2C%22isWorkSharedDraft%22%3Afalse%2C%22UFI2CommentsProvider_commentsKey%22%3A%22CometSinglePageContentContainerFeedQuery%22%2C%22hashtag%22%3Anull%2C%22canUserManageOffers%22%3Afalse%7D&server_timestamps=true&doc_id=5538090359650354&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%2C431626192%22%5D"
				});

				postData = this.fetchApi(url, "POST", publishbody, "ComposerStoryCreateMutation", lsd, this._driver.Url);
                if (string.IsNullOrEmpty(postData))
                {
					return false;
				}
				string legacy_story_hideable_id = Regex.Match(postData, "\"legacy_story_hideable_id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(legacy_story_hideable_id))
				{
					return false;
				}
				postID = pageID + "_" + legacy_story_hideable_id;
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private string createVideoPost(string url, string access_token, string video_asset_id, string description, string title, string thumbdata)
		{
			try
			{
				string jsstr = "function makePost(url, form_data) { return new Promise(function(resolve, reject) { const xhttp = new XMLHttpRequest();";
				jsstr += "xhttp.withCredentials = true;";
				jsstr += "xhttp.onload = function() {";
				jsstr += "if (this.status >= 200 && this.status < 300)";
				jsstr += "{";
				jsstr += "resolve(this.responseText);";
				jsstr += "}";
				jsstr += "else";
				jsstr += "{";
				jsstr += "reject({";
				jsstr += "status: this.status,";
				jsstr += "statusText: xhttp.statusText";
				jsstr += "});";
				jsstr += "}";
				jsstr += "};";
				jsstr += "xhttp.onerror = function() {";
				jsstr += "reject({";
				jsstr += "status: this.status,";
				jsstr += "statusText: xhttp.statusText";
				jsstr += "});";
				jsstr += "};";

				jsstr += "xhttp.open(\"POST\", url, true);";
				jsstr += "xhttp.setRequestHeader(\"Accept\", \" */*\");";
				jsstr += "xhttp.send(form_data);";
				jsstr += "});";
				jsstr += "} ";

				jsstr += "function dataURLToBlob(t){const n=t.split(\",\"),o=n[0].match(/:(.*?);/)[1],e=atob(n[1]);let a=e.length;const r=new Uint8Array(a);for(;a--;)r[a]=e.charCodeAt(a);return new Blob([r],{type:o})} ";

				jsstr += "var blob = dataURLToBlob('" + thumbdata + "');";

				jsstr += "var fileFormData = new FormData();";
				jsstr += "fileFormData.append('description', '" + description + "');";
				jsstr += "fileFormData.append('eventID', 'undefined');";
				jsstr += "fileFormData.append('specified_dialect', '');";
				jsstr += "fileFormData.append('preferred_thumbnail_index', 12);";
				jsstr += "fileFormData.append('thumb', blob);";
				jsstr += "fileFormData.append('access_token', '" + access_token + "');";
				jsstr += "fileFormData.append('title', '" + title + "');";
				jsstr += "fileFormData.append('video_asset_id', '" + video_asset_id + "');";
				jsstr += "fileFormData.append('published', true);";
				jsstr += "fileFormData.append('no_story', false);";
				jsstr += "fileFormData.append('embeddable', true);";
				jsstr += "fileFormData.append('secret', false);";
				jsstr += "fileFormData.append('social_actions', true);";
				jsstr += "fileFormData.append('locale', 'en_US');";

				jsstr += "const url = \"" + url + "\";";
				jsstr += "return await makePost(url, fileFormData);";
				return (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
			}
			catch (Exception ex)
			{
				this.log(ex.ToString());
				return null;
			}
		}

		public bool TKBMVideoPost(string file_url, string title, string message, string thumbPath)
		{
			try
			{
				if (string.IsNullOrEmpty(pageID))
				{
					this.log("Không thấy Page ID!");
					return false;
				}
				switchBusinessTabHandle();

				string rurl = "https://business.facebook.com/creatorstudio/published?content_table=POSTED_POSTS&post_type=VIDEOS&selected_single_page_id=" + pageID + "&reference=video_processed_notification_no_metadata_tip";
				this._driver.Navigate().GoToUrl(rurl);
				WaitLoading();
				Delay(500);

				string getData = this.fetchGet(rurl);
				string userAccessToken = Regex.Match(getData, "\"userAccessToken\":\"(.*?)\"").Groups[1].Value;
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				if (string.IsNullOrEmpty(userAccessToken))
				{
					this._driver.Navigate().GoToUrl(rurl);
					WaitLoading();
					Delay(500);
					getData = this.fetchGet(rurl);
					userAccessToken = Regex.Match(getData, "\"userAccessToken\":\"(.*?)\"").Groups[1].Value;
					fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
					lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				}

				if (string.IsNullOrEmpty(userAccessToken))
                {
					this.log("userAccessToken: FALSE!");
					return false;
				}

				// Upload video
				string video_id = "";
				string video_asset_id = "";
				this.log("Upload video...!");

				string uploadUrl = "https://graph-video.facebook.com/v15.0/" + pageID + "/videos?title=" + Uri.EscapeDataString(title) + "&access_token=" + userAccessToken + "&file_url=" + file_url;
				string upload_body = string.Concat(new string[] {
					"------WebKitFormBoundaryabkJ1dWwF1Okeym1\\r\\nContent-Disposition: form-data; name=\\\"title\\\"\\r\\n\\r\\n",
					Uri.EscapeDataString(title),
					"\\r\\n------WebKitFormBoundaryabkJ1dWwF1Okeym1\\r\\nContent-Disposition: form-data; name=\\\"access_token\\\"\\r\\n\\r\\n",
					userAccessToken,
					"\\r\\n------WebKitFormBoundaryabkJ1dWwF1Okeym1\\r\\nContent-Disposition: form-data; name=\\\"file_url\\\"\\r\\n\\r\\n",
					file_url,
					"\\r\\n------WebKitFormBoundaryabkJ1dWwF1Okeym1--\\r\\n"
				});
				string upload_postData = this.formDataFetchApi(uploadUrl, "POST", upload_body, "", "", "https://business.facebook.com/");

				if (!string.IsNullOrEmpty(upload_postData))
				{
					video_id = Regex.Match(upload_postData, "\"id\": \"(.*?)\"").Groups[1].Value;
					if (!string.IsNullOrEmpty(video_id))
					{
						this.log("Check video...!");
						bool checkVideo = false;
						int ChkVideoIndex = 1;
						while ((!checkVideo) && (ChkVideoIndex <= 8))
						{
							Delay(3000);
							string videodata = getVideo(video_id, userAccessToken);
							video_asset_id = Regex.Match(videodata, "\"video_asset_id\":\"(.*?)\"").Groups[1].Value;
							if (!string.IsNullOrEmpty(video_asset_id))
							{
								checkVideo = true;
							}
							ChkVideoIndex++;
						}
					}
				}

				if (string.IsNullOrEmpty(video_asset_id))
				{
					this.log("Không thấy Video ID!");
					return false;
				}

				string url = "https://business.facebook.com/video/post_from_asset/?video_id=" + video_id + "&source=media_manager_content_library&av=" + pageID;
				string publishbody = string.Concat(new string[] {
					"__asyncDialog=1&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmxa3-Q8zo9FEbEyHxquuVuCHwEBwCzod8pyEnCAwTwywhFUKbgS3eF98SmqbxW4e16wCzQdzobp948zLwAwCyEhx2786uVoyaxG4o4O5oaUgAwg8c9Eoyo6m3K6UG1lKEK12wOQcG3tpUdoK7UC5U7zxim68jK2i58hx-9wBUdrwEwDxC5o-5E6W1-wRwgHzouyUlxWU5GUpwoVUao9k2B6yoW18wnE4K5E5W7S6UgwNx6i8wxK2efK1zxy2q4Uowww9O14zo9oy2K4E9ocE8EnwhE2AyF8-awwwUwk85S4oiyUhzUiwzyES2e0UFU2RwQxq2e5o5ei5EjwiUpwCx2&__csr=&__req=29&__hs=19352.BP%3Amedia_manager_pkg.2.0.0.0.0&dpr=1&__ccg=EXCELLENT&__rev=1006771290&__s=cebcch%3A5qrp2i%3An8z566&__hsi=7181578030214430698&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25389&lsd=",
					lsd,
					"&__aaid=&__jssesw=1"
				});
				string postData = this.fetchApi(url, "POST", publishbody, "", lsd, "https://business.facebook.com/creatorstudio/published");
				if (string.IsNullOrEmpty(postData))
				{
					this.log("Không thấy token 1!");
					return false;
				}

				string accessTokenNini = Regex.Match(postData, "\"accessToken\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(accessTokenNini))
				{
					this.log("Không thấy token 1!");
					return false;
				}


				string post_id = "";
				byte[] imageArray = System.IO.File.ReadAllBytes(thumbPath);
				string base64ImageRepresentation = Convert.ToBase64String(imageArray);
				base64ImageRepresentation = "data:image/jpeg;base64," + base64ImageRepresentation;

				uploadUrl = "https://graph.facebook.com/v2.12/" + pageID + "/videos?access_token=" + accessTokenNini + "&suppress_http_code=1";
				upload_postData = createVideoPost(uploadUrl, accessTokenNini, video_asset_id, message, title, base64ImageRepresentation);
				video_id = Regex.Match(upload_postData, "\"id\": \"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(video_id))
				{
					this.log(upload_postData);
					return false;
				}

				string videodataNew = getVideoContent(video_id, userAccessToken);
				post_id = Regex.Match(videodataNew, "\"video_container_post_id\":\"(.*?)\"").Groups[1].Value;

				if (string.IsNullOrEmpty(post_id))
				{
					this.log(upload_postData);
					return false;
				}
				postID = pageID + "_" + post_id;
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool BoostPostlaunchingCamp(string type, string nlCampId, string pixel_id = "", bool newPost = false, bool isvideo = false)
		{
			try
			{
				switchRegTabHandle();

				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir + "\\camp2.0\\" + nlCampId + "\\";

				string jsonfile = dataPath + @"pe_camp_temp.json";
				if (!File.Exists(jsonfile))
				{
					jsonfile = dataPath + @"bp_camp_temp.json";
				}

				if (!File.Exists(jsonfile))
				{
					this.log("Not camp temp file!");
					return false;
				}
				string _json = File.ReadAllText(jsonfile);
				PeCampParam bpCampParam = JsonConvert.DeserializeObject<PeCampParam>(_json);
				string postMessage = bpCampParam.message;

				// Tao post
				if (string.IsNullOrEmpty(postID) || newPost)
                {
					bool createPost = false;
                    if (isvideo)
                    {
						string file_url = bpCampParam.video_s_url;
						string title = bpCampParam.title;
						string message = postMessage;
						if (string.IsNullOrEmpty(file_url))
                        {
							this.log("Not video file url!");
							return false;
						}

						string thumbPath = dataPath + @"thumb.jpg";
						if (!File.Exists(thumbPath))
						{
							this.log("Không thấy file thumb!");
							return false;
						}

						createPost = TKBMVideoPost(file_url, title, message, thumbPath);
					}
                    else
                    {
						string imgPath = dataPath + @"img_temp.jpg";
						if (!File.Exists(imgPath))
						{
							this.log("Không thấy file img_temp.jpg!");
							return false;
						}
						createPost = CreatePostAPI(imgPath, postMessage);
					}

                    if (!createPost)
                    {
						this.log("Not CREATE Post!");
						return false;
					}
				}

				String[] postIdData = postID.Split('_');
				if (postIdData.Length < 2)
				{
					this.log("Not Post ID!");
					return false;
				}

				string pageUrl = "https://www.facebook.com/ad_center/create/boostpost/?entry_point=new_timeline&page_id=" + postIdData[0] + "&target_id=" + postIdData[1];
				string url = pageUrl;
				this._driver.Navigate().GoToUrl(pageUrl);
				WaitLoading();
				Delay(500);

				string getData = _driver.PageSource;
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				this.log("postID: " + postID);

				// Todo - lay thong tin camp
				postMessage = Uri.EscapeDataString(postMessage);

				string currency = bpCampParam.currency;
                if (string.IsNullOrEmpty(currency))
                {
					this.log("Not camp temp currency!");
					return false;
				}

				string genders = bpCampParam.genders.ToString();
				string age_max = bpCampParam.age_max.ToString();
				string age_min = bpCampParam.age_min.ToString();
				string countries = "null";
				string countriesInput = bpCampParam.countries;
				if (!string.IsNullOrEmpty(countriesInput))
				{
					countriesInput = countriesInput.Replace(",", "%5C%22%2C%5C%22");
					countries = "%5B%5C%22" + countriesInput + "%5C%22%5D";
				}

				string interestsStr = "";
				if (!string.IsNullOrEmpty(bpCampParam.interests))
				{
					interestsStr = bpCampParam.interests;
				}
				string[] interestsData = interestsStr.Split('|');
				string interests_name = "";
				string interests_id = "";
				if (interestsData.Length > 1)
				{
					interests_name = interestsData[1];
					interests_id = interestsData[0];
				}

				string link = bpCampParam.link;
				link = Uri.EscapeDataString(link);

				string pixel = pixel_id;
                if (string.IsNullOrEmpty(pixel_id) && !string.IsNullOrEmpty(bpCampParam.pixel_id))
                {
					pixel = bpCampParam.pixel_id;

				}
				
				string conversion_domain = bpCampParam.conversion_domain;
				string conversion_domain_str = "";
				if (!string.IsNullOrEmpty(conversion_domain))
                {
					conversion_domain_str = "%22conversion_domain%22%3A%22" + conversion_domain + "%22%2C";
				}

				string lifetime_budget = bpCampParam.lifetime_budget.ToString();
				int campaign_length = 1;
				if (bpCampParam.campaign_length > campaign_length)
				{
					campaign_length = bpCampParam.campaign_length;
				}

				string estimates_lower = bpCampParam.estimates_lower.ToString();
				string estimates_upper = bpCampParam.estimates_upper.ToString();

				string adAccount = _ads_id;
                if (type == "TKBM")
                {
					adAccount = _bm_ads_id;
				}

                if (string.IsNullOrEmpty(adAccount))
                {
					this.log("Không chọn được tài khoản!");
					return false;
				}

				string target = "";
				if (!string.IsNullOrEmpty(interests_id) && !string.IsNullOrEmpty(interests_name))
				{
					target = target + "%2C%5C%22flexible_spec%5C%22%3A%5B%7B%5C%22interests%5C%22%3A%5B%7B%5C%22id%5C%22%3A%5C%22" + interests_id + "%5C%22%2C%5C%22name%5C%22%3A%5C%22" + interests_name + "%5C%22%7D%5D%7D%5D";
				}

				// BP
				string publishurl = "https://www.facebook.com/api/graphql/";
				string publishbody = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__usid=6-Trnjl3c10svgdt%3APrnjl3bwe8oy5%3A0-Arnjl3c1r1divs-RV%3D6%3AF%3D&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmxa13yoS1syUbFuC2-m1FwAxu13wIwk8KewSAxa68couw-wYwJwCw8x1q1txS6Ehw2nVE4W1Zg7m0A8swIwuo9oeUa8465udz81sbzo5-0Bo4O2-2l0Fwqo5W1ywnEfogwNxaU5afK0EUjwGzE8E7C222SUbEaUiwu-1sDwa6m58HK1wwa62i4UpwSyES0gq0Lo9o9o8UdUcobU2cw&__csr=gLOZTq8yHtpkzjdkzOuCWiiaCmytGHkyh4mKWmhviHayFsyqJ7XvlLHFi9rBhFExbqiCrLh9aG-Wl8yZ9uUkgyVqyvBzl-u9AxKtGiVam75yVpEb8G8By88EhCCDyEgoW5EkgG22UaVbBx91q2W8GfngF4y99Q4oO7F99bopxh5yu4ogyoSi4F8kmuVp8Xxet2edx66UV12E0jxjBw4Fg1eO1m19iaFR22yEo0C024U0skw1KOVE0kLw5XPwEhEOagbpqokTejAFpS0BoaRenzOet2sM9E7i5U2eO3oynGog2qSml9x1oCUEN8nwgbrBw090y03ki0gC0dAwM80TE0bdU0wi07MU4K3G5pk0SE0Hp6g6qq0q6qq9w4pwNxibUnjCBwmU1NF5Cw7B1a4hepgA40iw8pw1gepwBBwtE08tk08dg2kw2G80zO&__req=52&__hs=19353.HYP%3Abusiness_fb_pkg.2.1.0.0.0&dpr=1&__ccg=EXCELLENT&__rev=1006771362&__s=8h10qe%3Ang07qm%3Ax1k2i0&__hsi=7181751049592495008&__comet_req=12&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25312&lsd=",
					lsd,
					"&__aaid=&__spin_r=1006771362&__spin_b=trunk&__spin_t=1672131718&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=LWICometCreateBoostedComponentMutation&variables=%7B%22input%22%3A%7B%22boost_id%22%3Anull%2C%22creation_spec%22%3A%7B%22ads_lwi_goal%22%3A%22GET_PURCHASES%22%2C%22audience_option%22%3A%22NCPP%22%2C%22auto_boost_settings_id%22%3Anull%2C%22auto_targeting_sources%22%3Anull%2C%22billing_event%22%3A%22IMPRESSIONS%22%2C%22budget%22%3A",
					lifetime_budget,
					"%2C%22budget_type%22%3A%22LIFETIME_BUDGET%22%2C%22currency%22%3A%22",
					currency,
					"%22%2C%22duration_in_days%22%3A",
					campaign_length.ToString(),
					"%2C%22is_automatic_goal%22%3Afalse%2C%22legacy_ad_account_id%22%3A%22",
					adAccount,
					"%22%2C%22legacy_entry_point%22%3A%22new_timeline%22%2C%22logging_spec%22%3A%7B%22reach_estimates%22%3A%7B%22lower_estimates%22%3A",
					estimates_lower,
					"%2C%22upper_estimates%22%3A",
					estimates_upper,
					"%7D%2C%22spec_history%22%3A%5B%7B%22budget%22%3A",
					lifetime_budget,
					"%2C%22currency%22%3A%22",
					currency,
					"%22%7D%5D%7D%2C%22messenger_welcome_message%22%3Anull%2C%22pixel_event_type%22%3Anull%2C%22pixel_id%22%3A%22",
					pixel,
					"%22%2C%22placement_spec%22%3A%7B%22publisher_platforms%22%3A%5B%22FACEBOOK%22%5D%7D%2C%22regulated_categories%22%3A%5B%5D%2C%22regulated_category%22%3A%22NONE%22%2C%22retargeting_enabled%22%3Afalse%2C%22run_continuously%22%3Afalse%2C%22saved_audience_id%22%3Anull%2C%22special_ad_category_countries%22%3A%5B%5D%2C%22start_time%22%3Anull%2C%22surface%22%3Anull%2C%22targeting_spec_string%22%3A%22%7B%5C%22age_min%5C%22%3A",
					age_min,
					"%2C%5C%22age_max%5C%22%3A",
					age_max,
					"%2C%5C%22geo_locations%5C%22%3A%7B%5C%22countries%5C%22%3A",
					countries,
					"%2C%5C%22location_types%5C%22%3A%5B%5C%22home%5C%22%5D%7D%2C%5C%22genders%5C%22%3A%5B",
					genders,
					"%5D",
					target,
					"%7D%22%2C%22adgroup_specs%22%3A%5B%7B",
					conversion_domain_str,
					"%22creative%22%3A%7B%22body%22%3A%22",
					postMessage,
					"%22%2C%22call_to_action%22%3A%7B%22type%22%3A%22SHOP_NOW%22%2C%22value%22%3A%7B%22link%22%3A%22",
					link,
					"%22%7D%7D%2C%22degrees_of_freedom_spec%22%3A%7B%22degrees_of_freedom_type%22%3A%22USER_ENROLLED_LWI_ACO%22%7D%2C%22instagram_branded_content%22%3A%7B%7D%2C%22object_story_id%22%3A%22",
					postID,
					"%22%2C%22use_page_actor_override%22%3Anull%7D%7D%5D%2C%22cta_data%22%3A%7B%22is_cta_share_post%22%3Afalse%2C%22link%22%3A%22",
					link,
					"%22%2C%22type%22%3A%22SHOP_NOW%22%7D%2C%22objective%22%3A%22WEBSITE_CONVERSIONS%22%7D%2C%22external_dependent_ent_id%22%3Anull%2C%22flow_id%22%3A%22966b48a4-bb48-470b-98fb-",
					lsd.Substring(0, 12),
					"%22%2C%22manual_review_requested%22%3Afalse%2C%22page_id%22%3A%22",
					postIdData[0],
					"%22%2C%22product%22%3A%22BOOSTED_POST%22%2C%22target_id%22%3A%22",
					postIdData[1],
					"%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=4887866407975731&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%22%5D"
				});

				// this.log(publishbody); 
				string publishData = this.fetchApi(publishurl, "POST", publishbody, "LWICometCreateBoostedComponentMutation", lsd, pageUrl);
				string boosting_status = Regex.Match(publishData, "\"boosting_status\":\"(.*?)\"").Groups[1].Value;
				//success
				if (string.IsNullOrEmpty(boosting_status) || ("CREATING" != boosting_status))
				{
					this.log(publishbody);
					this.log(publishData);
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CheckCreatePage(string name)
		{
			try
			{
				if (!this.checkCloneToken())
				{
					return false;
				}

				string url = "https://graph.facebook.com/v11.0/" + _clone_uid + "/facebook_pages?access_token=" + _cloneAccessToken + "&limit=10&include_headers=false&pretty=0";
				string data = this.GetData(null, url, _cookie, this._userAgent);
                if (string.IsNullOrEmpty(data))
                {
					return false;
				}

				PageResBody pPage = JsonConvert.DeserializeObject<PageResBody>(data);
                if ((pPage != null) && (pPage.data != null))
                {
					foreach (page pitem in pPage.data)
					{
                        if (pitem.name == name)
                        {
							pageID = pitem.id;
						}
					}
				}

				if (string.IsNullOrEmpty(pageID))
				{
					this.log(data);
					return false;
				}
				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool CheckBmOwnedPage()
		{
			try
			{
				if (string.IsNullOrEmpty(_accessToken))
				{
					switchBusinessTabHandle();

					this._driver.Navigate().GoToUrl("https://business.facebook.com/settings/pages");
					WaitLoading();
					Delay(500);

					string getdata = this._driver.PageSource;
					_accessToken = Regex.Match(getdata, "\"accessToken\":\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(_accessToken))
					{
						_accessToken = Regex.Match(getdata, "\"accessToken\":\"(.*?)\"").NextMatch().Groups[1].Value;
					}
				}

				if (string.IsNullOrEmpty(_accessToken))
				{
					this.log("Not _accessToken!");
					return false;
				}

				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v10.0/",
					_bm_id,
					"/owned_pages?access_token=",
					_accessToken,
					"&_reqName=object%3Abusiness%2Fowned_pages&_reqSrc=BusinessConnectedOwnedPagesStore.brands&date_format=U&fields=%5B%22id%22%2C%22name%22%2C%22global_brand_page_name%22%2C%22location%22%2C%22category%22%2C%22link%22%2C%22locations.fields(id).limit(1)%22%2C%22parent_page%22%2C%22has_transitioned_to_new_page_experience%22%2C%22has_updated_profile_plus_perm_management%22%2C%22is_verified%22%2C%22is_published%22%2C%22page_created_time%22%2C%22picture.type(square)%22%2C%22username%22%2C%22userpermissions%22%2C%22global_brand_root_id%22%2C%22business%22%5D&limit=25&locale=en_US&method=get&pretty=0&sort=name_ascending&suppress_http_code=1&xref=fa86321a9bbae8"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				pageID = Regex.Match(data, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(pageID))
				{
					this.log(data);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CheckHasCamp(string adsId)
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v12.0/act_",
					adsId,
					"/ads?access_token=",
					_cloneAccessToken,
					"&locale=en_US&method=get&pretty=0&suppress_http_code=1"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				string ads = Regex.Match(data, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(ads))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private string getBMLink(string source = "mail.tm", int time = 6)
		{
			try
			{
				if (source == "emailfake.com")
				{
					return "";
				}

				if ((source == "mail.tm") || (source == "mail.gw"))
				{
					return getMailTMBmLinkAsync(time).Result;
				}

				if (source == "10minutemail.net")
				{
					return get10MinuteMailBmLinkAsync(time);
				}
				return "";
			}
			catch (Exception ex)
			{
				this.log(ex);
				switchBusinessTabHandle();
				return null;
			}
		}

		private string get10MinuteMailBmLinkAsync(int time = 6)
		{
			try
			{
				string link = "";
				int ChkLoadingIndex = 1;
				while (string.IsNullOrEmpty(link) && (ChkLoadingIndex <= time))
				{
					Delay(5000);
					MailContent[] messages = tenMinuteMail.GetEmails().Result;
					foreach (MailContent item in messages)
					{
						if (item.From.Contains("Facebook"))
						{
							string htmls = item.Html.First();
							htmls = HttpUtility.HtmlDecode(htmls);
							Match rgMatch = Regex.Match(htmls, @"https:\/\/fb\.me\/([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
							if (rgMatch != null && rgMatch.Groups.Count > 1)
							{
								link = @"https://fb.me/" + rgMatch.Groups[1].Value;
								break;
							}
						}
					}
					ChkLoadingIndex++;
				}

				return link;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		private async Task<string> getMailTMBmLinkAsync(int time = 6)
		{
			try
			{
				MessageInfo[] messages = await mailClient.GetAllMessages();
				string link = "";
				if (messages.Length > 0)
				{
					foreach (MessageInfo item in messages)
					{
						if (item.From.Name == "Facebook")
						{
							MessageSource source = await mailClient.GetMessageSource(item.Id);
							string rawMessage = source.Data;
							Match rgMatch = Regex.Match(rawMessage, @"https:\/\/fb\.me\/([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
							if (rgMatch != null && rgMatch.Groups.Count > 1)
							{
								link = @"https://fb.me/" + rgMatch.Groups[1].Value;
								break;
							}
						}
					}
				}

				int ChkLoadingIndex = 1;
				while (string.IsNullOrEmpty(link) && (ChkLoadingIndex <= time))
				{
					Delay(5000);
					messages = await mailClient.GetAllMessages();
					foreach (MessageInfo item in messages)
					{
						if (item.From.Name == "Facebook")
						{
							MessageSource source = await mailClient.GetMessageSource(item.Id);
							string rawMessage = source.Data;
							Match rgMatch = Regex.Match(rawMessage, @"https:\/\/fb\.me\/([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
                            if (rgMatch != null && rgMatch.Groups.Count > 1)
                            {
								link = @"https://fb.me/" + rgMatch.Groups[1].Value;
								break;
							}
						}
					}
					ChkLoadingIndex++;
				}

				return link;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		public string genBmReceiveLink(string source = "mail.tm", string gmail = "", string gpass = "", bool pageRole = false, bool adsRole = false, int time = 6, bool longlink = false)
		{
			try
			{
				switchBusinessTabHandle();

				if (string.IsNullOrEmpty(_bm_id))
				{
					string gurl = "https://business.facebook.com/settings/people";
					if (!this._driver.Url.Contains("business.facebook.com"))
					{
						this._driver.Navigate().GoToUrl(gurl);
						WaitLoading();
						Delay(500);
					}

					Uri theUri = new Uri(this._driver.Url);
					_bm_id = HttpUtility.ParseQueryString(theUri.Query).Get("business_id");
					this.log("BM: " + _bm_id);
					this.getBmInfo();
				}

				// Change bm name
				int Timestamp = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
				string brand_name = "Nini" + Timestamp.ToString();
				this.log("CHANGE BM NAME!");
				this.log("BM NAME: " + brand_name);

				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v12.0/",
					_bm_id,
					"?access_token=",
					_accessToken
				});

				string param = string.Concat(new string[]
				{
					"_reqName=path%3A%2F",
					_bm_id,
					"&_reqSrc=adsDaoGraphDataMutator&endpoint=%2F",
					_bm_id,
					"&locale=en_US&method=post&name=",
					brand_name,
					"&pretty=0&suppress_http_code=1&version=12.0&xref=f2e631bb0127c1c"
				});

				string postData = this.PostData(null, url, param, this._contentType, this._userAgent, _cookie);
				string item_id = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(item_id))
				{
					this.log("Not change BM name!");
					return "";
				}

				// Get mail
				string email = gmail;
                if (source != "gmail.com")
                {
					email = getEmail(source);
				}

                if (string.IsNullOrEmpty(email))
                {
					this.log("Not Email!");
					return "";
                }

				this.log(source + ": " + email);

				email = Uri.EscapeDataString(email);

				url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v3.0/",
					_bm_id,
					"/business_users?access_token=",
					_accessToken
				});

				param = string.Concat(new string[]
				{
					"brandId=",
					_bm_id,
					"&email=",
					email,
					"&method=post&pretty=0&roles=%5B%22ADMIN%22%5D&suppress_http_code=1"
				});

				postData = this.PostData(null, url, param, this._contentType, this._userAgent, _cookie);
				string user_id = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(user_id))
				{
					this.log("Not gen Link!");
					this.log(url);
					this.log(postData);
					return "";
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				url = "https://business.facebook.com/business/business_objects/update/permissions/";
				string referer = "https://business.facebook.com/settings/people?business_id=" + _bm_id;
				string body = "";
				string checkStatus = "";

				// set page role
				if (!string.IsNullOrEmpty(pageID) && pageRole)
                {
					this.log("set page role!");
					body = string.Concat(new string[]{
						"asset_ids[0]=",
						pageID,
						"&asset_type=page&business_id=",
						_bm_id,
						"&roles[0]=148446802486573&roles[1]=488311164896151&roles[2]=320213425882702&roles[3]=2012064802375865&roles[4]=1664540536918255&roles[5]=148759615915141",
						"&user_ids[0]=",
						user_id,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7xeUmBz8aolJ28S2q3mbwyyVuCEnxG2q12wAxiFEdpE6C4UKegcGAAzpoixWE-16xq2WdwJz-4fwAwgUgwqoqyoyazoO4o461twOxa7FEhwywCxq2u3K6UG1lKFpod8S3bg-3tpUdoK7UC4F87y78jxiUa8522m3K2y3WElUScyo720FoO12Kmu7EK3i2a3Fe6rxS11DwFg942B12ewi8doa84K5E5WUrorx2awCx5e8wxK2efxWiewjovCxeq4o2ZwQzUS2W2K4E6-bxu3ydCg-aw-z8c8-5aDBxSaBwKG3qcy85i4oKqbDyo-2-qaUK2e0UE7e&__csr=&__req=y&__hs=18998.BP%3Abrands_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004913447&__s=62mwxy%3Aezv9z2%3Ad0ur8r&__hsi=7049974944570054996-0&__comet_req=0&fb_dtsg=",
						fb_dtsg,
						"&jazoest=21902&lsd=",
						lsd,
						"&__spin_r=1004913447&__spin_b=trunk&__spin_t=1641450204&__jssesw=1"
					});

					postData = this.fetchApi(url, "POST", body, "", lsd, referer);
					checkStatus = Regex.Match(postData, "\"successes\":{\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(checkStatus))
					{
						this.log(postData);
						this.log("Not set page role!");
						return "";
					}
				}

				// set ads role
				if (!string.IsNullOrEmpty(_bm_ads_id) && adsRole)
                {
					this.log("set ads role!");
					body = string.Concat(new string[] {
						"asset_ids[0]=",
						_bm_ads_id,
						"&asset_type=ad-account&business_id=",
						_bm_id,
						"&roles[0]=151821535410699&roles[1]=610690166001223&roles[2]=864195700451909&roles[3]=186595505260379&",
						"&user_ids[0]=",
						user_id,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7xeUmBz8aolJ28S2q3mbwyyVuC3m2q12wAxu3Oq1FxebzA3aF98Sm4Euxa16xq2WdwJx64fwAwgUgwqoqxaazoO4o461twOxa7FEhwiomwDwXxK1wKEK3idwOxa3t2odoK7UC0A8sxe5bwuo898eUvy8fGxnzomws82Bz84aVpUaEd88EeAUtxS1PwBgak48W18wRwEwoE5WUeogwNxq8wio-7F8W1JCxeq4o2ZwQxS2W2K4E5y5EK5Ue8Sp3UG3WcwMzUkGdwEBwKG1cwl8hyVEjyo-2-qaUK0gq&__csr=&__req=s&__hs=18909.BP%3Abrands_pkg.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1004528366&__s=a6v1gc%3Ahs9qum%3Aek6adt&__hsi=7016917136682271993-0&__comet_req=0&fb_dtsg=",
						fb_dtsg,
						"&jazoest=22058&lsd=",
						lsd,
						"&__spin_r=1004528366&__spin_b=trunk&__spin_t=1633753333&__jssesw=1"
					});

					postData = this.fetchApi(url, "POST", body, "", lsd, referer);
					checkStatus = Regex.Match(postData, "\"successes\":{\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(checkStatus))
					{
						this.log(postData);
						this.log("Not set ads role!");
						return "";
					}
				}

				// Get link
				string bmLink = "";
				if (source == "gmail.com")
				{
					bmLink = getGMailTMBmLink(gmail, gpass, brand_name);
					int ChkIndex = 1;
					while (string.IsNullOrEmpty(bmLink) && (ChkIndex <= time))
					{
						Delay(5000);
						bmLink = getGMailTMBmLink(gmail, gpass, brand_name);
						ChkIndex++;
					}
				}
                else
                {
					bmLink = getBMLink(source, time);
				}
				this.log("BM Link: " + bmLink);

                if (longlink)
                {
					this._driver.Navigate().GoToUrl(bmLink);
					WaitLoading();
					Delay(1000);
					if (!this._driver.Url.Contains("business.facebook.com"))
					{
						WaitLoading();
						Delay(1000);
						if (!this._driver.Url.Contains("business.facebook.com"))
						{
							WaitLoading();
							Delay(1000);
							if (!this._driver.Url.Contains("business.facebook.com"))
							{
								WaitLoading();
								Delay(1000);
							}
						}
					}

					if (!this._driver.Url.Contains("business.facebook.com"))
					{
						this.log("Not create LongLink!");
                    }
                    else
                    {
						bmLink = this._driver.Url;
					}
				}

				return bmLink;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return "";
			}
		}

		public string getGMailTMBmLink(string gmail, string gpass, string bm)
		{
			string confirmUrl = "";
			try
			{
				using (var client = new ImapClient())
				{
					using (var cancel = new CancellationTokenSource())
					{
						client.Connect("imap.gmail.com", 993, true, cancel.Token);

						// If you want to disable an authentication mechanism,
						// you can do so by removing the mechanism like this:
						client.AuthenticationMechanisms.Remove("XOAUTH");

						client.Authenticate(gmail, gpass, cancel.Token);

						// The Inbox folder is always available...
						var inbox = client.Inbox;
						inbox.Open(FolderAccess.ReadOnly, cancel.Token);

						DateTime date = DateTime.Today.AddMinutes(-15);

						// let's try searching for some messages...
						var query = SearchQuery.DeliveredAfter(date)
							.And(SearchQuery.FromContains("notification@facebookmail.com"))
							.And(SearchQuery.BodyContains(bm));

						foreach (var uid in inbox.Search(query, cancel.Token))
						{
							var message = inbox.GetMessage(uid, cancel.Token);
							Match rgMatch = Regex.Match(message.HtmlBody, @"https:\/\/fb\.me\/([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
							if (rgMatch != null && rgMatch.Groups.Count > 1)
							{
								confirmUrl = @"https://fb.me/" + rgMatch.Groups[1].Value;
								break;
							}
						}

						client.Disconnect(true, cancel.Token);
					}
				}

				return confirmUrl;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return "";
			}
		}

		public bool setBm1Var()
		{
			try
			{
				_bm1_id = _bm_id;
				// _bm1_page_id = pageID;
				_bm1_ads_id = _bm_ads_id;
				_bm1_credential_id = _credential_id;
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool switchBm1()
		{
			try
			{
				switchBusinessTabHandle();

				_bm_id = _bm1_id;
				_bm_ads_id = _bm1_ads_id;
                if (!string.IsNullOrEmpty(_bm1_page_id))
                {
					pageID = _bm1_page_id;
				}
				_credential_id = _bm1_credential_id;
				string url = "https://business.facebook.com/settings/ad-accounts/?business_id=" + _bm_id;
				_driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);
				getBmInfo();
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool setBm2Var()
		{
			try
			{
				_bm2_id = _bm_id;
				// _bm2_page_id = pageID;
				_bm2_ads_id = _bm_ads_id;
				_bm2_credential_id = _credential_id;
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool switchBm2()
		{
			try
			{
				switchBusinessTabHandle();

				_bm_id = _bm2_id;
				_bm_ads_id = _bm2_ads_id;
				if (!string.IsNullOrEmpty(_bm2_page_id))
				{
					pageID = _bm2_page_id;
				}
				_credential_id = _bm2_credential_id;
				string url = "https://business.facebook.com/settings/ad-accounts/?business_id=" + _bm_id;
				_driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);
				getBmInfo();
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool setRemoveBm()
		{
			try
			{
				if (string.IsNullOrEmpty(_bm_id))
				{
					return false;
				}

				_sel_bm_id = _bm_id;

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool removeBm()
		{
			try
			{
				string rmBmId = "";
                if (!string.IsNullOrEmpty(_sel_bm_id))
                {
					rmBmId = _sel_bm_id;
                }
                else
                {
					rmBmId = _bm_id;
				}

				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v12.0/",
					_clone_uid,
					"/businesses?access_token=",
					_accessToken
				});

				string param = string.Concat(new string[]
				{
					"_reqName=path%3A%2F",
					_clone_uid,
					"%2Fbusinesses&_reqSrc=adsDaoGraphDataMutator&business=",
					rmBmId,
					"&endpoint=%2F",
					_clone_uid,
					"%2Fbusinesses&locale=en_US&method=delete&pretty=0&suppress_http_code=1&userID=",
					_clone_uid,
					"&version=12.0&xref=feb03f479feec"
				});

				string postData = this.PostData(null, url, param, this._contentType, this._userAgent, _cookie);
				string result = Regex.Match(postData, "\"success\":(.*?)}").Groups[1].Value;
				if (string.IsNullOrEmpty(result))
				{
					this.log(postData);
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private bool checkPro5Page(string pageid)
        {
			try
			{
				string url = "https://www.facebook.com/" + pageid;
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);
				if (!this._driver.Url.Contains("profile.php?id="))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CreateBmPage(string pageNamePr, bool pro5 = false)
		{
			try
			{
				switchBusinessTabHandle();
				string gurl = "https://business.facebook.com/settings/pages?business_id=" + _bm_id;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(gurl);
					WaitLoading();
					Delay(500);
				}

				if (string.IsNullOrEmpty(_bm_id))
				{
					Uri theUri = new Uri(this._driver.Url);
					_bm_id = HttpUtility.ParseQueryString(theUri.Query).Get("business_id");
					this.log("BM: " + _bm_id);
					this.getBmInfo();
				}

				string data = this._driver.PageSource;
				string value = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string value2 = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				_accessToken = Regex.Match(data, "\"accessToken\":\"(.*?)\"").Groups[1].Value;
                if (string.IsNullOrEmpty(_accessToken))
                {
					_accessToken = Regex.Match(data, "\"accessToken\":\"(.*?)\"").NextMatch().Groups[1].Value;
				}

				// _rNamePage = pageNamePr;
				_rNamePage = pageNamePr + " " + new Random().Next(100, 999).ToString();

				string url = "https://business.facebook.com/ajax/ads/create/page/create";
				string body = string.Concat(new string[] {
					"jazoest=22044&fb_dtsg=",
					value,
					"&page_name=",
					_rNamePage,
					"&category=2612&parent_category=2612&has_no_profile_pic=1&business_id=",
					_bm_id,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8aolJ28S2q3mbwyyVuC6EqwCwgE98nwYCwqojyUV0OGiidBxa7EiwhEmwKzobohx3U984e486C6EiyEScx611wnocEixWq4o4C5E9UeUrwobGbwQzocEiwTgC3mbx-9w9278jxiU7C22i3K7Uy3WElUS5E720FoO12Kmu2G3i2a3Fe7otwsU9k2B12ewi8doa86a1uK3C48comy84CfxWiewrpEjCx60Lod8twKwHxa1oxqbxu3ydCg-aw-z8c8-5azoa9obGwj85i4oKq4UCfwLCyKbw46w&__csr=&__req=q&__hs=18910.BP%3Abrands_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004529660&__s=fh5vc4%3A2fym3b%3Atd463m&__hsi=7017487579733694398-0&__comet_req=0&lsd=",
					value2,
					"&__spin_r=1004529660&__spin_b=trunk&__spin_t=1633886150&__jssesw=1"
				});

				string postData = this.fetchApi(url, "POST", body, "", value2, gurl);
				pageID = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(pageID))
				{
					CheckCreatePage(_rNamePage);
				}

				if (string.IsNullOrEmpty(pageID))
				{
					log("NOT PAGE ID!");
					return false;
				}

				log("pageID: " + pageID);

				url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v9.0/",
					_bm_id,
					"/owned_pages?access_token=",
					_accessToken
				});

				string data2 = string.Concat(new string[]
				{
					"_index=22&_reqName=object%3Abusiness%2Fowned_pages&_reqSrc=PageActions.brands&access_type=OWNER&code=&ig_password=&locale=en_US&method=post&page_id=",
					pageID,
					"&pretty=0&suppress_http_code=1&xref=f2d6af3b0c3bc8c"
				});

				postData = this.fetchApi(url, "POST", data2, "", "", gurl);
				string access_status = Regex.Match(postData, "\"access_status\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(access_status))
				{
					this.log("Set Role false!");
					return false;
				}
				this.log("Create Page Ok!");

				// Primary page
				url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v9.0/",
					_bm_id,
					"?access_token=",
					_accessToken
				});
				data2 = string.Concat(new string[]
				{
					"_index=30&_reqName=path%3A%2F",
					_bm_id,
					"&_reqSrc=adsDaoGraphDataMutator&endpoint=%2F",
					_bm_id,
					"&locale=en_US&method=post&pretty=0&primary_page=",
					pageID,
					"&suppress_http_code=1&version=9.0&xref=f124f0581047b8"
				});
				postData = this.fetchApi(url, "POST", data2, "", "", gurl);

				// Alow cookie
				url = "https://business.facebook.com/cookie/consent/";
				gurl = "https://business.facebook.com/settings/pages/" + pageID + "?business_id=" + _bm_id;
				body = string.Concat(new string[] {
					"accept_only_essential=false&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8aolJ28S2q3mbwyyVuCEb9o9E4a2i5aCwRCwqojyUV0OGiidBxa7GzU4q5EbES2SfUg-2i13x21FxG9y8Gdz8hwgo5S3a4EuCx62a2q5E9UeUryE5mWBBwQzocJ3UdRDwRyUvyoiAwu8sxe5bwEwk89oeUa8fGxnzoO9ws82Bz84aVpUuyUd88EeAUpK7o47BwFg942B12ewi8doa84K5E5WUrorx2364kUy26U8U-7F8W1dx-q4VEhwww9O3ifzobEaUiwrUK5Ue8Sp3UG3WcwMzUkGum2ym2WE4e8wl8hyVEKu9zUbVEHyU8U3ywbCU4K&__csr=&__req=j&__hs=19063.BP%3Abrands_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005189276&__s=%3Avko480%3A54f1fa&__hsi=7074236426230333366-0&__comet_req=0&fb_dtsg=",
					value,
					"&jazoest=22007&lsd=",
					value2,
					"&__spin_r=1005189276&__spin_b=trunk&__spin_t=1647099019&__jssesw=1"
				});

				postData = this.fetchApi(url, "POST", body, "", value2, gurl);
				string lid = Regex.Match(postData, "\"lid\":\"(.*?)\"").Groups[1].Value;
                if (string.IsNullOrEmpty(lid))
                {
					this.log("Cookie consent NOT OK!");
				}

                if (pro5)
                {
                    if (!checkPro5Page(pageID))
                    {
						this.log("Không phải PRO5 PAGE!");
						return false;
                    }
                    else
                    {
						this.log("PRO5 PAGE!");
					}
                }

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool BmAddOwnedPage()
		{
			try
			{
				switchBusinessTabHandle();

				string gurl = "https://business.facebook.com/settings/pages?business_id=" + _bm_id;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(gurl);
					WaitLoading();
					Delay(500);
				}

				if (string.IsNullOrEmpty(_bm_id))
				{
					Uri theUri = new Uri(this._driver.Url);
					_bm_id = HttpUtility.ParseQueryString(theUri.Query).Get("business_id");
					this.log("BM: " + _bm_id);
					this.getBmInfo();
				}

				string data = this._driver.PageSource;
				string value = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string value2 = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				_accessToken = Regex.Match(data, "\"accessToken\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(_accessToken))
				{
					_accessToken = Regex.Match(data, "\"accessToken\":\"(.*?)\"").NextMatch().Groups[1].Value;
				}

				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v9.0/",
					_bm_id,
					"/owned_pages?access_token=",
					_accessToken
				});

				string data2 = string.Concat(new string[]
				{
					"_index=22&_reqName=object%3Abusiness%2Fowned_pages&_reqSrc=PageActions.brands&access_type=OWNER&code=&ig_password=&locale=en_US&method=post&page_id=",
					pageID,
					"&pretty=0&suppress_http_code=1&xref=f2d6af3b0c3bc8c"
				});

				string postData = this.fetchApi(url, "POST", data2, "", "", gurl);
				string access_status = Regex.Match(postData, "\"access_status\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(access_status))
				{
					this.log(postData);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool changeLang()
		{
			try
			{
				switchRegTabHandle();

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string url = "https://www.facebook.com/api/graphql/";
				string refAv = "https://www.facebook.com/settings?tab=language&section=account&view";
				string body = string.Concat(new string[] {
						"av=",
						_clone_uid,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7AzHK4HwkEng5KbxG4VuC0BVU98nwgU29gS3q2ibwNw9G2S7o762S1DwUx609vCxS320om782Cwwwqo465o-cw5MKdwGwQw9m8wsU9kbxS2218wc61axe3S68f85qfK6E7e58jwGzEaE5e7oqBwJK2W5olwUwgojUlDw-wUws9ovUaU3VBwJCwLyESE2KwkQ0z8c84K2e&__csr=gHdaDeRGz_4JsjEpamAN2jvqTakBaRWZFGjRKBtJrhqFqCZrSQXqKumKG9BCgBkiaKByagzyrSUkml5QrDBz-uU-2u8G78rG5ECAUgyUSry98C488p8kUkxecAxeewABAxa9GchpE4W4p8941vwk856cAwMwxwIx67UGeCxu588A6985W13wSwRxt4wlEixO1VwwwvVo62363a4Xw8m0SEiBwNw_wPwqo3vwp-267UaUux602fa3-00MIoqxWaXx2cw0Elw1uG04Robdqo10o0h6w58XGWGEy19wgGV81Mo2Jw3yo0dpU&__req=m&__hs=19098.HYP%3Acomet_pkg.2.1.0.2.&dpr=1&__ccg=EXCELLENT&__rev=1005366137&__s=zvw1gy%3Ajr75cz%3Ag35z3l&__hsi=7087170822661874530-0&__comet_req=1&fb_dtsg=",
						fb_dtsg,
						"&jazoest=22044&lsd=",
						lsd,
						"&__spin_r=1005366137&__spin_b=trunk&__spin_t=1650110544&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useCometLocaleSelectorLanguageChangeMutation&variables=%7B%22locale%22%3A%22en_US%22%2C%22referrer%22%3A%22WWW_ACCOUNT_SETTINGS%22%2C%22fallback_locale%22%3Anull%7D&server_timestamps=true&doc_id=4698867456804533&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%22%5D"
					});

				string postData = this.fetchApi(url, "POST", body, "useCometLocaleSelectorLanguageChangeMutation", lsd, refAv);
				string status = Regex.Match(postData, "\"success\":(.*?)}").Groups[1].Value;
				if ("true" != status)
				{
					this.log(postData);
					return false;
				}

				SetCookieData();
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private bool switchProfile(string from, string to)
		{
			try
			{
				this.log("SwitchProfile: " + from + "-->" + to);
				switchRegTabHandle();
				string gurl = "https://www.facebook.com/profile.php";
				this._driver.Navigate().GoToUrl(gurl);
				WaitLoading();
				Delay(500);

				Uri theUri = new Uri(this._driver.Url);
				string profile_id = HttpUtility.ParseQueryString(theUri.Query).Get("id");
                if (profile_id == to)
                {
					this.log("Profile đã switch!");
					return true;
				}

				string getData = this.fetchGet("https://www.facebook.com/settings/?tab=profile");
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

                string url = "https://www.facebook.com/api/graphql/";
                string body = string.Concat(new string[] {
					"av=",
					from,
					"&__user=",
					from,
					"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgU765QdwSwAyUco5S3O2Saxa1NwJwpUe8hw6vwb-q7oc81xoswIK1Rwwwg8a8465o-cwfG12wOKdwGxu782lwv89kbxS2218wc61axe3S1lwlE-UqwsUkxe2GewyDwkUtxGm2SUbElxm3y11xfxmu3W3y1MBx_y88E3mK7FobpEbUGdwb6223908O3216AzUjwTwNwLwFg662S&__csr=gV15d4l4NIrOOiqb4TkOnkh4EIsj9YDqaIJvilPJ99fy4iATSGbi_Attqlup5BgGtmZupAGZdahkHJrWh8OCiUSmFoDqCLUN9164VQXg-BGKHyedBF4CHBK5UGbBKqKF9Qvh44F9aCzE8QqUSazecF1aA2eegCqHykdgiy8C9VbAF126Ey5UnDxmnyFDze78c8CES9yUgz8-9wHUmxTUiDxOp7Bx-5poiDUO3C9wh8vDy43Om4EC9guKi2W5oswICwQwuokDxGbx278x2ogx22ZwjE8o3owjo0L-1Eg08Qo1cES0nC02_S01Chw0NMw1CO06ZE4901Uq0w201aK04oC0dnwce1lyqlw48w4jzEgy2wNw1mO780gUweB44905dwci2C8a1Bw5sw4Zg2xw_wKw9x0kU18o3uw5hkg0oS&__req=15&__hs=19290.HYP%3Acomet_pkg.2.1.0.2.1&dpr=1&__ccg=EXCELLENT&__rev=1006449831&__s=ugslgi%3Au348b1%3A83wivl&__hsi=7158307567010700862&__comet_req=15&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25415&lsd=",
					lsd,
					"&__aaid=",
					_ads_id,
					"&__spin_r=1006449831&__spin_b=trunk&__spin_t=1666673358&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=CometProfileSwitchMutation&variables=%7B%22profile_id%22%3A%22",
					to,
					"%22%7D&server_timestamps=true&doc_id=4855674131145168&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%22%5D"
				});

                string postData = this.fetchApi(url, "POST", body, "CometProfileSwitchMutation", lsd, gurl);
                string name = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
                if (string.IsNullOrEmpty(name))
                {
                    this.log(body);
                    this.log(postData);
					this.log("SwitchProfile: " + from + "-->" + to + ": FALSE!");
					return false;
                }
				this._driver.Navigate().GoToUrl("https://www.facebook.com/profile.php");
				WaitLoading();
				Delay(500);

				theUri = new Uri(this._driver.Url);
				profile_id = HttpUtility.ParseQueryString(theUri.Query).Get("id");
				if (profile_id != to)
				{
					this.log("SwitchProfile: " + from + "-->" + to + ": FALSE!");
					return false;
				}

				this.log("SwitchProfile: " + from + "-->" + to + ": TRUE!");
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				this.log("SwitchProfile: " + from + "-->" + to + ": FALSE!");
				return false;
			}
		}

		public bool PageSetCountry()
		{
			string profile_id = "";
			try
			{
				switchRegTabHandle();

				// Check page
				bool sw = false;
				string url = "https://www.facebook.com/" + pageID;
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);
				string iframeStr = "";

				if (this._driver.Url.Contains("profile.php?id="))
				{
					Uri theUri = new Uri(this._driver.Url);
					profile_id = HttpUtility.ParseQueryString(theUri.Query).Get("id");
					sw = switchProfile(_clone_uid, profile_id);
					if (sw)
					{
						url = "https://www.facebook.com/settings?tab=followers&section=country_restrictions&view";
						this._driver.Navigate().GoToUrl(url);
						WaitLoading();
						Delay(500);
						iframeStr = "iframe[src*='=country_restrictions']";
					}
					else
					{
						this.log("NOT Switch Profile!");
						return false;
					}
                }
                else
                {
					url = "https://www.facebook.com/" + pageID + "/settings/?tab=settings&ref=page_edit&section=country_restrictions&view";
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
					iframeStr = "iframe[src*='=country_restrictions']";
				}

				WaitAjaxLoading(By.CssSelector(iframeStr), 9);
				ReadOnlyCollection<IWebElement> iFrames = this._driver.FindElements(By.CssSelector(iframeStr));
				if (iFrames.Count == 0)
                {
					if (sw)
                    {
						switchProfile(pageID, _clone_uid);
					}
					this.log("NOT SET COUNTRY IFRAME!");
					return false;
				}

				if (iFrames.Count > 0)
				{
					string data = this._driver.PageSource;
					string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
					string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

					IWebElement iFrame = iFrames.First();
					string ifSrc = iFrame.GetAttribute("src");
					Uri theUri = new Uri(ifSrc);

					string cquick_token = HttpUtility.ParseQueryString(theUri.Query).Get("cquick_token");
					string ctarget = HttpUtility.ParseQueryString(theUri.Query).Get("ctarget");
					string cquick = HttpUtility.ParseQueryString(theUri.Query).Get("cquick");
					string refAv = ifSrc;

					string countryName = Uri.EscapeDataString("United States");
					string body = "";
					string postData = "";
					string lid = "";

					if (sw)
                    {
						url = "https://www.facebook.com/ajax/settings/subscribers/country_restrictions.php";
						body = string.Concat(new string[] {
							"cquick_token=",
							cquick_token,
							"&ctarget=",
							ctarget,
							"&cquick=",
							cquick,
							"&jazoest=25546&fb_dtsg=",
							fb_dtsg,
							"&geo_gating[0]=US&text_geo_gating[0]=",
							countryName,
							"&geo_gating_inverted=0&__user=",
							profile_id,
							"&__a=1&__dyn=7xeUmBwjbg7ebwKheC1swgE98nwgU6C7UW3q327Eiw8OdwJw5ux60Vo2Vwb-q1ew8y11wbG782Cwn-2y1Qw5MKdwnU5W0IU9kbxS2218wc61uwZwlo8od8jxG0y8jwVw9O1iwKwHw8W1uwa-7U1bobodEGdw46wbS1bwzwqo&__csr=&__req=d&__hs=19267.BP%3ADEFAULT.2.0.0.0.0&dpr=1&__ccg=EXCELLENT&__rev=1006309715&__s=uki9au%3Amuurey%3Ae5tmu6&__hsi=7149930491339817869&__comet_req=0&lsd=",
							lsd,
							"&__spin_r=1006309715&__spin_b=trunk&__spin_t=1664722918"
						});

						postData = this.fetchApi(url, "POST", body, "", lsd, refAv);
						lid = Regex.Match(postData, "\"lid\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(lid))
						{
							switchProfile(profile_id, _clone_uid);
							this.log(postData);
							return false;
						}
						switchProfile(profile_id, _clone_uid);
					}
                    else
                    {
						url = "https://www.facebook.com/ajax/pages/edit_page/settings/country_restrictions.php?id=" + pageID;
						body = string.Concat(new string[] {
							"cquick_token=",
							cquick_token,
							"&ctarget=",
							ctarget,
							"&cquick=",
							cquick,
							"&jazoest=25809&fb_dtsg=",
							fb_dtsg,
							"&geo_gating[0]=US&text_geo_gating[0]=",
							countryName,
							"&geo_gating_inverted=0&__user=",
							_clone_uid,
							"&__a=1&__dyn=7xeUmwkHg7ebwKheC1swgE98nwgU6C7UW3q327E2vzobo1nEhwem0Ko2_CwjE3awbG782Cw8G1Qw5MKdwnU5W0IU9kbxS2218w4wwZwtUd8bE2swdq1iwmE2ewnE2Lx-0lK3qazo11E2ZwrU6C&__csr=&__req=5&__hs=19267.BP%3ADEFAULT.2.0.0.0.0&dpr=1&__ccg=GOOD&__rev=1006309757&__s=m4jdbh%3A591x9p%3Aiq6pt9&__hsi=7149985286751057558&__comet_req=0&lsd=",
							lsd,
							"&__spin_r=1006309757&__spin_b=trunk&__spin_t=1664735676"
						});

						postData = this.fetchApi(url, "POST", body, "", lsd, refAv);
						lid = Regex.Match(postData, "\"lid\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(lid))
						{
							this.log(postData);
							return false;
						}
					}
                }
                else
                {
					this.log("NOT SET COUNTRY IFRAME!");
					if (sw)
					{
						switchProfile(profile_id, _clone_uid);
					}
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
                if (!string.IsNullOrEmpty(profile_id))
                {
					switchProfile(profile_id, _clone_uid);
				}

				this.log(ex);
				return false;
			}
		}

		public bool ShareBmPageToOtherBm(string bmarcId = null)
		{
			try
			{
				return ShareBmPageToBm(pageID, _bm_id, _accessToken, bmarcId);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool RecBmPageFromBm(string page_id, string bmarcId, string bmarcToken)
		{
			try
			{
				return ShareBmPageToBm(page_id, bmarcId, bmarcToken, _bm_id);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CheckPro5Page(string pageid)
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v14.0/",
					pageid,
					"?access_token=",
					_accessToken,
					"&fields=[%22id%22,%22name%22,%22additional_profile_id%22]&locale=en_US&pretty=0"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				string additional_profile_id = Regex.Match(data, "\"additional_profile_id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(additional_profile_id))
				{
					return false;
				}
				this.log("PRO5!");
				return true;
			}
			catch
			{
				return false;
			}
		}

		public bool ShareBmPageToBm(string page_id, string fromBm, string fromToken, string toBm)
		{
			try
			{
				switchBusinessTabHandle();

				string role = "&permitted_tasks=%5B%22CREATE_CONTENT%22%2C%22MODERATE%22%2C%22MESSAGING%22%2C%22ADVERTISE%22%2C%22ANALYZE%22%2C%22MANAGE%22%5D&pretty=0&suppress_http_code=1&xref=f3d0ae5b27a9e1";
                if (CheckPro5Page(page_id))
                {
					role = "&permitted_tasks=%5B%22PROFILE_PLUS_CREATE_CONTENT%22%2C%22PROFILE_PLUS_MODERATE%22%2C%22PROFILE_PLUS_MESSAGING%22%2C%22PROFILE_PLUS_ADVERTISE%22%2C%22PROFILE_PLUS_ANALYZE%22%2C%22PROFILE_PLUS_FACEBOOK_ACCESS%22%5D&pretty=0&suppress_http_code=1&xref=f3d0ae5b27a9e1";
				}

				string url = string.Concat(new string[] {
					"https://graph.facebook.com/v9.0/",
					page_id,
					"/agencies?access_token=",
					fromToken
				});

				string data2 = string.Concat(new string[] {
					"__activeScenarioIDs=%5B%5D&__activeScenarios=%5B%5D&__interactionsMetadata=%5B%5D&_reqName=object%3Apage%2Fagencies&_reqSrc=BrandAgencyActions.brands&acting_brand_id=",
					fromBm,
					"&business=",
					toBm,
					"&locale=en_US&method=post&pageId=",
					page_id,
					role
				});

				string postData = this.PostData(null, url, data2, this._contentType, this._userAgent, _cookie);
				string resultshare = Regex.Match(postData, "\"success\":(.*?)}").Groups[1].Value;
				if (string.IsNullOrEmpty(resultshare))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool ShareAdToSelf()
		{
			try
			{
				switchBusinessTabHandle();

				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v12.0/",
					_bm_id,
					"/client_ad_accounts?access_token=",
					_accessToken
				});

				string body = string.Concat(new string[]
				{
					"_reqName=object%3Abrand%2Fclient_ad_accounts&_reqSrc=AdAccountActions.brands&access_type=AGENCY&adaccount_id=act_",
					_ads_id,
					"&locale=en_US&method=post&permitted_roles=%5B%5D&permitted_tasks=%5B%22ADVERTISE%22%2C%22ANALYZE%22%2C%22DRAFT%22%2C%22MANAGE%22%5D&pretty=0&suppress_http_code=1&xref=f5b0abfc5a7fa4"
				});

				string postData = this.fetchApi(url, "POST", body, "", "", "https://business.facebook.com/");
				string access_status = Regex.Match(postData, "\"access_status\":\"(.*?)\"").Groups[1].Value;

				if (string.IsNullOrEmpty(access_status))
				{
					return false;
				}

				shareAds = true;
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool BmPageDeleteUser()
		{
			try
			{
				switchBusinessTabHandle();

				string url = "https://business.facebook.com/settings/ad-accounts/?business_id=" + _bm_id;
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);
				String pageSource = this._driver.PageSource;
				string business_user_id = Regex.Match(pageSource, "\"business_user_id\":\"(.*?)\"").Groups[1].Value;

				url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v14.0/",
					pageID,
					"/userpermissions?access_token=",
					_accessToken
				});

				string body = string.Concat(new string[]
				{
					"__activeScenarioIDs=%5B%5D&__activeScenarios=%5B%5D&__interactionsMetadata=%5B%5D&_reqName=object%3Apage%2Fuserpermissions&_reqSrc=AssetPermissionActions.brands&business=",
					_bm_id,
					"&locale=en_US&method=delete&pageId=",
					pageID,
					"&pretty=0&remove_personal_connections=true&suppress_http_code=1&user=",
					business_user_id,
					"&xref=f2e40abbed1933c"
				});

				string postData = this.fetchApi(url, "POST", body, "", "", "https://business.facebook.com/");
				string status = Regex.Match(postData, "\"success\":(.*?)}").Groups[1].Value;

				if (string.IsNullOrEmpty(status))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool BmPageRemove()
		{
			try
			{
				switchBusinessTabHandle();
				string gurl = "https://business.facebook.com/settings/pages?business_id=" + _bm_id;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(gurl);
					WaitLoading();
					Delay(500);
				}

				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v14.0/",
					_bm_id,
					"/pages?access_token=",
					_accessToken
				});

				string body = string.Concat(new string[]
				{
					"__activeScenarioIDs=%5B%5D&__activeScenarios=%5B%5D&__interactionsMetadata=%5B%5D&_reqName=path%3A%2F",
					_bm_id,
					"%2Fpages&_reqSrc=adsDaoGraphDataMutator&brandID=",
					_bm_id,
					"&endpoint=%2F",
					_bm_id,
					"%2Fpages&locale=en_US&method=delete&page_id=",
					pageID,
					"&pretty=0&suppress_http_code=1&version=14.0&xref=f2700c470044748"
				});

				string postData = this.fetchApi(url, "POST", body, "", "", gurl);
				string status = Regex.Match(postData, "\"success\":(.*?)}").Groups[1].Value;

				if (string.IsNullOrEmpty(status))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool SetRoleAdOther(string type = "ad")
		{
			try
			{
				switchBusinessTabHandle();
				string ad = _ads_id;
				if (type == "BMad")
				{
					ad = _bm_ads_id;
				}

				string gurl = "https://business.facebook.com/settings/ad-accounts/" + ad + "?business_id=" + _bm_id;
				this._driver.Navigate().GoToUrl(gurl);
				WaitLoading();
				Delay(500);

				string getdata = this._driver.PageSource;
				_accessToken = Regex.Match(getdata, "\"accessToken\":\"(.*?)\"").Groups[1].Value;
				string fb_dtsg = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				if (string.IsNullOrEmpty(_accessToken))
				{
					_accessToken = Regex.Match(getdata, "\"accessToken\":\"(.*?)\"").NextMatch().Groups[1].Value;
				}

				// Get user
				string allUserStr = "";
				UserResBody userData = this.getBusinessUsers();
				if (userData != null)
				{
					userData.data.ForEach(uitem => {
						if ((uitem.first_name != rcBm_first_name) || (uitem.last_name != rcBm_last_name))
						{
							allUserStr = "&user_ids[0]=" + uitem.id;
						}
					});
				}

                if (string.IsNullOrEmpty(allUserStr))
                {
					this.log("NOT OTHER USER!");
					return false;
				}

				string url = "https://business.facebook.com/business/business_objects/update/permissions/";
				string body = string.Concat(new string[] {
					"asset_ids[0]=",
					ad,
					"&asset_type=ad-account&business_id=",
					_bm_id,
					"&roles[0]=151821535410699&roles[1]=610690166001223&roles[2]=864195700451909&roles[3]=186595505260379&",
					allUserStr,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8aolJ28S2q3mbwyyVuC3m2q12wAxu3Oq1FxebzA3aF98Sm4Euxa16xq2WdwJx64fwAwgUgwqoqxaazoO4o461twOxa7FEhwiomwDwXxK1wKEK3idwOxa3t2odoK7UC0A8sxe5bwuo898eUvy8fGxnzomws82Bz84aVpUaEd88EeAUtxS1PwBgak48W18wRwEwoE5WUeogwNxq8wio-7F8W1JCxeq4o2ZwQxS2W2K4E5y5EK5Ue8Sp3UG3WcwMzUkGdwEBwKG1cwl8hyVEjyo-2-qaUK0gq&__csr=&__req=s&__hs=18909.BP%3Abrands_pkg.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1004528366&__s=a6v1gc%3Ahs9qum%3Aek6adt&__hsi=7016917136682271993-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22058&lsd=",
					lsd,
					"&__spin_r=1004528366&__spin_b=trunk&__spin_t=1633753333&__jssesw=1"
				});

				string postData = this.fetchApi(url, "POST", body, "", lsd, gurl);
				string checkStatus = Regex.Match(postData, "\"successes\":{\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(checkStatus))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool SetRoleAd(string type = "ad")
		{
			try
			{
				switchBusinessTabHandle();

				string ad = _ads_id;
                if (type == "BMad")
                {
					ad = _bm_ads_id;
				}

				string gurl = "https://business.facebook.com/settings/ad-accounts/" + ad + "?business_id=" + _bm_id;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(gurl);
					WaitLoading();
					Delay(500);
				}

				string getdata = this._driver.PageSource;
				_accessToken = Regex.Match(getdata, "\"accessToken\":\"(.*?)\"").Groups[1].Value;
				string fb_dtsg = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				if (string.IsNullOrEmpty(_accessToken))
				{
					_accessToken = Regex.Match(getdata, "\"accessToken\":\"(.*?)\"").NextMatch().Groups[1].Value;
				}

				// Get user
				string allUserStr = "";
				UserResBody userData = this.getBusinessUsers();
				if (userData != null)
				{
					int index = 0;
					userData.data.ForEach(uitem => {
                        if ((!string.IsNullOrEmpty(rcBm_first_name)) && (!string.IsNullOrEmpty(rcBm_last_name)))
                        {
							if ((uitem.first_name == rcBm_first_name) || (uitem.last_name == rcBm_last_name))
							{
								allUserStr = "&user_ids[0]=" + uitem.id;
							}
                        }
                        else
                        {
							allUserStr = allUserStr + "&user_ids[" + index.ToString() + "]=" + uitem.id;
							index++;
						}
					});
				}

				if (string.IsNullOrEmpty(allUserStr))
				{
					this.log("NOT USER!");
					return false;
				}

				string url = "https://business.facebook.com/business/business_objects/update/permissions/";
				string body = string.Concat(new string[] {
					"asset_ids[0]=",
					ad,
					"&asset_type=ad-account&business_id=",
					_bm_id,
					"&roles[0]=151821535410699&roles[1]=610690166001223&roles[2]=864195700451909&roles[3]=186595505260379&",
					allUserStr,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8aolJ28S2q3mbwyyVuC3m2q12wAxu3Oq1FxebzA3aF98Sm4Euxa16xq2WdwJx64fwAwgUgwqoqxaazoO4o461twOxa7FEhwiomwDwXxK1wKEK3idwOxa3t2odoK7UC0A8sxe5bwuo898eUvy8fGxnzomws82Bz84aVpUaEd88EeAUtxS1PwBgak48W18wRwEwoE5WUeogwNxq8wio-7F8W1JCxeq4o2ZwQxS2W2K4E5y5EK5Ue8Sp3UG3WcwMzUkGdwEBwKG1cwl8hyVEjyo-2-qaUK0gq&__csr=&__req=s&__hs=18909.BP%3Abrands_pkg.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1004528366&__s=a6v1gc%3Ahs9qum%3Aek6adt&__hsi=7016917136682271993-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22058&lsd=",
					lsd,
					"&__spin_r=1004528366&__spin_b=trunk&__spin_t=1633753333&__jssesw=1"
				});

				string postData = this.fetchApi(url, "POST", body, "", lsd, gurl);
				string checkStatus = Regex.Match(postData, "\"successes\":{\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(checkStatus))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CreateBmAd()
		{
			try
			{
				switchBusinessTabHandle();

				if (string.IsNullOrEmpty(_accessToken))
                {
					string gurl = "https://business.facebook.com/settings/ad-accounts?business_id=" + _bm_id;
					string gdata = this.GetData(null, gurl, _cookie, this._userAgent);
					_accessToken = Regex.Match(gdata, "\"accessToken\":\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(_accessToken))
					{
						_accessToken = Regex.Match(gdata, "\"accessToken\":\"(.*?)\"").NextMatch().Groups[1].Value;
					}
				}

				_rNameAds = "Ads%20" + new Random().Next(100000, 999999).ToString();
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v9.0/",
					_bm_id,
					"/adaccount?access_token=",
					_accessToken
				});

				string data2 = string.Concat(new string[]
				{
					"__activeScenarioIDs=%5B%5D&__activeScenarios=%5B%5D&__interactionsMetadata=%5B%5D&_reqName=object%3Abrand%2Fadaccount&_reqSrc=AdAccountActions.brands&ad_account_created_from_bm_flag=true&currency=USD&end_advertiser=",
					_bm_id,
					"&invoicing_emails=%5B%5D&locale=en_US&media_agency=UNFOUND&method=post&name=",
					_rNameAds,
					"&partner=UNFOUND&po_number=&pretty=0&suppress_http_code=1&timezone_id=1&xref=f3d8fdf34a4fcb8"
				});

				// string postData = this.PostData(null, url, data2, this._contentType, this._userAgent, _cookie);
				string postData = this.fetchApi(url, "POST", data2, "", "", "https://business.facebook.com");
				_bm_ads_id = Regex.Match(postData, "\"account_id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(_bm_ads_id))
				{
					this.log("Create ads: NOT_OK");
					return false;
				}

				this.log("Ad ID: " + _bm_ads_id);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool ShareBmAd(string bmarcId)
		{
			try
			{
				switchBusinessTabHandle();

				string url = string.Concat(new string[]
					{
						"https://graph.facebook.com/v9.0/act_",
						_bm_ads_id,
						"/agencies?access_token=",
						_accessToken
					});

				string data2 = string.Concat(new string[]
					{
						"_reqName=adaccount%2Fagencies&_reqSrc=BrandAgencyActions.brands&accountId=",
						_bm_ads_id,
						"&acting_brand_id=",
						_bm_id,
						"&business=",
						bmarcId,
						"&locale=en_US&method=post&permitted_tasks=%5B%22ADVERTISE%22%2C%22ANALYZE%22%2C%22DRAFT%22%2C%22MANAGE%22%5D&pretty=0&suppress_http_code=1&xref=f3bacbd68ad2bd"
					});

				string postData = this.PostData(null, url, data2, this._contentType, this._userAgent, _cookie);
				string resultshare = Regex.Match(postData, "\"success\":(.*?)}").Groups[1].Value;

				if (string.IsNullOrEmpty(resultshare))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CheckBmAds()
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v9.0/act_",
					_bm_ads_id,
					"?access_token=",
					_accessToken,
					"&_reqName=adaccount&fields=[%22id%22,%22account_status%22,%22currency%22]"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				string account_status = Regex.Match(data, "\"account_status\": (.*?),").Groups[1].Value;
                if (account_status != "1")
                {
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CheckBmClientAds()
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v10.0/act_",
					_ads_id,
					"?access_token=",
					_accessToken,
					"&_reqName=adaccount&fields=[%22id%22,%22account_status%22,%22currency%22]"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				string account_status = Regex.Match(data, "\"account_status\": (.*?),").Groups[1].Value;
				if (account_status != "1")
				{
					this.log("TK Die!");
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CheckBmCard()
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v9.0/",
					_bm_id,
					"/creditcards?access_token=",
					_accessToken,
					"&fields=%5B%22card_type%22%2C%22credential_id%22%2C%22expiry_month%22%2C%22expiry_year%22%2C%22last4%22%5D&limit=1&pretty=0"
				});

				string data = this.GetData(null, url, _cookie, this._userAgent);
				string cardId = Regex.Match(data, "\"credential_id\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(cardId))
                {
					return false;
				}
				_credential_id = cardId;
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool logout()
		{
			try
			{
				this._driver.SwitchTo().Window(this.otherTabHandle);
				Delay(500);
				string url = "https://www.facebook.com/help/contact/logout?id=260749603972907";
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(500);

				// Logout btn
				if (!elmAutoFinAndClick("//button[@name='logout']", 15))
				{
					this.log("Not Logout Btn!");
					return false;
				}

				Delay(500);
				WaitLoading();
				if (this._driver.Url.Contains("help/contact/260749603972907"))
				{
					return true;
				}

				return false;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool getAdsId()
		{
			try
			{
				if (!this.checkCloneToken())
				{
					return false;
				}

				if (string.IsNullOrEmpty(_ads_id))
				{
					string url = "https://graph.facebook.com/v12.0/" + _clone_uid + "/adaccounts?access_token=" + _cloneAccessToken + "&limit=2&fields=[%22id%22,%22name%22,%22account_id%22,%22account_status%22,%22currency%22,%22adtrust_dsl%22]&sort=name_ascending&pretty=0";
					string data = this.GetData(null, url, _cookie, this._userAgent);
					AdAccountsResBody aData = JsonConvert.DeserializeObject<AdAccountsResBody>(data);
					if ((aData == null) || (aData.data == null) || (aData.data.Count == 0))
					{
						this.log("No Ads!");
						return false;
					}

					foreach (AdAccounts item in aData.data)
					{
						if (item.account_status == 1)
						{
							_ads_id = item.account_id;
							adAccount = item;
							break;
						}
					}

					if (string.IsNullOrEmpty(_ads_id))
					{
						this.log("Ads Die!");
						return false;
					}
					this.log("Ads id: " + _ads_id);
				}
				else
				{
					string url = "https://graph.facebook.com/v12.0/act_" + _ads_id + "?access_token=" + _cloneAccessToken + "&fields=[%22id%22,%22name%22,%22account_id%22,%22account_status%22,%22currency%22]&pretty=0";
					string data = this.GetData(null, url, _cookie, this._userAgent);
					adAccount = JsonConvert.DeserializeObject<AdAccounts>(data);
					if ((adAccount == null) || (adAccount.account_status != 1))
					{
						this.log("Ads Die!");
						return false;
					}
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool checkCloneToken()
		{
			try
			{
                if (string.IsNullOrEmpty(_cloneAccessToken))
                {
					// fetchGet
					switchRegTabHandle();

					string url = "https://www.facebook.com/ads/manager/account_settings/information";
                    if (!string.IsNullOrEmpty(_ads_id))
                    {
						url = "https://www.facebook.com/ads/manager/account_settings/information/?act=" + _ads_id + "&pid=p1&page=account_settings&tab=account_information";
					}

					if (!this._driver.Url.Contains("www.facebook.com"))
					{
						this._driver.Navigate().GoToUrl("https://www.facebook.com");
						WaitLoading();
						Delay(1000);
					}

					string getData = this.GetData(null, url, _cookie);
					_sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
					_cloneAccessToken = Regex.Match(getData, "access_token:\"(.*?)\"").Groups[1].Value;

					if (string.IsNullOrEmpty(_cloneAccessToken))
					{
						Delay(3000);
						getData = this.fetchGet(url);
						_sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
						_cloneAccessToken = Regex.Match(getData, "access_token:\"(.*?)\"").Groups[1].Value;
					}

					if (string.IsNullOrEmpty(_cloneAccessToken))
					{
						this._driver.SwitchTo().Window(this.otherTabHandle);
						Delay(500);
						this._driver.Navigate().GoToUrl(url);
						WaitLoading();
						Delay(3000);
						getData = this._driver.PageSource;
						_sessionID = Regex.Match(getData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;
						_cloneAccessToken = Regex.Match(getData, "access_token:\"(.*?)\"").Groups[1].Value;
						switchRegTabHandle();
					}

					if (string.IsNullOrEmpty(_cloneAccessToken) && string.IsNullOrEmpty(_ads_id))
					{
						this._driver.SwitchTo().Window(this.otherTabHandle);
						Delay(500);
						this._driver.Navigate().GoToUrl("https://www.facebook.com/ads/manager/accounts");
						WaitLoading();
						Delay(500);

						ReadOnlyCollection<IWebElement> tdAdsInfoObjs = this._driver.FindElements(By.TagName("table"));
						if (tdAdsInfoObjs.Count > 0)
						{
							HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();
							string getPageSourceData = this._driver.PageSource;
							doc.LoadHtml(getPageSourceData);

							List<List<string>> table = doc.DocumentNode.SelectSingleNode("//table")
							.Descendants("tr")
							.Where(tr => tr.Elements("td").Count() > 1)
							.Select(tr => tr.Elements("td").Select(td => td.InnerText.Trim()).ToList())
							.ToList();

							this._driver.Navigate().GoToUrl("https://www.google.com/");
							if (table.Count < 1)
							{
								return false;
							}
							else
							{
								List<string> ads = table.First();
								_ads_id = ads[1];
                                if (!string.IsNullOrEmpty(_ads_id))
                                {
									return this.checkCloneToken();
								}
							}
						}
					}

					if (string.IsNullOrEmpty(_cloneAccessToken))
					{
						this.log("AccessToken: NONE");
						return false;
					}

					this.log("AccessToken: " + _cloneAccessToken);
				}

				return true;
			}
			catch
			{
				return false;
			}
		}

		public bool checkBMFullPendingReq(string bmcode)
		{
			try
			{
				PageResBody pendingPage = this.autoRequest.getBmPendingPages(bmcode);
				int countp = 0;
                if (pendingPage != null)
                {
					countp = pendingPage.data.Count;
				}
				this.log("Có " + countp.ToString() + " Pending page!");

				AdsResBody pendingAds = this.autoRequest.getBmPendingAds(bmcode);
				int counta = 0;
				if (pendingAds != null)
				{
					counta = pendingAds.data.Count;
				}
				this.log("Có " + counta.ToString() + " Pending ads!");

                if (countp + counta < 2)
                {
					return false;
				}
				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool waitBillUp(int checkCount = 10, string minVal = "2", string type = "BMad")
		{
			try
			{
				string url = string.Concat(new string[]
				{
					"https://graph.facebook.com/v9.0/act_",
					_bm_ads_id,
					"?access_token=",
					_accessToken,
					"&_reqName=adaccount&fields=[%22id%22,%22account_status%22,%22currency%22,%22adspaymentcycle.fields(threshold_amount).limit(1)%22]&pretty=0"
				});

                if (type == "ad")
                {
					if (shareAds)
					{
						url = string.Concat(new string[]
						{
							"https://graph.facebook.com/v9.0/act_",
							_ads_id,
							"?access_token=",
							_accessToken,
							"&_reqName=adaccount&fields=[%22id%22,%22account_status%22,%22currency%22,%22adspaymentcycle.fields(threshold_amount).limit(1)%22]&pretty=0"
						});
					}
                    else
                    {
						url = "https://graph.facebook.com/v12.0/" + _clone_uid + "/adaccounts?access_token=" + _cloneAccessToken + "&limit=2&fields=[%22id%22,%22account_status%22,%22currency%22,%22adspaymentcycle.fields(threshold_amount).limit(1)%22]&pretty=0&sort=name_ascending";
					}
				}

				string data = this.GetData(null, url, _cookie, this._userAgent);
				string account_status = Regex.Match(data, "\"account_status\":(.*?),").Groups[1].Value;
				if (account_status != "1")
				{
					this.log("Ad account Die");
					return false;
				}

				bool isUp = false;
				int ChkIndex = 1;

				// threshold_amount
				string threshold_amount = Regex.Match(data, "\"threshold_amount\":(.*?)}").Groups[1].Value;
				int iThresholdAmount;
				Int32.TryParse(threshold_amount, out iThresholdAmount);

				int iMinVal;
				Int32.TryParse(minVal, out iMinVal);
				iMinVal = iMinVal * 100;
				if (iThresholdAmount > iMinVal)
                {
					return true;
				}

				this.log("Waiting...");

				while (!isUp && (ChkIndex <= checkCount))
				{
					Delay(5000);
					data = this.GetData(null, url, _cookie, this._userAgent);
					account_status = Regex.Match(data, "\"account_status\":(.*?),").Groups[1].Value;
					if (account_status != "1")
					{
						this.log("Ad account Die");
						isUp = false;
						ChkIndex = checkCount + 1;
                    }
                    else
                    {
						threshold_amount = Regex.Match(data, "\"threshold_amount\":(.*?)}").Groups[1].Value;
						Int32.TryParse(threshold_amount, out iThresholdAmount);
						if (iThresholdAmount > iMinVal)
						{
							this.log("threshold_amount: " + threshold_amount);
							isUp = true;
						}
					}

					ChkIndex++;
				}

				return isUp;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool apiWelcome()
		{
			try
			{
				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string refAv = "https://www.facebook.com/";
				string url = "https://www.facebook.com/cookie/consent/";
				string body = string.Concat(new string[] {
					"accept_only_essential=false&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHK4HwkEng5KbxG4VuC0BVU98nwgU29gS3q2ibwNw9G2S7o762S1DwUx609vCxS320om782Cwwwqo465o-cw5MKdwGxu782ly87e2l2Utwwwi831wiEjwZxy3O1mzXxG1Pxi4UaEW2G1jxS6FobrwKxm5oe8cEW4-5pUfEe872m7-2K1yw9qm2Sq2-azqwaW1jg2cwMwiU8UdUco&__csr=gN4NdQlSDtiOmJ9V4jJiL7qR6dbcAn9FFjlbfQALhcyqpfFHldBTTiLcBJWDiHroWFKKAtiVDmnGjGh2VFeVloCQ8LUCUjzpuh2EWczUy8gCmii2quUzhrWxqrwCD-48ryBgK9x62-dKFqDCDybyEWfz998hwjk5EswMwHwYwHDwjEfEyi4EC6HxS3-mu1cyEC6efwSxO1uwywywywIwea6U1cUG0IE6nw6owHwWg5m06uU0bS80Ql01Om01m_w0Ruw3doiAyUx0gE462y2S02hS0G80siw2I809Ao0Nq1Go1xEhDw59w1BO2O7U08so4u&__req=h&__hs=19168.HYP%3Acomet_pkg.2.1.0.2.0&dpr=1&__ccg=EXCELLENT&__rev=1005751412&__s=%3A1ofuz0%3Afw403l&__hsi=7113238409923533694-0&__comet_req=15&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25751&lsd=",
					lsd,
					"&__spin_r=1005751412&__spin_b=trunk&__spin_t=1656179877"
				});

				string postData = this.fetchApi(url, "POST", body, "", lsd, refAv, "include", "30605361");
				string lid = Regex.Match(postData, "\"lid\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(lid))
				{
					this.log("Not lid!");
				}

				refAv = "https://www.facebook.com/privacy/consent/user_cookie_choice/?source=pft_user_cookie_choice";
				url = "https://www.facebook.com/api/graphql/";
				body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmwlE7ibwKBWo2vwAxu13w8CewSwMwNw9G2S0im3y4o0B-q1ew65xO0FE2awt81sbzo5-0Boy1PwBgao6C0Mo5W3S1lwlEjxG1Pxi4UaEW0D888cobEaU2eU5O0HUvw4JwJwSyES0gq0Lo4K&__csr=gwmRgGQiEBkmKhUSfG5LG5UXxi5pu5E5W2-q6U9o88e8C6Vo9E2UwQw2VU2zw2Fo3cw04odw0CNyU4m&__req=6&__hs=19063.HYP%3Acomet_pkg.2.1.0.2.&dpr=1&__ccg=EXCELLENT&__rev=1005189276&__s=iz4bxd%3Aqeers8%3Au5s7kj&__hsi=7074237150035079282-0&__comet_req=1&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22062&lsd=",
					lsd,
					"&__spin_r=1005189276&__spin_b=trunk&__spin_t=1647099189&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useCometConsentPromptOutcomeBatchedMutation&variables=%7B%22input%22%3A%7B%22config_enum%22%3A%22USER_COOKIE_CHOICE_FRENCH_CNIL%22%2C%22extra_params_json%22%3A%22%7B%7D%22%2C%22flow_step_type%22%3A%22STANDALONE%22%2C%22outcome%22%3A%22APPROVED%22%2C%22server_on_complete_params_darray_json%22%3A%22%7B%5C%22first_party_trackers_on_foa%5C%22%3A%5C%22true%5C%22%2C%5C%22fb_trackers_on_other_companies%5C%22%3A%5C%22false%5C%22%2C%5C%22other_company_trackers_on_foa%5C%22%3A%5C%22false%5C%22%7D%22%2C%22source%22%3A%22pft_user_cookie_choice%22%2C%22surface%22%3A%22FACEBOOK_COMET%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=4771563969537412"
				});

				postData = this.fetchApi(url, "POST", body, "useCometConsentPromptOutcomeBatchedMutation", lsd, refAv);
				string action = Regex.Match(postData, "\"action\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(action))
				{
					this.log("Not action!");
				}

				apiAllowAllCookie();

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool apiAllowAllCookie()
		{
			try
			{
				switchRegTabHandle();

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string refAv = "https://www.facebook.com/privacy/consent/user_cookie_choice/?source=pft_user_cookie_choice";
				string url = "https://www.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmwlE7ibwKBWo2vwAxu13w8CewSwMwNw9G2S0im3y4o0B-q1ew65xO0FE2awt81sbzo5-0Boy1PwBgao6C0Mo5W3S1lwlEjxG1Pxi4UaEW0D888cobEaU2eU5O0HUvw4JwJwSyES0gq0Lo4K&__csr=gwmRgGQiEBkmKhUSfG5LG5UXxi5pu5E5W2-q6U9o88e8C6Vo9E2UwQw2VU2zw2Fo3cw04odw0CNyU4m&__req=6&__hs=19063.HYP%3Acomet_pkg.2.1.0.2.&dpr=1&__ccg=EXCELLENT&__rev=1005189276&__s=iz4bxd%3Aqeers8%3Au5s7kj&__hsi=7074237150035079282-0&__comet_req=1&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22062&lsd=",
					lsd,
					"&__spin_r=1005189276&__spin_b=trunk&__spin_t=1647099189&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useCometConsentPromptOutcomeBatchedMutation&variables=%7B%22input%22%3A%7B%22config_enum%22%3A%22USER_COOKIE_CHOICE_FRENCH_CNIL%22%2C%22extra_params_json%22%3A%22%7B%7D%22%2C%22flow_step_type%22%3A%22STANDALONE%22%2C%22outcome%22%3A%22APPROVED%22%2C%22server_on_complete_params_darray_json%22%3A%22%7B%5C%22first_party_trackers_on_foa%5C%22%3A%5C%22true%5C%22%2C%5C%22fb_trackers_on_other_companies%5C%22%3A%5C%22false%5C%22%2C%5C%22other_company_trackers_on_foa%5C%22%3A%5C%22false%5C%22%7D%22%2C%22source%22%3A%22pft_user_cookie_choice%22%2C%22surface%22%3A%22FACEBOOK_COMET%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=4771563969537412"
				});

				string postData = this.fetchApi(url, "POST", body, "useCometConsentPromptOutcomeBatchedMutation", lsd, refAv);
				string action = Regex.Match(postData, "\"action\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(action))
				{
					this.log("Not action!");
				}

				body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmwlE7ibwKBWo2vwAxu13w8CewSwMwNw9G2S0im3y4o0B-q1ew65xO0FE2awt81sbzo5-0Boy1PwBgao6C0Mo5W3S1lwlEjxG1Pxi4UaEW0D888cobEaU2eU5O0HUvw4JwJwSyES0gq0Lo4K&__csr=gwmRgGQiEBkmKhUSfG5LG5UXxi5pu5E5W2-q6U9o88e8C6Vo9E2UwQw2VU2zw2Fo3cw04odw0CNyU4m&__req=7&__hs=19063.HYP%3Acomet_pkg.2.1.0.2.&dpr=1&__ccg=EXCELLENT&__rev=1005189276&__s=iz4bxd%3Aqeers8%3Au5s7kj&__hsi=7074237150035079282-0&__comet_req=1&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22062&lsd=",
					lsd,
					"&__spin_r=1005189276&__spin_b=trunk&__spin_t=1647099189&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useCometConsentPromptEndOfFlowBatchedMutation&variables=%7B%22input%22%3A%7B%22config_enum%22%3A%22USER_COOKIE_CHOICE_FRENCH_CNIL%22%2C%22extra_params_json%22%3A%22%7B%7D%22%2C%22flow_step_type%22%3A%22STANDALONE%22%2C%22outcome%22%3A%22APPROVED%22%2C%22source%22%3A%22pft_user_cookie_choice%22%2C%22surface%22%3A%22FACEBOOK_COMET%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%223%22%7D%7D&server_timestamps=true&doc_id=4025101950860865"
				});

				postData = this.fetchApi(url, "POST", body, "useCometConsentPromptEndOfFlowBatchedMutation", lsd, refAv);
				string uri = Regex.Match(postData, "\"uri\":\"(.*?)\"").Groups[1].Value;
				if (!string.IsNullOrEmpty(uri))
				{
					this._driver.Navigate().GoToUrl(uri);
					WaitLoading();
					Delay(1000);
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool apiCometProfilePlus()
		{
			try
			{
				switchRegTabHandle();
				string gurl = "https://www.facebook.com/profile.php?id=" + _clone_uid;
				if (!this._driver.Url.Contains("www.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(gurl);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string refAv = gurl;
				string url = "https://www.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHxq1mxu1syaxG4VuC0BVU98nwgU765QdwSwMwyzE5S3O2Saxa1NwJwpUe8hw47w9u0LVEtwMw65xO2OU7m2210wEwgolzUO0-E4a3aUS2G5Usw9m1YwBgK7o884y0Mo4G4Ufo5m1mzXxG1Pxi4UaEW2au1jxS6FobrwKxm5oe8cEW4-5pUfEdK1MBx_y88E3qxWm2Sq2-azo2NwwwOg2cwMwhF8-4UdUcobUak1xxWewxw&__csr=gaBPgiOOgxdtgJTtldOOcy48BhsjuhXlQJqkWkCJjljA-GFnHVRBZ4FWr-GWQzu9JaABy9lhWGHJ7Rhd4GKlB-GzFbZ38x5DVpEyAjz8GXK68lGi68Tx7gCl7z5VECbDBK6oK8DAVoOEyi-4XQbAhEG7poqyd5AxnK3ObLgG4oc8CcwzyE9qyu48kG8DwOx2azEjzu3ii1CzUFABxqq8K3W7EbU_xq0B82wzEvxGqaxe0hWdxq0B820w7Tw0iaU0qfG0F8O3Twk80Rq00BvE0NO05g80hMg08eU3Mxm07To0JIE0l3wnE8Q03ka0aJw2BQ1Sg32w2vo17E0Vq2S0k22u&__req=2g&__hs=19314.HYP%3Acomet_pkg.2.1.0.2.1&dpr=1&__ccg=MODERATE&__rev=1006628507&__s=jf0tzi%3Atrtyaq%3A6o8yz7&__hsi=7167411156938771717&__comet_req=15&fb_dtsg=",
					fb_dtsg,
					"&jazoest=5537&lsd=",
					lsd,
					"&__spin_r=1006628507&__spin_b=trunk&__spin_t=1668792953&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=CometProfilePlusOnboardingDialogTransitionMutation&variables=%7B%22category_id%22%3A%221603%22%7D&server_timestamps=true&doc_id=4958075807610679&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%22%5D"
				});

				string postData = this.fetchApi(url, "POST", body, "CometProfilePlusOnboardingDialogTransitionMutation", lsd, refAv);
				string profile_plus_mutation = Regex.Match(postData, "\"profile_plus_mutation\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(profile_plus_mutation))
				{
					this.log(postData);
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool apiCloneEmailVerify(string source = "mail.tm")
		{
			try
			{
				switchRegTabHandle();

				string url = "https://www.facebook.com/settings?tab=account&section=email&view";
				if (!this._driver.Url.Contains("settings?tab=account&section=email&view"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(1000);
				}

				WaitAjaxLoading(By.CssSelector("iframe[src*='settings?tab=account&section=email&view']"), 15);
				ReadOnlyCollection<IWebElement> iFrames = this._driver.FindElements(By.CssSelector("iframe[src*='settings?tab=account&section=email&view']"));
				if (iFrames.Count == 0)
				{
					this.log("iFrame Not F!");
					return false;
				}

				IWebElement iFrame = iFrames.First();
				string ifSrc = iFrame.GetAttribute("src");
				Uri theUri = new Uri(ifSrc);

				string cquick_token = HttpUtility.ParseQueryString(theUri.Query).Get("cquick_token");
				string ctarget = HttpUtility.ParseQueryString(theUri.Query).Get("ctarget");
				string cquick = HttpUtility.ParseQueryString(theUri.Query).Get("cquick");

				string newEmail = getEmail(source);
				if (string.IsNullOrEmpty(newEmail))
				{
					this.log("Not New Email!");
					return false;
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string refAv = ifSrc;

				url = "https://www.facebook.com/settings/email/add/submit/";
				string body = string.Concat(new string[] {
					"cquick_token=",
					cquick_token,
					"&ctarget=",
					ctarget,
					"&cquick=",
					cquick,
					"&jazoest=21904&fb_dtsg=",
					fb_dtsg,
					"&new_email=",
					newEmail,
					"&qp_h=&source_added=web_settings&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBwjbg7ebwKheC1swgE98nwgU6C7UW3q327E2vzobohw5cx60Vo2Vwb-q1ew8y11wbG782Cwn-2y1Qw5MKdwnU5Wcwaq2l2Utwwwi8188fo2IwKw9O0RE5a1qw8W1uwa-7U1mUdEGdw46wbS&__csr=&__req=a&__hs=19069.BP%3ADEFAULT.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005216400&__s=2i6hnv%3A2qhvc9%3Aibljkd&__hsi=7076477036099042987-0&__comet_req=0&lsd=",
					lsd,
					"&__spin_r=1005216400&__spin_b=trunk&__spin_t=1647620703&ajax_password=",
					_clone_pass,
					"&confirmed=1"
				});

				string postData = this.fetchApi(url, "POST", body, "", lsd, refAv);
				string errorSummary = Regex.Match(postData, "\"errorSummary\":\"(.*?)\"").Groups[1].Value;
				if (!string.IsNullOrEmpty(errorSummary))
				{
					this.log("Not change email!");
					return false;
				}

				string lid = Regex.Match(postData, "\"lid\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(lid))
				{
					this.log("Not change email!");
					return false;
				}

				// Get email verify
				string linkVerify = getCloneEmailVerifyLink(source);
				if (string.IsNullOrEmpty(linkVerify))
				{
					this.log("Not link Verify!");
					return false;
				}

				this.log("Link Verify: " + linkVerify);
				this._driver.Navigate().GoToUrl(linkVerify);
				WaitLoading();
				Delay(1000);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool apiBMEmailVerify()
		{
			try
			{
				switchBusinessTabHandle();

				if (string.IsNullOrEmpty(_bm_id))
				{
					return false;
				}

				if (string.IsNullOrEmpty(_accessToken))
				{
					return false;
				}

				string newEmail = getEmail("mail.tm");
				if (string.IsNullOrEmpty(newEmail))
				{
					this.log("Not New Email!");
					return false;
				}

				string url = "https://graph.facebook.com/v12.0/" + _business_user_id + "?access_token=" + _accessToken;
				string dataPost = string.Concat(new string[]
				{
					"_reqName=object%3Abusiness_user&_reqSrc=UserServerActions.brands&locale=en_US&method=post&pending_email=",
					newEmail,
					"&personaId=",
					_business_user_id,
					"&pretty=0&suppress_http_code=1&xref=f231b1e44df24b"
				});

				string datapost = this.PostData(null, url, dataPost, this._contentType, this._userAgent, null);
				string status = Regex.Match(datapost, "\"success\":(.*?)}").Groups[1].Value;
				if ("true" != status)
				{
					this.log(datapost);
					return false;
				}

				// Get email verify
				string linkVerify = getMailTMBMVerifyLinkAsync().Result;
				if (string.IsNullOrEmpty(linkVerify))
				{
					this.log("Not link Verify!");
					return false;
				}

				this.log("Link Verify: " + linkVerify);
				this._driver.Navigate().GoToUrl(linkVerify);
				WaitLoading();
				Delay(1000);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private async Task<string> getMailTMBMVerifyLinkAsync()
		{
			try
			{
				MessageInfo[] messages = await mailClient.GetAllMessages();
				string link = "";
				if (messages.Length > 0)
				{
					foreach (MessageInfo item in messages)
					{
						if (item.From.Name == "Facebook")
						{
							MessageSource source = await mailClient.GetMessageSource(item.Id);
							string rawMessage = source.Data;
							byte[] byteArray = Encoding.UTF8.GetBytes(rawMessage);
							MemoryStream stream = new MemoryStream(byteArray);
							MimeParser parser = new MimeParser(stream, MimeFormat.Entity);
							MimeMessage mimeMessage = parser.ParseMessage();
							string htmls = HttpUtility.HtmlDecode(mimeMessage.HtmlBody);
							Match rgMatch = Regex.Match(htmls, @"https:\/\/www\.facebook\.com\/verify\/email\/checkpoint\/([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
							if (rgMatch != null && rgMatch.Groups.Count > 1)
							{
								link = @"https://www.facebook.com/verify/email/checkpoint/" + rgMatch.Groups[1].Value;
								break;
							}
						}
					}
				}

				int ChkLoadingIndex = 1;
				while (string.IsNullOrEmpty(link) && (ChkLoadingIndex <= 6))
				{
					Delay(5000);
					messages = await mailClient.GetAllMessages();
					foreach (MessageInfo item in messages)
					{
						if (item.From.Name == "Facebook")
						{
							MessageSource source = await mailClient.GetMessageSource(item.Id);
							string rawMessage = source.Data;
							byte[] byteArray = Encoding.UTF8.GetBytes(rawMessage);
							MemoryStream stream = new MemoryStream(byteArray);
							MimeParser parser = new MimeParser(stream, MimeFormat.Entity);
							MimeMessage mimeMessage = parser.ParseMessage();
							string htmls = HttpUtility.HtmlDecode(mimeMessage.HtmlBody);
							Match rgMatch = Regex.Match(htmls, @"https:\/\/www\.facebook\.com\/verify\/email\/checkpoint\/([\w\-\.,@?^=%&amp;:/~\+#]*[\w\-\@?^=%&amp;/~\+#])?");
							if (rgMatch != null && rgMatch.Groups.Count > 1)
							{
								link = @"https://www.facebook.com/verify/email/checkpoint/" + rgMatch.Groups[1].Value;
								break;
							}
						}
					}
					ChkLoadingIndex++;
				}

				return link;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		public bool apiVuotHanChe()
		{
			try
			{
				string data = this._driver.PageSource;
				string value = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string value2 = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				string refAv = "https://www.facebook.com/ad_center/create/boostpost/?entry_point=composer&page_id=" + pageID;

				string url = "https://www.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__usid=6-Tr1mfaqdw7ama%3APr1mfao1p62uw1%3A0-Ar1mfaq1jmhm2d-RV%3D6%3AF%3D&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHxqU5a9zk1ryaxG4VuC0BVU98nwgUb84ibyQdwSAx-bwNxW3W3O2Saxa1NxZ38C1twUx609vCxS320om78c87m2210x-8wgolzUO0n2US2G3i0Boy13wLwBgK7o884y0Mo5W3e9xy3O1mzXxG1Pxi4UaEW2au1NxGm2SUnxq5olwUwgojUlwhEe872m7-8wywLy82tK7FobodEGdwb622390&__csr=ggjZjFEvn8B5l9ddtYrSBZvilLqlQzqp7ZidvjkRN1dkBGShXHEgFAWAmCiAiFvrhRKvzbVkqajKiAXDhkbG9FWhoG9Ay9pCHKleUDydGmbxWEW8A_WAF4Q54ZammifVrUy5KqmmfLJ29HJdrnyGyUWdzEKiazbGaVGy99VFy4DGHCHHhUGy5AWzU98-ayoyFujiJ3EyKbyoGaAx2U8HCzbG78jz9E9UlBGUqQQdggyoyAquuAq4VpEgxh6wwyU8EdqU-u6U9UbUuDG2ufgSEG6EC5kU8o-ii9zEoz8G6_UpypWKarAx-5mmcyU-16wxBxW68K5ob9Eoy8S0yUswSzKE-7ppU4GcwBGcwjod8rU3ag0J-1Gw2XU1k40gyVEC09pwkE1o40bxw7zg5Z1C267k2203d60Co0DO15K02tG0MUcHy81jo0wX4aQegm4wIkE0C2kE2qgmgmPa12yo1So0lWxP808mw16u0i-1Pg0s0c0cuNFoKlw37o2Tzo&__req=17&__hs=18927.HYP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004622927&__s=d57ujy%3Aldykki%3A228re3&__hsi=7023614240062472637-0&__comet_req=1&fb_dtsg=",
					value,
					"&jazoest=21959&lsd=",
					value2,
					"&__spin_r=1004622927&__spin_b=trunk&__spin_t=1635312624&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useAdIntegrityCertifyMutation&variables=%7B%22input%22%3A%7B%22source%22%3A%22lwi_www%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=3824044827678930&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%22%5D"
				});

				string postData = this.fetchApi(url, "POST", body, "useAdIntegrityCertifyMutation", value2, refAv, "include");
				string certified_user_name = Regex.Match(postData, "\"certified_user_name\":\"(.*?)\"").Groups[1].Value;
				//success
				if (string.IsNullOrEmpty(certified_user_name))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool BMAdCampChangeBudget(string budgetVal = "150")
		{
			try
			{
				switchBusinessTabHandle();
				string rurl = "https://business.facebook.com/adsmanager/manage/campaigns?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
				string fGetData = this.fetchGet(rurl);
				string fb_dtsg = Regex.Match(fGetData, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(fb_dtsg))
				{
					rurl = "https://business.facebook.com/adsmanager/manage/accounts?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					fGetData = this.fetchGet(rurl);
					fb_dtsg = Regex.Match(fGetData, "\"token\":\"(.*?)\"").Groups[1].Value;
				}

				if (string.IsNullOrEmpty(fb_dtsg))
				{
					this.log("Get Addraft: FALSE!");
					return false;
				}
				string lsd = Regex.Match(fGetData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string accessTokenj = Regex.Match(fGetData, "window.__accessToken=\"(.*?)\"").Groups[1].Value;

				string campId = Regex.Match(fGetData, "\"effective_status\":\"ACTIVE\",\"id\":\"(.*?)\"").Groups[1].Value;
				string draft_id = Regex.Match(fGetData, "\"draft_version\":\"1\",\"id\":\"(.*?)\"").Groups[1].Value;
				string sessionID = Regex.Match(fGetData, "\"sessionID\":\"(.*?)\"").Groups[1].Value;

				rurl = "https://graph.facebook.com/v10.0/" + draft_id + "/addraft_fragments_with_publish?_app=ADS_MANAGER&_reqName=object%3Aaddraft%2Faddraft_fragments_with_publish&access_token=" + accessTokenj + "&method=post&qpl_active_flow_ids=270210707&qpl_active_flow_instance_ids=270210707_f1ab840000000&__cppo=1";
				string body = string.Concat(new string[] {
						"__activeScenarioIDs=%5B%22%22%5D&__activeScenarios=%5B%22instant_publish_inline%22%5D&__business_id=",
						_bm_id,
						"&_app=ADS_MANAGER&_priority=HIGH&_reqName=object%3Aaddraft%2Faddraft_fragments_with_publish&_reqSrc=AdsDraftFragmentDataManager&_sessionID=",
						sessionID,
						"&account_id=",
						_bm_ads_id,
						"&action=modify&ad_object_id=",
						campId,
						"&ad_object_type=campaign&include_headers=false&locale=en_US&method=post&parent_ad_object_id=",
						_bm_ads_id,
						"&pretty=0&publish=true&qpl_active_flow_ids=270210707&qpl_active_flow_instance_ids=270210707_f1ab840000000&suppress_http_code=1&validate=false&values=%5B%7B%22field%22%3A%22lifetime_budget%22%2C%22old_value%22%3A%22",
						"1600",
						"%22%2C%22new_value%22%3A%22",
						budgetVal + "00" +
						"%22%7D%5D&xref=f32e523c1fa584c"
					});

				string postData = this.fetchApi(rurl, "POST", body, "", lsd, "https://business.facebook.com/", "omit");
				string ad_object_id = Regex.Match(postData, "\"ad_object_id\":\"(.*?)\"").Groups[1].Value;
				string success = Regex.Match(postData, "\"success\":(.*?),").Groups[1].Value;
				//success
				if (string.IsNullOrEmpty(ad_object_id) || (success != "true"))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool CheckAdAllInOneAsync(string checkType = "ad")
		{
			try
			{
				switchRegTabHandle();

				bool checkAds = false;
                if (checkType == "ad")
                {
					if (shareAds)
					{
						checkAds = this.CheckBmClientAds();
					}
					else
					{
						checkAds = this.getAdsId();
						switchRegTabHandle();
					}
				}
				else
				{
					if (checkType == "BMad")
                    {
						checkAds = this.CheckBmAds();
					}
				}

				return checkAds;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		// budget schedule
		public bool SetChangeBudgetSchedule(string viaid, int time)
        {
            try
            {
				string url = "http://nguyenlieu.site/api/public/api/v1/budget/schedule";
				var reqParams = new RequestParams();
				reqParams["via_id"] = viaid;
				reqParams["ads_id"] = string.IsNullOrEmpty(_ads_id) ? "" : _ads_id;
				reqParams["time"] = time;
                string datapost = this.autoRequest.PostToStoreData(url, reqParams);
                string status = Regex.Match(datapost, "\"status\":(.*?),").Groups[1].Value;
                if (string.IsNullOrEmpty(status))
                {
                    return false;
                }
                return true;
            }
            catch (Exception ex)
            {
				this.log(ex.Message);
				return false;
            }
        }

		// get card
		public string getCardFromNguyenLieu()
		{
			try
			{
                string getdata = this.autoRequest.GetFromStoreData("http://nguyenlieu.site/api/public/api/v1/card/active_record");
                return Regex.Match(getdata, "\"code\":\"(.*?)\"").Groups[1].Value;
			}
			catch
			{
				return null;
			}
		}

		// storeClone
		public bool storeCard(string card)
		{
			try
			{
				string url = "http://nguyenlieu.site/api/public/api/v1/card/store";
				var reqParams = new RequestParams();
				reqParams["code"] = card;
				string datapost = this.autoRequest.PostToStoreData(url, reqParams);
				string status = Regex.Match(datapost, "\"status\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(status))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		private string GetData(xNet.HttpRequest http, string url, string cookie = null, string userArgent = "", string wproxyport = null)
		{
			string result;
			try
			{
				if (http == null)
				{
					http = new xNet.HttpRequest
					{
						Cookies = new CookieDictionary(false)
					};
				}
				if (!string.IsNullOrEmpty(cookie))
				{
					this.AddCookie(http, cookie);
				}
				if (!string.IsNullOrEmpty(userArgent))
				{
					http.UserAgent = userArgent;
				}
				else
				{
					http.UserAgent = this._userAgent;
				}

				if (_use_proxy)
				{
					string port = _proxy_port;
                    if (!string.IsNullOrEmpty(wproxyport))
                    {
						port = wproxyport;
					}
					string _proxy_str = _proxy_host + ":" + port;
					if (_proxy_type == "socks5")
					{
						http.Proxy = Socks5ProxyClient.Parse(_proxy_str);
					}
					if (_proxy_type == "http")
					{
						http.Proxy = ProxyClient.Parse(ProxyType.Http, _proxy_str);
					}
				}

				http.ConnectTimeout = 120000;

				http.EnableEncodingContent = true;

				http.AddHeader("accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
				http.AddHeader("accept-language", "en-US,en;q=0.9");
				http.AddHeader("sec-ch-ua", "\"Chromium\";v=\"94\", \"Google Chrome\";v=\"94\", \";Not A Brand\";v=\"99\"");
				http.AddHeader("sec-ch-ua-mobile", "?0");
				http.AddHeader("sec-ch-ua-platform", "\"Windows\"");
				http.AddHeader("sec-fetch-dest", "document");
				http.AddHeader("sec-fetch-mode", "navigate");
				http.AddHeader("sec-fetch-site", "none");
				http.AddHeader("sec-fetch-user", "?1");
				http.AddHeader("upgrade-insecure-requests", "1");

				result = http.Get(url, null).ToString();
			}
			catch (Exception ex2)
			{
				result = ex2.Message;
			}
			return result;
		}

		private void AddCookie(xNet.HttpRequest http, string cookie)
		{
			string[] array = cookie.Split(new char[]
			{
				';'
			});
			for (int i = 0; i < array.Length; i++)
			{
				string[] array2 = array[i].Split(new char[]
				{
					'='
				});
				if (array2.Count<string>() > 1)
				{
					try
					{
						http.Cookies.Add(array2[0], array2[1]);
					}
					catch
					{
					}
				}
			}
		}

		private string PostData(xNet.HttpRequest http, string url, string data = null, string contentType = null, string userArgent = "", string cookie = null, string lsd = null, string friendly = null, string referrer = null, bool wproxy = false)
		{
			string result;
			try
			{
				if (http == null)
				{
					http = new xNet.HttpRequest
					{
						Cookies = new CookieDictionary(false)
					};
				}
				if (!string.IsNullOrEmpty(cookie))
				{
					this.AddCookie(http, cookie);
				}
				if (string.IsNullOrEmpty(userArgent))
				{
					http.UserAgent = this._userAgent;
				}
				else
				{
					http.UserAgent = userArgent;
				}

				if (string.IsNullOrEmpty(contentType))
				{
					contentType = this._contentType;
				}

				if (_use_proxy)
				{
					string _proxy_str = _proxy_host + ":" + _proxy_port;
					if (_proxy_type == "socks5")
					{
						http.Proxy = Socks5ProxyClient.Parse(_proxy_str);
					}
					if (_proxy_type == "http")
					{
						http.Proxy = ProxyClient.Parse(ProxyType.Http, _proxy_str);
					}
				}

                http.AddHeader("accept-language", "en-US,en;q=0.9");
				http.AddHeader("sec-ch-ua", "\"Chromium\";v=\"94\", \"Google Chrome\";v=\"94\", \";Not A Brand\";v=\"99\"");
				http.AddHeader("sec-ch-ua-mobile", "?0");
				http.AddHeader("sec-ch-ua-platform", "\"Windows\"");
				http.AddHeader("sec-fetch-dest", "empty");
				http.AddHeader("sec-fetch-mode", "cors");
				http.AddHeader("sec-fetch-site", "same-origin");
				if (!string.IsNullOrEmpty(lsd))
                {
					http.AddHeader("x-fb-lsd", lsd);
				}

                if (!string.IsNullOrEmpty(friendly))
                {
					http.AddHeader("x-fb-friendly-name", friendly);
				}

                if (!string.IsNullOrEmpty(referrer))
                {
					http.Referer = referrer;
				}

				result = http.Post(url, data, contentType).ToString();
			}
			catch (Exception ex)
			{
				this.log(ex);
				result = ex.Message;
			}
			return result;
		}

		public bool shareAdsToBmByAPI(string bmcode, NLBm bm)
		{
			try
			{
				shareToBmErrCode = "";

				if (string.IsNullOrEmpty(_ads_id))
				{
					this.log("Ads id empty!");
					return false;
				}

				switchRegTabHandle();

				if (this.autoRequest.checkHasAds(bmcode, _ads_id))
				{
					this.log("Ads đã share!");
					return true;
				}

				bool pending = true;
				int ChkLoadingIndex = 1;
				while (pending && (ChkLoadingIndex <= 5))
				{
					pending = this.checkBMFullPendingReq(bmcode);
					if (pending)
					{
						this.log("PENDING ReQ = 2 ..., đợi chút... !");
						Delay(5000);
					}
					ChkLoadingIndex++;
				}

				string postData = this.autoRequest.postBmAddClientAds(bmcode, _ads_id);
				string statusRq = Regex.Match(postData, "\"access_status\":\"(.*?)\"").Groups[1].Value;
				if (statusRq != "PENDING")
				{
					string error_subcode = Regex.Match(postData, "\"error_subcode\":(.*?),").Groups[1].Value;
					if ((error_subcode == "1752207") || (error_subcode == "1752272"))
					{
						shareToBmErrCode = "1752207";
						this.log("PENDING ReQ = 2! Hãy hủy bớt PENDING ReQ và chạy lại!");
					}
					else
					{
						if (!this._driver.Url.Contains("facebook.com/checkpoint"))
						{
							shareToBmErrCode = "change_bm_link";
						}
					}
					this.log("BM request false!");
					return false;
				}

                string url = "https://www.facebook.com/ads/manager/account_settings/information/?act=" + _ads_id + "&pid=p1&page=account_settings&tab=account_information";
                string rfUrl = "https://www.facebook.com/ads/manager/account_settings/information/?act=" + _ads_id + "&pid=p1&page=account_settings&tab=account_information";
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);

				// check loading
				string loaddingst = "//*[@role='progressbar']/svg";
				bool loading = this.waitLoadingAction(loaddingst);

				string respondRequestBtn = "a[href*='adaccount/agency/accept_reject_dialog']";
				WaitAjaxLoading(By.CssSelector(respondRequestBtn), 45);
				ReadOnlyCollection<IWebElement> respondRequestBtnObj = this._driver.FindElements(By.CssSelector(respondRequestBtn));
				if (respondRequestBtnObj.Count == 0)
				{
					this.log("Respond Request Not F!");
					cancelShareAdToBm(bmcode, _ads_id);
					return false;
				}

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				string async_get_token = Regex.Match(getdata, "\"async_get_token\":\"(.*?)\"").Groups[1].Value;

				string ad_market_id = Regex.Match(getdata, "ad_market_id=(.*?)&").Groups[1].Value;
				if (string.IsNullOrEmpty(ad_market_id))
				{
					this.log("NOT ad_market_id!");
					cancelShareAdToBm(bmcode, _ads_id);
					return false;
				}

				// accept_reject_dialog
				url = "https://www.facebook.com/adaccount/agency/accept_reject_dialog/?ad_market_id=" + ad_market_id + "&agency_id=" + bm.bm_id + "&fb_dtsg_ag=" + async_get_token + "&__user=" + _clone_uid + 
					"&__a=1&__dyn=7xeUmBz8aolJ28S2q3m9U8EJ4Wqxu6E9E4a6oF1eFGxK5FEG48corxebJ2ocWAAzpoixWE-bwWxeEixKdwJz-4dxe488o8ogwqoqyoyazoO4o463a2G3a4EuCx62a2q5E89EeUryFEdUmKFpobQUTwMQm9xe2dum3mbK6UC4F87y744FAcK2y5oeEjx63K2y11xnzoO9ws8nxS1Lz84aVpUuyUd88EeAUy4bxS11DwFg94bxR12ewzwAwRyUszUeUmwnHxJxK48G2q4kUy26U8U-7EjwjovCxeq4o882sAwLzUS2W2K4E6-bxu3ydCgqw-z8c8-5aDBxSaBwKG3qcy85i4oKqbDyoOEbVEHyU8U3ywsU" +
					"&__csr=&__req=k&__hs=18992.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1004899887&__s=9t79mi%3Ampp9wm%3A7o31y4&__hsi=7047827912217829079-0&__comet_req=0&jazoest=24937&__spin_r=1004899887&__spin_b=trunk&__spin_t=1640950309";
				string getData = this.fetchGet(url);
				string hash = Regex.Match(getData, "operation=0&amp;ext=(.*?)&amp;hash=(.*?)\\\\").Groups[2].Value;
				string ext = Regex.Match(getData, "operation=0&amp;ext=(.*?)&amp;hash=(.*?)\\\\").Groups[1].Value;

                if (string.IsNullOrEmpty(hash))
                {
					this.log("NOT hash + ext!");
					cancelShareAdToBm(bmcode, _ads_id);
					return false;
				}

				url = "https://www.facebook.com/adaccount/agency/request/accept_reject/?ad_market_id=" + ad_market_id + "&agency_id=" + bm.bm_id + "&operation=0&ext=" + ext + "&hash=" + hash +
					"&fb_dtsg_ag=" + async_get_token + "&__user=" + _clone_uid + 
					"&__a=1&__dyn=7xeUmBz8aolJ28S2q3m9U8EJ4Wqxu6E9E4a6oF1eFGxK5FEG48corxebJ2ocWAAzpoixWE-bwWxeEixKdwJz-4dxe488o8ogwqoqyoyazoO4o463a2G3a4EuCx62a2q5E89EeUryFEdUmKFpobQUTwMQm9xe2dum3mbK6UC4F87y744FAcK2y5oeEjx63K2y11xnzoO9ws8nxS1Lz84aVpUuyUd88EeAUy4bxS11DwFg94bxR12ewzwAwRyUszUeUmwnHxJxK48G2q4kUy26U8U-7EjwjovCxeq4o882sAwLzUS2W2K4E6-bxu3ydCgqw-z8c8-5aDBxSaBwKG3qcy85i4oKqbDyoOEbVEHyU8U3ywsU" +
					"&__csr=&__req=k&__hs=18992.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1004899887&__s=9t79mi%3Ampp9wm%3A7o31y4&__hsi=7047827912217829079-0&__comet_req=0&jazoest=24937&__spin_r=1004899887&__spin_b=trunk&__spin_t=1640950309";
				getData = this.fetchShareAds(url, rfUrl, token2);

				if (!this.autoRequest.checkHasAds(bmcode, _ads_id))
				{
					this.log("NOT check data!");
					cancelShareAdToBm(bmcode, _ads_id);
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private string fetchShareAds(string url, string referrer, string lsd)
		{
			try
			{
				string sec = "\\\"Chromium\\\";v=\\\"94\\\", \\\"Google Chrome\\\";v=\\\"94\\\", \\\";Not A Brand\\\";v=\\\"99\\\"";
				string win = "\\\"Windows\\\"";


				string lsdStr = "";
				if (!string.IsNullOrEmpty(lsd))
				{
					lsdStr = "\"x-fb-lsd\": \"" + lsd + "\"";
				}

				string referrerStr = "";
				if (!string.IsNullOrEmpty(referrer))
				{
					referrerStr = "\"referrer\": \"" + referrer + "\",";
				}

				string jsstr = "let response = await fetch(\"" + url + "\", {" +
									"\"headers\": {" +
										"\"accept\": \"*/*\"," +
										"\"accept-language\": \"en-US,en;q=0.9\"," +
										"\"sec-ch-ua\": \"" + sec + "\"," +
										"\"sec-ch-ua-mobile\": \"?0\"," +
										"\"sec-ch-ua-platform\": \"" + win + "\"," +
										"\"sec-fetch-dest\": \"empty\"," +
										"\"sec-fetch-mode\": \"cors\"," +
										"\"sec-fetch-site\": \"same-origin\"," +
										"\"viewport-width\": \"1920\"," +
										lsdStr +
									"}," + referrerStr +
									"\"referrerPolicy\": \"origin-when-cross-origin\"," +
									"\"body\": null," +
									"\"method\": \"GET\"," +
									"\"mode\": \"cors\"," +
									"\"credentials\": \"include\"" +
								"}); return await response.text();";

				return (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
			}
			catch (Exception ex)
			{
				this.log(ex);
				return null;
			}
		}

		public bool createProfile(string pageNamePr)
		{
			try
			{
				string rfurl = "https://www.facebook.com/pages/creation/?ref_type=launch_point";
				if (!this._driver.Url.Contains("facebook.com"))
				{
					this._driver.Navigate().GoToUrl(rfurl);
					WaitLoading();
					Delay(1000);
				}

				_rNamePage = pageNamePr + " " + new Random().Next(100000, 999999).ToString();

				string getdata = this._driver.PageSource;
				string _fb_dtsg = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string _lsd = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string url = "https://www.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHxq1mxu1syaxG4VuC0BVU98nwgU765QdwSwAyU8EW1twYwJyEiwsobo6u3y4o11U2nwb-q7oc81xoswIK1Rwwwg8a8465o-cwfG12wOKdwGxu782lwv89kbxS2218wc61axe3e9wlo5qfK6E7e58jwGzE8FU5e7oqBwJK2W5olwUwOzEjUlDw-wUws9ovUy2a0SEuBwJCwLyES0Io88cA0z8c84qifxe3u362-2B0oo&__csr=gX1b3Bn4YrF8QkxiirNz8D9qnbPFlWsGcsHlln8Phajm-zshZeJptFAFaAJ5iFtaF4HKFrlybW8F4qt7tpeAy5ypFlUJehoGjyQt-iRKh4H8F8iz27haUG5oyt6G48Saz4EO4p9UOUzgK4Uggyh7gtAAK2mbyqK2CjyEix658rCy8yEC2J7xW8xy9xu3ngfbxq7ohAwPAyC1pwMAwi-1UxO1sKq10Bx63a221jxGq4UC2y1TwKyrxm6E4i2m2a1nG2m0DE561lw6Nw0IFw3n80cKo02NHw26830g0sKyo0Agw0tvw1oq0m22602EG2607Uo6u0oG0vC&__req=1w&__hs=19279.HYP%3Acomet_pkg.2.1.0.2.1&dpr=1&__ccg=EXCELLENT&__rev=1006390824&__s=roccf0%3A1okzuw%3Atrg2ry&__hsi=7154279510038328993&__comet_req=15&fb_dtsg=",
					_fb_dtsg,
					"&jazoest=25278&lsd=",
					_lsd,
					"&__aaid=",
					_ads_id,
					"&__spin_r=1006390824&__spin_b=trunk&__spin_t=1665735503&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=AdditionalProfilePlusCreationMutation&variables=%7B%22input%22%3A%7B%22bio%22%3A%22%22%2C%22categories%22%3A%5B%22139225689474222%22%5D%2C%22creation_source%22%3A%22comet%22%2C%22name%22%3A%22",
					Uri.EscapeDataString(_rNamePage),
					"%22%2C%22page_referrer%22%3A%22launch_point%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%223%22%7D%7D&server_timestamps=true&doc_id=4722866874428654&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D30605361%22%5D"
				});

				string resData = this.fetchApi(url, "POST", body, "CometPageCreateMutation", _lsd, "https://www.facebook.com/pages/creation/?ref_type=launch_point");
				string profileID = Regex.Match(resData, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(profileID))
				{
					return false;
				}

				this.log("profileID: " + profileID);
				// Get page id
				string pageUrl = "https://www.facebook.com/profile.php?id=" + profileID;
				this._driver.Navigate().GoToUrl(pageUrl);
				WaitLoading();
				Delay(1000);
				string getData = this._driver.PageSource;
				pageID = Regex.Match(getData, "\"delegate_page_id\":\"(.*?)\"").Groups[1].Value;

				if (string.IsNullOrEmpty(pageID))
				{
					return false;
				}

				this.log("pageID: " + pageID);
				return true;
			}
			catch
			{
				return false;
			}
		}

		public bool createClonePage(string pageNamePr)
		{
			try
			{
				string url = "https://www.facebook.com/pages/creation/?ref_type=launch_point";
				if (!this._driver.Url.Contains("facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(1000);
				}

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				string async_get_token = Regex.Match(getdata, "\"async_get_token\":\"(.*?)\"").Groups[1].Value;

				_rNamePage = pageNamePr + " " + new Random().Next(100000, 999999).ToString();

				string body = "av=" + _clone_uid + "&__user=" + _clone_uid + "&__a=1&__dyn=7AzHJ16U9ob8ng5K8G6EjBWo2nDwAxu13wsoKbmbwSwAyUcoeU5W2Saxa1NwJwpUe8hwaG0Z82_CxS320om78c87m2210x-8wgolzUO0n2US2G3i0Boy1PwBgK7o884y0Mo4G4UcUC68f85qfK6E7e58jwGzEaE5e7oqBwJK5Umxm5oe846mdUlDw-wUwxwjFo8oy2a2-3a1PwyBwJwSyES0Io88cA&__csr=gphslf2YQB8lIntPOiNcRqNT5sYnfn9lQBHJRcgLlO4lpyirFruZVQXXJaSKZV4iGgwyHALuFRnZaTtaG8QyZbloyHJRheijXmXpDWJQnAv8FniiRGimjBJ6V968VVGh6inRyWWXV44uch4EV3Ki8y8OmhatqGCiGhEx4KcheCmFbyEC8yGylzefxp5y94qUymbWK-jah6DGClqBCUO8Gh2pfQeFUix29VUyueK5Jz8aFozxieDz8-GgHhGG4eqVk2u5pUhFbChJ4CGEBxqi6p89FUGfxuWy9lJ5yeeAzWxGFohAyom_AmiKmayGBGU9EB1TwDUizCqFF4i6AEOUQxRzbAyFbyKu4A4uu6LgjxC1mCg8Eoy8mBx-4okhEixyESm5Ugy8qDxGK7V8izp8K7poaQ2y9wGw3RU0LK8w4sw2783Dg12EdQ0tjG6F81oQ0oy0mpw1fK2e04Ho1Fo08s81T822g3Cw5Bg4B0Ih-4hxkawbiq488r80jaawWy8nz-r4ehw4Cw920kG320euwm8ga0HE7W1tG0Se0akw2C80uCDOw2ZA1RU1kU7EMhw2zrS79648elg5S4GioD5iw" +
								"&__req=20&__hs=18908.HYP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004522533&__s=9aaa4n%3Am5n0ja%3A4l2fjc&__hsi=7016556646384247452-0&__comet_req=1&fb_dtsg=" + token1 + "&jazoest=22048&lsd=" + token2 + "&__spin_r=1004522533&__spin_b=trunk&__spin_t=1633669400&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=CometPageCreateMutation&variables=%7B%22input%22%3A%7B%22categories%22%3A%5B%22200046713342752%22%5D%2C%22description%22%3A%22%22%2C%22" +
								"name%22%3A%22" + _rNamePage + "%22%2C%22publish%22%3Atrue%2C%22ref%22%3A%22launch_point%22%2C%22actor_id%22%3A%22" + _clone_uid + "%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=6015849741773814";

				string resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "CometPageCreateMutation", token2, "https://www.facebook.com/pages/creation/?ref_type=launch_point");
				pageID = Regex.Match(resData, "\"id\":\"(.*?)\"").Groups[1].Value;
				pageLink = Regex.Match(resData, "\"url\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(pageID))
				{
                    if (!string.IsNullOrEmpty(_cloneAccessToken))
                    {
						Delay(3000);
						url = "https://graph.facebook.com/v11.0/" + _clone_uid + "/facebook_pages?access_token=" + _cloneAccessToken + "&limit=25&fields=[%22category%22,%22name%22]&include_headers=false&pretty=0";
						string data = this.GetData(null, url, _cookie, this._userAgent);

						MatchCollection nameMatches = Regex.Matches(resData, "\"name\":\"(.*?)\"");
						MatchCollection idMatches = Regex.Matches(resData, "\"id\":\"(.*?)\"");

						int index = 0;
						foreach (Match match in nameMatches)
						{
							string name_str = match.Groups[1].Value;
							if (name_str == _rNamePage)
							{
								pageID = idMatches[index].Groups[1].Value;
								break;
							}

							index++;
						}

						if (string.IsNullOrEmpty(pageID))
						{
							this.log("PAGE FALSE!");
							return false;
						}
					}
                    else
                    {
						return false;
					}
				}
				this.log("pageID: " + pageID);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool changePageAvata()
		{
			try
			{
				switchRegTabHandle();

				string url = "https://www.facebook.com/" + _rNamePage.Replace(" ", "-") + "-" + pageID;
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);

				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir;
				string m_Path = dataPath + "\\DATA\\page\\avata.jpg";
				if (!File.Exists(m_Path))
				{
					this.log("NOT " + m_Path);
					return false;
				}

				if (!elmAutoFinAndJsClick("//div[(@aria-label='Update Profile Picture' or @aria-label='Update profile picture') and @role='button' and not(@aria-disabled='true')]"))
				{
					this.log("Not Update Profile Picture Btn!");
					return false;
				}

				if (!elmAutoFinAndJsClick("//div[@role='menuitem']"))
				{
					this.log("Not Edit Profile Picture!");
					return false;
				}

				string modStr = "//div[@aria-label='Update Profile Picture' and @role='dialog']";
				WaitAjaxLoading(By.XPath(modStr), 10);
				Delay(1000);

				string imgFileInputStr = "//input[contains(@accept, 'image') and @type='file']";
				WaitAjaxLoading(By.XPath(imgFileInputStr), 10);
				Delay(500);
				ReadOnlyCollection<IWebElement> imgFileInputObjs = this._driver.FindElements(By.XPath(imgFileInputStr));
				if (imgFileInputObjs.Count < 3)
				{
					this.log("Not File Input!");
					return false;
				}

				IWebElement sendFileInput = imgFileInputObjs.Last();
				Unhide(this._driver, sendFileInput);
				sendFileInput.SendKeys(m_Path);

				Delay(1000);
				// check loading
				string loaddingst = "//*[@data-visualcompletion='loading-state' and @role='progressbar']";
				bool loading = true;
				int ChkLoadingIndex = 1;
				while (loading && (ChkLoadingIndex <= 8))
				{
					Delay(3000);
					ReadOnlyCollection<IWebElement> cplLoadingObjs = this._driver.FindElements(By.XPath(loaddingst));
					if (cplLoadingObjs.Count > 0)
					{
						loading = true;
					}
					else
					{
						loading = false;
					}
					ChkLoadingIndex++;
				}
				Delay(2000);

				if (!elmAutoFinAndJsClick("//div[@aria-label='Save' and @role='button' and not(@aria-disabled='true')]"))
				{
					this.log("Not Save Btn!");
					return false;
				}

				loading = true;
				ChkLoadingIndex = 1;
				while (loading && (ChkLoadingIndex <= 8))
				{
					Delay(3000);
					ReadOnlyCollection<IWebElement> cplLoadingObjs = this._driver.FindElements(By.XPath(loaddingst));
					if (cplLoadingObjs.Count > 0)
					{
						loading = true;
					}
					else
					{
						loading = false;
					}
					ChkLoadingIndex++;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool apiEditClonePage()
		{
			try
			{
				switchRegTabHandle();
				string email = _rNamePage.Replace(" ", "").ToLower() + "%40gmail.com";

				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir;
				string m_Path = dataPath + "\\DATA\\page\\info_temp.txt";
				if (!File.Exists(m_Path))
				{
					this.log("NOT " + m_Path);
					return false;
				}

				string infoTemp = File.ReadAllText(m_Path);
				String[] infoData = infoTemp.Split('|');
				if (infoData.Count() < 4)
				{
					this.log("Page Data IVALID!");
					return false;
				}

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				bool success = true;
				// Email
				string body = string.Concat(new string[]
				{
					"av=",
					pageID,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgUb84i5QdwSwAyU8EW1twYwJyEiwsobo6u3y4o0B-q7oc81xoszU887m2210x-8wgolzUO0n2US2G3i0Boy1PwBgK7o884y0Mo4G4UcUC68f85qfK6E7e58jwGzEaE5e7oqBwJK5Umxm5oe8464-5pUfEe872m7-8wywLwOwsU8Ai2S3qazo2NwwwOg&__csr=is5ve_9iZkBd5YjtbmhaHTkQv5YDA44lmB94QCzi_Zl94iOlifi_klaGAZdGZbqlblWGKH8qHGOkFrBLAiQiVoKqFkVSHigNQuqGjhAdyu8iyoR4Qyrj_GQ4FHAyEiigyER6WV9FEDJepoF5GnK4WCzUxaXxeiiEO8BghK5EiyHBzVUC-qb-XyoshV8gACwEgy9Axm26fxy6pEhy8mizoyFueAAxqEsG7olBxu4UKcyp8K4oy5RKWzfAAzSiUgguyVbyoyiuVFAiEC4o8U-U8oG49-2t0zyKiayUa_xzyo_x21rxh1OaxWawZx64EgwKz8cUmUSQ78G9yoqxmeyAq2i59EtwnEfUsVoarxO4oCbxim4U88sxm22m9w-wyCzEOu3G0IV8nwZw9i06p87e0gK14g3dwlo2tBw10m4E0wq2d97wrE06mCc-0ne0vG8B9wiVE3VwFw5Qwp1gK2B0HggoPw26k3q684hxGhYUowuoG0jG0FE2hBAxi7UdE1iU0Sy12wDwJw4nw1nK02460hAM2ww6uwrE13Wwdq48y4E462S3u3bg&__req=m&__hs=18919.HYP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004575751&__s=l0etze%3A62ek8t%3Anl5qbk&__hsi=7020645072777264046-0&__comet_req=1&fb_dtsg=",
					token1,
					"&jazoest=22016&lsd=",
					token2,
					"&__spin_r=1004575751&__spin_b=trunk&__spin_t=1634621311&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PagesCometEditPageEmailMutation&variables=%7B%22input%22%3A%7B%22email%22%3A%22",
					email,
					"%22%2C%22end_point%22%3A%22SETTINGS_PAGE_INFO%22%2C%22entry_point%22%3A%22PAGE_SETTINGS%22%2C%22page_id%22%3A%22",
					pageID,
					"%22%2C%22actor_id%22%3A%22",
					pageID,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=3777933279001601"
				});
				string resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "PagesCometEditPageEmailMutation", token2, "https://www.facebook.com");
				string resEmail = Regex.Match(resData, "\"email\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(resEmail))
				{
					success = false;
					this.log("Not Update Email!");
				}

                // Web
                if (!string.IsNullOrEmpty(infoData[0]))
                {
					body = string.Concat(new string[] {
						"av=",
						pageID,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgUb84i5QdwSwAyU8EW1twYwJyEiwsobo6u3y4o0B-q7oc81xoszU887m2210x-8wgolzUO0n2US2G3i0Boy1PwBgK7o884y0Mo4G4UcUC68f85qfK6E7e58jwGzEaE5e7oqBwJK5Umxm5oe8464-5pUfEe872m7-8wywLwOwsU8Ai2S3qazo2NwwwOg&__csr=is5ve_9iZkBd5YjtbmhaHTkQv5YDA44lmB94QCzi_Zl94iOlifi_klaGAZdGZbqlblWGKH8qHGOkFrBLAiQiVoKqFkVSHigNQuqGjhAdyu8iyoR4Qyrj_GQ4FHAyEiigyER6WV9FEDJepoF5GnK4WCzUxaXxeiiEO8BghK5EiyHBzVUC-qb-XyoshV8gACwEgy9Axm26fxy6pEhy8mizoyFueAAxqEsG7olBxu4UKcyp8K4oy5RKWzfAAzSiUgguyVbyoyiuVFAiEC4o8U-U8oG49-2t0zyKiayUa_xzyo_x21rxh1OaxWawZx64EgwKz8cUmUSQ78G9yoqxmeyAq2i59EtwnEfUsVoarxO4oCbxim4U88sxm22m9w-wyCzEOu3G0IV8nwZw9i06p87e0gK14g3dwlo2tBw10m4E0wq2d97wrE06mCc-0ne0vG8B9wiVE3VwFw5Qwp1gK2B0HggoPw26k3q684hxGhYUowuoG0jG0FE2hBAxi7UdE1iU0Sy12wDwJw4nw1nK02460hAM2ww6uwrE13Wwdq48y4E462S3u3bg&__req=10&__hs=18919.HYP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004575751&__s=o8mbab%3A62ek8t%3Anl5qbk&__hsi=7020645072777264046-0&__comet_req=1&fb_dtsg=",
						token1,
						"&jazoest=22016&lsd=",
						token2,
						"&__spin_r=1004575751&__spin_b=trunk&__spin_t=1634621311&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PagesCometEditPageWebsiteMutation&variables=%7B%22input%22%3A%7B%22end_point%22%3A%22SETTINGS_PAGE_INFO%22%2C%22entry_point%22%3A%22PAGE_SETTINGS%22%2C%22page_id%22%3A%22",
						pageID,
						"%22%2C%22url%22%3A%22",
						infoData[0],
						"%22%2C%22actor_id%22%3A%22",
						pageID,
						"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=3702761179847217"
					});
					resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "PagesCometEditPageWebsiteMutation", token2, "https://www.facebook.com");
					string resUrl = Regex.Match(resData, "\"url\":\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(resUrl))
					{
						success = false;
						this.log("Not Update Web!");
					}
				}

				// Address
				if (!string.IsNullOrEmpty(infoData[1]))
                {
					body = string.Concat(new string[]
					{
						"av=",
						pageID,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgUb84i5QdwSwAyU8EW1twYwJyEiwsobo6u3y4o0B-q7oc81xoszU887m2210x-8wgolzUO0n2US2G3i0Boy1PwBgK7o884y0Mo4G4UcUC68f85qfK6E7e58jwGzEaE5e7oqBwJK5Umxm5oe8464-5pUfEe872m7-8wywLwOwsU8Ai2S3qazo2NwwwOg&__csr=is4O9b5vtBkhkl9cI_Z9d9ax5cQONLnRl8Jq9s-Jt9Z9En4cnnn4OcAygMRdb98TJDCjmKSJQnFlAQbADjQiqnBDGAbBjnKAVaGlryQExHqBxan-RQlaQ6upzeubKWG8Keih8zxSiiFkAiGm4GKaHzpF8lz5yKE8VExe7EWA8wLUymiFpHCV8hyaG6oy9x2laax6bAwWK5e6EO4U-ryp8S9zECaGUyEG68KewPrgmAu9y9AbDz8iK4W-4k6UswCgswCzEaQCexaaL8czXRCG269xKi2m9wyJ6Ugxy2q689EfQ2iu2K2uq5-9wMjyVQ2m9AyoyewCU4Wex63O7o4W10U-bwExi366e4Vk9G9zEK1ugvxCium7EfEe8-3q0UUrw1rm3m09gxR90pE15A1tw4HxS0gy1_w7iw7pG01AXx24u0nq0uCfzsE560-Eao1u85Qle1UyA0inw4ozUnQ1zRaEjgmAw_w5Aw9a0D8gwKVUdo1iU0O21dwgE9UG0jy05pE08do1i8kxro7K09Og2uG0Re68jwgobo8o4zg&__req=j&__hs=18919.HYP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004576019&__s=08mgnx%3A62ek8t%3Adknxce&__hsi=7020654624275598766-0&__comet_req=1&fb_dtsg=",
						token1,
						"&jazoest=21976&lsd=",
						token2,
						"&__spin_r=1004576019&__spin_b=trunk&__spin_t=1634623535&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PagesCometEditPageAddressMutation&variables=%7B%22input%22%3A%7B%22city_id%22%3A%22108424279189115%22%2C%22city_name%22%3A%22New%20York%2C%20NY%22%2C%22end_point%22%3A%22SETTINGS_PAGE_INFO%22%2C%22entry_point%22%3A%22PAGE_SETTINGS%22%2C%22pageid%22%3A%22",
						pageID,
						"%22%2C%22street%22%3A%22New%20York%20University%22%2C%22zipcode%22%3A%2210000%22%2C%22actor_id%22%3A%22",
						pageID,
						"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=4206177669441381"
					});
					resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "PagesCometEditPageAddressMutation", token2, "https://www.facebook.com");
					string resFullAddress = Regex.Match(resData, "\"full_address\":\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(resFullAddress))
					{
						success = false;
						this.log("Not Update Address!");
					}
				}
				
				return success;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool apiEditBmPage()
		{
			try
			{
				switchBusinessTabHandle();

				string url = "https://business.facebook.com/" + _rNamePage.Replace(" ", "-") + "-" + pageID + "/page/info/editing/?tab=page_info";

				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);

				string email = _rNamePage.Replace(" ", "").ToLower() + "%40gmail.com";

				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir;
				string m_Path = dataPath + "\\DATA\\page\\info_temp.txt";
				if (!File.Exists(m_Path))
				{
					this.log("NOT " + m_Path);
					return false;
				}

				string infoTemp = File.ReadAllText(m_Path);
				String[] infoData = infoTemp.Split('|');
				if (infoData.Count() < 4)
				{
					this.log("Page Data IVALID!");
					return false;
				}

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				bool success = true;
				// Email
				string body = string.Concat(new string[]
				{
					"av=",
					pageID,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgUb84i5QdwSwAyU8EW1twYwJyEiwsobo6u3y4o0B-q7oc81xoszU887m2210x-8wgolzUO0n2US2G3i0Boy1PwBgK7o884y0Mo4G4UcUC68f85qfK6E7e58jwGzEaE5e7oqBwJK5Umxm5oe8464-5pUfEe872m7-8wywLwOwsU8Ai2S3qazo2NwwwOg&__csr=is5ve_9iZkBd5YjtbmhaHTkQv5YDA44lmB94QCzi_Zl94iOlifi_klaGAZdGZbqlblWGKH8qHGOkFrBLAiQiVoKqFkVSHigNQuqGjhAdyu8iyoR4Qyrj_GQ4FHAyEiigyER6WV9FEDJepoF5GnK4WCzUxaXxeiiEO8BghK5EiyHBzVUC-qb-XyoshV8gACwEgy9Axm26fxy6pEhy8mizoyFueAAxqEsG7olBxu4UKcyp8K4oy5RKWzfAAzSiUgguyVbyoyiuVFAiEC4o8U-U8oG49-2t0zyKiayUa_xzyo_x21rxh1OaxWawZx64EgwKz8cUmUSQ78G9yoqxmeyAq2i59EtwnEfUsVoarxO4oCbxim4U88sxm22m9w-wyCzEOu3G0IV8nwZw9i06p87e0gK14g3dwlo2tBw10m4E0wq2d97wrE06mCc-0ne0vG8B9wiVE3VwFw5Qwp1gK2B0HggoPw26k3q684hxGhYUowuoG0jG0FE2hBAxi7UdE1iU0Sy12wDwJw4nw1nK02460hAM2ww6uwrE13Wwdq48y4E462S3u3bg&__req=m&__hs=18919.HYP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004575751&__s=l0etze%3A62ek8t%3Anl5qbk&__hsi=7020645072777264046-0&__comet_req=1&fb_dtsg=",
					token1,
					"&jazoest=22016&lsd=",
					token2,
					"&__spin_r=1004575751&__spin_b=trunk&__spin_t=1634621311&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PagesCometEditPageEmailMutation&variables=%7B%22input%22%3A%7B%22email%22%3A%22",
					email,
					"%22%2C%22end_point%22%3A%22SETTINGS_PAGE_INFO%22%2C%22entry_point%22%3A%22PAGE_SETTINGS%22%2C%22page_id%22%3A%22",
					pageID,
					"%22%2C%22actor_id%22%3A%22",
					pageID,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=3777933279001601"
				});
				string resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "PagesCometEditPageEmailMutation", token2, url);
				string resEmail = Regex.Match(resData, "\"email\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(resEmail))
				{
					success = false;
					this.log("Not Update Email!");
				}

				// Web
				if (!string.IsNullOrEmpty(infoData[0]))
				{
					body = string.Concat(new string[] {
						"av=",
						pageID,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgUb84i5QdwSwAyU8EW1twYwJyEiwsobo6u3y4o0B-q7oc81xoszU887m2210x-8wgolzUO0n2US2G3i0Boy1PwBgK7o884y0Mo4G4UcUC68f85qfK6E7e58jwGzEaE5e7oqBwJK5Umxm5oe8464-5pUfEe872m7-8wywLwOwsU8Ai2S3qazo2NwwwOg&__csr=is5ve_9iZkBd5YjtbmhaHTkQv5YDA44lmB94QCzi_Zl94iOlifi_klaGAZdGZbqlblWGKH8qHGOkFrBLAiQiVoKqFkVSHigNQuqGjhAdyu8iyoR4Qyrj_GQ4FHAyEiigyER6WV9FEDJepoF5GnK4WCzUxaXxeiiEO8BghK5EiyHBzVUC-qb-XyoshV8gACwEgy9Axm26fxy6pEhy8mizoyFueAAxqEsG7olBxu4UKcyp8K4oy5RKWzfAAzSiUgguyVbyoyiuVFAiEC4o8U-U8oG49-2t0zyKiayUa_xzyo_x21rxh1OaxWawZx64EgwKz8cUmUSQ78G9yoqxmeyAq2i59EtwnEfUsVoarxO4oCbxim4U88sxm22m9w-wyCzEOu3G0IV8nwZw9i06p87e0gK14g3dwlo2tBw10m4E0wq2d97wrE06mCc-0ne0vG8B9wiVE3VwFw5Qwp1gK2B0HggoPw26k3q684hxGhYUowuoG0jG0FE2hBAxi7UdE1iU0Sy12wDwJw4nw1nK02460hAM2ww6uwrE13Wwdq48y4E462S3u3bg&__req=10&__hs=18919.HYP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004575751&__s=o8mbab%3A62ek8t%3Anl5qbk&__hsi=7020645072777264046-0&__comet_req=1&fb_dtsg=",
						token1,
						"&jazoest=22016&lsd=",
						token2,
						"&__spin_r=1004575751&__spin_b=trunk&__spin_t=1634621311&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PagesCometEditPageWebsiteMutation&variables=%7B%22input%22%3A%7B%22end_point%22%3A%22SETTINGS_PAGE_INFO%22%2C%22entry_point%22%3A%22PAGE_SETTINGS%22%2C%22page_id%22%3A%22",
						pageID,
						"%22%2C%22url%22%3A%22",
						infoData[0],
						"%22%2C%22actor_id%22%3A%22",
						pageID,
						"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=3702761179847217"
					});
					resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "PagesCometEditPageWebsiteMutation", token2, url);
					string resUrl = Regex.Match(resData, "\"url\":\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(resUrl))
					{
						success = false;
						this.log("Not Update Web!");
					}
				}

				// Address
				if (!string.IsNullOrEmpty(infoData[1]))
				{
					body = string.Concat(new string[]
					{
						"av=",
						pageID,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7AzHxqU5a5Q1ryaxG4VuC0BVU98nwgUb84i5QdwSwAyU8EW1twYwJyEiwsobo6u3y4o0B-q7oc81xoszU887m2210x-8wgolzUO0n2US2G3i0Boy1PwBgK7o884y0Mo4G4UcUC68f85qfK6E7e58jwGzEaE5e7oqBwJK5Umxm5oe8464-5pUfEe872m7-8wywLwOwsU8Ai2S3qazo2NwwwOg&__csr=is4O9b5vtBkhkl9cI_Z9d9ax5cQONLnRl8Jq9s-Jt9Z9En4cnnn4OcAygMRdb98TJDCjmKSJQnFlAQbADjQiqnBDGAbBjnKAVaGlryQExHqBxan-RQlaQ6upzeubKWG8Keih8zxSiiFkAiGm4GKaHzpF8lz5yKE8VExe7EWA8wLUymiFpHCV8hyaG6oy9x2laax6bAwWK5e6EO4U-ryp8S9zECaGUyEG68KewPrgmAu9y9AbDz8iK4W-4k6UswCgswCzEaQCexaaL8czXRCG269xKi2m9wyJ6Ugxy2q689EfQ2iu2K2uq5-9wMjyVQ2m9AyoyewCU4Wex63O7o4W10U-bwExi366e4Vk9G9zEK1ugvxCium7EfEe8-3q0UUrw1rm3m09gxR90pE15A1tw4HxS0gy1_w7iw7pG01AXx24u0nq0uCfzsE560-Eao1u85Qle1UyA0inw4ozUnQ1zRaEjgmAw_w5Aw9a0D8gwKVUdo1iU0O21dwgE9UG0jy05pE08do1i8kxro7K09Og2uG0Re68jwgobo8o4zg&__req=j&__hs=18919.HYP%3Acomet_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004576019&__s=08mgnx%3A62ek8t%3Adknxce&__hsi=7020654624275598766-0&__comet_req=1&fb_dtsg=",
						token1,
						"&jazoest=21976&lsd=",
						token2,
						"&__spin_r=1004576019&__spin_b=trunk&__spin_t=1634623535&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=PagesCometEditPageAddressMutation&variables=%7B%22input%22%3A%7B%22city_id%22%3A%22108424279189115%22%2C%22city_name%22%3A%22New%20York%2C%20NY%22%2C%22end_point%22%3A%22SETTINGS_PAGE_INFO%22%2C%22entry_point%22%3A%22PAGE_SETTINGS%22%2C%22pageid%22%3A%22",
						pageID,
						"%22%2C%22street%22%3A%22New%20York%20University%22%2C%22zipcode%22%3A%2210000%22%2C%22actor_id%22%3A%22",
						pageID,
						"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=4206177669441381"
					});
					resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "PagesCometEditPageAddressMutation", token2, url);
					string resFullAddress = Regex.Match(resData, "\"full_address\":\"(.*?)\"").Groups[1].Value;
					if (string.IsNullOrEmpty(resFullAddress))
					{
						success = false;
						this.log("Not Update Address!");
					}
				}

				return success;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool setPrepaidBalance()
		{
			try
			{
				string url = "https://www.facebook.com/ads/manager/account_settings/account_billing/?act=" + _ads_id + "&pid=p1&page=account_settings&tab=account_billing_settings";
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(3000);

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string jsstr = "let response = await fetch(\"https://secure.facebook.com/ajax/payment/token_proxy.php?tpe=/api/graphql/\", {" +
								"headers:" +
								"{" +
									"\"content-type\": \"application/x-www-form-urlencoded\"" +
								"}," +
								"referrer: \"https://www.business.facebook.com/\"," +
								"body: \"av=" + _clone_uid + "&payment_dev_cycle=prod&__user=" + _clone_uid + "&__a=1&__dyn=&__req=1g&__beoa=0&__pc=PHASED:powereditor_pkg&dpr=1.5&__ccg=EXCELLENT&__rev=1002523221&__s=&__hsi=6&__comet_req=0&fb_dtsg=" + token1 + "&jazoest=22058&__spin_r=1002523221&__spin_b=trunk&__spin_t=&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingPrepayUtilsCreateStoredBalanceMutation&variables={\\\"input\\\":{\\\"client_mutation_id\\\":\\\"3\\\",\\\"actor_id\\\":\\\"" + _clone_uid + "\\\",\\\"logging_data\\\":{\\\"logging_counter\\\":21,\\\"logging_id\\\":\\\"\\\"},\\\"payment_account_id\\\":\\\"" + _ads_id + "\\\"}}&server_timestamps=true&doc_id=3138742652811181\"," +
								"method: \"POST\"," +
								"mode: \"cors\"," +
								"credentials: \"include\"" +
							"}); return await response.text();";

				string res = (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
				string ad_account = Regex.Match(res, "\"ad_account\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(ad_account))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool setBmAdPrepaidBalance()
		{
			try
			{
				switchBusinessTabHandle();

				string url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + _bm_ads_id + "&pid=p1&business_id=" + _bm_id + "&page=account_settings&tab=account_billing_settings";
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(3000);

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string jsstr = "let response = await fetch(\"https://business.secure.facebook.com/ajax/payment/token_proxy.php?tpe=/api/graphql/\", {" +
								"headers:" +
								"{" +
									"\"content-type\": \"application/x-www-form-urlencoded\"" +
								"}," +
								"referrer: \"https://www.business.facebook.com/\"," +
								"body: \"av=" + _clone_uid + "&payment_dev_cycle=prod&__user=" + _clone_uid + "&__a=1&__dyn=&__req=1g&__beoa=0&__pc=PHASED:powereditor_pkg&dpr=1.5&__ccg=EXCELLENT&__rev=1002523221&__s=&__hsi=6&__comet_req=0&fb_dtsg=" + token1 + "&jazoest=22058&__spin_r=1002523221&__spin_b=trunk&__spin_t=&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingPrepayUtilsCreateStoredBalanceMutation&variables={\\\"input\\\":{\\\"client_mutation_id\\\":\\\"3\\\",\\\"actor_id\\\":\\\"" + _clone_uid + "\\\",\\\"logging_data\\\":{\\\"logging_counter\\\":21,\\\"logging_id\\\":\\\"\\\"},\\\"payment_account_id\\\":\\\"" + _bm_ads_id + "\\\"}}&server_timestamps=true&doc_id=3138742652811181\"," +
								"method: \"POST\"," +
								"mode: \"cors\"," +
								"credentials: \"include\"" +
							"}); return await response.text();";

				string res = (string)((IJavaScriptExecutor)this._driver).ExecuteScript(jsstr);
				string ad_account = Regex.Match(res, "\"ad_account\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(ad_account))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private bool cancelSharePageToBm(string code, string page_id)
		{
			try
			{
				this.log("Cancel Share Page!");
				string postdata = this.autoRequest.cancelAddPageToBm(code, page_id);
				string statusRq = Regex.Match(postdata, "\"success\":(.*?)}").Groups[1].Value;
                if (statusRq != "true")
                {
                    this.log("Share Cancel false!");
                    return false;
                }
                return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}
		
		private bool cancelShareAdToBm(string code, string ads_id)
		{
			try
			{
                this.log("Cancel Share!");
                string postdata = this.autoRequest.cancelAddAdsToBm(code, ads_id);
                string statusRq = Regex.Match(postdata, "\"success\":(.*?)}").Groups[1].Value;
                if (statusRq != "true")
                {
                    this.log("Share Cancel false!");
                    return false;
                }

                return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool sharePageToBm(string bmcode)
		{
			try
			{
				shareToBmErrCode = "";

				if (string.IsNullOrEmpty(pageID))
                {
                    this.log("Không có Page Id!");
                    return false;
                }

				switchRegTabHandle();

				if (this.autoRequest.checkHasPage(bmcode, pageID))
				{
					this.log("Page đã share!");
					return true;
				}

				// check PendingReq
				bool pending = true;
				int ChkLoadingIndex = 1;
				while (pending && (ChkLoadingIndex <= 5))
				{
					pending = this.checkBMFullPendingReq(bmcode);
                    if (pending)
                    {
						this.log("PENDING ReQ = 2 ..., đợi chút... !");
						Delay(5000);
					}
					ChkLoadingIndex++;
				}

				// post rq
				string postdata = this.autoRequest.postBmAddClientPage(bmcode, pageID);
				string statusRq = Regex.Match(postdata, "\"access_status\":\"(.*?)\"").Groups[1].Value;
				this.log("statusRq: " + statusRq);
				if (statusRq != "PENDING")
				{
					string error_subcode = Regex.Match(postdata, "\"error_subcode\":(.*?),").Groups[1].Value;
					if ((error_subcode == "1752207") || (error_subcode == "1752272"))
					{
						shareToBmErrCode = "1752207";
						this.log("PENDING ReQ = 2! Hãy hủy bớt PENDING ReQ và chạy lại!");
					}
                    else
                    {
						if (error_subcode != "1752041")
                        {
							if (!this._driver.Url.Contains("facebook.com/checkpoint"))
							{
								shareToBmErrCode = "change_bm_link";
								this.log(shareToBmErrCode);
							}
						}
					}

					if (error_subcode != "1752041")
					{
						this.log(postdata);
						this.log("BM request false!");
						return false;
					}
				}

				string url = "https://www.facebook.com/" + _rNamePage.Replace(" ", "-") + "-" + pageID + "/settings/?tab=admin_roles";
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);

				WaitAjaxLoading(By.CssSelector("iframe[src*='" + _rNamePage.Replace(" ", "-") + "-" + pageID + "']"), 15);
				ReadOnlyCollection<IWebElement> iFrame = this._driver.FindElements(By.CssSelector("iframe[src*='" + _rNamePage.Replace(" ", "-") + "-" + pageID + "']"));
				if (iFrame.Count == 0)
				{
					this.log("iFrame Not F!");
					cancelSharePageToBm(bmcode, pageID);
					return false;
				}

				this._driver.SwitchTo().Frame(iFrame.First());
				WaitAjaxLoading(By.CssSelector("a[src*='request_id=']"), 20);
				Delay(1000);
				string getiFramedata = this._driver.PageSource;
				this._driver.SwitchTo().DefaultContent();

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				string cquick = Regex.Match(getdata, "cquick=(.*?)&").Groups[1].Value;
				string cquick_token = Regex.Match(getdata, "cquick_token=(.*?)&").Groups[1].Value;

				string requestId = Regex.Match(getiFramedata, "request_id=(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(requestId))
				{
					this.log("NOT requestId!");
					return false;
				}

				url = "https://www.facebook.com/" + pageID + "/edit_page_admins/?action=accept_request&admin_id=" + requestId + "&admin_type=pending_proxy";
				string datarq = string.Concat(new string[]
				{
					"__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8aolJ28S1syU8EJ4Wodo9E4a2i5Uf9E6C4UKewPGi4FoixW4E4q5EbES2S4og-2i13x21FxG7oScx611wnocEixWq4o4C5E89EeUrwobGbwQzocEiwTgfUK2C0A8swEK1VwwAwXx-8wgolzUO9ws82Bz84aVpUaEd88EeAUy58twsU9kbxS4oW18wRwEwoE5WUeogwNxq8wio-7EjwrpEjCx60Lod8twKwHxa1oxqbxu3ydwyw-z8c8-5awSBwKG1cwl8hyVEjyo-2-qaUK0gq&__csr=&__req=9&__hs=18908.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1004523054&__s=0vkeb6%3Ay26ukz%3Aph5806&__hsi=7016629232517407617-0&__comet_req=0&cquick=",
					cquick,
					"&cquick_token=",
					cquick_token,
					"&ctarget=https%3A%2F%2Fwww.facebook.com&fb_dtsg=",
					token1,
					"&jazoest=21959&lsd=",
					token2,
					"&__spin_r=1004523054&__spin_b=trunk&__spin_t=1633686252"
				});
				string referrer = url + "&cquick=" + cquick + "&cquick_token=" + cquick_token + "&ctarget=https%3A%2F%2Fwww.facebook.com";
				postdata = this.fetchApi(url, "POST", datarq, "", token2, referrer);

				string check = Regex.Match(postdata, "\"lid\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(check))
				{
					this.log("accept_request FALSE!");
					return false;
				}

				url = "https://www.facebook.com/presma/admin_roles/existing_roles/mutate/";
				datarq = string.Concat(new string[]
				{
					"page_id=",
					pageID,
					"&proxy_or_business_id=",
					requestId,
					"&mutation_type=ACCEPT_PROXY&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8aolJ28S1syU8EJ4Wodo9E4a2i5Uf9E6C4UKewPGi4FoixW4E4q5EbES2S4og-2i13x21FxG7oScx611wnocEixWq4o4C5E89EeUrwobGbwQzocEiwTgfUK2C0A8swEK1VwwAwXx-8wgolzUO9ws82Bz84aVpUaEd88EeAUy58twsU9kbxS4oW18wRwEwoE5WUeogwNxq8wio-7EjwrpEjCx60Lod8twKwHxa1oxqbxu3ydwyw-z8c8-5awSBwKG1cwl8hyVEjyo-2-qaUK0gq&__csr=&__req=a&__hs=18908.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=GOOD&__rev=1004523054&__s=0vkeb6%3Ay26ukz%3Aph5806&__hsi=7016629232517407617-0&__comet_req=0&cquick=",
					cquick,
					"&cquick_token=",
					cquick_token,
					"&ctarget=https%3A%2F%2Fwww.facebook.com&fb_dtsg=",
					token1,
					"&jazoest=21959&lsd=",
					token2,
					"&__spin_r=1004523054&__spin_b=trunk&__spin_t=1633686252&ajax_password=",
					_clone_pass,
					"&confirmed=1"
				});
				postdata = this.fetchApi(url, "POST", datarq, "", token2, referrer);

				check = Regex.Match(postdata, "\"lid\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(check))
				{
					this.log("mutate FALSE!");
				}

				// Check page
				if (!this.autoRequest.checkHasPage(bmcode, pageID))
				{
					this.log("SHARE FALSE!");
					cancelSharePageToBm(bmcode, pageID);
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private void log(string s)
		{
			bool flag = this._logDelegate != null;
			if (flag)
			{
				this._logDelegate("Thread " + this._ThreadName + "->" + s);
			}
		}

		private void log(Exception ex)
		{
			bool flag = this._logDelegate != null;
			if (flag)
			{
				this._logDelegate(string.Concat(new string[]
				{
					"Thread ",
					this._ThreadName,
					"->",
					ex.Message,
					"\n",
					ex.StackTrace
				}));
			}
		}

		private void clearWebField(IWebElement element)
		{
			while (!element.GetAttribute("value").Equals(""))
			{
				element.SendKeys(OpenQA.Selenium.Keys.End);
				element.SendKeys(OpenQA.Selenium.Keys.Backspace);
			}
		}

		public async Task<bool> addContactBanking()
		{
			bool res = false;
			await Task.Run(async delegate ()
			{
				try
				{
					HttpClient _client = new HttpClient();
					string text = await _client.GetStringAsync("https://fake-it.ws/de/");
					string _rs = text;
					text = null;
					string _tmpName = Regex.Match(_rs, "row..Name[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
					_tmpName = _tmpName.Replace("row\">Name</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
					this.log(_tmpName);
					string _address = Regex.Match(_rs, "row..Address[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
					_address = _address.Replace("row\">Address</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
					this.log(_address);
					string _city = Regex.Match(_rs, "row..City[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
					_city = _city.Replace("row\">City</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
					this.log(_city);
					string _postCode = Regex.Match(_rs, "row..Postcode[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
					_postCode = _postCode.Replace("row\">Postcode</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
					this.log(_postCode);
					string _iban = Regex.Match(_rs, "id..iban.{10,200}[/]span", RegexOptions.Singleline).ToString();
					_iban = _iban.Replace("</span></span", "").Trim().Replace("id=\"iban\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "");
					this.log(_iban);
					string _BIC = Regex.Match(_rs, "row..BIC[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
					_BIC = _BIC.Replace("row\">BIC</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
					this.log(_BIC);

					// Open link
					string url = "https://business.facebook.com/help/contact/649167531904667?ref=4";
					this._driver.Navigate().GoToUrl(url);
					Delay(1000);
					WaitLoading();

					string selectAdAccountStr = "//div[contains(@class, 'uiPopover')]/a[@role='button']/span[contains(@class, '55pe')]";
					WaitAjaxLoading(By.XPath(selectAdAccountStr));
					ReadOnlyCollection<IWebElement> selectAdAccountObjs = this._driver.FindElements(By.XPath(selectAdAccountStr));
					if (selectAdAccountObjs.Count > 0)
					{
						selectAdAccountObjs.First().Click();
						string optionAdAccountStr = "//span[contains(@class, '54nh')]";
						WaitAjaxLoading(By.XPath(optionAdAccountStr));
						ReadOnlyCollection<IWebElement> optionAdAccountObjs = this._driver.FindElements(By.XPath(optionAdAccountStr));
						if (optionAdAccountObjs.Count > 0)
						{
							optionAdAccountObjs[1].Click();

							string radPaymentMethodStr = "//label[contains(@class, 'iInputLabelLabel')]";
							WaitAjaxLoading(By.XPath(radPaymentMethodStr));
							ReadOnlyCollection<IWebElement> radPaymentMethodObjs = this._driver.FindElements(By.XPath(radPaymentMethodStr));
							if (radPaymentMethodObjs.Count > 0)
							{
								radPaymentMethodObjs[2].Click();
								Delay(1000);
								radPaymentMethodObjs[11].Click();

								// ads/payments/support/add_payment_method
								string addPaymentBtn = "a[href*='ads/payments/support/add_payment_method']";
								WaitAjaxLoading(By.CssSelector(addPaymentBtn));
								ReadOnlyCollection<IWebElement> btnAddPayment = this._driver.FindElements(By.CssSelector(addPaymentBtn));
								if (btnAddPayment.Count > 0)
								{
									btnAddPayment.First().Click();
									Delay(5000);

									string new_direct_debit_v2_input_str = "//label[@for='new_direct_debit_v2_input']";
									WaitAjaxLoading(By.XPath(new_direct_debit_v2_input_str), 30);
									Delay(1000);
									ReadOnlyCollection<IWebElement> new_direct_debit_v2_input = this._driver.FindElements(By.XPath(new_direct_debit_v2_input_str));
									if (new_direct_debit_v2_input.Count > 0)
									{
										new_direct_debit_v2_input.First().Click();
										Delay(500);

										string btnContinueStr = "//button[contains(@class, 'layerConfirm') and @type='submit']";
										ReadOnlyCollection<IWebElement> btnContinueObjs = this._driver.FindElements(By.XPath(btnContinueStr));
										if (btnContinueObjs.Count > 0)
										{
											btnContinueObjs.First().Click();
											Delay(1000);
											WaitLoading();
											string inputTagName = "//input[@type='text']";
											WaitAjaxLoading(By.XPath(inputTagName), 30);
											Delay(3000);
											ReadOnlyCollection<IWebElement> inputs = this._driver.FindElements(By.XPath(inputTagName));
											if (inputs.Count > 5)
											{
												inputs[3].Click();
												inputs[3].SendKeys(_tmpName);
												Delay(500);
												inputs[4].Click();
												inputs[4].SendKeys(_iban);
												Delay(500);
												inputs[5].Click();
												inputs[5].SendKeys(_BIC);
												Delay(500);
												inputs[6].Click();
												inputs[6].SendKeys(_address);
												Delay(500);
												inputs[7].Click();
												inputs[7].SendKeys(_city);
												Delay(500);
												inputs[8].Click();
												inputs[8].SendKeys(_postCode);
												Delay(500);
												// checkbox
												string inputAccountName = "//input[@type='checkbox']";
												WaitAjaxLoading(By.XPath(inputAccountName));
												Delay(1000);
												IWebElement input = this._driver.FindElement(By.XPath(inputAccountName));
												input.FindElement(By.XPath("..")).Click();
											}
											else
											{
												this.log("Not Banking Form!");
												return;
											}

											string AdsPaymentsDirectDebitButton_str = "AdsPaymentsDirectDebitButton";
											WaitAjaxLoading(By.Id(AdsPaymentsDirectDebitButton_str));
											IWebElement AdsPaymentsDirectDebitButton = this._driver.FindElement(By.Id(AdsPaymentsDirectDebitButton_str));
											AdsPaymentsDirectDebitButton.Click();

											res = true;
											return;
										}
									}
								}
							}
						}
					}
				}
				catch (Exception ex2)
				{
					this.log(ex2);
					return;
				}
			});

			this.log("End banking!");
			return res;
		}

		public bool apiAddBanking(string country_code, string currency_code)
		{
			try
			{
				switchRegTabHandle();
				HttpClient _client = new HttpClient();
				string text = _client.GetStringAsync("https://fake-it.ws/de/").Result;
				string _rs = text;
				text = null;
				string _tmpName = Regex.Match(_rs, "row..Name[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
				_tmpName = _tmpName.Replace("row\">Name</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
				this.log(_tmpName);
				string _address = Regex.Match(_rs, "row..Address[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
				_address = _address.Replace("row\">Address</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
				this.log(_address);
				string _city = Regex.Match(_rs, "row..City[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
				_city = _city.Replace("row\">City</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
				this.log(_city);
				string _postCode = Regex.Match(_rs, "row..Postcode[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
				_postCode = _postCode.Replace("row\">Postcode</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
				this.log(_postCode);
				string _iban = Regex.Match(_rs, "id..iban.{10,200}[/]span", RegexOptions.Singleline).ToString();
				_iban = _iban.Replace("</span></span", "").Trim().Replace("id=\"iban\" data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "");
				this.log(_iban);
				string _BIC = Regex.Match(_rs, "row..BIC[<][/]th[>].{10,200}[/]span", RegexOptions.Singleline).ToString();
				_BIC = _BIC.Replace("row\">BIC</th>", "").Trim().Replace("<td class=\"copy\"><span data-toggle=\"tooltip\" data-placement=\"top\" title=\"Click To Copy\">", "").Replace("</span", "");
				this.log(_BIC);

				string url = "https://www.facebook.com/ads/manager/account_settings/account_billing/?act=" + _bm_ads_id + "&pid=p1&business_id=" + _bm_id + "&page=account_settings&tab=account_billing_settings";
				string referrer = url;
				if (!this._driver.Url.Contains("ads/manager/account_settings"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string context_id = _clone_uid;
				this.log("Context_id: " + context_id);

				string adId = _ads_id;

				// Change country
				string gurl = "https://business.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					context_id,
					"&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqwOwCwgE98kGqErxSax2366UjyUW3qiidBxa7GzUK1exm6US2SfUkxe488o8ogw8i9y8G6Ehwt8aE4m1qwCwuE9FEdUmCBBwLghUcd5xS2dum11K6UC4F87y744FA48a8lwWxe4oeUa85vzoO0AE2qwgHzouwg85WUpwxwgum2B0Agak48W2e2i11xOfwXxq1uxZxK48G2q4kUy26U8U-7Ejwjo9Ejxy220D98fUS2W2K4E7K1uCgqw8O58Gm7o7CcwnohxabDyoOEdEGcgjgW0UE7e19w&__csr=&__req=13&__hs=19016.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004968841&__s=f8ihyb%3Aftbrln%3A0ve61w&__hsi=7056708312450659488-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=21845&lsd=",
					lsd,
					"&__spin_r=1004968841&__spin_b=trunk&__spin_t=1643017938&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					adId,
					"%22%2C%22currency%22%3A%22",
					currency_code,
					"%22%2C%22logging_data%22%3A%7B%22logging_counter%22%3A10%2C%22logging_id%22%3A%222852959273%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22%22%2C%22country_code%22%3A%22",
					country_code,
					"%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22second_tax_id%22%3A%22%22%2C%22second_tax_id_type%22%3Anull%2C%22tax_exempt%22%3Afalse%2C%22tax_id%22%3A%22%22%2C%22tax_id_type%22%3A%22NONE%22%2C%22eu_vat_tax_country%22%3A%22DE%22%7D%2C%22timezone%22%3Anull%2C%22actor_id%22%3A%22",
					context_id,
					"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=4699960830024588"
				});

				string postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
				string business_country_code = Regex.Match(postData, "\"business_country_code\":\"(.*?)\"").Groups[1].Value;
				string currency = Regex.Match(postData, "\"currency\":\"(.*?)\"").Groups[1].Value;

				if ((country_code != business_country_code) || (currency_code != currency))
				{
					this.log("Change country FALSE!");
					// return false;
				}
				else
				{
					this.log("Change country TRUE!");
				}

				// Add
				gurl = "https://secure.facebook.com/ajax/payment/token_proxy.php?tpe=%2Fapi%2Fgraphql%2F";
				referrer = "https://www.facebook.com/";
				body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&payment_dev_cycle=prod&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqwOwCwgE98kGqErxSax2366UjyUW3qiidBxa7GzUK1exm6US2SfUkxe488o8ogw8i9y8G6Ehwt8aE4m1qwCwuE9FEdUmCBBwLghUcd5xS2dum11K6UC4F87y744FA48a8lwWxe4oeUa85vzoO0AE2qwgHzouwg85WUpwxwgum2B0Agak48W2e2i11xOfwXxq1uxZxK48G2q4kUy26U8U-7Ejwjo9Ejxy220D98fUS2W2K4E7K1uCgqw8O58Gm7o7CcwnohxabDyoOEdEGcgjgW0UE7e19w&__req=1c&__hs=19016.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004968841&__s=f8ihyb%3Aftbrln%3A0ve61w&__hsi=7056708312450659488-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=21845&lsd=",
					lsd,
					"&__spin_r=1004968841&__spin_b=trunk&__spin_t=1643017938&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingDirectDebitSEPAMutation&variables=%7B%22input%22%3A%7B%22account_holder_name%22%3A%22",
					_tmpName,
					"%22%2C%22account_number_last_four%22%3A%22",
					_iban.Substring(_iban.Length - 4),
					"%22%2C%22approval%22%3Atrue%2C%22bank_account_number%22%3A%7B%22sensitive_string_value%22%3A%22",
					_iban,
					"%22%7D%2C%22bank_address%22%3A%7B%22city%22%3A%22",
					_city,
					"%22%2C%22country_code%22%3A%22",
					country_code,
					"%22%2C%22postal_code%22%3A%22",
					_postCode,
					"%22%2C%22street_address%22%3A%22",
					_address,
					"%22%7D%2C%22bank_code%22%3A%22",
					_BIC,
					"%22%2C%22geo_type%22%3A%22SEPA%22%2C%22logging_data%22%3A%7B%22logging_counter%22%3A25%2C%22logging_id%22%3A%222852959273%22%7D%2C%22payment_account_id%22%3A%22",
					adId,
					"%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%223%22%7D%7D&server_timestamps=true&doc_id=2759481864176536"
				});

				postData = this.fetchApi(gurl, "POST", body, "useBillingDirectDebitSEPAMutation", "", referrer);
				string direct_debit_id = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;

				if (string.IsNullOrEmpty(direct_debit_id))
				{
					this.log("BillingDirectDebit FALSE!");
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		private bool AddCardNext(int index, string[] btnNextText, string type = "aria-label")
        {
			try
			{
				string btnNextName = genXpathString(btnNextText, type);
				WaitAjaxLoading(By.XPath(btnNextName), 10);
				Delay(500);
				ReadOnlyCollection<IWebElement> _listButtons = this._driver.FindElements(By.XPath(btnNextName));
				if (_listButtons.Count < index + 1)
				{
					this.log("Not Next Buttons!");
					return false;
				}

				IWebElement btnNext = _listButtons[index];
				if (!this.isValid(btnNext))
				{
					Delay(1000);
				}

				if (!this.isValid(btnNext))
				{
					Delay(1000);
				}

				if (!this.isValid(btnNext))
				{
					Delay(1000);
				}

				if (!this.isValid(btnNext))
				{
					this.log("Next Btn isValid!");
					return false;
				}

				btnNext.Click();
				Delay(500);
				WaitLoading();
				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2.Message);
				this.log("Next Btn ERROR!");
				return false;
			}
		}

		private bool AddCardSelCountry(string country)
		{
			try
			{
				string[] countryData = country.Split('|');
				if (countryData.Length < 1)
				{
					this.log("Dữ liệu không đúng định dạng!");
					countryData = "United States of America|US Dollar|US Dollars".Split('|');
				}

				// Country/Region Country/Region
				Delay(1500);
				string selCountryRegiontagstr = "//label[@aria-label='Country/Region' or @aria-label='Country/region']";
				WaitAjaxLoading(By.XPath(selCountryRegiontagstr));
				ReadOnlyCollection<IWebElement> selCountryRegionObjs = this._driver.FindElements(By.XPath(selCountryRegiontagstr));
				if (selCountryRegionObjs.Count == 0)
				{
					return false;
				}

				if (selCountryRegionObjs.Count > 0)
				{
					if (!this.isValid(selCountryRegionObjs.First()))
					{
						this.log("Delay Country/Region Valid...!");
						Delay(3000);
						if (!this.isValid(selCountryRegionObjs.First()))
						{
							this.log("Delay Country/Region Valid...!");
							Delay(3000);
						}
					}

					selCountryRegionObjs.First().Click();
					// United States of America
					string optCountryRegiontagstr = "//div[@role='option']//span[text()='" + countryData[0] + "']";
					WaitAjaxLoading(By.XPath(optCountryRegiontagstr));
					ReadOnlyCollection<IWebElement> optCountryRegionObjs = this._driver.FindElements(By.XPath(optCountryRegiontagstr));
					if (optCountryRegionObjs.Count > 0)
					{
						optCountryRegionObjs.First().Click();
						Delay(500);
					}
					else
					{
						this.log("Country False!");
						return false;
					}

                    // Currency
                    if (countryData.Length > 1)
                    {
						string selCurrencytagstr = "//label[@aria-label='Currency' and @role='button']";
						WaitAjaxLoading(By.XPath(selCurrencytagstr));
						ReadOnlyCollection<IWebElement> selCurrencyObjs = this._driver.FindElements(By.XPath(selCurrencytagstr));
						if (selCurrencyObjs.Count > 0)
						{
							if (!this.isValid(selCurrencyObjs.First()))
							{
								this.log("Delay Currency Valid...!");
								Delay(3000);
								if (!this.isValid(selCurrencyObjs.First()))
								{
									this.log("Delay Currency Valid...!");
									Delay(3000);
								}
							}
							selCurrencyObjs.First().Click();
							Delay(500);

							string optCurrencytagstr = "//div[@role='option']//span[text()='" + countryData[1] + "' or text()='" + countryData[2] + "']";
							WaitAjaxLoading(By.XPath(optCurrencytagstr));
							ReadOnlyCollection<IWebElement> optCurrencyObjs = this._driver.FindElements(By.XPath(optCurrencytagstr));
							if (optCurrencyObjs.Count > 0)
							{
								if (!this.isValid(optCurrencyObjs.First()))
								{
									this.log("Delay Currency Valid...!");
									Delay(3000);
								}
								optCurrencyObjs.First().Click();
								Delay(500);
							}
							else
							{
								this.log("Currency False!");
								return false;
							}
						}
						else
						{
							this.log("Currency False!");
							return false;
						}
					}
				}
				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2.Message);
				this.log("SelCountry ERROR!");
				return false;
			}
		}

		private bool AddCardFilForm(string card, string zipcode)
        {
			try
			{
				if (string.IsNullOrEmpty(card))
				{
					this.log("Not card!");
					return false;
				}

				Delay(1000);
				string inputTagName = "//div[@role='dialog']//input[@type='text' and @aria-invalid='false']";
				WaitAjaxLoading(By.XPath(inputTagName), 12);
				ReadOnlyCollection<IWebElement> inputs = this._driver.FindElements(By.XPath(inputTagName));
				if (inputs.Count > 3)
				{
					String[] cardData = card.Split('|');

					inputs[0].Click();
					clearWebField(inputs[0]);
					inputs[0].SendKeys(_card_name);
					Delay(500);

					inputs[1].Click();
					clearWebField(inputs[1]);
					inputs[1].SendKeys(cardData[0]);
					Delay(500);

					inputs[2].Click();
					clearWebField(inputs[2]);
					string datestr = cardData[1] + cardData[2].Substring(cardData[2].Length - 2);
					inputs[2].SendKeys(datestr);
					Delay(500);

					inputs[3].Click();
					clearWebField(inputs[3]);
					inputs[3].SendKeys(cardData[3]);
					Delay(500);

                    if (!string.IsNullOrEmpty(zipcode))
                    {
						inputs = this._driver.FindElements(By.XPath(inputTagName));
						if (inputs.Count > 4)
						{
							inputs[4].Click();
							clearWebField(inputs[4]);
							inputs[4].SendKeys(zipcode);
							Delay(500);
						}
					}
				}
				else
				{
					this.log("Not Add Card Form!");
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2.Message);
				this.log("AddCardFilForm ERROR!");
				return false;
			}
		}

		public bool gotoUrl(string url)
		{
			try
			{
				url = url.Replace("#page_id#",pageID);
				url = url.Replace("#ads_id#",_ads_id);
				url = url.Replace("#bm_ads_id#",_bm_ads_id);
				url = url.Replace("#bm_id#",_bm_id);
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);
				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool removeCardFromBm()
		{
			bool res = false;
			return res;
		}

		private UserResBody getBusinessUsers()
		{
			try
			{
				string html = string.Empty;
				string dfurl = @"https://graph.facebook.com/v11.0/{0}/business_users?access_token={1}&fields=[%22email%22,%22first_name%22,%22last_name%22]&limit=500&locale=en_US&method=get&pretty=0&sort=name_ascending&suppress_http_code=1";
				string url = string.Format(dfurl, _bm_id, _accessToken);
				html = this.GetData(null, url, _cookie, this._userAgent);
				return JsonConvert.DeserializeObject<UserResBody>(html);
			}
			catch
			{
				return null;
			}
		}

		public bool checkBmUser(string first_name, string last_name, int waitTime = 8)
		{
			try
			{
				bool check = false;
				UserResBody userData = this.getBusinessUsers();
				if (userData != null && userData.data != null)
				{
					userData.data.ForEach(uitem => {
						if ((uitem.first_name == first_name) && (uitem.last_name == last_name))
						{
							check = true;
						}
					});
				}

				int ChkIndex = 1;

				while (!check && (ChkIndex <= waitTime))
				{
					Delay(5000);
					this.log("Check Lần: " + ChkIndex.ToString());
					userData = this.getBusinessUsers();
					if (userData != null && userData.data != null)
					{
						userData.data.ForEach(uitem => {
							if ((uitem.first_name == first_name) && (uitem.last_name == last_name))
							{
								check = true;
							}
						});
					}
					ChkIndex++;
				}

				return check;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public Task<bool> TKCNAddCnpjCode(string account_id, string cnpjCode)
		{
			try
			{
				/*string url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + _ads_id;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string value = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string value2 = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string referrerUrl = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + account_id + "&pid=p1&business_id=" + bmId + "&page=account_settings&tab=account_billing_settings";

				string url = "https://business.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__usid=6-Trb28xt3n651%3APrb28xq1f89aeu%3A0-Arb28xaphe8kg-RV%3D6%3AF%3D&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqxK49o9E4a2imeGqErxSaScwNxK4UKewSAAzpoixWE-bwjElxKdwJz-9yEjx226264824yoyaxG4oaEugaoaEix6221qwCwuE9FEdUmCBBwLghUcd5xS2dum4E8EKUryoiAwu8sgiCggwExm3G4UhwXwEwl-dwh85G5o24xS2iUS7E421uK6o8o47BxSbg942B12ewzwAwgoszUeUmwkEK7S6UgyE9Ehjy88rwzzXxG4U4S2q4UowwwnEfp8fUS2W2K4EbVo4m1uCgqw8O58Gm0BUO1tx64EKu9zawSyES2e0UFU6K15K1bwzxy&__csr=&__req=z&__hs=19110.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005427508&__s=xqqkyq%3Aoyom4n%3Arqhcwo&__hsi=7091710078461912148-0&__comet_req=0&fb_dtsg=",
					value,
					"&jazoest=21959&lsd=",
					value2,
					"&__spin_r=1005427508&__spin_b=trunk&__spin_t=1651167422&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					account_id,
					"%22%2C%22currency%22%3Anull%2C%22logging_data%22%3A%7B%22logging_counter%22%3A9%2C%22logging_id%22%3A%223565877616%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22%22%2C%22country_code%22%3A%22BR%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22tax_id%22%3A%22",
					cnpjCode,
					"%22%2C%22tax_id_type%22%3A%22BRAZIL_CNPJ%22%7D%2C%22timezone%22%3Anull%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=5302523886432625"
				});

				string postData = this.asynFetchApi(url, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", value2, referrerUrl, "include");*/
				return Task.FromResult(true);
			}
			catch
			{
				return Task.FromResult(false);
			}
		}

		public bool TKCNSBM_AddCard(string ad_country_code, string card_country_code, string currency_code, string timezone, string cardcode)
		{
			try
			{
				return _BMAD_AddCard(_ads_id, ad_country_code, card_country_code, currency_code, timezone, cardcode);
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool TKBM_UpdateCountryInfo(string country_code, string currency_code, string timezone)
		{
			try
			{
				switchBusinessTabHandle();
				string url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + _bm_ads_id + "&pid=p1&page=account_settings&tab=account_billing_settings";
				string referrer = url;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string context_id = _clone_uid;
				this.log("Context_id: " + context_id);

				// Change country
				string gurl = "https://business.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					context_id,
					"&__usid=6-Trjeaijgm7fgb%3APrjeaic1t3r414%3A0-Arjeaij1ntdvoo-RV%3D6%3AF%3D&__user=",
					context_id,
					"&__a=1&__dyn=7AgSXghF3Gxd2um5rpUR0Bxpxa9yUqDBBheCFohK49o9EeAq2imeGqFEkG4VEHoOqqE88lBxeipe9wNWAAzppFuUuGfxW2vxi4EOezoK26UKbC-mdwTzUOESegGbwgEmK9y8Gdz8hyUuxqt1eiUO4EgCyku4oS4EWfGUhwyg9p44889EScxyu6UGq13yHGmmUTxJe9LgbdkGypVRg8Rpo8ESibKegK9xubwr8sxep3bBAzECi9lpubwIxecAwXzogyo465ubUO9xSfwgEnxaFo5a7EN1O74qVpVoKcyU98-m79UcF8y4bwRwFVpGxebxa4AbxR2V8W5Hy88Q3mbzryE-5o9ohAwkEKA6Sag-48GUtAx5e8xSXzUjzXyE-eAwEwAz8iCxeq4qz8gwDzE9Ufp8bU-dwKUjyEG4E949BGUryry8K5Ue8Sp1G3WaUjxyU-5aChoCUOdBwFjQfwCz8ym9yUixi48hyVEKu9DAm22eCyKqQfJ3Eiwj9pEcE4avAxa9whU4embw_wzzq-3W49bg99oak48-eC-dDVbBw&__csr=&__req=24&__hs=19272.BP%3Aads_manager_pkg.2.0.0.0.0&dpr=1&__ccg=UNKNOWN&__rev=1006348267&__s=4bcw9r%3Aqg6z6b%3A4m9bmi&__hsi=7151834630159076695&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25073&lsd=",
					lsd,
					"&__aaid=",
					_bm_ads_id,
					"&__spin_r=1006348267&__spin_b=trunk&__spin_t=1665166260&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					_bm_ads_id,
					"%22%2C%22currency%22%3A%22",
					currency_code,
					"%22%2C%22logging_data%22%3A%7B%22logging_counter%22%3A12%2C%22logging_id%22%3A%221760257091%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22country_code%22%3A%22",
					country_code,
					"%22%2C%22city%22%3A%22%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22email%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22phone_number%22%3A%22%22%2C%22second_tax_id%22%3A%22%22%2C%22second_tax_id_type%22%3Anull%2C%22tax_exempt%22%3Afalse%2C%22tax_id%22%3A%22%22%2C%22tax_id_type%22%3A%22NONE%22%2C%22tax_registration_status%22%3A%22%22%7D%2C%22timezone%22%3A",
					timezone,
					"%2C%22actor_id%22%3A%22",
					context_id,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=5472142756196040&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D270213817%22%5D"
				});

				string postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
				string business_country_code = Regex.Match(postData, "\"business_country_code\":\"(.*?)\"").Groups[1].Value;
				string res_currency = Regex.Match(postData, "\"currency\":\"(.*?)\"").Groups[1].Value;

				if ((country_code != business_country_code) || (res_currency != currency_code))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool TKBM_UpdateInfo(string cnpjcode = "")
		{
			try
			{
				switchBusinessTabHandle();
				string url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + _bm_ads_id + "&pid=p1&page=account_settings&tab=account_billing_settings";
				string referrer = url;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string context_id = _clone_uid;
				this.log("Context_id: " + context_id);
				bool checkRes = false;

				if (!string.IsNullOrEmpty(cnpjcode))
				{
					this.log("Add mã CNPJ!");
					string gurl = "https://business.facebook.com/api/graphql/";
					string body = string.Concat(new string[] {
						"av=",
						context_id,
						"&__usid=6-Trdrpaim9zkq0%3APrdrp9gu0346d%3A0-Ardrpai1p7ja13-RV%3D6%3AF%3D&__user=",
						context_id,
						"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqxK49o9E4a2imeGqErxSaScwNxK4UKewSAAzpoixWE-bwjElxKdwJz-9yEjx226264824yoyaxG4oaEugaoaEix6221qwCwuE9FEdUmCBBwLghUcd5xS2dum4E8EKUryonwu8sgiCggwExm3G4UhwXwEwl-dz8dU5G5o24xS2iUS7E421uK6o8o47BxSbg942B12ewzwAwgoszUeUmwkEK7S6UgyE9Ehjy88rwzzXxG4U4S2q4UowwwnEfp8fUS2W2K4EbVo4m1uCgqw8O58Gm0BUO1tx64EKu9zawSyES2e0UFU6K15K1bwzxy&__csr=&__req=22&__hs=19163.BP%3Aads_campaign_manager_pkg.2.0.1.0.&dpr=1&__ccg=EXCELLENT&__rev=1005711386&__s=vu3jxe%3A3hykkd%3A2ui2wn&__hsi=7111238366404836431-0&__comet_req=0&fb_dtsg=",
						fb_dtsg,
						"&jazoest=25413&lsd=",
						lsd,
						"&__aaid=",
						_bm_ads_id,
						"&__spin_r=1005711386&__spin_b=trunk&__spin_t=1655714205&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
						_bm_ads_id,
						"%22%2C%22currency%22%3Anull%2C%22logging_data%22%3A%7B%22logging_counter%22%3A54%2C%22logging_id%22%3A%222103911769%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22%22%2C%22country_code%22%3A%22BR%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22tax_id%22%3A%22",
						cnpjcode,
						"%22%2C%22tax_id_type%22%3A%22BRAZIL_CNPJ%22%7D%2C%22timezone%22%3Anull%2C%22actor_id%22%3A%22",
						context_id,
						"%22%2C%22client_mutation_id%22%3A%2210%22%7D%7D&server_timestamps=true&doc_id=5428097817221702"
					});

					string postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
					string checkpmId = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
					if (!string.IsNullOrEmpty(checkpmId))
					{
						checkRes = true;
					}
				}

				return checkRes;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool TKBM_AddCard(string ad_country, string card_country, string currency_code, string timezone, string cardcode, string screen1 = "", string cnpjcode1 = "")
		{
			try
			{
				return _BMAD_AddCard(_bm_ads_id, ad_country, card_country, currency_code, timezone, cardcode, screen1, cnpjcode1);
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool _BMAD_AddCard(string adId, string ad_country, string card_country, string currency, string timezone, string cardcode, string screen1 = "", string cnpjcode = "")
		{
			try
			{
				switchBusinessTabHandle();

				string url = "https://business.facebook.com/";
				if (screen1 == "")
				{
					url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + adId + "&pid=p1&page=account_settings&tab=account_billing_settings"; ;
				}

				if (screen1 == "pe")
				{
					url = "https://business.facebook.com/adsmanager/manage/accounts?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string referrer = url;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string context_id = _clone_uid;
				this.log("Context_id: " + context_id);

				// Change country
				string gurl = "https://business.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					context_id,
					"&__usid=6-Trjeaijgm7fgb%3APrjeaic1t3r414%3A0-Arjeaij1ntdvoo-RV%3D6%3AF%3D&__user=",
					context_id,
					"&__a=1&__dyn=7AgSXghF3Gxd2um5rpUR0Bxpxa9yUqDBBheCFohK49o9EeAq2imeGqFEkG4VEHoOqqE88lBxeipe9wNWAAzppFuUuGfxW2vxi4EOezoK26UKbC-mdwTzUOESegGbwgEmK9y8Gdz8hyUuxqt1eiUO4EgCyku4oS4EWfGUhwyg9p44889EScxyu6UGq13yHGmmUTxJe9LgbdkGypVRg8Rpo8ESibKegK9xubwr8sxep3bBAzECi9lpubwIxecAwXzogyo465ubUO9xSfwgEnxaFo5a7EN1O74qVpVoKcyU98-m79UcF8y4bwRwFVpGxebxa4AbxR2V8W5Hy88Q3mbzryE-5o9ohAwkEKA6Sag-48GUtAx5e8xSXzUjzXyE-eAwEwAz8iCxeq4qz8gwDzE9Ufp8bU-dwKUjyEG4E949BGUryry8K5Ue8Sp1G3WaUjxyU-5aChoCUOdBwFjQfwCz8ym9yUixi48hyVEKu9DAm22eCyKqQfJ3Eiwj9pEcE4avAxa9whU4embw_wzzq-3W49bg99oak48-eC-dDVbBw&__csr=&__req=24&__hs=19272.BP%3Aads_manager_pkg.2.0.0.0.0&dpr=1&__ccg=UNKNOWN&__rev=1006348267&__s=4bcw9r%3Aqg6z6b%3A4m9bmi&__hsi=7151834630159076695&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25073&lsd=",
					lsd,
					"&__aaid=",
					adId,
					"&__spin_r=1006348267&__spin_b=trunk&__spin_t=1665166260&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					adId,
					"%22%2C%22currency%22%3A%22",
					currency,
					"%22%2C%22logging_data%22%3A%7B%22logging_counter%22%3A12%2C%22logging_id%22%3A%221760257091%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22country_code%22%3A%22",
					ad_country,
					"%22%2C%22city%22%3A%22%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22email%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22phone_number%22%3A%22%22%2C%22second_tax_id%22%3A%22%22%2C%22second_tax_id_type%22%3Anull%2C%22tax_exempt%22%3Afalse%2C%22tax_id%22%3A%22%22%2C%22tax_id_type%22%3A%22NONE%22%2C%22tax_registration_status%22%3A%22%22%7D%2C%22timezone%22%3A",
					timezone,
					"%2C%22actor_id%22%3A%22",
					context_id,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=5472142756196040&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D270213817%22%5D"
				});

				string postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
				string business_country_code = Regex.Match(postData, "\"business_country_code\":\"(.*?)\"").Groups[1].Value;
				string res_currency = Regex.Match(postData, "\"currency\":\"(.*?)\"").Groups[1].Value;

				if ((ad_country != business_country_code) || (res_currency != currency))
				{
					this.log("Change country FALSE!");
				}
				else
				{
					this.log("Change country TRUE!");
				}

				// Add card
				bool checkRes = false;
				String[] cardTypeData = cardcode.Split('|');
				foreach (string cardTypeItem in cardTypeData)
                {
					this.log("Mã Thẻ: " + cardTypeItem);
					string card = this._getCardDelegate(cardTypeItem);
					if (!string.IsNullOrEmpty(card))
					{
						this.log("Thẻ: " + card);
						String[] cardData = card.Split('|');

						gurl = "https://business.secure.facebook.com/ajax/payment/token_proxy.php?tpe=%2Fapi%2Fgraphql%2F";
						body = string.Concat(new string[] {
							"av=",
							context_id,
							"&payment_dev_cycle=prod&__usid=6-Trjeaijgm7fgb%3APrjeaic1t3r414%3A2-Arjeaij1ntdvoo-RV%3D6%3AF%3D&__user=",
							context_id,
							"&__a=1&__dyn=7AgSXghF3Gxd2um5rpUR0Bxpxa9yUqDBBheCFohK49o9EeAq2imeGqFEkG4VEHoOqqE88lBxeipe9wNWAAzppFuUuGfxW2vxi4EOezoK26UKbC-mdwTzUOESegGbwgEmK9y8Gdz8hyUuxqt1eiUO4EgCyku4oS4EWfGUhwyg9p44889EScxyu6UGq13yHGmmUTxJe9LgbdkGypVRg8Rpo8ESibKegK9xubwr8sxep3bBAzECi9lpubwIxecAwXzogyo465ubUO9xSfwgEnxaFo5a7EN1O74qVpVoKcyU98-m79UcF8y4bwRwFVpGxebxa4AbxR2V8W5Hy88Q3mbzryE-5o9ohAwkEKA6Sag-48GUtAx5e8xSXzUjzXyE-eAwEwAz8iCxeq4qz8gwDzE9Ufp8bU-dwKUjyEG4E949BGUryry8K5Ue8Sp1G3WaUjxyU-5aChoCUOdBwFjQfwCz8ym9yUixi48hyVEKu9DAm22eCyKqQfJ3Eiwj9pEcE4avAxa9whU4embw_wzzq-3W49bg99oak48-eC-dDVbBw&__req=2r&__hs=19272.BP%3Aads_manager_pkg.2.0.0.0.0&dpr=1&__ccg=UNKNOWN&__rev=1006348267&__s=4bcw9r%3Aqg6z6b%3A4m9bmi&__hsi=7151834630159076695&__comet_req=0&fb_dtsg=",
							fb_dtsg,
							"&jazoest=25073&lsd=",
							lsd,
							"&__aaid=",
							adId,
							"&__spin_r=1006348267&__spin_b=trunk&__spin_t=1665166260&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingAddCreditCardMutation&variables=%7B%22input%22%3A%7B%22billing_address%22%3A%7B%22country_code%22%3A%22",
							card_country,
							"%22%7D%2C%22billing_logging_data%22%3A%7B%22logging_counter%22%3A29%2C%22logging_id%22%3A%221760257091%22%7D%2C%22cardholder_name%22%3A%22",
							_card_name,
							"%22%2C%22credit_card_first_6%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(0,6),
							"%22%7D%2C%22credit_card_last_4%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(cardData[0].Length - 4),
							"%22%7D%2C%22credit_card_number%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0],
							"%22%7D%2C%22csc%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[3],
							"%22%7D%2C%22expiry_month%22%3A%22",
							cardData[1],
							"%22%2C%22expiry_year%22%3A%22",
							cardData[2],
							"%22%2C%22payment_account_id%22%3A%22",
							adId,
							"%22%2C%22payment_type%22%3A%22MOR_ADS_INVOICE%22%2C%22unified_payments_api%22%3Atrue%2C%22actor_id%22%3A%22",
							context_id,
							"%22%2C%22client_mutation_id%22%3A%225%22%7D%7D&server_timestamps=true&doc_id=4987045951343337&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D270213817%22%5D"
						});

						referrer = "https://business.facebook.com/";
						postData = this.fetchApi(gurl, "POST", body, "useBillingAddCreditCardMutation", lsd, referrer);
						string credential_id = Regex.Match(postData, "\"credential_id\":\"(.*?)\"").Groups[1].Value;

						if (!string.IsNullOrEmpty(credential_id))
						{
							checkRes = true;
						}
						else
						{
							this.log("FALSE!");
							this.log(postData);
						}

						if (checkRes)
						{
							if (!string.IsNullOrEmpty(cnpjcode))
							{
								this.log("Add mã CNPJ!");
								gurl = "https://business.facebook.com/api/graphql/";
								body = string.Concat(new string[] {
									"av=",
									context_id,
									"&__usid=6-Trdrpaim9zkq0%3APrdrp9gu0346d%3A0-Ardrpai1p7ja13-RV%3D6%3AF%3D&__user=",
									context_id,
									"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqxK49o9E4a2imeGqErxSaScwNxK4UKewSAAzpoixWE-bwjElxKdwJz-9yEjx226264824yoyaxG4oaEugaoaEix6221qwCwuE9FEdUmCBBwLghUcd5xS2dum4E8EKUryonwu8sgiCggwExm3G4UhwXwEwl-dz8dU5G5o24xS2iUS7E421uK6o8o47BxSbg942B12ewzwAwgoszUeUmwkEK7S6UgyE9Ehjy88rwzzXxG4U4S2q4UowwwnEfp8fUS2W2K4EbVo4m1uCgqw8O58Gm0BUO1tx64EKu9zawSyES2e0UFU6K15K1bwzxy&__csr=&__req=22&__hs=19163.BP%3Aads_campaign_manager_pkg.2.0.1.0.&dpr=1&__ccg=EXCELLENT&__rev=1005711386&__s=vu3jxe%3A3hykkd%3A2ui2wn&__hsi=7111238366404836431-0&__comet_req=0&fb_dtsg=",
									fb_dtsg,
									"&jazoest=25413&lsd=",
									lsd,
									"&__aaid=",
									adId,
									"&__spin_r=1005711386&__spin_b=trunk&__spin_t=1655714205&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
									adId,
									"%22%2C%22currency%22%3Anull%2C%22logging_data%22%3A%7B%22logging_counter%22%3A54%2C%22logging_id%22%3A%222103911769%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22%22%2C%22country_code%22%3A%22BR%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22tax_id%22%3A%22",
									cnpjcode,
									"%22%2C%22tax_id_type%22%3A%22BRAZIL_CNPJ%22%7D%2C%22timezone%22%3Anull%2C%22actor_id%22%3A%22",
									context_id,
									"%22%2C%22client_mutation_id%22%3A%2210%22%7D%7D&server_timestamps=true&doc_id=5428097817221702"
								});

								postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
								string checkpmId = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
								if (string.IsNullOrEmpty(checkpmId))
								{
									this.log("Add mã CNPJ: FALSE!");
								}
							}
						}
					}
					else
					{
						this.log("Hết thẻ!");
						break;
					}
				}

				return checkRes;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool TKCN_UpdateInfo(string cnpjcode = "")
		{
			try
			{
				switchRegTabHandle();

				string url = "https://www.facebook.com/ads/manager/account_settings/account_billing/?act=" + _ads_id + "&pid=p1&page=account_settings&tab=account_billing_settings";
				string referrer = url;
				if (!this._driver.Url.Contains("www.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				bool checkRes = false;
				if (!string.IsNullOrEmpty(cnpjcode))
				{
					this.log("Add mã CNPJ!");
					string gurl = "https://www.facebook.com/api/graphql/";
					string body = string.Concat(new string[] {
									"av=",
									_clone_uid,
									"&__usid=6-Trdrpaim9zkq0%3APrdrp9gu0346d%3A0-Ardrpai1p7ja13-RV%3D6%3AF%3D&__user=",
									_clone_uid,
									"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqxK49o9E4a2imeGqErxSaScwNxK4UKewSAAzpoixWE-bwjElxKdwJz-9yEjx226264824yoyaxG4oaEugaoaEix6221qwCwuE9FEdUmCBBwLghUcd5xS2dum4E8EKUryonwu8sgiCggwExm3G4UhwXwEwl-dz8dU5G5o24xS2iUS7E421uK6o8o47BxSbg942B12ewzwAwgoszUeUmwkEK7S6UgyE9Ehjy88rwzzXxG4U4S2q4UowwwnEfp8fUS2W2K4EbVo4m1uCgqw8O58Gm0BUO1tx64EKu9zawSyES2e0UFU6K15K1bwzxy&__csr=&__req=22&__hs=19163.BP%3Aads_campaign_manager_pkg.2.0.1.0.&dpr=1&__ccg=EXCELLENT&__rev=1005711386&__s=vu3jxe%3A3hykkd%3A2ui2wn&__hsi=7111238366404836431-0&__comet_req=0&fb_dtsg=",
									fb_dtsg,
									"&jazoest=25413&lsd=",
									lsd,
									"&__spin_r=1005711386&__spin_b=trunk&__spin_t=1655714205&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
									_ads_id,
									"%22%2C%22currency%22%3Anull%2C%22logging_data%22%3A%7B%22logging_counter%22%3A54%2C%22logging_id%22%3A%222103911769%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22%22%2C%22country_code%22%3A%22BR%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22tax_id%22%3A%22",
									cnpjcode,
									"%22%2C%22tax_id_type%22%3A%22BRAZIL_CNPJ%22%7D%2C%22timezone%22%3Anull%2C%22actor_id%22%3A%22",
									_clone_uid,
									"%22%2C%22client_mutation_id%22%3A%2210%22%7D%7D&server_timestamps=true&doc_id=5428097817221702"
								});

					referrer = "https://www.facebook.com/";
					string postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
					string pmId = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
					if (!string.IsNullOrEmpty(pmId))
					{
						checkRes = true;
					}
				}

				return checkRes;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool TKCN_AddCard(string ad_country, string card_country, string currency, string timezone, string cardcode, string cnpjcode = "")
		{
			try
			{
				switchRegTabHandle();
				string url = "https://www.facebook.com/ads/manager/account_settings/account_billing/?act=" + _ads_id + "&pid=p1&page=account_settings&tab=account_billing_settings";
				string referrer = url;
				if (!this._driver.Url.Contains("www.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				this.log("ad country: " + ad_country);
				this.log("card country: " + card_country);
				this.log("currency: " + currency);

				// Update country - currency code
				string gurl = "https://www.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
							"av=",
							_clone_uid,
							"&__usid=6-Trcl56x1mvzvh0%3APrcl55x129yoh1%3A0-Arcl56x1ay82i3-RV%3D6%3AF%3D&__user=",
							_clone_uid,
							"&__a=1&__dyn=7xeUmBz8aolJ28S2q3m9U8EJ4WqxK49o9E4a6oF4zGCG6UmCyJz8corxebJ2ocWAAzpoixWE-bwWxeEixKdwJz-9CS4Ugwxwxx21FxG9y8Gdz8hwGxqt0FwGxa4oS4EuCx62a2q5E89EeUryFEdUmKFpobQUTwMQm9xe2dum4E8EKUryonwu8sgiCgOUa8lwWxe4oeUa8465udz8C2S12xu5pU6-cxS2iVpUuyUd88EeAUy4bxS11VotyQ2h2UtggzE8U98doK78-3K5E5abK6S6UgyE9Ehjy88rwzzXxG4U4S7VEjCx6221uwZAwLzUS2W2K4EbVoeoK5Ue8Sp1G3WcwMzUkGum2ym2WEdEO8wl8hyVEKu9zawLCyKbwzweau1HwhrwiU8Uow&__csr=&__req=15&__hs=19140.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005599335&__s=7vx2m1%3Axnxhl3%3Adu9dbn&__hsi=7102710296425399513-0&__comet_req=0&fb_dtsg=",
							fb_dtsg,
							"&jazoest=22000&lsd=",
							lsd,
							"&__spin_r=1005599335&__spin_b=trunk&__spin_t=1653728610&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
							_ads_id,
							"%22%2C%22currency%22%3A%22",
							currency,
							"%22%2C%22logging_data%22%3A%7B%22logging_counter%22%3A10%2C%22logging_id%22%3A%222228448019%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22%22%2C%22country_code%22%3A%22",
							ad_country,
							"%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22second_tax_id%22%3A%22%22%2C%22second_tax_id_type%22%3Anull%2C%22tax_exempt%22%3Afalse%2C%22tax_id%22%3A%22%22%2C%22tax_id_type%22%3A%22NONE%22%7D%2C%22timezone%22%3A",
							timezone,
							"%2C%22actor_id%22%3A%22",
							_clone_uid,
							"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=5726871777340354"
						});

				string postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
				string pmId = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(pmId))
				{
					this.log("Update Country: FALSE!");
				}

				// Add card
				bool checkRes = false;
				String[] cardTypeData = cardcode.Split('|');
				foreach (string cardTypeItem in cardTypeData)
                {
					this.log("Mã Thẻ: " + cardTypeItem);
					string card = this._getCardDelegate(cardTypeItem);
					if (!string.IsNullOrEmpty(card))
					{
						this.log("Thẻ: " + card);
						String[] cardData = card.Split('|');

						gurl = "https://secure.facebook.com/ajax/payment/token_proxy.php?tpe=%2Fapi%2Fgraphql%2F";
						body = string.Concat(new string[] {
							"av=",
							_clone_uid,
							"&payment_dev_cycle=prod&__usid=6-Trcl56x1mvzvh0%3APrcl55x129yoh1%3A0-Arcl56x1ay82i3-RV%3D6%3AF%3D&__user=",
							_clone_uid,
							"&__a=1&__dyn=7xeUmBz8aolJ28S2q3m9U8EJ4WqxK49o9E4a6oF4zGCG6UmCyJz8corxebJ2ocWAAzpoixWE-bwWxeEixKdwJz-9CS4Ugwxwxx21FxG9y8Gdz8hwGxqt0FwGxa4oS4EuCx62a2q5E89EeUryFEdUmKFpobQUTwMQm9xe2dum4E8EKUryonwu8sgiCgOUa8lwWxe4oeUa8465udz8C2S12xu5pU6-cxS2iVpUuyUd88EeAUy4bxS11VotyQ2h2UtggzE8U98doK78-3K5E5abK6S6UgyE9Ehjy88rwzzXxG4U4S7VEjCx6221uwZAwLzUS2W2K4EbVoeoK5Ue8Sp1G3WcwMzUkGum2ym2WEdEO8wl8hyVEKu9zawLCyKbwzweau1HwhrwiU8Uow&__req=1g&__hs=19140.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005599335&__s=7vx2m1%3Axnxhl3%3Adu9dbn&__hsi=7102710296425399513-0&__comet_req=0&fb_dtsg=",
							fb_dtsg,
							"&jazoest=22000&lsd=",
							lsd,
							"&__spin_r=1005599335&__spin_b=trunk&__spin_t=1653728610&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingAddCreditCardMutation&variables=%7B%22input%22%3A%7B%22billing_address%22%3A%7B%22country_code%22%3A%22",
							card_country,
							"%22%7D%2C%22billing_logging_data%22%3A%7B%22logging_counter%22%3A20%2C%22logging_id%22%3A%222228448019%22%7D%2C%22cardholder_name%22%3A%22LEE%20NGUYEN%22%2C%22credit_card_first_6%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(0,6),
							"%22%7D%2C%22credit_card_last_4%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(cardData[0].Length - 4),
							"%22%7D%2C%22credit_card_number%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0],
							"%22%7D%2C%22csc%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[3],
							"%22%7D%2C%22expiry_month%22%3A%22",
							cardData[1],
							"%22%2C%22expiry_year%22%3A%22",
							cardData[2],
							"%22%2C%22payment_account_id%22%3A%22",
							_ads_id,
							"%22%2C%22payment_type%22%3A%22MOR_ADS_INVOICE%22%2C%22unified_payments_api%22%3Atrue%2C%22actor_id%22%3A%22",
							_clone_uid,
							"%22%2C%22client_mutation_id%22%3A%223%22%7D%7D&server_timestamps=true&doc_id=4126726757375265"
						});

						referrer = "https://business.facebook.com/";
						postData = this.fetchApi(gurl, "POST", body, "useBillingAddCreditCardMutation", lsd, referrer);
						string credential_id = Regex.Match(postData, "\"credential_id\":\"(.*?)\"").Groups[1].Value;

						if (!string.IsNullOrEmpty(credential_id))
						{
							checkRes = true;
						}
						else
						{
							this.log("FALSE!");
							this.log(postData);
						}
					}
					else
					{
						this.log("Hết thẻ!");
						break;
					}
				}

				if (checkRes)
				{
					if (!string.IsNullOrEmpty(cnpjcode))
					{
						this.log("Add mã CNPJ!");
						gurl = "https://www.facebook.com/api/graphql/";
						body = string.Concat(new string[] {
									"av=",
									_clone_uid,
									"&__usid=6-Trdrpaim9zkq0%3APrdrp9gu0346d%3A0-Ardrpai1p7ja13-RV%3D6%3AF%3D&__user=",
									_clone_uid,
									"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqxK49o9E4a2imeGqErxSaScwNxK4UKewSAAzpoixWE-bwjElxKdwJz-9yEjx226264824yoyaxG4oaEugaoaEix6221qwCwuE9FEdUmCBBwLghUcd5xS2dum4E8EKUryonwu8sgiCggwExm3G4UhwXwEwl-dz8dU5G5o24xS2iUS7E421uK6o8o47BxSbg942B12ewzwAwgoszUeUmwkEK7S6UgyE9Ehjy88rwzzXxG4U4S2q4UowwwnEfp8fUS2W2K4EbVo4m1uCgqw8O58Gm0BUO1tx64EKu9zawSyES2e0UFU6K15K1bwzxy&__csr=&__req=22&__hs=19163.BP%3Aads_campaign_manager_pkg.2.0.1.0.&dpr=1&__ccg=EXCELLENT&__rev=1005711386&__s=vu3jxe%3A3hykkd%3A2ui2wn&__hsi=7111238366404836431-0&__comet_req=0&fb_dtsg=",
									fb_dtsg,
									"&jazoest=25413&lsd=",
									lsd,
									"&__spin_r=1005711386&__spin_b=trunk&__spin_t=1655714205&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
									_ads_id,
									"%22%2C%22currency%22%3Anull%2C%22logging_data%22%3A%7B%22logging_counter%22%3A54%2C%22logging_id%22%3A%222103911769%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22%22%2C%22country_code%22%3A%22BR%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22tax_id%22%3A%22",
									cnpjcode,
									"%22%2C%22tax_id_type%22%3A%22BRAZIL_CNPJ%22%7D%2C%22timezone%22%3Anull%2C%22actor_id%22%3A%22",
									_clone_uid,
									"%22%2C%22client_mutation_id%22%3A%2210%22%7D%7D&server_timestamps=true&doc_id=5428097817221702"
								});

						referrer = "https://www.facebook.com/";
						postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
						pmId = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(pmId))
						{
							this.log("Add mã CNPJ: FALSE!");
						}
					}
				}

				return checkRes;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool _makeChinhCard(string account_id)
		{
			try
			{
				switchBusinessTabHandle();

				BillingAMNexusRootQuery biilData = this.getAdPaymentInfo(account_id);
				if ((biilData == null) || (biilData.data == null) || (biilData.data.billable_account_by_payment_account == null)
					|| (biilData.data.billable_account_by_payment_account.billing_payment_account == null)
					|| (biilData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods == null)
					|| (biilData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods.Count == 0)
					)
				{
					this.log("KHÔNG LẤY ĐƯỢC THẺ!");
					return false;
				}

				credential credential = new credential { };
				foreach (BillingPaymentMethod billPm in biilData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods)
				{
					if (!billPm.is_primary)
					{
						credential = billPm.credential;
						break;
					}
				}

				if ((credential == null) || (string.IsNullOrEmpty(credential.credential_id)))
				{
					this.log("KHÔNG CÓ THẺ MAKE CHÍNH!");
					return false;
				}

				this.log("THẺ: " + credential.display_string + " " + credential.last_four_digits);

				string data = this._driver.PageSource;
				string value = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string value2 = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string referrerUrl = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + account_id + "&pid=p1&page=account_settings&tab=account_billing_settings";
				string url = "https://business.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__usid=6-Trfk85cmhdehp%3APrfk7neipzi6b%3A0-Arfk7gv1g576k-RV%3D6%3AF%3D&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmxa3-Q8zo9EdoDwyyVuCErx2m2q12wABzGCG6UtyJz8corxebzEdF98Sm4EuGfyU4W5orzobo-byEjx226264824yoyaxG4oaEugaoaEix6221qwCwuE9FEdUmCBBwLghUcd5xS2dum4E8EKUryonwu8sgiCggwExm3G4UhwXwEwl-dwh85G5o24xS2iUS7E421uK6o8o47BxSbwBgak48W2e2i11xOfwXxq1iyUvorx2awCx5e8wxK2efK6Ejwjo9Ejxy221uwZAw_zobEaUiwLBwho5Wp1G0z8kyFo2nz85S4oiyVUCcG3qazo8U3yDwqU4C1bwzxy1gwLw&__csr=&__req=w&__hs=19198.BP%3Aads_campaign_manager_pkg.2.0.0.0.0&dpr=1&__ccg=EXCELLENT&__rev=1005901695&__s=95r894%3Apy9btc%3Au69lk8&__hsi=7124165446501113005&__comet_req=0&fb_dtsg=",
					value,
					"&jazoest=25501&lsd=",
					value2,
					"&__spin_r=1005901695&__spin_b=trunk&__spin_t=1658724026&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingMakePrimaryMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					account_id,
					"%22%2C%22primary_funding_id%22%3A%22",
					credential.credential_id,
					"%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=5655324851148937"
				});

				string postData = this.fetchApi(url, "POST", body, "useBillingMakePrimaryMutation", value2, referrerUrl);
                if (postData.Contains("error"))
                {
					this.log(postData);
					return false;
				}
				return true;
			}
			catch
			{
				return false;
			}
		}

		private bool bmSubmitAddCard()
        {
			try
			{
				string[] btnNextText = new string[] { "Save" };
				if (AddCardNext(0, btnNextText))
				{
					// check loading
					string loaddingst = "//*[@aria-label='data-visualcompletion' and @role='progressbar']";
					WaitAjaxLoading(By.XPath(loaddingst), 5);
					bool loading = true;
					int ChkLoadingIndex = 1;
					while (loading && (ChkLoadingIndex <= 10))
					{
						Delay(3000);
						ReadOnlyCollection<IWebElement> cplLoadingObjs = this._driver.FindElements(By.XPath(loaddingst));
						if (cplLoadingObjs.Count > 0)
						{
							loading = true;
						}
						else
						{
							loading = false;
						}

						ChkLoadingIndex++;
					}

					// check verify
					loaddingst = "//*[@role='dialog']//form[contains(@action, 'payments/3ds2_authenticate')]";
					WaitAjaxLoading(By.XPath(loaddingst), 5);
					loading = true;
					ChkLoadingIndex = 1;
					while (loading && (ChkLoadingIndex <= 10))
					{
						Delay(3000);
						ReadOnlyCollection<IWebElement> cplLoadingObjs = this._driver.FindElements(By.XPath(loaddingst));
						if (cplLoadingObjs.Count > 0)
						{
							loading = true;
						}
						else
						{
							loading = false;
						}

						ChkLoadingIndex++;
					}
				}

				// Done
				string[] cplAlertText = {
										"Success",
										"Done"
								};
				string cplAlert = genXpathString(cplAlertText, "aria-label");
				WaitAjaxLoading(By.XPath(cplAlert));
				Delay(1000);
				ReadOnlyCollection<IWebElement> cplAlertObjs = this._driver.FindElements(By.XPath(cplAlert));
				if (cplAlertObjs.Count == 0)
                {
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool autoAddCardToBm(string country, string cardcode)
		{
			try
			{
				this._driver.Navigate().GoToUrl("https://business.facebook.com/settings/payment-methods?business_id=" + _bm_id);
				WaitLoading();
				Delay(1000);

				if (!elmAutoFinAndClick("//div[@role='presentation']//button[@type='button' and not(@aria-disabled='true')]"))
				{
					if (!elmAutoFinAndClick("//div[@role='region']//div[@role='button' and @aria-busy='false']"))
					{
						this.log("Not Add Payment button 1!");
						return false;
					}

					if (!elmAutoFinAndClick("//div[@data-testid='ContextualLayerRoot']//div[@role='menuitem']"))
					{
						this.log("Not Add Payment button 2!");
						return false;
					}
				}

				string[] btnNextText = {
					"Next",
					"Save"
				};
				string AssignFinanceEditor = "//*[text()='Add Payment Information' or text()='Add payment information']";
				WaitAjaxLoading(By.XPath(AssignFinanceEditor), 15);
				ReadOnlyCollection<IWebElement> AssignFinanceEditorObjs = this._driver.FindElements(By.XPath(AssignFinanceEditor));
				if (AssignFinanceEditorObjs.Count > 0)
				{
					bool selCountry = AddCardSelCountry(country);
					if (!selCountry)
					{
						this.log("Not Select Country!");
					}

					if (!AddCardNext(0, btnNextText))
					{
						this.log("Not Add Payment Information Next!");
						return false;
					}
				}

				this.log("Mã Thẻ: " + cardcode);
				string card = this._getCardDelegate(cardcode);
				if (!string.IsNullOrEmpty(card))
				{
					this.log("Card: " + card);
					bool filform = this.AddCardFilForm(card, "");
					if (filform)
					{
						bool check = bmSubmitAddCard();
						if (!check)
						{
							check = bmSubmitAddCard();
						}
						if (!check)
						{
							bmSubmitAddCard();
						}
					}
				}
				else
				{
					this.log("Hết thẻ!");
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool addCardToBm(string country, string cardcode)
		{
			try
			{
				switchBusinessTabHandle();

				this._driver.Navigate().GoToUrl("https://business.facebook.com/settings/payment-methods?business_id=" + _bm_id);
				WaitLoading();
				Delay(1000);

				// Assign Finance Editor
				if (!this.assignFinanceEditor())
				{
					this.log("assignFinanceEditor: FALSE!");
				}

				String[] cardTypeData = cardcode.Split('|');
				foreach (string cardTypeItem in cardTypeData)
                {
					autoAddCardToBm(country, cardTypeItem);
				}

				if (!CheckBmCard())
				{
					this.log("CheckBmCard: FALSE!");
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		private bool assignFinanceEditor()
		{
			try
			{
				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				// Get user
				string userid = "";
				UserResBody userData = this.getBusinessUsers();
				if (userData != null)
				{
					userid = userData.data.First().id;
				}

				if (string.IsNullOrEmpty(userid))
				{
					this.log("Not BM USER!");
					return false;
				}

				this.log("BM USER: " + userid);
				string gurl = "https://business.facebook.com/api/graphql/";
				string bodyfl = string.Concat(new string[] {
						"av=",
						_clone_uid,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7xeUmBz8aolJ28S2q3mbwyyVuCEnxG2q12xCagjGq3mq1FxebzA3aF98Sm4EuGfwhEmwKzobo_x3U984e486C6EC8yEScx611wnocEixV5x62a2q5Eqz8eUryE5mWBBwQzoO2ng-3tpUdoK7UC4F87y78jxiUa8522m3K2y3WElUScyo723m1Lz84aVpUuyUd88EeAUpK7o47BwFg942B12ewi8doa84K5E5WUrorx2awCx5e8wxK2efxWiewjovCxeq4o882swQzUS2W2K4E6-bxu3ydCg-aw-z8c8-5aDBwEBwKG3qcy85i4oKqbDyo-2-qaUK2e0UE7e15K&__csr=&__req=w&__hs=19018.BP%3Abrands_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004979175&__s=wd8sxk%3Anf31ht%3Ar014mw&__hsi=7057368182225428159-0&__comet_req=0&fb_dtsg=",
						fb_dtsg,
						"&jazoest=21954&lsd=",
						lsd,
						"&__spin_r=1004979175&__spin_b=trunk&__spin_t=1643171576&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingMIAssignFinanceEditorMutation&variables=%7B%22businessID%22%3A%22",
						_bm_id,
						"%22%2C%22userID%22%3A%22",
						userid,
						"%22%7D&server_timestamps=true&doc_id=4099396833437726"
					});

				string postDatafl = this.fetchApi(gurl, "POST", bodyfl, "useBillingMIAssignFinanceEditorMutation", lsd, "https://business.facebook.com");
				string assign_finance_editor = Regex.Match(postDatafl, "\"assign_finance_editor\":{(.*?)}}").Groups[1].Value;
				if (string.IsNullOrEmpty(assign_finance_editor))
				{
					this.log(postDatafl);
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool apiAddCardToBm(string country, string cardcode)
		{
			try
			{
				switchBusinessTabHandle();

				if (string.IsNullOrEmpty(_payment_account_id))
				{
					this.getBmInfo();
				}

				string url = "https://business.facebook.com/settings/payment-methods?business_id=" + _bm_id;
				string referrer = url;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string country_code = "US";
				if (!string.IsNullOrEmpty(country))
				{
					country_code = country;
				}

                if (!this.assignFinanceEditor())
                {
					this.log("assignFinanceEditor: FALSE!");
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				string gurl = "";
				bool checkRes = false;
				String[] cardTypeData = cardcode.Split('|');
				foreach (string cardTypeItem in cardTypeData)
                {
					this.log("Mã Thẻ: " + cardTypeItem);
					string card = this._getCardDelegate(cardTypeItem);
					if (!string.IsNullOrEmpty(card))
					{
						this.log("Thẻ: " + card);
						String[] cardData = card.Split('|');

						gurl = "https://business.secure.facebook.com/ajax/payment/token_proxy.php?tpe=%2Fapi%2Fgraphql%2F";
						string body = string.Concat(new string[] {
							"av=",
							_clone_uid,
							"&payment_dev_cycle=prod&__user=",
							_clone_uid,
							"&__a=1&__dyn=7xeUmBz8aolJ28S2q3mbwyyVuCEnxG2q12xCagjGq3mq1FxebzA3aF98Sm4EuGfwhEmwKzobo_x3U984e486C6EC8yEScx611wnocEixV5x62a2q5Eqz8eUryE5mWBBwQzoO2ng-3tpUdoK7UC4F87y78jxiUa8522m3K2y3WElUScyo723m1Lz84aVpUuyUd88EeAUpK7o47BwFg942B12ewi8doa84K5E5WUrorx2awCx5e8wxK2efxWiewjovCxeq4o882swQzUS2W2K4E6-bxu3ydCg-aw-z8c8-5aDBxSaBwKG3qcy85i4oKqbDyo-2-qaUK2e0UE7e19w&__req=15&__hs=19014.BP%3Abrands_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004968007&__s=8n01wr%3Atylkwm%3Afwf49v&__hsi=7055913223476034682-0&__comet_req=0&fb_dtsg=",
							fb_dtsg,
							"&jazoest=21860&lsd=",
							lsd,
							"&__spin_r=1004968007&__spin_b=trunk&__spin_t=1642832817&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingAddCreditCardMutation&variables=%7B%22input%22%3A%7B%22billing_address%22%3A%7B%22country_code%22%3A%22",
							country_code,
							"%22%7D%2C%22billing_logging_data%22%3A%7B%22logging_counter%22%3A10%2C%22logging_id%22%3A%222718477307%22%7D%2C%22cardholder_name%22%3A%22",
							"LEE%20NGUYEN",
							"%22%2C%22credit_card_first_6%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(0,6),
							"%22%7D%2C%22credit_card_last_4%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(cardData[0].Length - 4),
							"%22%7D%2C%22credit_card_number%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0],
							"%22%7D%2C%22csc%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[3],
							"%22%7D%2C%22expiry_month%22%3A%22",
							cardData[1],
							"%22%2C%22expiry_year%22%3A%22",
							cardData[2],
							"%22%2C%22payment_account_id%22%3A%22",
							_payment_account_id,
							"%22%2C%22payment_type%22%3A%22MOR_ADS_INVOICE%22%2C%22unified_payments_api%22%3Atrue%2C%22actor_id%22%3A%22",
							_clone_uid,
							"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=4126726757375265"
						});

						referrer = "https://business.facebook.com/";
						string postData = this.fetchApi(gurl, "POST", body, "useBillingAddCreditCardMutation", lsd, referrer);
						_credential_id = Regex.Match(postData, "\"credential_id\":\"(.*?)\"").Groups[1].Value;

						if (!string.IsNullOrEmpty(_credential_id))
						{
							checkRes = true;
							Card cardItem = new Card()
							{
								code = cardTypeItem,
								bm_id = _bm_id,
								credential_id = _credential_id
							};
							_bm_cards.Add(cardItem);
						}
						else
						{
							this.log("FALSE!");
							this.log(postData);
						}
					}
					else
					{
						this.log("Hết thẻ!");
						break;
					}
				}

				return checkRes;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool adAddCardBp(string ad_country_code, string card_country_code, string currency_code, string timezone, string cardcode, string[] stepcode = null)
		{
			try
			{
				Delay(1000);
				reBPost = false;
				string url = stepcode[0] + pageID + stepcode[1];
				switchRegTabHandle();
				return this.addCardBpApiAsync(url, ad_country_code, card_country_code, currency_code, timezone, cardcode, "", stepcode);
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool adPermitCard()
		{
			try
			{
				switchRegTabHandle();

				bool checkAds = this.CheckAdAllInOneAsync();
				if (checkAds)
				{
					this.log("TK chưa die!");
					this.log("Wait...");
				}

				int ChkLoadingIndex = 1;
				while (checkAds && (ChkLoadingIndex <= 3))
				{
					Delay(3000);
					checkAds = this.CheckAdAllInOneAsync();
					if (checkAds)
					{
						this.log("TK vẫn chưa die!");
						this.log("Wait...");
					}
					ChkLoadingIndex++;
				}

				if (checkAds)
				{
					this.log("TK vẫn chưa die!");
					return false;
				}

				string fblink = "https://web.facebook.com/payments/risk/preauth/?ad_account_id=" + _ads_id + "&entrypoint=AYMTAdAccountUnrestrictLinkGenerator";
				if (!this._driver.Url.Contains("payments/risk/preauth"))
				{
					this._driver.Navigate().GoToUrl(fblink);
					WaitLoading();
					Delay(1000);
				}

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[]
				{
					"av=",
					_clone_uid,
					"&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AgNe-4amaUmgDxyHqAyqomzEdoK2a8F4Wodo9ES2N6wAxu13wFw_xebzEcWAAzokK7HzE4q3yczobohxi2i6osz8nwvoiyEqx60xUnwho2VwgE7Oq7ooxu1ZgC11x-9w9278jw8qbzoiAwVUa9u4-2e5o-cwzwbyE4S58uzHzoaEd85WcK58gy898uwECzEmgK7ohzE4y8w9Cu5E5WEeE4a4Hy8ky8aU-Uqw8y4UeoaE761iwKwHxa1owyxu16waiaxiaw4qxa4UCfwSyES2e1BwDwkQ&__csr=&__req=h&__hs=18985.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004896088&__s=4ekj5m%3A2w9lef%3A4e4gn1&__hsi=7045145945401038571-0&__comet_req=0&fb_dtsg=",
					token1,
					"&jazoest=21908&lsd=",
					token2,
					"&__spin_r=1004896088&__spin_b=trunk&__spin_t=1640325471&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingPreauthPermitMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					_ads_id,
					"%22%2C%22entry_point%22%3A%22BILLING_2_0%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=3514448948659909"
				});

				string resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "useBillingPreauthPermitMutation", token2, fblink);
				string success = Regex.Match(resData, "\"success\":(.*?)}").Groups[1].Value;
				if ("true" != success)
				{
					this.log("Res success: false!");
					this.log("Sẽ không set là false!");
					// return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public bool BmAdPermitCard()
		{
			try
			{
				switchRegTabHandle();

				bool checkAds = this.CheckAdAllInOneAsync("BMad");
				if (checkAds)
				{
					this.log("TK chưa die!");
					this.log("Wait...");
				}

				int ChkLoadingIndex = 1;
				while (checkAds && (ChkLoadingIndex <= 5))
				{
					Delay(3000);
					checkAds = this.CheckAdAllInOneAsync("BMad");
					if (checkAds)
					{
						this.log("TK vẫn chưa die!");
						this.log("Wait...");
					}
					ChkLoadingIndex++;
				}

				if (checkAds)
				{
					this.log("TK vẫn chưa die!");
					return false;
				}

				string fblink = "https://web.facebook.com/payments/risk/preauth/?ad_account_id=" + _bm_ads_id + "&entrypoint=AYMTAdAccountUnrestrictLinkGenerator";
				if (!this._driver.Url.Contains("payments/risk/preauth"))
				{
					this._driver.Navigate().GoToUrl(fblink);
					WaitLoading();
					Delay(1000);
				}

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[]
				{
					"av=",
					_clone_uid,
					"&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7AgNe-4amaUmgDxyHqAyqomzEdoK2a8F4Wodo9ES2N6wAxu13wFw_xebzEcWAAzokK7HzE4q3yczobohxi2i6osz8nwvoiyEqx60xUnwho2VwgE7Oq7ooxu1ZgC11x-9w9278jw8qbzoiAwVUa9u4-2e5o-cwzwbyE4S58uzHzoaEd85WcK58gy898uwECzEmgK7ohzE4y8w9Cu5E5WEeE4a4Hy8ky8aU-Uqw8y4UeoaE761iwKwHxa1owyxu16waiaxiaw4qxa4UCfwSyES2e1BwDwkQ&__csr=&__req=h&__hs=18985.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004896088&__s=4ekj5m%3A2w9lef%3A4e4gn1&__hsi=7045145945401038571-0&__comet_req=0&fb_dtsg=",
					token1,
					"&jazoest=21908&lsd=",
					token2,
					"&__spin_r=1004896088&__spin_b=trunk&__spin_t=1640325471&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingPreauthPermitMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					_bm_ads_id,
					"%22%2C%22entry_point%22%3A%22BILLING_2_0%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=3514448948659909"
				});

				string resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "useBillingPreauthPermitMutation", token2, fblink);
				string success = Regex.Match(resData, "\"success\":(.*?)}").Groups[1].Value;
				if ("true" != success)
				{
					this.log("Res success: false!");
					this.log("Sẽ không set là false!");
					// return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public BillingAMNexusRootQuery getAdPaymentInfo(string ads_id)
		{
			try
			{
				switchBusinessTabHandle();
				string url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + ads_id;
				string referrer = url;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string html = string.Empty;
				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string rfurl = url;

				string body = string.Concat(new string[]
					{
						"av=",
						_clone_uid,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7xeUmBz8fXgydwn8DwyyVuCErx2m2q12wABzGCG6UtyJz8corxebzEdF98Sm4EuGfyU4W5orzobo_yoG4UgwxwOw8i9y8G6EhwGxV0FwGxa4o885G2q1WwCCwTxqqmm2Z17wMQm7o8RUowyyXwBxai1UxN1B122y5oeEjx63K2y1nUS14wmElw8i7o9bzouwg85WUpwxwgpUtyQ2h0Fx6ewzwAwgoszUeUmwkEK7S6UgyE9Ehjy88rwzzXxG4U4S2q4UowwwnEfp8fUS2W2K4EbVo4m1uCgqw8O58Gm0BUO1tx64EKu9zawSyES2e0UFU6K15K1bwzw&__csr=&__req=4&__hs=19089.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005314593&__s=p97l4y%3A9cqrq8%3Ao3g3j5&__hsi=7083783231213117167-0&__comet_req=0&fb_dtsg=",
						token1,
						"&jazoest=22033&lsd=",
						token2,
						"&__spin_r=1005314593&__spin_b=trunk&__spin_t=1649321809&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAMNexusRootQuery&variables=%7B%22paymentAccountID%22%3A%22",
						ads_id,
						"%22%7D&server_timestamps=true&doc_id=4943226942458921"
					});

				html = this.fetchApi("https://business.facebook.com/api/graphql/", "POST", body, "BillingAMNexusRootQuery", token2, rfurl);
				return JsonConvert.DeserializeObject<BillingAMNexusRootQuery>(html);
			}
			catch (Exception ex2)
			{
				this.log(ex2.Message);
				return null;
			}
		}

		public bool checkAdPaymentHold(string ads_id)
		{
			try
			{
				switchBusinessTabHandle();
				string url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + ads_id;
				string referrer = url;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string rfurl = url;

				string body = string.Concat(new string[]
					{
						"av=",
						_clone_uid,
						"&__user=",
						_clone_uid,
						"&__a=1&__dyn=7xeUmxa3-Q8zo5O9U8EKnFG2Om2q12wAxiFGxK7oG48corxebzEdF98Sm4EuGfyU4W5orzobo-5EjwOwOw8i9y8G6Ehwt8aE4m1qwCwuE9FEdUmCBBwLghUbqQGxBa2du17K2m5U7y746k48a8lwWxe4oeUa85vzo4i1qw9G7o9bzouwg85WUpwoVUao9k2C4oW2e2i11xOfwXxq1uxZxK48GU8EhAy88rwzzXwKwjo9Ejxy220D98fUS2W2K4E7K1uCgqw8O58Gm0BUO1tx64EKu9zawSyES2e0UFU6K19wiU8U6Ci2G1bzEG2q1pw&__csr=&__req=5&__hs=19390.BP%3Aads_campaign_manager_pkg.2.0.0.0.0&dpr=1&__ccg=EXCELLENT&__rev=1006899802&__s=83zpwh%3Aq2buls%3Aynesyj&__hsi=7195454622753355937&__comet_req=0&fb_dtsg=",
						token1,
						"&jazoest=25243&lsd=",
						token2,
						"&__aaid=&__spin_r=1006899802&__spin_b=trunk&__spin_t=1675322331&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingHubPaymentSettingsViewQuery&variables=%7B%22assetID%22%3A%22",
						ads_id,
						"%22%7D&server_timestamps=true&doc_id=5792199927494104"
					});

				string resData = this.fetchApi("https://business.facebook.com/api/graphql/", "POST", body, "BillingHubPaymentSettingsViewQuery", token2, rfurl);

				string event_context = Regex.Match(resData, "\"event_context\":\"(.*?)\"").Groups[1].Value;
				if ((!string.IsNullOrEmpty(event_context)) && (event_context == "APPROVE_FUNDS_HOLD"))
				{
					this.log("TK HOLD!");
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2.Message);
				return false;
			}
		}

		public bool adUpdateSpend(string country = "DE|EUR|2")
		{
			try
			{
				string[] countryData = country.Split('|');
				if (countryData.Length < 3)
				{
					this.log("Dữ liệu country không đúng định dạng!");
					return false;
				}

				string fblink = "https://www.facebook.com/ads/manager/account_settings/account_billing/?act=" + _ads_id + "&pid=p1&page=account_settings&tab=account_billing_settings";
				if (!this._driver.Url.Contains("facebook.com"))
				{
					this._driver.Navigate().GoToUrl(fblink);
					WaitLoading();
					Delay(1000);
				}

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[]
				{
					"av=",
					_clone_uid,
					"&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqwOwCwgE98kGqErxSax2366UjyUW3qiidBxa7GzUK1exm6US2SfUkxe488o8ogw8i9y8G6Ehwt8aE4m1qwCwuE9FEdUmCBBwLghUcd5xS2dum11K6UC4F87y744FA48a8lwWxe4oeUa85vzoO0AE2qwgHzouwg85WUpwxwgum2B0Agak48W2e2i11xOfwXxq1uxZxK48G2q4kUy26U8U-7Ejwjo9Ejxy220D98fUS2W2K4E7K1uCgqw8O58Gm7o7CcwnohxabDyoOEdEGcgjgW0UE7e&__csr=&__req=y&__hs=18985.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004896111&__s=zhofpy%3Awxkdl1%3Ado9lu5&__hsi=7045161039604744718-0&__comet_req=0&fb_dtsg=",
					token1,
					"&jazoest=22125&lsd=",
					token2,
					"&__spin_r=1004896111&__spin_b=trunk&__spin_t=1640329379&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingUpdateAccountSpendLimitScreenMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					_ads_id,
					"%22%2C%22new_spend_limit%22%3A%7B%22amount%22%3A%22" + countryData[2] + "%22%2C%22currency%22%3A%22" + countryData[1] + "%22%7D%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=5615899425146711"
				});

				string resData = this.fetchApi("https://www.facebook.com/api/graphql/", "POST", body, "useBillingUpdateAccountSpendLimitScreenMutation", token2, fblink);
				string offset_amount = Regex.Match(resData, "\"offset_amount\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(offset_amount))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public bool BMAdUpdateSpend(string country = "DE|EUR|2")
		{
			try
			{
				string[] countryData = country.Split('|');
				if (countryData.Length < 3)
				{
					this.log("Dữ liệu country không đúng định dạng!");
					return false;
				}

				string fblink = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + _bm_ads_id + "&pid=p1&business_id=" + _bm_id + "&page=account_settings&tab=account_billing_settings";
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(fblink);
					WaitLoading();
					Delay(1000);
				}

				string getdata = this._driver.PageSource;
				string token1 = Regex.Match(getdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				string token2 = Regex.Match(getdata, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string body = string.Concat(new string[]
				{
					"av=",
					_clone_uid,
					"&__usid=&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqwOwCwgE98kGqErxSax2366UjyUW3qiidBxa7GzUK1exm6US2SfUkxe488o8ogw8i9y8G6Ehwt8aE4m1qwCwuE9FEdUmCBBwLghUcd5xS2dum11K6UC4F87y744FA48a8lwWxe4oeUa85vzoO0AE2qwgHzouwg85WUpwxwgum2B0Agak48W2e2i11xOfwXxq1uxZxK48G2q4kUy26U8U-7Ejwjo9Ejxy220D98fUS2W2K4E7K1uCgqw8O58Gm7o7CcwnohxabDyoOEdEGcgjgW0UE7e&__csr=&__req=y&__hs=18985.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004896111&__s=zhofpy%3Awxkdl1%3Ado9lu5&__hsi=7045161039604744718-0&__comet_req=0&fb_dtsg=",
					token1,
					"&jazoest=22125&lsd=",
					token2,
					"&__spin_r=1004896111&__spin_b=trunk&__spin_t=1640329379&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingUpdateAccountSpendLimitScreenMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					_bm_ads_id,
					"%22%2C%22new_spend_limit%22%3A%7B%22amount%22%3A%22" + countryData[2] + "%22%2C%22currency%22%3A%22" + countryData[1] + "%22%7D%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=5615899425146711"
				});

				string resData = this.fetchApi("https://business.facebook.com/api/graphql/", "POST", body, "useBillingUpdateAccountSpendLimitScreenMutation", token2, fblink);
				string offset_amount = Regex.Match(resData, "\"offset_amount\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(offset_amount))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public bool addCardPageAds(string ad_country_code, string card_country_code, string currency_code, string timezone, string cardcode, string checkType, string cnpjcode1 = "", string[] stepcode = null)
		{
			bool sw = false;
			string profile_id = _clone_uid;
			try
			{
				switchRegTabHandle();

				if (string.IsNullOrEmpty(pageID))
                {
					this.log("Không có Page ID!");
					return false;
				}

				// Check page
				string url = "https://www.facebook.com/" + pageID;
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);

				if (this._driver.Url.Contains("profile.php?id="))
				{
					Uri theUri = new Uri(this._driver.Url);
					profile_id = HttpUtility.ParseQueryString(theUri.Query).Get("id");
					this.log("Page PRO5: " + profile_id);
					sw = switchProfile(_clone_uid, profile_id);
					if (!sw)
					{
						this.log("NOT Switch Profile!");
						return false;
					}
				}

				string getData = this.fetchGet(this._driver.Url);
				string fb_dtsg = Regex.Match(getData, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(getData, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string context_id = profile_id;
				this.log("Context_id: " + context_id);

				string adId = _ads_id;
				if (checkType == stepcode[4])
				{
					adId = _bm_ads_id;
				}

				// Change country
				string gurl = "https://www.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					context_id,
					"&__usid=&__user=",
					context_id,
					"&__a=1&__dyn=7AzHJ16-cxp2u6Xgydg9pE98C8GUmDDJ4WqF1e6E9ES3i6oF1eFGUpxqqax2qqEboymubyQUC37GiidBCDK7Gzu16UkzWzoSdwJJ4hfzLBzogwCyEhx21FxuHBy8G6EhwgpRxm2iq9CJ1-23wKCxy9wHwOCzoboryFFE4uWBBKdxRedU88CnhteEjwzlBBxO4EKVEkyoOmi1Dx3xiGxejgOXCxS55zEvyojz9e3mUugy2u6olULxq1zyEpz4fwhEa4bwWDKdxWbxmezXwmkVQaBwxwwwwVoC7Q2h2UthF98Wcxq68F0gohyE-3K4p84y5ovorx2awCx5e8xSXzUjzUuyqV8jx248nxm498W6axS3u5U4O6Ua8S2m8UuyEiwBx5e6U8EnwUzpA6E7y8yF8-48CQewRBCwSz8OEiwwy8gx64EKuiicG3qazodE5a1YDU&__csr=&__req=1v&__hs=18914.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004552454&__s=q089re%3Ae8qvpw%3Atjg4sm&__hsi=7018819346167009374-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22076&lsd=",
					lsd,
					"&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=",
					stepcode[8],
					"&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					adId,
					"%22%2C%22currency%22%3A%22",
					currency_code,
					stepcode[6],
					ad_country_code,
					stepcode[7],
					"%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22second_tax_id%22%3A%22%22%2C%22second_tax_id_type%22%3Anull%2C%22tax_exempt%22%3Afalse%2C%22tax_id%22%3A%22%22%2C%22tax_id_type%22%3A%22NONE%22%7D%2C%22timezone%22%3A",
					timezone,
					"%2C%22actor_id%22%3A%22",
					context_id,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=4119854918068296"
				});

				string referrer = "https://www.facebook.com/";

				string postData = this.fetchApi(gurl, "POST", body, stepcode[8], lsd, referrer);
                if (string.IsNullOrEmpty(postData))
                {
					this.log("Change country FALSE!");
                }
                else
                {
					string business_country_code = Regex.Match(postData, "\"business_country_code\":\"(.*?)\"").Groups[1].Value;
					string currency = Regex.Match(postData, "\"currency\":\"(.*?)\"").Groups[1].Value;
					if ((ad_country_code != business_country_code) || (currency_code != currency))
					{
						this.log("Change country FALSE!");
					}
					else
					{
						this.log("Change country TRUE!");
					}
				}

				// Add card
				bool checkRes = false;
				String[] cardTypeData = cardcode.Split('|');
				foreach (string cardTypeItem in cardTypeData)
				{
					this.log("Mã Thẻ: " + cardTypeItem);
					string card = this._getCardDelegate(cardTypeItem);
					if (!string.IsNullOrEmpty(card))
					{
						this.log("Thẻ: " + card);
						String[] cardData = card.Split('|');

						gurl = "https://secure.facebook.com/ajax/payment/token_proxy.php?tpe=%2Fapi%2Fgraphql%2F";
						body = string.Concat(new string[] {
							"av=",
							context_id,
							"&payment_dev_cycle=prod&__usid=6-Trja8yw1o532u2%3APrja8xc1j5bwno%3A0-Arja8ywbznx8s-RV%3D6%3AF%3D&__user=",
							context_id,
							"&__a=1&__dyn=7AzHxqU5a9zk1ryUqxenFw9uu2i5U4e2O1gyQdwSAx-bwNwnof8botwsovgO9wnoe8hw47w9u0LVEtwyzo1xoswIwuo88422y11xmfz81sbzoaEnxO1ywOwj8bU9kbxS2218wc61axe3S6Ueo5qfK6E7e58jwGzE8FU5e7oqBwJK2W5olwUwOzEjUlDw-CwLU72m7-2K1LwqEuBxe6pEbUGdwb622390sUowMwBwwAzUjwTwNwLwFg&__req=5h&__hs=19270.HYP%3Acomet_pkg.2.1.0.2.1&dpr=1&__ccg=EXCELLENT&__rev=1006329165&__s=ogmxg0%3A4mvvf6%3Avillyg&__hsi=7151024273977814060&__comet_req=15&fb_dtsg=",
							fb_dtsg,
							"&jazoest=25744&lsd=",
							lsd,
							"&__spin_r=1006329165&__spin_b=trunk&__spin_t=1664977584&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingAddCreditCardMutation&variables=%7B%22input%22%3A%7B%22billing_address%22%3A%7B%22country_code%22%3A%22",
							card_country_code,
							"%22%7D%2C%22billing_logging_data%22%3A%7B%22logging_counter%22%3A21%2C%22logging_id%22%3A%221725756273%22%7D%2C%22cardholder_name%22%3A%22A%20Cuong%22%2C%22credit_card_first_6%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(0,6),
							"%22%7D%2C%22credit_card_last_4%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(cardData[0].Length - 4),
							"%22%7D%2C%22credit_card_number%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0],
							"%22%7D%2C%22csc%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[3],
							"%22%7D%2C%22expiry_month%22%3A%22",
							cardData[1],
							"%22%2C%22expiry_year%22%3A%22",
							cardData[2],
							"%22%2C%22payment_account_id%22%3A%22",
							adId,
							"%22%2C%22payment_type%22%3A%22MOR_ADS_INVOICE%22%2C%22unified_payments_api%22%3Atrue%2C%22actor_id%22%3A%22",
							context_id,
							"%22%2C%22client_mutation_id%22%3A%226%22%7D%7D&server_timestamps=true&doc_id=4987045951343337&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D431626192%22%5D"
						});

						postData = this.fetchApi(gurl, "POST", body, "useBillingAddCreditCardMutation", lsd, referrer);
						if (string.IsNullOrEmpty(postData))
						{
							if (sw)
							{
								switchProfile(profile_id, _clone_uid);
							}
                        }

						string credential_id = Regex.Match(postData, "\"credential_id\":\"(.*?)\"").Groups[1].Value;
						if (!string.IsNullOrEmpty(credential_id))
						{
							checkRes = true;
						}
						else
						{
							this.log("FALSE!");
							this.log(postData);
						}
					}
					else
					{
						this.log("Hết thẻ!");
						break;
					}
				}

				if (checkRes)
				{
					if (!string.IsNullOrEmpty(cnpjcode1))
					{
						this.log("Add mã CNPJ!");
						gurl = "https://www.facebook.com/api/graphql/";
						body = string.Concat(new string[] {
							"av=",
							context_id,
							"&__usid=6-Trdrpaim9zkq0%3APrdrp9gu0346d%3A0-Ardrpai1p7ja13-RV%3D6%3AF%3D&__user=",
							context_id,
							"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqxK49o9E4a2imeGqErxSaScwNxK4UKewSAAzpoixWE-bwjElxKdwJz-9yEjx226264824yoyaxG4oaEugaoaEix6221qwCwuE9FEdUmCBBwLghUcd5xS2dum4E8EKUryonwu8sgiCggwExm3G4UhwXwEwl-dz8dU5G5o24xS2iUS7E421uK6o8o47BxSbg942B12ewzwAwgoszUeUmwkEK7S6UgyE9Ehjy88rwzzXxG4U4S2q4UowwwnEfp8fUS2W2K4EbVo4m1uCgqw8O58Gm0BUO1tx64EKu9zawSyES2e0UFU6K15K1bwzxy&__csr=&__req=22&__hs=19163.BP%3Aads_campaign_manager_pkg.2.0.1.0.&dpr=1&__ccg=EXCELLENT&__rev=1005711386&__s=vu3jxe%3A3hykkd%3A2ui2wn&__hsi=7111238366404836431-0&__comet_req=0&fb_dtsg=",
							fb_dtsg,
							"&jazoest=25413&lsd=",
							lsd,
							"&__spin_r=1005711386&__spin_b=trunk&__spin_t=1655714205&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
							adId,
							"%22%2C%22currency%22%3Anull%2C%22logging_data%22%3A%7B%22logging_counter%22%3A54%2C%22logging_id%22%3A%222103911769%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22%22%2C%22country_code%22%3A%22BR%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22tax_id%22%3A%22",
							cnpjcode1,
							"%22%2C%22tax_id_type%22%3A%22BRAZIL_CNPJ%22%7D%2C%22timezone%22%3Anull%2C%22actor_id%22%3A%22",
							context_id,
							"%22%2C%22client_mutation_id%22%3A%2210%22%7D%7D&server_timestamps=true&doc_id=5428097817221702&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D431626192%22%5D"
						});
						postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
						string checkpmId = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
						if (string.IsNullOrEmpty(checkpmId))
						{
							this.log("Add mã CNPJ: FALSE!");
						}
					}
				}

				if (sw)
				{
					switchProfile(profile_id, _clone_uid);
				}
				return checkRes;
			}
			catch (Exception ex2)
			{
				if (sw)
				{
					switchProfile(profile_id, _clone_uid);
				}
				this.log(ex2);
				return false;
			}
		}

		public bool setTax(string adId, string country_code, string state, string city, string street1, string street2, string zip)
		{
			try
			{
				switchBusinessTabHandle();
				string url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + adId + "&pid=p1&page=account_settings&tab=account_billing_settings";
				string referrer = url;

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string gurl = "https://business.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
						"av=",
						_clone_uid,
						"&__usid=&__user=",
						_clone_uid,
						"&__a=1&__dyn=7xeUmBz8fXgydwCCwKy5xGubBWqwIBwCwgE98kGqK6otyEgwNxKubyQdwSAAzpoixWE-bwjElxKdwJyFe58jx22q78gxO1DyoyaxG4o7i2G15wmE9E7G2qq3u5FFpobQ4u33hpEmwznBwgrxK9xai1UUkBxep12U98lwWxe4odXwEwl-dw8m6o6i3m12KdxW267E5WUpwxwgum2B0Agak48W2e2i11xOfwXxq1uxZxK48G2q4kUy26U8U-7Ejwjo9Ejxy220D98fUS2W2K4E9o5m1uCgqw8O58Gm0BUO1tx64EKu9zawSyEN1d3E3ywsU4mU&__csr=&__req=17&__hs=19059.BP%3Aads_campaign_manager_pkg.2.1.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005163005&__s=5h9nu2%3A1qz6dc%3A7q8a6e&__hsi=7072720168249529896-0&__comet_req=0&fb_dtsg=",
						fb_dtsg,
						"&jazoest=22208&lsd=",
						lsd,
						"&__spin_r=1005163005&__spin_b=trunk&__spin_t=1646745989&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsUpdateAccountMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
						adId,
						"%22%2C%22currency%22%3Anull%2C%22logging_data%22%3A%7B%22logging_counter%22%3A22%2C%22logging_id%22%3A%221772507110%22%7D%2C%22tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22",
						Uri.EscapeDataString(city),
						"%22%2C%22country_code%22%3A%22",
						country_code,
						"%22%2C%22state%22%3A%22",
						state,
						"%22%2C%22street1%22%3A%22",
						Uri.EscapeDataString(street1),
						"%22%2C%22street2%22%3A%22",
						Uri.EscapeDataString(street2),
						"%22%2C%22zip%22%3A%22",
						zip,
						"%22%7D%2C%22business_name%22%3A%22a%22%2C%22is_personal_use%22%3Afalse%7D%2C%22timezone%22%3Anull%2C%22actor_id%22%3A%22",
						_clone_uid,
						"%22%2C%22client_mutation_id%22%3A%224%22%7D%7D&server_timestamps=true&doc_id=4699960830024588"
					});

				string postData = fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsUpdateAccountMutation", lsd, referrer);
				string billable_account_tax_info = Regex.Match(postData, "\"billable_account_tax_info\":{(.*?)}").Groups[1].Value;
				if (string.IsNullOrEmpty(billable_account_tax_info))
				{
					this.log(postData);
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool addCardSuite(string ad_country_code, string card_country_code, string currency_code, string timezone, string cardcode, string checkType, string[] stepcode = null)
		{
			try
			{
				if (string.IsNullOrEmpty(pageID))
				{
					this.log("Không có Page ID!");
					return false;
				}

				switchBusinessTabHandle();

				// Check page
				string url = "https://business.facebook.com/latest/ad_center/ads_summary?asset_id=" + pageID + "&nav_ref=bm_more_tools_header";
                if (!string.IsNullOrEmpty(_bm_id))
                {
					url = "https://business.facebook.com/latest/ad_center/ads_summary?asset_id=" + pageID + "&business_id=" + _bm_id + "&nav_ref=bm_more_tools_header";
				}
				this._driver.Navigate().GoToUrl(url);
				WaitLoading();
				Delay(1000);

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string context_id = _clone_uid;
				this.log("Context_id: " + context_id);

				string adId = _ads_id;
				if (checkType == stepcode[4])
				{
					adId = _bm_ads_id;
				}

				// Change country
				string gurl = "https://business.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					context_id,
					"&__usid=&__user=",
					context_id,
					"&__a=1&__dyn=7AzHJ16-cxp2u6Xgydg9pE98C8GUmDDJ4WqF1e6E9ES3i6oF1eFGUpxqqax2qqEboymubyQUC37GiidBCDK7Gzu16UkzWzoSdwJJ4hfzLBzogwCyEhx21FxuHBy8G6EhwgpRxm2iq9CJ1-23wKCxy9wHwOCzoboryFFE4uWBBKdxRedU88CnhteEjwzlBBxO4EKVEkyoOmi1Dx3xiGxejgOXCxS55zEvyojz9e3mUugy2u6olULxq1zyEpz4fwhEa4bwWDKdxWbxmezXwmkVQaBwxwwwwVoC7Q2h2UthF98Wcxq68F0gohyE-3K4p84y5ovorx2awCx5e8xSXzUjzUuyqV8jx248nxm498W6axS3u5U4O6Ua8S2m8UuyEiwBx5e6U8EnwUzpA6E7y8yF8-48CQewRBCwSz8OEiwwy8gx64EKuiicG3qazodE5a1YDU&__csr=&__req=1v&__hs=18914.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004552454&__s=q089re%3Ae8qvpw%3Atjg4sm&__hsi=7018819346167009374-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=22076&lsd=",
					lsd,
					"&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=",
					stepcode[8],
					"&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					adId,
					"%22%2C%22currency%22%3A%22",
					currency_code,
					stepcode[6],
					ad_country_code,
					stepcode[7],
					"%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22second_tax_id%22%3A%22%22%2C%22second_tax_id_type%22%3Anull%2C%22tax_exempt%22%3Afalse%2C%22tax_id%22%3A%22%22%2C%22tax_id_type%22%3A%22NONE%22%7D%2C%22timezone%22%3A",
					timezone,
					"%2C%22actor_id%22%3A%22",
					context_id,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=4119854918068296"
				});

				string postData = this.fetchApi(gurl, "POST", body, stepcode[8], lsd, url);
				if (string.IsNullOrEmpty(postData))
				{
					this.log("Change country FALSE!");
				}
				else
				{
					string business_country_code = Regex.Match(postData, "\"business_country_code\":\"(.*?)\"").Groups[1].Value;
					string currency = Regex.Match(postData, "\"currency\":\"(.*?)\"").Groups[1].Value;
					if ((ad_country_code != business_country_code) || (currency_code != currency))
					{
						this.log("Change country FALSE!");
					}
					else
					{
						this.log("Change country TRUE!");
					}
				}

				// Add card
				bool checkRes = false;
				String[] cardTypeData = cardcode.Split('|');
				foreach (string cardTypeItem in cardTypeData)
				{
					this.log("Mã Thẻ: " + cardTypeItem);
					string card = this._getCardDelegate(cardTypeItem);
					if (!string.IsNullOrEmpty(card))
					{
						this.log("Thẻ: " + card);
						String[] cardData = card.Split('|');

						gurl = "https://business.secure.facebook.com/ajax/payment/token_proxy.php?tpe=%2Fapi%2Fgraphql%2F";
						body = string.Concat(new string[] {
							"av=",
							context_id,
							"&payment_dev_cycle=prod&__usid=6-Trja8yw1o532u2%3APrja8xc1j5bwno%3A0-Arja8ywbznx8s-RV%3D6%3AF%3D&__user=",
							context_id,
							"&__a=1&__dyn=7AzHxqU5a9zk1ryUqxenFw9uu2i5U4e2O1gyQdwSAx-bwNwnof8botwsovgO9wnoe8hw47w9u0LVEtwyzo1xoswIwuo88422y11xmfz81sbzoaEnxO1ywOwj8bU9kbxS2218wc61axe3S6Ueo5qfK6E7e58jwGzE8FU5e7oqBwJK2W5olwUwOzEjUlDw-CwLU72m7-2K1LwqEuBxe6pEbUGdwb622390sUowMwBwwAzUjwTwNwLwFg&__req=5h&__hs=19270.HYP%3Acomet_pkg.2.1.0.2.1&dpr=1&__ccg=EXCELLENT&__rev=1006329165&__s=ogmxg0%3A4mvvf6%3Avillyg&__hsi=7151024273977814060&__comet_req=15&fb_dtsg=",
							fb_dtsg,
							"&jazoest=25744&lsd=",
							lsd,
							"&__spin_r=1006329165&__spin_b=trunk&__spin_t=1664977584&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingAddCreditCardMutation&variables=%7B%22input%22%3A%7B%22billing_address%22%3A%7B%22country_code%22%3A%22",
							card_country_code,
							"%22%7D%2C%22billing_logging_data%22%3A%7B%22logging_counter%22%3A21%2C%22logging_id%22%3A%221725756273%22%7D%2C%22cardholder_name%22%3A%22A%20Cuong%22%2C%22credit_card_first_6%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(0,6),
							"%22%7D%2C%22credit_card_last_4%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0].Substring(cardData[0].Length - 4),
							"%22%7D%2C%22credit_card_number%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[0],
							"%22%7D%2C%22csc%22%3A%7B%22sensitive_string_value%22%3A%22",
							cardData[3],
							"%22%7D%2C%22expiry_month%22%3A%22",
							cardData[1],
							"%22%2C%22expiry_year%22%3A%22",
							cardData[2],
							"%22%2C%22payment_account_id%22%3A%22",
							adId,
							"%22%2C%22payment_type%22%3A%22MOR_ADS_INVOICE%22%2C%22unified_payments_api%22%3Atrue%2C%22actor_id%22%3A%22",
							context_id,
							"%22%2C%22client_mutation_id%22%3A%226%22%7D%7D&server_timestamps=true&doc_id=4987045951343337&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D431626192%22%5D"
						});

						postData = this.fetchApi(gurl, "POST", body, "useBillingAddCreditCardMutation", lsd, url);
						if (!string.IsNullOrEmpty(postData))
						{
							string credential_id = Regex.Match(postData, "\"credential_id\":\"(.*?)\"").Groups[1].Value;
							if (!string.IsNullOrEmpty(credential_id))
							{
								checkRes = true;
							}
							else
							{
								this.log("FALSE!");
								this.log(postData);
							}
						}
					}
					else
					{
						this.log("Hết thẻ!");
						break;
					}
				}

				return checkRes;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool addCardPageAdsSuiteTest(string ad_country, string card_country, string currency, string timezone, string cardcode, string checkType, string cnpjcode1 = "", string[] stepcode = null)
		{
			try
			{
				String[] cardTypeData = cardcode.Split('|');
                if (cardTypeData.Length < 2)
                {
					this.log("Phải cấu hình 2 thẻ, thẻ đầu tiên add ở SUITE!");
					return false;
				}
				this.log("Add MỒI card suite ...");
				addCardSuite(ad_country, card_country, currency, timezone, cardTypeData[0], checkType, stepcode);
				this.log("ADD THẺ CHÍNH ...");
				cardcode = String.Join("|", cardTypeData, 1, cardTypeData.Length - 1);
				return addCardPageAds(ad_country, card_country, currency, timezone, cardcode, checkType, cnpjcode1, stepcode);
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool BMadAddCardBP(string ad_country_code, string card_country_code, string currency_code, string timezone, string cardcode, string[] stepcode = null)
		{
			try
			{
				Delay(1000);
				string url = "https://business.facebook.com/content_management/?business_id=" + _bm_id;
				switchBusinessTabHandle();
				return this.addCardBpApiAsync(url, ad_country_code, card_country_code, currency_code, timezone, cardcode, "BMad", stepcode);
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		private bool addCardBpApiAsync(string url, string ad_country_code, string card_country_code, string currency_code, string timezone, string cardcode, string checkType, string[] stepcode)
		{
			try
			{
				if (!this._driver.Url.Contains("facebook.com"))
                {
                    this._driver.Navigate().GoToUrl(url);
                    WaitLoading();
                    Delay(500);

                    if (!this._driver.Url.Contains(stepcode[2]))
                    {
                        bool checkNavigate = false;
                        int ChkLoadingIndex = 1;
                        while ((!checkNavigate) && (ChkLoadingIndex <= 8))
                        {
                            Delay(3000);
                            this._driver.Navigate().GoToUrl(url);
                            WaitLoading();
                            Delay(500);
                            checkNavigate = this._driver.Url.Contains(stepcode[2]);
                            ChkLoadingIndex++;
                        }
                    }

                    if (!this._driver.Url.Contains(stepcode[2]))
                    {
                        this.log("Navigate FALSE!");
                        return false;
                    }
                }

                string data = this._driver.PageSource;
                string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
                string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

                string context_id = pageID;
                this.log("Context_id: " + context_id);

                string adId = _ads_id;
                if (checkType == stepcode[4])
                {
                    adId = _bm_ads_id;
                }

				// Change country
				string gurl = stepcode[5];
                string body = string.Concat(new string[] {
                    "av=",
                    context_id,
                    "&__usid=&__user=",
                    _clone_uid,
                    "&__a=1&__dyn=7AzHJ16-cxp2u6Xgydg9pE98C8GUmDDJ4WqF1e6E9ES3i6oF1eFGUpxqqax2qqEboymubyQUC37GiidBCDK7Gzu16UkzWzoSdwJJ4hfzLBzogwCyEhx21FxuHBy8G6EhwgpRxm2iq9CJ1-23wKCxy9wHwOCzoboryFFE4uWBBKdxRedU88CnhteEjwzlBBxO4EKVEkyoOmi1Dx3xiGxejgOXCxS55zEvyojz9e3mUugy2u6olULxq1zyEpz4fwhEa4bwWDKdxWbxmezXwmkVQaBwxwwwwVoC7Q2h2UthF98Wcxq68F0gohyE-3K4p84y5ovorx2awCx5e8xSXzUjzUuyqV8jx248nxm498W6axS3u5U4O6Ua8S2m8UuyEiwBx5e6U8EnwUzpA6E7y8yF8-48CQewRBCwSz8OEiwwy8gx64EKuiicG3qazodE5a1YDU&__csr=&__req=1v&__hs=18914.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004552454&__s=q089re%3Ae8qvpw%3Atjg4sm&__hsi=7018819346167009374-0&__comet_req=0&fb_dtsg=",
                    fb_dtsg,
                    "&jazoest=22076&lsd=",
                    lsd,
                    "&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=",
                    stepcode[8],
                    "&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
                    adId,
                    "%22%2C%22currency%22%3A%22",
                    currency_code,
                    stepcode[6],
					ad_country_code,
                    stepcode[7],
                    "%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22second_tax_id%22%3A%22%22%2C%22second_tax_id_type%22%3Anull%2C%22tax_exempt%22%3Afalse%2C%22tax_id%22%3A%22%22%2C%22tax_id_type%22%3A%22NONE%22%7D%2C%22timezone%22%3A",
					timezone,
					"%2C%22actor_id%22%3A%22",
                    context_id,
                    "%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=4119854918068296"
                });

                string referrer = "https://business.facebook.com/content_management/PUBLISHED_POSTS?business_id=" + _bm_id + "&global_scope_id=" + _bm_id + "&context_id=" + pageID + "&context_type=page";

                string postData = this.fetchApi(gurl, "POST", body, stepcode[8], lsd, referrer);
                string business_country_code = Regex.Match(postData, "\"business_country_code\":\"(.*?)\"").Groups[1].Value;
                string currency = Regex.Match(postData, "\"currency\":\"(.*?)\"").Groups[1].Value;
                if ((ad_country_code != business_country_code) || (currency_code != currency))
                {
                    this.log("Change country FALSE!");
                }
                else
                {
                    this.log("Change country TRUE!");
                }

				// Add card
				bool checkRes = false;
				String[] cardTypeData = cardcode.Split('|');
				foreach (string cardTypeItem in cardTypeData)
				{
					this.log("Mã Thẻ: " + cardTypeItem);
					string card = this._getCardDelegate(cardTypeItem);
					if (!string.IsNullOrEmpty(card))
					{
						this.log("Thẻ: " + card);
						String[] cardData = card.Split('|');

						gurl = stepcode[9];
						body = string.Concat(new string[] {
							"av=",
							context_id,
							"&payment_dev_cycle=prod&__usid=&__user=",
							_clone_uid,
							"&__a=1&__dyn=7AzHJ16-cxp2u6Xgydg9pE98C8GUmDDJ4WqF1e6E9ES3i6oF1eFGUpxqqax2qqEboymubyQUC37GiidBCDK7Gzu16UkzWzoSdwJJ4hfzLBzogwCyEhx21FxuHBy8G6EhwgpRxm2iq9CJ1-23wKCxy9wHwOCzoboryFFE4uWBBKdxRedU88CnhteEjwzlBBxO4EKVEkyoOmi1Dx3xiGxejgOXCxS55zEvyojz9e3mUugy2u6olULxq1zyEpz4fwhEa4bwWDKdxWbxmezXwmkVQaBwxwwwwVoC7Q2h2UthF98Wcxq68F0gohyE-3K4p84y5ovorx2awCx5e8xSXzUjzUuyqV8jx248nxm498W6axS3u5U4O6Ua8S2m8UuyEiwBx5e6U8EnwUzpA6E7y8yF8-48CQewRBCwSz8OEiwwy8gx64EKuiicG3qazodE5a1YDU&__req=24&__hs=18914.BP%3ADEFAULT.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1004552454&__s=2nfpm9%3Ae8qvpw%3Atjg4sm&__hsi=7018819346167009374-0&__comet_req=0&fb_dtsg=",
							fb_dtsg,
							"&jazoest=22076&lsd=",
							lsd,
							"&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingAddCreditCardMutation&variables=%7B%22input%22%3A%7B%22billing_address%22%3A%7B%22country_code%22%3A%22",
							card_country_code,
							"%22%7D%2C%22billing_logging_data%22%3A%7B%22logging_counter%22%3A26%2C%22logging_id%22%3A%22144129722%22%7D%2C%22cardholder_name%22%3A%22",
							_card_name,
							stepcode[10],
							cardData[0].Substring(0,6),
							stepcode[11],
							cardData[0].Substring(cardData[0].Length - 4),
							stepcode[12],
							cardData[0],
							stepcode[13],
							cardData[3],
							"%22%7D%2C%22expiry_month%22%3A%22",
							cardData[1],
							"%22%2C%22expiry_year%22%3A%22",
							cardData[2],
							"%22%2C%22payment_account_id%22%3A%22",
							adId,
							stepcode[14],
							context_id,
							"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=4126726757375265"
						});

						referrer = "https://business.facebook.com/";
						postData = this.fetchApi(gurl, "POST", body, "useBillingAddCreditCardMutation", lsd, referrer);
						string credential_id = Regex.Match(postData, "\"credential_id\":\"(.*?)\"").Groups[1].Value;

						if (!string.IsNullOrEmpty(credential_id))
						{
							checkRes = true;
						}
						else
						{
							this.log("FALSE!");
							this.log(postData);
						}
					}
					else
					{
						this.log("Hết thẻ!");
						break;
					}
				}

				return checkRes;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool cloneCreateNewAd(string country_code = "US", string currency_code = "USD", string viaAd = null)
		{
			try
			{
				switchRegTabHandle();
				string url = "https://www.facebook.com/ads/manager/account_settings/account_billing/?act=" + viaAd + "&pid=p1&page=account_settings&tab=account_billing_settings";
				string referrer = url;
				if (!this._driver.Url.Contains("manager/account_settings/account_billing"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string context_id = _clone_uid;

				// Create ad
				string gurl = "https://www.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					context_id,
					"&__usid=&__user=",
					context_id,
					"&__a=1&__dyn=7xeUmBz8fXgydwCwRyu2abBWqwOwCwgE98kGqErxSax2366UjyUW3qiidBxa7GzUK1exm6US2SfUkxe488o8ogw8i9y8G6Ehwt8aE4m1qwCwuE9FEdUmCBBwLghUcd5xS2dum11K6UC4F87y744FA48a8lwWxe4oeUa85vzoO0AE2qwgHzouwg85WUpwxwgum2B0Agak48W2e2i11xOfwXxq1uxZxK48G2q4kUy26U8U-7Ejwjo9Ejxy220D98fUS2W2K4E7K1uCgqw8O58Gm0BUO1tx64EKu9zawSyES2e0UE7e15K&__csr=&__req=x&__hs=19030.BP%3Aads_campaign_manager_pkg.2.0.0.0.&dpr=1&__ccg=EXCELLENT&__rev=1005032468&__s=bo0jly%3Ahf0re0%3Adyzb61&__hsi=7061916934546218594-0&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=21956&lsd=",
					lsd,
					"&__spin_r=1005032468&__spin_b=trunk&__spin_t=1644230665&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingAccountInformationUtilsCreateAccountMutation&variables=%7B%22input%22%3A%7B%22current_payment_legacy_account_id%22%3A%22",
					viaAd,
					"%22%2C%22logging_data%22%3A%7B%22logging_counter%22%3A17%2C%22logging_id%22%3A%221045632040%22%7D%2C%22new_country_code%22%3A%22",
					country_code,
					"%22%2C%22new_currency_code%22%3A%22",
					currency_code,
					"%22%2C%22new_tax%22%3A%7B%22business_address%22%3A%7B%22city%22%3A%22%22%2C%22country_code%22%3A%22UA%22%2C%22state%22%3A%22%22%2C%22street1%22%3A%22%22%2C%22street2%22%3A%22%22%2C%22zip%22%3A%22%22%7D%2C%22business_name%22%3A%22%22%2C%22is_personal_use%22%3Afalse%2C%22tax_exempt%22%3Afalse%2C%22tax_id%22%3A%22%22%7D%2C%22new_timezone%22%3A%22America%2FLos_Angeles%22%2C%22actor_id%22%3A%22",
					context_id,
					"%22%2C%22client_mutation_id%22%3A%222%22%7D%7D&server_timestamps=true&doc_id=4649659218451741"
				});

				string postData = this.fetchApi(gurl, "POST", body, "BillingAccountInformationUtilsCreateAccountMutation", lsd, referrer);
				string payment_legacy_account_id = Regex.Match(postData, "\"payment_legacy_account_id\":\"(.*?)\"").Groups[1].Value;
                if (string.IsNullOrEmpty(payment_legacy_account_id))
                {
					this.log(postData);
					return false;
				}
				_ads_id = payment_legacy_account_id;
				this.log("Ads id: " + _ads_id);
				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool BMadBillingMakePrimaryState(string[] stepData, bool without911 = false, string screen = "")
		{
			try
			{
				switchBusinessTabHandle();

				string url = "https://business.facebook.com/";
                if (screen == "")
                {
					url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + _bm_ads_id + "&pid=p1&business_id=" + _bm_id + "&page=account_settings&tab=account_billing_settings";
				}

				if (screen == "pe")
				{
					url = "https://business.facebook.com/adsmanager/manage/campaigns?act=" + _bm_ads_id + "&business_id=" + _bm_id + "&nav_entry_point=bm_ad_account_open_in_ads_manager_button";
				}

				Delay(1000);

				string referrer = url;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string context_id = _clone_uid;
				this.log("Context_id: " + context_id);

				string adId = _bm_ads_id;
				string gurl = "https://business.facebook.com/api/graphql/";
				string body = "";
				string postData = "";

				string bm_credential_id = "";
				foreach (Card billPm in _bm_cards)
				{
					if ((stepData.Contains(billPm.code)) && (billPm.bm_id == _bm_id))
					{
						bm_credential_id = billPm.credential_id;
						break;
					}
				}

				// MakePrimary
				string credential_id = "";
				body = string.Concat(new string[] {
					"av=",
					context_id,
					"&__usid=6-Trjeii84muvl7%3APrjeiju1adu87n%3A0-Arjeii8fr4ow7-RV%3D6%3AF%3D&__user=",
					context_id,
					"&__a=1&__dyn=7xeUmxa3-Q8zo9EdoK2abBWqBx659o9E4a2i5aCG6WxGax2366UjyUW3eF98SmcBxWE-16wFxKdwJzUi-mdwTxO482ey8G6EhwgpUtz8aE4mewEwyg9poe8S2S6UGq1eKFprwHgS-2TlaECfl0zlBwRyXxK9xu1UxO4VA49o8E-mdwIxe4oeUS6E465ubUmwJwmE2dz8txSqUS7EK2ifwnp8pwVwFVoC7U9k2B2V8W2e2i11xOfwXxq1uxZxK48GUtAx5e8wxK2efKazUjwjo9EjxyEtwDzE6mi3-dwKwHxa2i4XxK9xy5Ue8Sp1G0z8kyFoqwZCx22qcxybwDx24oiyVUCcG3qaz44QewnUO1RDU6C13BwiU8UgU4659899o4KeyEsyo&__csr=&__req=1s&__hs=19272.BP%3Aads_manager_pkg.2.0.0.0.0&dpr=1&__ccg=UNKNOWN&__rev=1006348864&__s=rp8020%3Axjp69p%3Aq9lwkk&__hsi=7151879393292879261&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25619&lsd=",
					lsd,
					"&__aaid=",
					adId,
					"&__spin_r=1006348864&__spin_b=trunk&__spin_t=1665176682&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=BillingMakePrimaryStateMutation&variables=%7B%22input%22%3A%7B%22billable_account_payment_legacy_account_id%22%3A%22",
					adId,
					"%22%2C%22logging_data%22%3A%7B%22logging_counter%22%3A23%2C%22logging_id%22%3A%223319799712%22%7D%2C%22primary_funding_id%22%3A%22",
					bm_credential_id,
					"%22%2C%22actor_id%22%3A%22",
					context_id,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=7114252661983063&fb_api_analytics_tags=%5B%22qpl_active_flow_ids%3D270213817%22%5D"
				});

				if (!without911)
                {
					postData = this.fetchApi(gurl, "POST", body, "BillingMakePrimaryStateMutation", lsd, referrer);
				}
                else
                {
					_use_proxy = false;
					postData = this.PostData(null, gurl, body, this._contentType, this._userAgent, _cookie, lsd, "BillingMakePrimaryStateMutation", referrer, true);
					_use_proxy = true;
				}

				credential_id = Regex.Match(postData, "\"credential_id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(credential_id))
				{
					this.log("BillingMakePrimaryState FALSE!");
					return false;
				}
				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool bmRemoveOneCard(string credential, string fb_dtsg, string lsd, string bm)
		{
			try
			{

				string referrer = "https://business.facebook.com/settings/payment-methods/" + credential + "?business_id=" + bm;
				string url = "https://business.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmxa2C5rgydwCwRyU8EKnFG2Om2q12xCagmCwRCwqojyUV0OGiidBxa7EiwhEmwKzobo8LwAwgUgwqoqyoyazoO4o461twOxa7Am4o8E9EmxGcwXxKaCwjHGbwQzoO2ng-3tpUdoK7UC5U7y78jxiUa8522m3K2y3WElUScyo723m1Lz84aVpUuyUd88EeAUpK1vDwFwBgak48W18wRwEwiUmwnHxJxK48G2q4p8y26U8U-UvzE4S7VEjCx6220D8d8-dwKwHxa1LyUnwUzpA6EfEO32fxiFVoa9obGwgUy1kx6bCyVUCfwLCyKbwzweau0Jo4K2e1FAwtElK2q&__csr=&__req=o&__hs=19359.BP%3Abrands_pkg.2.0.0.0.0&dpr=1&__ccg=EXCELLENT&__rev=1006774693&__s=w6p678%3A1nz3xs%3Auadoeu&__hsi=7184026775453251982&__comet_req=0&fb_dtsg=",
					fb_dtsg,
					"&jazoest=25344&lsd=",
					lsd,
					"&__aaid=&__spin_r=1006774693&__spin_b=trunk&__spin_t=1672661578&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=RemoveFundingSourceButtonV2CCMutation&variables=%7B%22biz_id%22%3A%22",
					bm,
					"%22%2C%22fs_id%22%3A%22",
					credential,
					"%22%7D&server_timestamps=true&doc_id=4511944528910719"
				});

				string postData = this.fetchApi(url, "POST", body, "RemoveFundingSourceButtonV2CCMutation", lsd, referrer);
				if (string.IsNullOrEmpty(postData))
				{
					this.log(postData);
					return false;
				}

				string idcr = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(idcr))
				{
					this.log(postData);
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool bmRemoveCardAsync()
		{
			try
			{
				switchBusinessTabHandle();

				string referrer = "https://business.facebook.com/settings/payment-methods/?business_id=" + _bm_id;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(referrer);
					WaitLoading();
					Delay(500);
				}

                if (string.IsNullOrEmpty(_credential_id))
                {
					this.log("Not credential_id!");
					return false;
				}

				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;
				bool rm = bmRemoveOneCard(_credential_id, fb_dtsg, lsd, _bm_id);
                if (rm)
                {
					var itemToRemove = _bm_cards.Single(r => r.credential_id == _credential_id);
					_bm_cards.Remove(itemToRemove);
				}
				return rm;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool bmRemoveAllCardAsync()
		{
			try
			{
				switchBusinessTabHandle();

				string referrer = "https://business.facebook.com/settings/payment-methods/?business_id=" + _bm_id;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(referrer);
					WaitLoading();
					Delay(500);
				}
				
				string data = this._driver.PageSource;
				string fb_dtsg = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string lsd = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				bool rm = true;
				foreach (Card billPm in _bm_cards.ToList())
                {
                    if (billPm.bm_id == _bm_id)
                    {
                        if (!bmRemoveOneCard(billPm.credential_id, fb_dtsg, lsd, _bm_id))
                        {
							this.log("KHÔNG XÓA ĐƯỢC THẺ: " + billPm.code);
							rm = false;
                        }
                        else
                        {
							_bm_cards.Remove(billPm);
						}
					}
                }
				return rm;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		public bool removeCardAsync(string adsId, string[] stepData)
		{
			try
			{
				switchBusinessTabHandle();

				string url = url = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + adsId + "&pid=p1page=account_settings&tab=account_billing_settings";
				string referrer = url;
				if (!this._driver.Url.Contains("business.facebook.com"))
				{
					this._driver.Navigate().GoToUrl(url);
					WaitLoading();
					Delay(500);
				}

				BillingAMNexusRootQuery biilData = this.getAdPaymentInfo(adsId);
				if ((biilData == null) || (biilData.data == null) || (biilData.data.billable_account_by_payment_account == null)
					|| (biilData.data.billable_account_by_payment_account.billing_payment_account == null)
					|| (biilData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods == null)
					|| (biilData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods.Count == 0)
					)
				{
					this.log("KHÔNG LẤY ĐƯỢC THẺ!");
					return false;
				}

				bool res = true;

				foreach (BillingPaymentMethod billPm in biilData.data.billable_account_by_payment_account.billing_payment_account.billing_payment_methods)
				{
					if (stepData.Contains("all") || (!billPm.is_primary))
					{
						credential credential = billPm.credential;
						if ((credential != null) && (!string.IsNullOrEmpty(credential.credential_id)))
						{
                            if (_removeCardAsync(adsId, credential.credential_id))
                            {
								this.log("Xóa thẻ: " + credential.display_string + " " + credential.last_four_digits + "--> TRUE!");
                            }
                            else
                            {
								res = false;
								this.log("Xóa thẻ: " + credential.display_string + " " + credential.last_four_digits + "--> FALSE!");
							}
						}
					}
				}

				return res;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		private bool _removeCardAsync(string account_id, string credential_id)
		{
			try
			{
				string data = this._driver.PageSource;
				string value = Regex.Match(data, "\"token\":\"(.*?)\"").Groups[1].Value;
				string value2 = Regex.Match(data, "\"token\":\"(.*?)\"").NextMatch().Groups[1].Value;

				string referrerUrl = "https://business.facebook.com/ads/manager/account_settings/account_billing/?act=" + account_id + "&pid=p1&page=account_settings&tab=account_billing_settings";
				string url = "https://business.facebook.com/api/graphql/";
				string body = string.Concat(new string[] {
					"av=",
					_clone_uid,
					"&__usid=6-Trfp0219c095j%3APrfpxwj19n084i%3A0-Arfpiwi1fziy3g-RV%3D6%3AF%3D&__user=",
					_clone_uid,
					"&__a=1&__dyn=7xeUmxa3-Q8zo9EdoDwyyVuCErx2m2q12wABzGCG6UtyJz8corxebzEdF98Sm4EuGfyU4W5orzobo-byEjx226264824yoyaxG4oaEugaoaEix6221qwCwuE9FEdUmCBBwLghUcd5xS2dum4E8EKUryonwu8sgiCggwExm3G4UhwXwEwl-dwh85G5o24xS2iUS7E421uK6o8o47BxSbwBgak48W2e2i11xOfwXxq1iyUvorx2awCx5e8wxK2efK6Ejwjo9Ejxy221uwZAw_zobEaUiwLBwho5Wp1G0z8kyFo2nz85S4oiyVUCcG3qaz44Qeweau1Hwio4K2e1FwLw&__csr=&__req=v&__hs=19201.BP%3Aads_campaign_manager_pkg.2.0.0.0.0&dpr=1&__ccg=EXCELLENT&__rev=1005924255&__s=yctgpi%3A8fxh95%3Aflzlx9&__hsi=7125313512229703980&__comet_req=0&fb_dtsg=",
					value,
					"&jazoest=25326&lsd=",
					value2,
					"&__spin_r=1005924255&__spin_b=trunk&__spin_t=1658991331&__jssesw=1&fb_api_caller_class=RelayModern&fb_api_req_friendly_name=useBillingRemovePMMutation&variables=%7B%22input%22%3A%7B%22payment_account_id%22%3A%22",
					account_id,
					"%22%2C%22payment_method_id%22%3A%22",
					credential_id,
					"%22%2C%22actor_id%22%3A%22",
					_clone_uid,
					"%22%2C%22client_mutation_id%22%3A%221%22%7D%7D&server_timestamps=true&doc_id=5091884294183137"
				});

				string postData = this.fetchApi(url, "POST", body, "useBillingRemovePMMutation", value2, referrerUrl);
                if (string.IsNullOrEmpty(postData))
                {
					this.log(postData);
					return false;
				}
				string idcr = Regex.Match(postData, "\"id\":\"(.*?)\"").Groups[1].Value;
				if (string.IsNullOrEmpty(idcr))
				{
					this.log(postData);
					return false;
				}

				return true;
			}
			catch (Exception ex2)
			{
				this.log(ex2);
				return false;
			}
		}

		private IWebElement getElement(By by, IWebDriver webdrv = null)
		{
			IWebElement result;
			try
			{
				if (webdrv == null)
				{
					webdrv = _driver;
				}

				int _count = 0;
				bool isValidData = false;
				IWebElement _ele = null;
				while (!isValidData)
				{
					_count++;
					bool flag = _count > 3;
					if (flag)
					{
						break;
					}
					_ele = webdrv.FindElement(by);
					isValidData = this.isValid(_ele);
					Thread.Sleep(1000);
				}
				result = _ele;
			}
			catch
			{
				result = null;
			}
			return result;
		}

		private void fillInput(IWebElement _ele, string s)
		{
			_ele.Click();
			_ele.SendKeys(OpenQA.Selenium.Keys.Control + "a");
			_ele.SendKeys("\b");
			_ele.SendKeys(s);
		}

		public void killChrome()
		{
			this.checkAndKillChromeDriverProcess();
			this.checkAndKillChromeProcess();
		}

		private string GetMainModuleFilepath(int processId)
		{
			string wmiQueryString = "SELECT ProcessId, ExecutablePath FROM Win32_Process WHERE ProcessId = " + processId;
			using (var searcher = new ManagementObjectSearcher(wmiQueryString))
			{
				using (var results = searcher.Get())
				{
					ManagementObject mo = results.Cast<ManagementObject>().FirstOrDefault();
					if (mo != null)
					{
						return (string)mo["ExecutablePath"];
					}
				}
			}
			return null;
		}

		public static readonly object Sync = new object();
		private bool checkAndKillChromeProcess()
		{
			Process[] pname = new Process[]{ };
			lock (Sync)
			{
				pname = Process.GetProcessesByName("chrome");
			}

			bool flag = pname.Length != 0;
			if (flag)
			{
				for (int i = 0; i < pname.Length; i++)
				{
					try
					{
						string path = pname[i].MainModule.FileName;
						if (!string.IsNullOrEmpty(path) && path.Contains(_dataDir))
						{
							lock (Sync)
							{
								pname[i].CloseMainWindow();
								pname[i].Kill();
								pname[i].WaitForExit();
							}
						}
					}
					catch
					{
					}
				}
			}

			return true;
		}

		private bool checkAndKillChromeDriverProcess()
		{
			Process[] pname = new Process[] { };
			lock (Sync)
			{
				pname = Process.GetProcessesByName("chromedriver");
			}
			bool flag = pname.Length != 0;
			bool result;
			if (flag)
			{
				for (int i = 0; i < pname.Length; i++)
				{
					try
					{
						string path = pname[i].MainModule.FileName;
						if (!string.IsNullOrEmpty(path) && path.Contains(_driverDir))
						{
							lock (Sync)
							{
								pname[i].CloseMainWindow();
								pname[i].Kill();
								pname[i].WaitForExit();
							}
						}
					}
					catch
					{
					}
				}
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		public bool initChromePrivate()
		{
			try
			{
				this.killChrome();
				bool flag = this._driver != null;
				if (flag)
				{
					this.QuitDriver();
				}

				var options = new ChromeOptions();
				options.AddArguments("--incognito");

				options.AddArguments("--disable-notifications");
				options.AddArguments("--disable-application-cache");
				options.AddArguments("--no-sandbox");
				options.AddArguments("--disable-extensions");
				options.AddArguments("--dns-prefetch-disable");
				options.AddArguments("--disable-gpu");
				options.PageLoadStrategy = PageLoadStrategy.Normal;

				if (_use_proxy)
				{
					string _proxy_str = _proxy_host + ":" + _proxy_port;
					if (_proxy_type == "socks5")
					{
						options.AddArguments("--proxy-server=socks5://" + _proxy_str);
					}
					if (_proxy_type == "http")
					{
						Proxy proxy = new Proxy();
						proxy.Kind = ProxyKind.Manual;
						proxy.IsAutoDetect = false;
						proxy.SslProxy = _proxy_str;
						options.Proxy = proxy;
					}
				}
				options.AddArgument("--ignore-certificate-errors");
				options.AddArgument("ignore-certificate-errors");
				options.AddArgument("--allow-insecure-localhost");
				options.AcceptInsecureCertificates = true;

				var driverService = ChromeDriverService.CreateDefaultService(_driverDir);
				driverService.HideCommandPromptWindow = true;
				this._driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(15.0));
				Delay(1000);

				((IJavaScriptExecutor)this._driver).ExecuteScript("window.open();");
				Delay(1000);
				ReadOnlyCollection<string> windows = this._driver.WindowHandles;
				this.regTabHandle = windows[0];
				this.otherTabHandle = windows[1];

				this._driver.SwitchTo().Window(this.regTabHandle);
				this._driver.Navigate().GoToUrl("https://www.facebook.com");
				Delay(1000);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool initChromePortable(string chrome, string userAgent = "", bool reset = false)
		{
			try
			{
				this.killChrome();
				this.QuitDriver();

				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir;

				string folderPath = dataPath + "\\via";
				string viaPath = folderPath + "\\" + chrome + "\\GoogleChromePortable";

				var options = new ChromeOptions();
				options.BinaryLocation = viaPath + "\\App\\Chrome-bin\\chrome.exe";
				StringBuilder builder = new StringBuilder(viaPath);
				builder.Replace("\\", "/");
				options.AddArgument("--user-data-dir=" + builder.ToString() + "/Data/profile");
				options.AddArgument("profile-directory=Default");

				// options.AddArguments("--incognito");
				if (!string.IsNullOrEmpty(userAgent))
				{
					options.AddArguments("--user-agent=" + userAgent);
				}

				options.AddArguments("--disable-notifications");
				options.AddArguments("--disable-application-cache");
				options.AddArguments("--no-sandbox");
				options.AddArguments("--disable-extensions");
				options.AddArguments("--dns-prefetch-disable");
				options.AddArguments("--disable-gpu");
				options.PageLoadStrategy = PageLoadStrategy.Normal;

				if (_use_proxy)
				{
					string _proxy_str = _proxy_host + ":" + _proxy_port;
					if (_proxy_type == "socks5")
					{
						options.AddArguments("--proxy-server=socks5://" + _proxy_str);
					}
					if (_proxy_type == "http")
					{
						Proxy proxy = new Proxy();
						proxy.Kind = ProxyKind.Manual;
						proxy.IsAutoDetect = false;
						proxy.SslProxy = _proxy_str;
						options.Proxy = proxy;
					}
				}
				options.AddArgument("--ignore-certificate-errors");
				options.AddArgument("--allow-insecure-localhost");
				options.AcceptInsecureCertificates = true;

				var driverService = ChromeDriverService.CreateDefaultService(_driverDir);
				driverService.HideCommandPromptWindow = true;
				this._driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(15.0));
				Delay(1000);
				((IJavaScriptExecutor)this._driver).ExecuteScript("window.open();");
				Delay(1000);
				((IJavaScriptExecutor)this._driver).ExecuteScript("window.open();");
				Delay(1000);
				((IJavaScriptExecutor)this._driver).ExecuteScript("window.open();");
				Delay(1000);
				ReadOnlyCollection<string> windows = this._driver.WindowHandles;
				this.regTabHandle = windows[0];
				this.businessTabHandle = windows[3];
				this.otherTabHandle = windows[2];
				this.shopifyHandle = windows[1];

				this._driver.SwitchTo().Window(this.regTabHandle);
                if (reset)
                {
					this.portableResetSetting(this._driver);
				}
				this._driver.Navigate().GoToUrl("https://www.facebook.com");
				Delay(1000);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		public bool initChromeProfile(string chrome)
		{
			try
			{
				this.killChrome();
				bool flag = this._driver != null;
				if (flag)
				{
					this._driver.Quit();
				}

				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\" + _dataDir;

				string folderPath = dataPath + "\\via";
				string viaPath = folderPath + "\\" + chrome + "\\GoogleChromePortable";

				var options = new ChromeOptions();
				options.BinaryLocation = viaPath + "\\App\\Chrome-bin\\chrome.exe";
				StringBuilder builder = new StringBuilder(viaPath);
				builder.Replace("\\", "/");
				options.AddArgument("--user-data-dir=" + builder.ToString() + "/Data/profile");
				options.AddArgument("profile-directory=Nini1");

				options.EnableMobileEmulation("iPhone 8");
				options.AddArguments("--window-size=400,900");

				options.AddArguments("--disable-notifications");
				options.AddArguments("--disable-application-cache");
				options.AddArguments("--no-sandbox");
				options.AddArguments("--disable-extensions");
				options.AddArguments("--dns-prefetch-disable");
				options.AddArguments("--disable-gpu");
				options.PageLoadStrategy = PageLoadStrategy.Normal;

				if (_use_proxy)
				{
					string _proxy_str = _proxy_host + ":" + _proxy_port;
					if (_proxy_type == "socks5")
					{
						options.AddArguments("--proxy-server=socks5://" + _proxy_str);
					}
					if (_proxy_type == "http")
					{
						Proxy proxy = new Proxy();
						proxy.Kind = ProxyKind.Manual;
						proxy.IsAutoDetect = false;
						proxy.SslProxy = _proxy_str;
						options.Proxy = proxy;
					}
				}

				options.AddArgument("--ignore-certificate-errors");
				options.AddArgument("ignore-certificate-errors");
				options.AddArgument("--allow-insecure-localhost");
				options.AcceptInsecureCertificates = true;

				var driverService = ChromeDriverService.CreateDefaultService(_driverDir);
				driverService.HideCommandPromptWindow = true;
				this._driver = new ChromeDriver(driverService, options, TimeSpan.FromMinutes(15.0));
				Delay(1000);
				((IJavaScriptExecutor)this._driver).ExecuteScript("window.open();");
				Delay(1000);
				((IJavaScriptExecutor)this._driver).ExecuteScript("window.open();");
				Delay(1000);
				ReadOnlyCollection<string> windows = this._driver.WindowHandles;
				this.regTabHandle = windows[0];
				this.otherTabHandle = windows[1];
				this.shopifyHandle = windows[2];

				this._driver.SwitchTo().Window(this.regTabHandle);
				this.portableResetSetting(this._driver);

				this._driver.Navigate().GoToUrl("https://facebook.com");
				Delay(1000);
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex);
				return false;
			}
		}

		private void portableCleanCache(IWebDriver chrome_driver)
		{
			try
			{
				chrome_driver.Navigate().GoToUrl("chrome://settings/clearBrowserData");
				Delay(500);
				WaitLoading(chrome_driver);

				// Clean cache
				string btnTagName = "//settings-ui";
				WaitAjaxLoading(By.XPath(btnTagName), 3, chrome_driver);
				ReadOnlyCollection<IWebElement> root1 = chrome_driver.FindElements(By.XPath(btnTagName));
				if (root1.Count > 0)
				{
					// reset
					IWebElement shadowRoot1 = ExpandRootElement(root1.First(), chrome_driver);
					IWebElement root2 = shadowRoot1.FindElement(By.TagName("settings-main"));
					IWebElement shadowRoot2 = ExpandRootElement(root2, chrome_driver);

					IWebElement root3 = shadowRoot2.FindElement(By.TagName("settings-basic-page"));
					IWebElement shadowRoot3 = ExpandRootElement(root3, chrome_driver);

					IWebElement basicPage = shadowRoot3.FindElement(By.Id("basicPage"));
					IWebElement root4 = basicPage.FindElement(By.CssSelector("settings-section[section=privacy]"));

					IWebElement root5 = root4.FindElement(By.TagName("settings-privacy-page"));
					IWebElement shadowRoot5 = ExpandRootElement(root5, chrome_driver);

					IWebElement root6 = shadowRoot5.FindElement(By.CssSelector("settings-clear-browsing-data-dialog"));
					IWebElement shadowRoot6 = ExpandRootElement(root6, chrome_driver);

					IWebElement root7 = shadowRoot6.FindElement(By.CssSelector("cr-dialog"));
					IWebElement btncontainer = root7.FindElement(By.CssSelector("div[slot='button-container']"));
					IWebElement btn = btncontainer.FindElement(By.CssSelector("cr-button[id=clearBrowsingDataConfirm]"));
					btn.Click();

					// check loading
					bool loading = true;
					int ChkLoadingIndex = 1;
					while (loading && (ChkLoadingIndex <= 3))
					{
						Delay(3000);
						if (chrome_driver.Url.Contains("settings/clearBrowserData"))
						{
							loading = true;
						}
						else
						{
							loading = false;
						}
						ChkLoadingIndex++;
					}

					WaitLoading(chrome_driver);
				}
			}
			catch (Exception ex)
			{
				this.log(ex);
				return;
			}
		}
		private void portableResetSetting(IWebDriver chrome_driver)
		{
			try
			{
				chrome_driver.Navigate().GoToUrl("chrome://settings/resetProfileSettings");
				Delay(500);
				WaitLoading(chrome_driver);

				string btnTagName = "//settings-ui";
				WaitAjaxLoading(By.XPath(btnTagName), 3, chrome_driver);
				ReadOnlyCollection<IWebElement> root1 = chrome_driver.FindElements(By.XPath(btnTagName));
				if (root1.Count > 0)
				{
					// reset
					IWebElement shadowRoot1 = ExpandRootElement(root1.First(), chrome_driver);
					ReadOnlyCollection<IWebElement> root2s = shadowRoot1.FindElements(By.CssSelector("div[id=container] > settings-main[id=main]"));
					if (root2s.Count > 0)
					{
						IWebElement shadowRoot2 = ExpandRootElement(root2s.First(), chrome_driver);
						ReadOnlyCollection<IWebElement> root3s = shadowRoot2.FindElements(By.CssSelector("settings-basic-page[role=main]"));
						if (root3s.Count > 0)
						{
							IWebElement shadowRoot3 = ExpandRootElement(root3s.First(), chrome_driver);
							ReadOnlyCollection<IWebElement> root4s = shadowRoot3.FindElements(By.CssSelector("div[id=advancedPage] > settings-section[section=reset] > settings-reset-page"));
							if (root4s.Count > 0)
							{
								IWebElement shadowRoot4 = ExpandRootElement(root4s.First(), chrome_driver);
								ReadOnlyCollection<IWebElement> root5s = shadowRoot4.FindElements(By.CssSelector("settings-animated-pages[id=reset-pages] > div > settings-reset-profile-dialog"));
								if (root5s.Count > 0)
								{
									IWebElement shadowRoot5 = ExpandRootElement(root5s.First(), chrome_driver);
									ReadOnlyCollection<IWebElement> btns = shadowRoot5.FindElements(By.CssSelector("cr-dialog > div[slot=button-container] > cr-button[id=reset]"));
									if (btns.Count > 0)
									{
										btns.First().Click();
										Delay(1000);

										// check loading
										bool loading = true;
										int ChkLoadingIndex = 1;
										while (loading && (ChkLoadingIndex <= 3))
										{
											Delay(3000);
											if (chrome_driver.Url.Contains("settings/resetProfileSettings"))
											{
												loading = true;
											}
											else
											{
												loading = false;
											}
											ChkLoadingIndex++;
										}

										WaitLoading(chrome_driver);
										Delay(1000);
									}
								}
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				this.log(ex);
				return;
			}
		}

		private IWebElement ExpandRootElement(IWebElement element, IWebDriver webdrv = null)
		{
			if (webdrv == null)
			{
				webdrv = _driver;
			}
			IWebElement ele = (IWebElement)((IJavaScriptExecutor)webdrv).ExecuteScript("return arguments[0].shadowRoot", element);
			return ele;
		}

		private bool isValid(IWebElement _ele)
		{
			bool result;
			try
			{
				bool flag = _ele == null;
				if (flag)
				{
					result = false;
				}
				else
				{
					result = (_ele.Displayed && _ele.Enabled);
				}
			}
			catch
			{
				result = false;
			}
			return result;
		}

		bool disposed;
		protected virtual void Dispose(bool disposing)
		{
			if (!disposed)
			{
				if (disposing)
				{
					//dispose managed resources
				}
			}
			//dispose unmanaged resources
			disposed = true;
		}

		public void Dispose()
		{
			this.QuitDriver();
			Dispose(true);
			GC.SuppressFinalize(this);
		}
	}
}
