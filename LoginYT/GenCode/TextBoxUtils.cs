﻿using LoginYT.GenCode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace GenCode
{
	internal class TextBoxUtils
	{
		public static TextBoxUtils Instance
		{
			get
			{
				TextBoxUtils._instance = (TextBoxUtils._instance ?? new TextBoxUtils());
				return TextBoxUtils._instance;
			}
		}

		public void AppendRichText(Control ctl, RichTextBox tb, string text)
		{
			try
			{
				bool invokeRequired = tb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetTextCallback d = new TextBoxUtils.SetTextCallback(this.AppendRichText);
					ctl.Invoke(d, new object[]
					{
						ctl,
						tb,
						text
					});
				}
				else
				{
					tb.AppendText(text);
					this.setFocusEndTextbox(tb);
				}
			}
			catch
			{
			}
		}

		public void AppendRichText2(Control ctl, RichTextBox tb, string text)
		{
			try
			{
				bool invokeRequired = tb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetTextCallback d = new TextBoxUtils.SetTextCallback(this.AppendRichText2);
					ctl.Invoke(d, new object[]
					{
						ctl,
						tb,
						text
					});
				}
				else
				{
					bool flag = tb.Text.Length > 20000;
					if (flag)
					{
						tb.Text = "";
					}
					tb.AppendText(text);
					this.setFocusEndTextbox(tb);
				}
			}
			catch
			{
			}
		}

		public string RichTextReadAndRemoveLastLine(Control ctl, RichTextBox tb, bool rm)
		{
			try
			{
				bool invokeRequired = tb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetTextReturnValCallback d = new TextBoxUtils.SetTextReturnValCallback(this.RichTextReadAndRemoveLastLine);
					string controltext = (string)ctl.Invoke(d, new object[]
					{
						ctl,
						tb,
						rm
					});

					return controltext;
				}
				else
				{
					List<string> myList = tb.Lines.ToList();
					myList = myList.Where(s => !string.IsNullOrWhiteSpace(s)).ToList();
					string last = myList.Last();
					if (rm && myList.Count > 0)
					{
						myList.RemoveAt(myList.Count - 1);
						tb.Lines = myList.ToArray();
						tb.Refresh();
					}

					return last;
				}
			}
			catch
			{
				return "";
			}
		}

		public string RichTextReadLineByIndex(Control ctl, RichTextBox tb, int index)
		{
			try
			{
				bool invokeRequired = tb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetTextReturnValByIndexCallback d = new TextBoxUtils.SetTextReturnValByIndexCallback(this.RichTextReadLineByIndex);
					string controltext = (string)ctl.Invoke(d, new object[]
					{
						ctl,
						tb,
						index
					});

					return controltext;
				}
				else
				{
					List<string> myList = tb.Lines.ToList();
					return myList[index];
				}
			}
			catch
			{
				return "";
			}
		}

		public string[] RichTextReadAll(Control ctl, RichTextBox tb)
		{
			try
			{
				bool invokeRequired = tb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetLinesReturnValCallback d = new TextBoxUtils.SetLinesReturnValCallback(this.RichTextReadAll);
					string[] controltext = (string[])ctl.Invoke(d, new object[]
					{
						ctl,
						tb
					});

					return controltext;
				}
				else
				{
					return tb.Lines;
				}
			}
			catch
			{
				return null;
			}
		}

		public void AppendRichTextLn(Control ctl, RichTextBox tb, string text)
		{
			this.AppendRichText(ctl, tb, text + "\n");
		}

		public void AppendRichTextLn2(Control ctl, RichTextBox tb, string text)
		{
			this.AppendRichText2(ctl, tb, text + "\n");
		}

		public void SetButtonText(Control ctl, Button tb, string text)
		{
			try
			{
				bool invokeRequired = tb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetButtonCallback d = new TextBoxUtils.SetButtonCallback(this.SetButtonText);
					ctl.Invoke(d, new object[]
					{
						ctl,
						tb,
						text
					});
				}
				else
				{
					tb.Text = text;
				}
			}
			catch
			{
			}
		}

		public void SetInputText(Control ctl, TextBox tb, string text)
		{
			try
			{
				bool invokeRequired = tb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetTextBoxCallback d = new TextBoxUtils.SetTextBoxCallback(this.SetInputText);
					ctl.Invoke(d, new object[]
					{
						ctl,
						tb,
						text
					});
				}
				else
				{
					tb.Text = text;
				}
			}
			catch
			{
			}
		}

		public void AppendText(Control ctl, TextBox tb, string text)
		{
			try
			{
				bool invokeRequired = tb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetTextBoxCallback d = new TextBoxUtils.SetTextBoxCallback(this.AppendText);
					ctl.Invoke(d, new object[]
					{
						ctl,
						tb,
						text
					});
				}
				else
				{
					tb.AppendText(text);
				}
			}
			catch
			{
			}
		}

		public void AppendTextLn(Control ctl, TextBox tb, string text)
		{
			this.AppendText(ctl, tb, text + "\n");
		}

		public void SetLabelText(Control ctl, Label tb, string text, System.Drawing.Color color)
		{
			try
			{
				bool invokeRequired = tb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetLabelCallback d = new TextBoxUtils.SetLabelCallback(this.SetLabelText);
					ctl.Invoke(d, new object[]
					{
						ctl,
						tb,
						text,
						color
					});
				}
				else
				{
					tb.Text = text;
					tb.ForeColor = color;
				}
			}
			catch (Exception error)
			{
				System.Diagnostics.Debug.WriteLine(error.Message);
			}
		}

		public void AddComboBoxItem(Control ctl, ComboBox cb, ComboboxItem item)
		{
			try
			{
				bool invokeRequired = cb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.AddComboBoxItemCallback d = new TextBoxUtils.AddComboBoxItemCallback(this.AddComboBoxItem);
					ctl.Invoke(d, new object[]
					{
						ctl,
						cb,
						item
					});
				}
				else
				{
                    if (item == null)
                    {
						cb.Items.Clear();
                    }
                    else
                    {
						cb.Items.Add(item);
					}
				}
			}
			catch (Exception error)
			{
				System.Diagnostics.Debug.WriteLine(error.Message);
			}
		}

		public void SetComboBoxSelect(Control ctl, ComboBox cb, int index)
		{
			try
			{
				bool invokeRequired = cb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.SetComboBoxSelectCallback d = new TextBoxUtils.SetComboBoxSelectCallback(this.SetComboBoxSelect);
					ctl.Invoke(d, new object[]
					{
						ctl,
						cb,
						index
					});
				}
				else
				{
					cb.SelectedIndex = index;
				}
			}
			catch (Exception error)
			{
				System.Diagnostics.Debug.WriteLine(error.Message);
			}
		}

		public string getComboBoxSelect(Control ctl, ComboBox cb)
		{
			try
			{
				bool invokeRequired = cb.InvokeRequired;
				if (invokeRequired)
				{
					TextBoxUtils.getComboBoxSelectCallback d = new TextBoxUtils.getComboBoxSelectCallback(this.getComboBoxSelect);
					string controltext = (string)ctl.Invoke(d, new object[]
					{
						ctl,
						cb
					});

					return controltext;
				}
				else
				{
                    if (cb.SelectedItem != null)
                    {
						return (cb.SelectedItem as ComboboxItem).Value.ToString();
                    }
                    else
                    {
						return null;
                    }
				}
			}
			catch (Exception error)
			{
				System.Diagnostics.Debug.WriteLine(error.Message);
				return null;
			}
		}

		public void setFocusEndTextbox(RichTextBox _tb)
		{
			_tb.SelectionStart = _tb.Text.Length;
			_tb.ScrollToCaret();
		}


		private static TextBoxUtils _instance;

		private delegate void SetTextCallback(Control ctl, RichTextBox tb, string text);

		private delegate string SetTextReturnValCallback(Control ctl, RichTextBox tb, bool rm);

		private delegate string SetTextReturnValByIndexCallback(Control ctl, RichTextBox tb, int index);

		private delegate string[] SetLinesReturnValCallback(Control ctl, RichTextBox tb);

		private delegate void SetTextBoxCallback(Control ctl, TextBox tb, string text);

		private delegate void SetButtonCallback(Control ctl, Button tb, string text);

		private delegate void SetLabelCallback(Control ctl, Label tb, string text, System.Drawing.Color color);

		private delegate void AddComboBoxItemCallback(Control ctl, ComboBox cb, ComboboxItem item);
		private delegate void SetComboBoxSelectCallback(Control ctl, ComboBox cb, int index);
		private delegate string getComboBoxSelectCallback(Control ctl, ComboBox cb);
	}
}
