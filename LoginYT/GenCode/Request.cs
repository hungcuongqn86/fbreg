﻿using LoginYT.GenCode;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using xNet;
using FormUrlEncodedContent = System.Net.Http.FormUrlEncodedContent;

namespace GenCode
{
	public class AutoRequest
    {
		private string _userAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/94.0.4606.61 Safari/537.36";
		// private string _contentType = "application/x-www-form-urlencoded";
		public string _store_token = "";
		public string _ip = "";
		public string _ThreadName { get; set; }
		public string _api_url_root = "http://nguyenlieu.site/api/public";
		// public string _api_url_root = "http://localhost";
		public string _api_ver = "/api/v1";

		public delegate void LogDelegate(string s);
		public AutoRequest.LogDelegate _logDelegate { get; set; }

		private static readonly HttpClient client = new HttpClient();

		public AutoRequest(string ip)
		{
			try
			{
				this._ip = ip;
				string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
				string dataPath = desktopPath + "\\tut25flowdata\\DATA\\sys\\nini.txt";
				if (File.Exists(dataPath))
				{
					System.IO.StreamReader file = new System.IO.StreamReader(dataPath);
					string key = file.ReadToEnd();
					file.Close();
					if (!string.IsNullOrEmpty(key))
					{
						this._store_token = "Bearer " + key;
						client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", key);
					}
				}
			}
			catch (Exception error)
			{
				this.log(error);
			}
		}

		public string GetFromStoreData(string url)
		{
			string result;
			try
			{
				xNet.HttpRequest http = new xNet.HttpRequest
				{
					Cookies = new CookieDictionary(false)
				};

				http.UserAgent = this._userAgent;
				http.AddHeader("Authorization", _store_token);
				result = http.Get(url, null).ToString();
			}
			catch (Exception ex2)
			{
				result = ex2.Message;
			}
			return result;
		}

		public string PostToStoreData(string url, RequestParams data = null)
		{
			string result;
			try
			{
				xNet.HttpRequest http = new xNet.HttpRequest
				{
					Cookies = new CookieDictionary(false)
				};

				http.UserAgent = this._userAgent;
                if (!string.IsNullOrEmpty(_store_token))
                {
					http.AddHeader("Authorization", _store_token);
				}
				result = http.Post(url, data).ToString();
			}
			catch (Exception ex)
			{
				// this.log(ex);
				result = ex.Message;
			}
			return result;
		}

		private async Task<string> WebPostToStoreDataAsync(string url, Dictionary<string, string> data = null)
		{
			string result;
			try
			{
				System.Net.Http.HttpContent content = new FormUrlEncodedContent(data);
				var response = await client.PostAsync(url, content);
				result = await response.Content.ReadAsStringAsync();
			}
			catch (Exception ex)
			{
				result = ex.Message;
			}
			return result;
		}

		private void log(string s)
		{
			bool flag = this._logDelegate != null;
			if (flag)
			{
				this._logDelegate("Thread " + this._ThreadName + "->" + s);
			}
		}

		private void log(Exception ex)
		{
			bool flag = this._logDelegate != null;
			if (flag)
			{
				this._logDelegate(string.Concat(new string[]
				{
					"Thread ",
					this._ThreadName,
					"->",
					ex.Message,
					"\n",
					ex.StackTrace
				}));
			}
		}

		public string checkKey()
		{
			try
			{
                if (string.IsNullOrEmpty(_store_token))
                {
					return null;
				}

				string url = _api_url_root + _api_ver + "/checkKey?ip=" + _ip;
				string getdata = this.GetFromStoreData(url);
				return Regex.Match(getdata, "\"email\":\"(.*?)\"").Groups[1].Value; ;
			}
			catch
			{
				return null;
			}
		}

		public int activeKey(string key)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/activekey";
				RequestParams reqParams = new RequestParams();
				reqParams["key"] = key;
				reqParams["ip"] = this._ip;
				string postdata = this.PostToStoreData(url, reqParams);
				string token = Regex.Match(postdata, "\"token\":\"(.*?)\"").Groups[1].Value;
				if (!string.IsNullOrEmpty(token))
				{
					string desktopPath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
					string dataPath = desktopPath + "\\tut25flowdata\\DATA\\sys";
					bool exists = System.IO.Directory.Exists(dataPath);
					if (!exists)
						System.IO.Directory.CreateDirectory(dataPath);

					File.WriteAllText(dataPath + "\\nini.txt", token);

					this._store_token = "Bearer " + token;
					client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
					return 0;
				}

				string errorStr = Regex.Match(postdata, "\"error\":(.*?)}").Groups[1].Value;
				if (string.IsNullOrEmpty(errorStr))
				{
					return 9;
				}

				return Int32.Parse(errorStr);
			}
			catch
			{
				return 10;
			}
		}

		public string[] getFlow(string fl)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/checkKey?ip=" + _ip;
				string getdata = this.GetFromStoreData(url);
				NlUserResBody userData = JsonConvert.DeserializeObject<NlUserResBody>(getdata);
                if (userData == null || userData.success == null)
                {
					return null;
				}

				var flowdatastr = userData.success.GetType().GetProperty(fl);
                if (flowdatastr == null)
                {
					return null;
				}
				var flowdata = flowdatastr.GetValue(userData.success, null);
				if (flowdata == null)
				{
					return null;
				}

				return flowdata.ToString().Split(new string[] { "\r\n", "\r", "\n" }, StringSplitOptions.None);
			}
			catch
			{
				return null;
			}
		}
		
		public string getSetting(string key)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/setting/valbykey?key=" + key;
				string getdata = this.GetFromStoreData(url);
				return Regex.Match(getdata, "\"value\":\"(.*?)\"").Groups[1].Value;
			}
			catch
			{
				return null;
			}
		}
		public string setClone(bool checkip)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/mclone/setuse";
                if (checkip)
                {
					url = url + "?hip=" + _ip;
				}
				string getdata = this.GetFromStoreData(url);
				CloneResBody resq = JsonConvert.DeserializeObject<CloneResBody>(getdata);
                if ((resq==null) || (!resq.status) || (resq.data == null))
                {
					return null;
				}

				return resq.data.uid + "|" + resq.data.pass + "|" + resq.data.s2fa + "|" + resq.data.verify_link + "|" + resq.data.cookie;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public bool resetClone(string uid)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/mclone/resetuid/" + uid;
				RequestParams reqParams = new RequestParams();
				string getdata = this.PostToStoreData(url, reqParams);
				CloneResBody resq = JsonConvert.DeserializeObject<CloneResBody>(getdata);
				if ((resq == null) || (!resq.status))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public bool removeClone(string uid)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/mclone/deleteuid/" + uid;
				RequestParams reqParams = new RequestParams();
				string getdata = this.PostToStoreData(url, reqParams);
				CloneResBody resq = JsonConvert.DeserializeObject<CloneResBody>(getdata);
				if ((resq == null) || (!resq.status))
				{
					return false;
				}

				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public BmLinkResBody getBmLink(string bmId)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/bmlink/trim/" + bmId;
				string getdata = this.GetFromStoreData(url);
				return JsonConvert.DeserializeObject<BmLinkResBody>(getdata);
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public bool checkDataCheckExist(string code)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/datacheck/check_exist/" + code;
				string getdata = this.GetFromStoreData(url);
				string coderq = Regex.Match(getdata, "\"code\":\"(.*?)\"").Groups[1].Value;
                if (!string.IsNullOrEmpty(coderq))
                {
					return true;
				}
				return false;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		public BmLinkResBody getBmLinkAndRemove(string bmId)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/bmlink/getandremove/" + bmId;
				string getdata = this.GetFromStoreData(url);
				return JsonConvert.DeserializeObject<BmLinkResBody>(getdata);
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public string[] getStepCodes(string step)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/step/stepcode/" + step;
				string getdata = this.GetFromStoreData(url);
				string data = Regex.Match(getdata, "\"data\":\"(.*?)\"").Groups[1].Value;
				data = data.Replace("\\/", "/");

				if (string.IsNullOrEmpty(data))
				{
					return new string[] { };
				}
				return data.Split('|');
			}
			catch
			{
				return new string[] { };
			}
		}

		public bool storeCamp(string camp)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/camp/store";
				var values = new Dictionary<string, string>
				{
					{ "url", camp }
				};

				Task<string> datatask = WebPostToStoreDataAsync(url, values);
				string datapost = datatask.Result;
				// this.log("Tam Log: " + datapost);
				string status = Regex.Match(datapost, "\"status\":(.*?),").Groups[1].Value;
				if (string.IsNullOrEmpty(status))
				{
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		// BM
		public NLBm getBmDetail(string code)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/bm/detail/" + code;
				string getdata = this.GetFromStoreData(url);
				NLBmDetailResBody nlRes = JsonConvert.DeserializeObject<NLBmDetailResBody>(getdata);
				if ((nlRes == null) || (!nlRes.status) || (nlRes.data == null))
				{
					return null;
				}

				return nlRes.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public PageResBody getBmPendingPages(string code)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/bm/pendingpage/" + code;
				string getdata = this.GetFromStoreData(url);
				NLPageResBody nlRes = JsonConvert.DeserializeObject<NLPageResBody>(getdata);
                if ((nlRes == null) || (!nlRes.status))
                {
					return null;
				}

				return nlRes.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public AdsResBody getBmPendingAds(string code)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/bm/pendingads/" + code;
				string getdata = this.GetFromStoreData(url);
				NLAdsResBody nlRes = JsonConvert.DeserializeObject<NLAdsResBody>(getdata);
				if ((nlRes == null) || (!nlRes.status))
				{
					return null;
				}

				return nlRes.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		// Add page
		public string postBmAddClientPage(string code, string page_id)
		{
			try
			{
				RequestParams reqParams = new RequestParams();
				reqParams["page_id"] = page_id;
				string url = _api_url_root + _api_ver + "/bm/addpage/" + code;
				return this.PostToStoreData(url, reqParams);
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		// Check page
		public bool checkHasPage(string code, string page_id)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/bm/clientpages/" + code;
				string getdata = this.GetFromStoreData(url);
				NLPageResBody nlRes = JsonConvert.DeserializeObject<NLPageResBody>(getdata);
				if ((nlRes == null) || (!nlRes.status) || (nlRes.data == null) || (nlRes.data.data == null) || (nlRes.data.data.Count == 0))
				{
					return false;
				}

				bool res = false;
				foreach (page item in nlRes.data.data)
                {
                    if (item.id == page_id)
                    {
						res = true;
						break;
					}
                }

				return res;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		// Add ads
		public string postBmAddClientAds(string code, string ads_id)
		{
			try
			{
				RequestParams reqParams = new RequestParams();
				reqParams["ads_id"] = ads_id;
				string url = _api_url_root + _api_ver + "/bm/addads/" + code;
				return this.PostToStoreData(url, reqParams);
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		// Check ads
		public bool checkHasAds(string code, string ads_id)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/bm/clientads/" + code;
				string getdata = this.GetFromStoreData(url);
				NLAdsResBody nlRes = JsonConvert.DeserializeObject<NLAdsResBody>(getdata);
				if ((nlRes == null) || (!nlRes.status) || (nlRes.data == null) || (nlRes.data.data == null) || (nlRes.data.data.Count == 0))
				{
					return false;
				}

				bool res = false;
				foreach (ads item in nlRes.data.data)
				{
					if (item.id == "act_" + ads_id)
					{
						res = true;
						break;
					}
				}

				return res;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return false;
			}
		}

		// Cancel Add ads
		public string cancelAddAdsToBm(string code, string ads_id)
		{
			try
			{
				RequestParams reqParams = new RequestParams();
				reqParams["ads_id"] = ads_id;
				string url = _api_url_root + _api_ver + "/bm/canceladdads/" + code;
				return this.PostToStoreData(url, reqParams);
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		// Cancel Add page
		public string cancelAddPageToBm(string code, string page_id)
		{
			try
			{
				RequestParams reqParams = new RequestParams();
				reqParams["page_id"] = page_id;
				string url = _api_url_root + _api_ver + "/bm/canceladdpage/" + code;
				return this.PostToStoreData(url, reqParams);
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		// Set Page Role
		public string setPageRole(string bmid, string page_id, string type, string user_page)
		{
			try
			{
				RequestParams reqParams = new RequestParams();
				reqParams["type"] = type;
				reqParams["page_id"] = page_id;
				reqParams["user_page"] = user_page;
				string url = _api_url_root + _api_ver + "/bm/setrolepage/" + bmid;
				return this.PostToStoreData(url, reqParams);
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		// Add bm
		public NLBm postBmStore(string bm_id, string token, string uid, string cookie)
		{
			try
			{
				RequestParams reqParams = new RequestParams();
				reqParams["bm_id"] = bm_id;
				reqParams["token"] = token;
				reqParams["uid"] = uid;
				reqParams["cookie"] = cookie;
				string url = _api_url_root + _api_ver + "/bm/store";
				string postRes = this.PostToStoreData(url, reqParams);
				NLBmDetailResBody nlRes = JsonConvert.DeserializeObject<NLBmDetailResBody>(postRes);
				if ((nlRes == null) || (!nlRes.status) || (nlRes.data == null))
				{
					return null;
				}
				return nlRes.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		// Update bm
		public NLBm postBmUpdate(string id, string bm_id, string token, string uid, string cookie)
		{
			try
			{
				RequestParams reqParams = new RequestParams();
				reqParams["id"] = id;
				reqParams["bm_id"] = bm_id;
				reqParams["token"] = token;
				reqParams["uid"] = uid;
				reqParams["cookie"] = cookie;
				string url = _api_url_root + _api_ver + "/bm/update/" + id;
				string postRes = this.PostToStoreData(url, reqParams);
				NLBmDetailResBody nlRes = JsonConvert.DeserializeObject<NLBmDetailResBody>(postRes);
				if ((nlRes == null) || (!nlRes.status) || (nlRes.data == null))
				{
					return null;
				}
				return nlRes.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		// Profile Page
		public NLProPage getProPageDetail(string code)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/propage/detail/" + code;
				string getdata = this.GetFromStoreData(url);
				NLProPageDetailResBody nlRes = JsonConvert.DeserializeObject<NLProPageDetailResBody>(getdata);
				if ((nlRes == null) || (!nlRes.status) || (nlRes.data == null))
				{
					return null;
				}

				return nlRes.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public string inviteAdminPage(string pagecode, string admin_id)
		{
			try
			{
				RequestParams reqParams = new RequestParams();
				reqParams["admin_id"] = admin_id;
				string url = _api_url_root + _api_ver + "/propage/invite/" + pagecode;
				return this.PostToStoreData(url, reqParams);
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public NLResource getResource(string typecode)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/resource/setuse?type=" + typecode;
				string getdata = this.GetFromStoreData(url);
				NLResourceResBody resourceResBody = JsonConvert.DeserializeObject<NLResourceResBody>(getdata);
				if ((resourceResBody == null) || (!resourceResBody.status) || (resourceResBody.data == null))
				{
					return null;
				}

				return resourceResBody.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public List<NLParam> getParams()
		{
			try
			{
				string url = _api_url_root + _api_ver + "/param/getbyuser";
				string getdata = this.GetFromStoreData(url);
				NLParamsResBody paramResBody = JsonConvert.DeserializeObject<NLParamsResBody>(getdata);
				if ((paramResBody == null) || (!paramResBody.status) || (paramResBody.data == null))
				{
					return null;
				}

				return paramResBody.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public NLParam getParam(string paramId)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/param/detail/" + paramId;
				string getdata = this.GetFromStoreData(url);
				NLParamResBody paramResBody = JsonConvert.DeserializeObject<NLParamResBody>(getdata);
				if ((paramResBody == null) || (!paramResBody.status) || (paramResBody.data == null))
				{
					return null;
				}

				return paramResBody.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		//
		public List<NLCamp> getCamps()
		{
			try
			{
				string url = _api_url_root + _api_ver + "/campaign/getbyuser";
				string getdata = this.GetFromStoreData(url);
				NLCampsResBody campResBody = JsonConvert.DeserializeObject<NLCampsResBody>(getdata);
				if ((campResBody == null) || (!campResBody.status) || (campResBody.data == null))
				{
					return null;
				}

				return campResBody.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}

		public NLCamp getCamp(string campId)
		{
			try
			{
				string url = _api_url_root + _api_ver + "/campaign/detail/" + campId;
				string getdata = this.GetFromStoreData(url);
				NLCampResBody paramResBody = JsonConvert.DeserializeObject<NLCampResBody>(getdata);
				if ((paramResBody == null) || (!paramResBody.status) || (paramResBody.data == null))
				{
					return null;
				}

				return paramResBody.data;
			}
			catch (Exception ex)
			{
				this.log(ex.Message);
				return null;
			}
		}
	}
}
