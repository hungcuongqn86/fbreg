const fs = require('fs');
const { Client, LocalAuth } = require('whatsapp-web.js');
const qrcode = require('qrcode-terminal');

// Path where the message data will be stored
const MESSAGE_FILE_PATH = './message.json';

const client = new Client({
    authStrategy: new LocalAuth()
});

client.on('qr', qr => {
    qrcode.generate(qr, {small: true});
});

client.on('ready', () => {
    console.log('Hihi!');
});

client.initialize();

client.on('message', message => {
	// console.log(message);
	if(message.body.includes('Facebook confirmation code')){
		const r = /\d+/;
		const match = message.body.match(r);
		if(match.length > 0){
			const code = match[0];
			console.log('Code: ' + code);
			fs.writeFile(MESSAGE_FILE_PATH, code, (err) => {
				if (err) {
					console.error(err);
				}
			});
		}
	}
});